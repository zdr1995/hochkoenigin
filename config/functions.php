<?php

    /*
     * functions needed for frontend AND backend
     */

    function specialEmailInfos($data, $context, $infos){
      return specialEmailInfosEliteComment($data, $context, $infos);
    }

    function specialEmailInfosEliteComment($data, $context, $infos){
      //init
      $return = false;
      $elite_data = [];

      //get contact data
      switch ($context['view']) {
        case 'request':
        case 'contact':
        case 'coupon':
        case 'brochure':
          //EAnrede
            if(array_key_exists('salutation', $data) && !empty($data['salutation'])){
              if(isset($infos['salutations'][$data['salutation']]) && !empty($infos['salutations'][$data['salutation']])){
                $elite_data['EAnrede'] = $infos['salutations'][$data['salutation']];
                if(in_array(strtolower($elite_data['EAnrede']), ['herr', 'mr.', 'sig.', 'monsieur'])){
                  $elite_data['EGeschlecht'] = 'M';
                } else{
                  $elite_data['EGeschlecht'] = 'W';
                }
              }
            }
          //ETitel
            if(array_key_exists('title', $data) && !empty($data['title'])){
              $elite_data['ETitel'] = $data['title'];
            }
          //EVorname
            if(array_key_exists('firstname', $data) && !empty($data['firstname'])){
              $elite_data['EVorname'] = $data['firstname'];
            }
          //ENachname
            if(array_key_exists('lastname', $data) && !empty($data['lastname'])){
              $elite_data['ENachname'] = $data['lastname'];
            }
          //EGeburtstag
            if(array_key_exists('birthday', $data) && !empty($data['birthday'])){
              $elite_data['EGeburtstag'] = $data['birthday'];
            }
          //EE-Mail
            if(array_key_exists('email', $data) && !empty($data['email'])){
              $elite_data['EE-Mail'] = $data['email'];
            }
          //ETelefon
            if(array_key_exists('phone', $data) && !empty($data['phone'])){
              $elite_data['ETelefon'] = $data['phone'];
            }
          //EFax
            if(array_key_exists('fax', $data) && !empty($data['fax'])){
              $elite_data['EFax'] = $data['fax'];
            }
          //EHandy
            if(array_key_exists('mobile', $data) && !empty($data['mobile'])){
              $elite_data['EHandy'] = $data['mobile'];
            }
          //EStraße
            if(array_key_exists('address', $data) && !empty($data['address'])){
              $elite_data['EStraße'] = $data['address'];
            }
          //EPLZ
            if(array_key_exists('zip', $data) && !empty($data['zip'])){
              $elite_data['EPLZ'] = $data['zip'];
            }
          //EOrt
            if(array_key_exists('city', $data) && !empty($data['city'])){
              $elite_data['EOrt'] = $data['city'];
            }
          //ELand (Lang)
            if(array_key_exists('country', $data) && !empty($data['country'])){
              if(isset($infos['countries'][$data['country']]) && !empty($infos['countries'][$data['country']])){
                $elite_data['ELand (Lang)'] = $infos['countries'][$data['country']];
              }
            }
          //ESprache
          // echo "<pre>" . print_r($data,1) . "</pre>"; exit;
            if(isset($infos['request']->params['language']) && !empty($infos['request']->params['language'])){
              switch ($infos['request']->params['language']) {
                case 'de':
                  $elite_data['ESprache'] = 'D';
                  break;
                case 'en':
                  $elite_data['ESprache'] = 'E';
                  break;
                case 'it':
                  $elite_data['ESprache'] = 'I';
                  break;
                case 'fr':
                  $elite_data['ESprache'] = 'F';
                  break;
                default:
                  break;
              }
            }


          //room infos

              $rooms = $glue = '';
              $room_data = '';
              if(array_key_exists('rooms', $data) && is_array($data['rooms']) && count($data['rooms']) > 0){
                  foreach($data['rooms'] as $option){
                    $rooms .= $glue;
                    if(count($option) > 1) $rooms .= '- ';
                                  $person_room_glue = $persons_and = '';
                    if(array_key_exists('adults', $option) && !empty($option['adults'])){
                      $rooms .= sprintf(__dn('fe', '%s adult', '%s adults', $option['adults']), $option['adults']);
                      $persons_and = ' ' . __d('fe', 'and') . ' ';
                      $person_room_glue = ' ' . __d('fe', 'in a room') . ' ';
                    }
                    if(array_key_exists('children', $option) && !empty($option['children'])){
                      $rooms .= $persons_and . sprintf(__dn('fe', '%s child', '%s children', $option['children']), $option['children']);
                      $person_room_glue = ' ' . __d('fe', 'in a room') . ' ';
                      if(array_key_exists('ages', $option) && is_array($option['ages']) && count($option['ages']) > 0){
                        $agestring = $age_glue = '';
                        $agecnt = 1;
                        foreach($option['ages'] as $_age){
                          $agestring .= $age_glue . $_age['age'];
                          $agecnt++;
                          if($agecnt == count($option['ages'])){
                            $age_glue = ' ' . __d('fe', 'and') . ' ';
                          } else{
                            $age_glue = ', ';
                          }
                        }
                        if(!empty($agestring)){
                          if(count($option['ages']) == 1){
                            $rooms .= ' (' . sprintf(__d('fe', 'at the age of %s'), $agestring) . ')';
                          } else{
                            $rooms .= ' (' . sprintf(__d('fe', 'at the ages of %s'), $agestring) . ')';
                          }
                        }
                      }
                    }
                                      if(array_key_exists('room', $option) && !empty($option['room'])){
                                          if(array_key_exists($option['room'],$infos['rooms'])){
                                              $rooms .= $person_room_glue . '"' . $infos['rooms'][$option['room']] . '"';
                                          }else{
                                              $rooms .= $person_room_glue . '"' . $map['misc']['missing_room'] . ' (' . $option['room'] . ')"';
                                          }
                                          $glue = "\n";
                                      }
                    if(array_key_exists('package', $option) && !empty($option['package'])){
                      if(array_key_exists($option['package'],$infos['packages'])){
                        $rooms .= ' ' . sprintf(__d('fe', 'with the package "%s"'), $infos['packages'][$option['package']]);
                                          }else{
                                              $rooms .= $person_room_glue . '"' . $map['misc']['missing_package'] . ' (' . $option['package'] . ')"';
                                          }
                    }
                                  }
                              }
                              if(strlen($glue) > 0){
                                  $room_data = $rooms;
                              }

                              if(!empty($room_data)){
                                $room_data = "\nZIMMER: \n" . $room_data . "\n";
                              }


          //EAnfragebemerkungen
            if(array_key_exists('message', $data) && !empty($data['message'])){
              $elite_data['EAnfragebemerkungen'] = $room_data . "\nBEMERKUNG: \n" . $data['message'];
            }
          break;
        default:
          break;
      }

      //get reservation data
      switch ($context['view']) {
        case 'request':
          //EAnkunft
            if(array_key_exists('arrival', $data) && !empty($data['arrival'])){
              $elite_data['EAnkunft'] = date('d.m.Y', strtotime($data['arrival']));
            }
          //EAbreise
            if(array_key_exists('departure', $data) && !empty($data['departure'])){
              $elite_data['EAbreise'] = date('d.m.Y', strtotime($data['departure']));
            }

          //EDauer
            $elite_data['EDauer'] = round((strtotime(date('d-m-Y 00:00:00',strtotime($data['departure']))) - strtotime(date('d-m-Y 00:00:00',strtotime($data['arrival'])))) / (60 * 60 * 24), 0, PHP_ROUND_HALF_UP);

            //general room info
            if(isset($data['rooms'])){
              // $elite_data['EKategorie'] = '';
              $elite_data['EZimmeranzahl'] = 0;
              $elite_data['EErwachsene'] = 0;
              $elite_data['EKinder'] = 0;
              $kinderalter = [];
              $__glue = '';
              foreach($data['rooms'] as $__room){
                // $elite_data['EKategorie'] .= $__glue . $__room['room'];
                $__glue = ', ';
                $elite_data['EZimmeranzahl']++;
                if(isset($__room['adults'])){
                  $elite_data['EErwachsene'] += $__room['adults'];
                }
                if(isset($__room['ages']) && is_array($__room['ages'])){
                  foreach($__room['ages'] as $__age){
                    if(isset($__age['age'])){
                      $elite_data['EKinder']++;
                      $kinderalter[] = $__age['age'];
                    }
                  }
                }
              }
            }
            $elite_data['EKinder'] = $elite_data['EKinder'] . ' (' . join("/",$kinderalter) . ')';

            //TODO: but don't know how to input multiple rooms
              // EKategorie
              // EAusstattung
              // EVerpflegung
              // Emaximaler Preis
              // EZimmer
          break;
        default:
          break;
      }

      //build return
      if(is_array($elite_data) && count($elite_data) > 0){
        $return = "<!-- ELITE Anfrageemail\n";
        foreach($elite_data as $ed_key => $ed){
          $return .= $ed_key . ": " . $ed . "</td>\n";
        }
        $return .= "!-->";
      }

      return $return;
    }

    function infoTable($data, $context, $infos){
        // init
        $line = 0;
        $table = '';
        $skip = ['id','captcha'];
        $map = array_key_exists('map', $infos) ? $infos['map'] : [];

        // special behavioir for coupon
        if($context['view'] == 'coupon' && array_key_exists('coupon_type', $data)){
            if($data['coupon_type'] == 'vacation'){
                $skip = array_merge($skip, ['value']);
            }else if($data['coupon_type'] == 'value'){
                $skip = array_merge($skip, ['arrival','departure','adults','children','comment']);
            }
        }

        // special behavioir for request
        if($context['view'] == 'request'){
            $_rooms = $_adults = $_children = $_ages = $_packages = '';

            if (array_key_exists('rooms', $data)) {
                foreach ($data['rooms'] as $__k => $__room) {
                    $_glue = count($data['rooms']) > 1 && count($data['rooms'])!== $__k + 1 ? ';&nbsp;' : ''; 
                    if(isset($infos['rooms'][$__room['room']])){
                      $_rooms .= $infos['rooms'][$__room['room']] . $_glue;   
                    }
                    if(isset($infos['packages'][$__room['package']])){
                      $_packages .= $infos['packages'][$__room['package']] . $_glue;  
                    }
                    $_adults .= $__room['adults'] . $_glue;
                    $_children .= $__room['children'] . $_glue;
                    if (array_key_exists('ages', $__room)) {
                        foreach ($__room['ages'] as $__a => $__age) {
                            $__glue = count($__room['ages']) > 1 && count($__room['ages'])!== $__a + 1 ? ',&nbsp;' : '';
                            $_ages .= $__age['age'] . $__glue;
                        }
                        $_ages .= $_glue;
                    } else {

                        $_ages .= '0' . $_glue;
                    }
                }
            }

            $_data['salutation'] = array_key_exists('salutation', $data) ? $data['salutation'] : '';
            $_data['firstname'] = array_key_exists('firstname', $data) ? $data['firstname'] : '';
            $_data['lastname'] = array_key_exists('lastname', $data) ? $data['lastname'] : '';
            $_data['email'] = array_key_exists('email', $data) ? $data['email'] : '';
            $_data['street'] = array_key_exists('address', $data) ? $data['address'] : '';
            $_data['zip'] = array_key_exists('zip', $data) ? $data['zip'] : '';
            $_data['city'] = array_key_exists('city', $data) ? $data['city'] : '';
            $_data['country'] = array_key_exists('country', $data) ? $data['country'] : '';
            $_data['phone'] = array_key_exists('phone', $data) ? $data['phone'] : '';
            $_data['fax'] = array_key_exists('phone', $data) ? $data['phone'] : '';
            $_data['origin'] = __d('fe', 'Homepage');
            $_data['type'] = __d('fe', 'Online request');
            $_data['_rooms'] = $_rooms;
            $_data['arrival'] = array_key_exists('arrival', $data) ? $data['arrival'] : '';
            $_data['departure'] = array_key_exists('departure', $data) ? $data['departure'] : '';
            $_data['_adults'] = $_adults;
            $_data['_children'] = $_children;
            $_data['_ages'] = $_ages;
            $_data['_package'] = $_packages;
            $_data['options'] = '&nbsp';
            $_data['_message'] = array_key_exists('message', $data) ? $data['message'] : '';

            $_key['_rooms'] = __d('fe', 'Room categories');
            $_key['_adults'] = __d('fe', 'Adults quantity');
            $_key['_children'] = __d('fe', 'Children quantity');
            $_key['_ages'] = __d('fe', 'Children ages');
            $_key['_package'] = __d('fe', 'Packages');
            $_key['origin'] = __d('fe', 'Origin');
            $_key['type'] = __d('fe', 'Request type');
            $_key['options'] = __d('fe', 'Options');
            $_key['_message'] = __d('fe', 'Comment');
            $_key['street'] = __d('fe', 'Street');
            $_key['fax'] = __d('fe', 'Fax');

            $data = $_data;
        }

        if($context['view'] == 'request'){
            $data['_roomquantities'] = $glue = '';
            $cats = explode(';', str_replace('&nbsp;', '', $data['_rooms']));
            foreach ($cats as $cat) {
                if(!empty($cat)){
                    $data['_roomquantities'] .= $glue . '1';
                    $glue = '; ';
                }
            }
            $_request_data = [
            'Anrede' => isset($data['salutation']) && array_key_exists('salutations', $infos) && is_array($infos['salutations']) && array_key_exists($data['salutation'], $infos['salutations']) ? $infos['salutations'][$data['salutation']] : '',
            'Vorname' => isset($data['firstname']) ? $data['firstname'] : '',
            'Name' => isset($data['lastname']) ? $data['lastname'] : '',
            'eMail' => isset($data['email']) ? $data['email'] : '',
            'Strasse' => isset($data['street']) ? $data['street'] : '',
            'PLZ' => isset($data['zip']) ? $data['zip'] : '',
            'Ort' => isset($data['city']) ? $data['city'] : '',
            'Land' => isset($data['country']) && array_key_exists('countries', $infos) && is_array($infos['countries']) && array_key_exists($data['country'], $infos['countries']) ? $infos['countries'][$data['country']] : '',
            'Telefon' => isset($data['phone']) ? $data['phone'] : '',
            'Fax' => isset($data['fax']) ? $data['fax'] : '',
            'Herkunft' => 'Homepage',
            'Anfrageart' => 'Unverbindliche Anfrage',
            'Zimmerkategorie' => isset($data['_rooms']) ? str_replace(';', ',', str_replace('&nbsp;', ' ', $data['_rooms'])) : '',
            'Anzahl Zimmer' => isset($data['_roomquantities']) ? str_replace(';', ',', $data['_roomquantities']) : '',
            'Anreise' => isset($data['arrival']) ? date('d.m.Y', strtotime($data['arrival'])) : '',
            'Abreise' => isset($data['departure']) ? date('d.m.Y', strtotime($data['departure'])) : '',
            'Anzahl Erwachsene' => isset($data['_adults']) ? str_replace(';', ',', str_replace('&nbsp;', ' ', $data['_adults'])) : '',
            'Anzahl Kinder' => isset($data['_children']) ? str_replace(';', ',', str_replace('&nbsp;', ' ', $data['_children'])) : '',
            'Alter der Kinder' => isset($data['_ages']) ? str_replace(';', ',', str_replace('&nbsp;', ' ', $data['_ages'])) : '',
            'Pauschalen' => isset($data['_package']) ? str_replace(';', ',', str_replace('&nbsp;', ' ', $data['_package'])) : '',
            'Optionen' => isset($data['options']) ? $data['options'] : '',
            'Kommentar' => isset($data['_message']) ? $data['_message'] : '',
            ];

            $table .= "ANFRAGEIMPORT HOMEPAGE";
            $table .= "\n";
            $table .= "\n";
            foreach($_request_data as $_r_d_k => $_r_d_v){
              if($_r_d_k == 'Kommentar'){
                $table .= $_r_d_k . ":\n" . $_r_d_v . "\n";
              } else{
                $table .= $_r_d_k . ': ' . $_r_d_v . "\n";
              }
            }

        } else{
            if(is_array($data)){
                $table .= '<table cellpadding="5" cellspacing="0" width="100%">';
                foreach($data as $k => $v){
                    if(!in_array($k, $skip)){

                        // key
                        $value = '';
                        $key = array_key_exists('desc', $map) && is_array($map['desc']) && array_key_exists($k, $map['desc']) ? $map['desc'][$k] : ucfirst($k);

                        // value
                        switch($k){
                            case "salutation":
                            case "salutation_recipient":
                                $value = array_key_exists('salutations', $infos) && is_array($infos['salutations']) && array_key_exists($v, $infos['salutations']) ? $infos['salutations'][$v] : trim($v);
                                break;
                            case "email":
                                $value = trim($v);
                                if(!empty($value)){
                                    $value = '<a href="mailto:' . $value . '">' . $value . '</a>';
                                }
                                break;
                            case "country":
                                $value = array_key_exists('countries', $infos) && is_array($infos['countries']) && array_key_exists($v, $infos['countries']) ? $infos['countries'][$v] : trim($v);
                                break;
                            case "newsletter":
                                if($v == 1){
                                    $value = array_key_exists('misc', $map) && is_array($map['misc']) && array_key_exists('yes', $map['misc']) ? $map['misc']['yes'] : $v;
                                }else{
                                    $value = array_key_exists('misc', $map) && is_array($map['misc']) && array_key_exists('no', $map['misc']) ? $map['misc']['no'] : $v;
                                }
                                break;
                            case "coupon_type":
                                $value = array_key_exists('options', $map) && is_array($map['options']) && array_key_exists($v, $map['options']) ? $map['options'][$v] : ucfirst($v);
                                break;
                            case "message":
                            case "comment":
                                $value = nl2br($v);
                                break;
                            case "arrival":
                            case "departure":
                                $value = date("d.m.Y", strtotime($v));
                                break;
                            case "ages":
                                $ages = $glue = '';
                                if(is_array($v) && count($v) > 0){
                                    foreach($v as $option){
                                        if(array_key_exists('age', $option)){
                                            $ages .= $glue . $option['age'];
                                            $glue = ', ';
                                        }
                                    }
                                }
                                if(strlen($glue) > 0){
                                    $value = $ages;
                                }
                                break;
                            case "rooms":
                                $rooms = $glue = '';
                                if(is_array($v) && count($v) > 0){
                                    foreach($v as $option){
    									$rooms .= $glue;
    									if(count($option) > 1) $rooms .= '&bull; ';
                                		$person_room_glue = $persons_and = '';
    									if(array_key_exists('adults', $option) && !empty($option['adults'])){
    										$rooms .= sprintf(__dn('fe', '%s adult', '%s adults', $option['adults']), $option['adults']);
    										$persons_and = ' ' . __d('fe', 'and') . ' ';
    										$person_room_glue = ' ' . __d('fe', 'in a room') . ' ';
    									}
    									if(array_key_exists('children', $option) && !empty($option['children'])){
    										$rooms .= $persons_and . sprintf(__dn('fe', '%s child', '%s children', $option['children']), $option['children']);
    										$person_room_glue = ' ' . __d('fe', 'in a room') . ' ';
    										if(array_key_exists('ages', $option) && is_array($option['ages']) && count($option['ages']) > 0){
    											$agestring = $age_glue = '';
    											$agecnt = 1;
    											foreach($option['ages'] as $_age){
    												$agestring .= $age_glue . $_age['age'];
    												$agecnt++;
    												if($agecnt == count($option['ages'])){
    													$age_glue = ' ' . __d('fe', 'and') . ' ';
    												} else{
    													$age_glue = ', ';
    												}
    											}
    											if(!empty($agestring)){
    												if(count($option['ages']) == 1){
    													$rooms .= ' (' . sprintf(__d('fe', 'at the age of %s'), $agestring) . ')';
    												} else{
    													$rooms .= ' (' . sprintf(__d('fe', 'at the ages of %s'), $agestring) . ')';
    												}
    											}
    										}
    									}
                                        if(array_key_exists('room', $option) && !empty($option['room'])){
                                            if(array_key_exists($option['room'],$infos['rooms'])){
                                                $rooms .= $person_room_glue . '"' . $infos['rooms'][$option['room']] . '"';
                                            }else{
                                                $rooms .= $person_room_glue . '"' . $map['misc']['missing_room'] . ' (' . $option['room'] . ')"';
                                            }
                                            $glue = '</br>';
                                        }
    									if(array_key_exists('package', $option) && !empty($option['package'])){
    										if(array_key_exists($option['package'],$infos['packages'])){
    											$rooms .= ' ' . sprintf(__d('fe', 'with the package "%s"'), $infos['packages'][$option['package']]);
                                            }else{
                                                $rooms .= $person_room_glue . '"' . $map['misc']['missing_package'] . ' (' . $option['package'] . ')"';
                                            }
    									}
                                    }
                                }
                                if(strlen($glue) > 0){
                                    $value = $rooms;
                                }
                                break;
                            case "room":
                                if(array_key_exists($v,$infos['rooms'])){
                                    $value = $infos['rooms'][$v];
                                }else{
                                    $value = $map['misc']['missing_room'] . ' (' . $v . ')';
                                }
                                break;
                            case "packages":
                                $packages = $glue = '';
                                if(is_array($v) && count($v) > 0){
                                    foreach($v as $option){
                                        if(array_key_exists('package', $option) && !empty($option['package'])){
                                            if(array_key_exists($option['package'],$infos['packages'])){
                                                $packages .= $glue . $infos['packages'][$option['package']];
                                            }else{
                                                $packages .= $glue . $map['misc']['missing_package'] . ' (' . $option['package'] . ')';
                                            }
                                            $glue = '</br>';
                                        }
                                    }
                                }
                                if(strlen($glue) > 0){
                                    $value = $packages;
                                }
                                break;
                            case "interests":
                                $options = $glue = '';
                                if(is_array($v) && count($v) > 0){
                                    foreach($v as $option){
                                        if(array_key_exists($option, $infos['interests']) && is_array($infos['interests'][$option]) && array_key_exists('title', $infos['interests'][$option])){
                                            $options .= $glue . $infos['interests'][$option]['title'];
                                        }else{
                                            $options .= $glue . $map['misc']['missing_interest'] . ' (' . $option . ')';
                                        }
                                        $glue = '</br>';
                                    }
                                }
                                if(strlen($glue) > 0){
                                    $value = $options;
                                }
                                break;
                            default:
                                $value = trim($v);
                                break;
                        }

                        if(!empty($value)){
                            if ($context['view'] == 'request') {
                                $table .= '<tr class=""><td class="left" valign="top"><strong>' . $key . ':</strong>&nbsp;' . $value . '</td></tr>';
                                $line++;
                            } else {
                                $cls = $line%2 ? 'even' : 'odd';
                                $table .= '<tr class="' . $cls. '"><td class="left" valign="top"><strong>' . $key . ':</strong></td><td class="right" valign="top">' . $value . '</td></tr>';
                                $line++;
                            }
                        }
                    }
                }
                $table .= '</table>';
            }
        }

        return $table;
    }

    function replyTeaser($data, $context, $infos){

        // init
        $teaser = '';

        if(array_key_exists('reply_teaser', $context) && is_array($context['reply_teaser']) && array_key_exists(0, $context['reply_teaser']) && is_array($context['reply_teaser'][0])){

            try{
                $img = $infos['fullBaseUrl'] . $context['reply_teaser'][0]['details']['image'][0]['details']['seo']['1_banner'];
                $focus = $infos['fullBaseUrl'] . $context['reply_teaser'][0]['details']['image'][0]['details']['focus'][1]['css'];
                if($context['reply_teaser'][0]['details']['link'][0]['type'] == 'node'){
                    $url = $infos['fullBaseUrl'] . 'redirect' . DS . $infos['request']->params['language'] . DS . $context['reply_teaser'][0]['details']['link'][0]['details']['node']['route'];
                }else if($context['reply_teaser'][0]['details']['link'][0]['type'] == 'element' && $context['reply_teaser'][0]['details']['link'][0]['details']['code'] == 'link'){
                    $url = $context['reply_teaser'][0]['details']['link'][0]['details']['link'];
                }
            }catch(Exeption $e){
                $img = false;
                $url = false;
            }

            if($img && $url){
                $teaser .= '<table class="hidden-mobile" cellpadding="0" cellspacing="0" width="100%">';
                    $teaser .= '<tr class="hidden-mobile">';
                        $teaser .= '<td class="hidden-mobile">&nbsp;</td>';
                    $teaser .= '</tr>';
                    $teaser .= '<tr class="hidden-mobile">';
                        $teaser .= '<td class="hidden-mobile">';

                            $teaser .= '<table class="hidden-mobile" cellpadding="0" cellspacing="0" width="100%">';
                                $teaser .= '<tr class="hidden-mobile">';
                                    $teaser .= '<td class="hidden-mobile" style="width:270px; vertical-align:top;">';
                                        $teaser .= '<img style="width:100%;" src="' . $img . '" style="object-position:' . $focus . ';" />';
                                    $teaser .= '</td>';
                                    $teaser .= '<td class="hidden-mobile" style="width:20px; line-height:0; font-size:0;">';
                                        $teaser .= '&nbsp;';
                                    $teaser .= '</td>';
                                    $teaser .= '<td class="hidden-mobile" style="width:270px; vertical-align:top;">';
                                        $teaser .= '<table class="hidden-mobile" cellpadding="0" cellspacing="0" width="100%">';
                                            $teaser .= '<tr class="hidden-mobile">';
                                                $teaser .= '<td class="hidden-mobile" style="vertical-align:top;">';
                                                    $teaser .= '<h2>' . $context['reply_teaser'][0]['details']['headline'] . '</h2>';
                                                    $teaser .= $context['reply_teaser'][0]['details']['content'];
                                                $teaser .= '</td>';
                                            $teaser .= '</tr>';
                                            $teaser .= '<tr class="hidden-mobile">';
                                                $teaser .= '<td class="hidden-mobile">&nbsp;</td>';
                                            $teaser .= '</tr>';
                                            $teaser .= '<tr class="hidden-mobile">';
                                                $teaser .= '<td class="hidden-mobile" style="height:40px; background-color:#8C7265; vertical-align:middle; text-align:center;">';
                                                    $teaser .= '<a style="color:#FFFFFF; text-decoration:none; vertical-align: middle; text-align:center; font-weight: bold;" href="' . $url . '">' . $context['reply_teaser'][0]['details']['linktext'] . '</a>';
                                                $teaser .= '</td>';
                                            $teaser .= '</tr>';
                                        $teaser .= '</table>';
                                    $teaser .= '</td>';
                                $teaser .= '</tr>';
                            $teaser .= '</table>';

                        $teaser .= '</td>';
                    $teaser .= '</tr>';
                    $teaser .= '<tr class="hidden-mobile">';
                        $teaser .= '<td class="hidden-mobile">&nbsp;</td>';
                    $teaser .= '</tr>';
                $teaser .= '</table>';
            }
        }
        return $teaser;
    }

    function custom_number_format($number, $decimals = 0, $dec_point = ".", $thousands_sep = ","){
        $res = number_format($number, $decimals, $dec_point, $thousands_sep);
        list(,$adp) = explode($dec_point, $res, 2);
        if($adp == '00'){
            $res = number_format($number, 0, $dec_point, $thousands_sep) . $dec_point . '-';
        }
        return $res;
    }

    function custom_array_filter($array) {
        if(is_array($array)){
            foreach ($array as $k => $v) {
                if (is_array($v)) {
                    $array[$k] = custom_array_filter($v);
                }
            }
            $array = array_filter($array, function($var){
                if(is_array($var)){
                    return count($var) > 0 ? true : false;
                }else{
                    return strlen($var) ? true : false;
                }
            });
        }
        return $array;
    }
