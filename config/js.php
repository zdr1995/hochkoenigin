<?php
use Cake\Core\Configure;
//NOTE: You can add files here
//NOTE: Be aware to add the files in the correct order!
//NOTE: Keep in mind that you need to resave 1 js file after adding one her, because the bundle will only be regenerated if one file is newer than the bundle

Configure::write('jsconfig', [
    'debug-on-our-ip' => false,
    'basic' => [
        'files' => [
            // misc
                '/frontend/js/misc/promise.js', //must stay before promise
                '/frontend/js/misc/fetch.js', // must stay at the very top here
                '/frontend/js/misc/init.js',
                '/frontend/js/misc/loadCss.js',
                '/frontend/js/misc/modernizr-custom.js',
                '/frontend/js/misc/video.min.js',
                '/frontend/js/misc/on-screen.umd.min.js',
        
            // jQuery
                '/frontend/js/jQuery/jquery.min.js',
                '/frontend/js/jQuery/jquery.waypoints.min.js',
                '/frontend/js/jQuery/jquery.waypoints.sticky.min.js',
                '/frontend/js/jQuery/jquery.bxslider.min.js',
                '/frontend/js/jQuery/jquery.smooth-scroll.min.js',
        
            // FontAwesome
                // '/frontend/js/fontawesome/fontawesome-all.min.js',
        
            // PICKADATE
                '/frontend/js/pickadate/picker.js',
                '/frontend/js/pickadate/picker.date.js',
                // '/frontend/js/pickadate/translations/' . str_replace('/','',$_SERVER['REQUEST_URI']) . '.js',
                '/frontend/js/pickadate/translations/de.js',
        
            // countUP
                '/frontend/js/countUp/jquery.countTo.js',
        
            // main
                '/frontend/js/functions_extended.js',
                '/frontend/js/functions.js',
        ],
        'path' => [
            '/webroot/frontend/js/bundle_basic.min.js',
        ]
    ],
    'extended' => [
        'files' => [
            // misc
                // '/frontend/js/misc/promise.js', //must stay before promise
                // '/frontend/js/misc/fetch.js', // must stay at the very top here
                // '/frontend/js/misc/init.js',
                // '/frontend/js/misc/modernizr-custom.js',
                // '/frontend/js/misc/video.min.js',
                // '/frontend/js/misc/on-screen.umd.min.js',

            // jQuery
                // '/frontend/js/jQuery/jquery.min.js',
                // '/frontend/js/jQuery/jquery.waypoints.min.js',
                // '/frontend/js/jQuery/jquery.waypoints.sticky.min.js',
                // '/frontend/js/jQuery/jquery.bxslider.min.js',
                // '/frontend/js/jQuery/jquery.smooth-scroll.min.js',

            // FontAwesome
                // '/frontend/js/fontawesome/fontawesome-all.min.js',

            // PICKADATE
                // '/frontend/js/pickadate/picker.js',
                // '/frontend/js/pickadate/picker.date.js',
                // '/frontend/js/pickadate/translations/' . str_replace('/','',$_SERVER['REQUEST_URI']) . '.js',
                // '/frontend/js/pickadate/translations/de.js',

            // countUP
                // '/frontend/js/countUp/jquery.countTo.js',

            // main
                // '/frontend/js/functions_extended.js',
        ],
        'path' => [
            '/webroot/frontend/js/bundle.min.js',
        ]
    ]
]);
