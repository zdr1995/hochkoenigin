<?php

$_setup = [

    // domains
    'plugin_i18n' => [
        'Backend' => ['name' => __d('be', 'Backend'), 'domain' => 'be', 'list' => 'languages', 'fetch' => true],
        'Frontend' => ['name' => __d('be', 'Website'), 'domain' => 'fe', 'list' => 'translations', 'fetch' => true],
        'Countries' => ['name' => __d('be', 'Countries'), 'domain' => 'country', 'list' => 'translations', 'fetch' => true],
        'Salutations' => ['name' => __d('be', 'Salutations'), 'domain' => 'salutation', 'list' => 'translations', 'fetch' => true],
    ],

    // technical contacts
    'contact' => [
        'admin' => [
            'name' => 'Medienjaeger Programmierung',
            'email' => 'coders@medienjaeger.at',
        ],
        'support' => [
            'name' => 'Ludwig Jaeger',
            'email' => 'support@medienjaeger.at',
        ],
    ],

    // currencies
    'currencies' => ['EUR' => '&euro;'],

    // image settings
    'images' => [
        'use_categories' => true, // true, false or element
        'focus' => true,
        'sizes' => [
            'auto' => [
                'thumbs' => ['width' => 300, 'height' => 200],
                'popup' => ['width' => 800, 'height' => 600],
            ],
            'ecard' => [
                'view' => ['width' => 1000, 'height' => 625],
                'gallery' => ['width' => 750, 'height' => 468],
                'thumbs' => ['width' => 235, 'height' => 146],
            ],
            'purposes' => [
                1 => ['name' => __d('be', 'Header'), 'width' => 2500, 'height' => 1250, 'editor' => false, 'thumbs' => [ // header & content
                    ['width' => 2000, 'height' => 700, 'folder' => 'bigbanner'], // banner #3 big room
                    ['width' => 2000, 'height' => 500, 'folder' => 'footerbanner'], // banner #4 footer image
                    ['width' => 1800, 'height' => 680, 'folder' => 'banner'], // banner #1
                    ['width' => 1250, 'height' => 625, 'folder' => 'middle'], // teaser offers ; teaser beauty ; teaser services ; banner #2 2sp
                    ['width' => 625, 'height' => 312, 'folder' => 'small'],
                ]],
                2 => ['name' => __d('be', 'Room & Offer & Gallery'), 'width' => 1800, 'height' => 750, 'thumbs' => [ // room & offer
                    ['width' => 1200, 'height' => 500, 'folder' => 'overview'],
                    ['width' => 549, 'height' => 320, 'folder' => 'plan'],
                    ['width' => 170, 'height' => 110, 'folder' => 'menu'],
                ]],
                3 => ['name' => __d('be', 'Downloads'), 'width' => 300, 'height' => false, 'thumbs' => false], // downloads
                4 => ['name' => __d('be', 'Quadrat'), 'width' => 1250, 'height' => 1250, 'thumbs' => [ // header & content
                    ['width' => 414, 'height' => 414, 'folder' => 'topslider'], // pool
                    ['width' => 336, 'height' => 336, 'folder' => 'banner5'], // banner #5 footer image
                ]],

            ],
        ],
        'quality' => [
            'png' => 0,
            'jpg' => 80,
        ]
    ],

    // seo
    'seo' => [
        'meta' => [
            'title' => ['min' => 0, 'max' => 55],
            'desc' => ['min' => 80, 'max' => 156]
        ],
        'images' => [
            'folder' => 'seo',
        ],
        'canonical' => []
    ],

    // editor links classes
    'editor' => [
        'links' => [
            'nolink' => __d('be', 'No link'),
            'phone' => __d('be', 'Phone'),
            'email' => __d('be', 'E-Mail'),
        ]
    ],

    // themes
    'themes' => [
        'website' => __d('be', 'Website'),
    ],

    // pagination
    'pagination' => [
        'limit' => 25
    ],

    // categories
    'categories' => [
        'code' => 'none', // change also in categories table!
    ],

    // allowed
    'allowed' => [],

];

return $_setup;
