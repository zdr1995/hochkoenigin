<?php
use Cake\Core\Configure;
//NOTE: You can add files here
//NOTE: Be aware to add the files in the correct order!
//NOTE: Keep in mind that you need to resave 1 css file after adding one her, because the bundle will only be regenerated if one file is newer than the bundle

Configure::write('cssconfig', [
    'debug-on-our-ip' => false,
    'basic' => [
        'files' => [
            //PICKADATE
                // '/frontend/css/pickadate/default.css',
                // '/frontend/css/pickadate/default.date.css',
                // '/frontend/css/pickadate/default.time.css',
                // '/frontend/css/pickadate/rtl.css',

            // MISC
                '/frontend/css/misc/reset.css',
                '/frontend/css/misc/animate.css',
                '/frontend/css/misc/effects.css',
                // '/frontend/css/misc/fa-all.min.css',
                // '/frontend/css/misc/video-js.css',
                // '/frontend/css/misc/weather-icons.min.css',
                // '/frontend/css/misc/flag-icon.min.css',
                // '/frontend/css/misc/jquery.bxslider.css',

            // SASS
                '/frontend/css/styles.scss',
                '/frontend/css/retina_big.scss',
                '/frontend/css/mobile.scss',
        ],
        'path' => [
            '/webroot/frontend/css/bundle_basic.min.css',
        ]
    ],
    'extended' => [
        'files' => [
            //PICKADATE
                '/frontend/css/pickadate/default.css',
                '/frontend/css/pickadate/default.date.css',
                // '/frontend/css/pickadate/default.time.css',
                // '/frontend/css/pickadate/rtl.css',

            // MISC
                // '/frontend/css/misc/reset.css',
                // '/frontend/css/misc/animate.css',
                // '/frontend/css/misc/effects.css',
                // '/frontend/css/misc/fa-all.min.css',
                '/frontend/css/misc/video-js.css',
                '/frontend/css/misc/weather-icons.min.css',
                // '/frontend/css/misc/flag-icon.min.css',
                // '/frontend/css/misc/jquery.bxslider.css',

            // SASS
                // '/frontend/css/styles.scss',
                // '/frontend/css/retina_big.scss',
                // '/frontend/css/mobile.scss',
        ],
        'path' => [
            '/webroot/frontend/css/bundle.min.css',
        ]
    ],
]);
