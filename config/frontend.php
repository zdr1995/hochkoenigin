<?php

use Cake\Core\Configure;

$_global = [

    // member?
    'member' => false,

    // email sender
    'sender' => [
        'name' => 'Hochkönigin GmbH & Co KG',
        'email' => 'urlaub@hochkoenigin.com',
    ],

    // newsletter
    //IMPORTANT INFO - you have to:
    //  - give db settings
    //  - set the type
    //  - set settings under Konfigurieren / Newsletter in CMS
    'newsletter' => [
        'type' => 'tm6', // false / internal / tm5 / tm6 (add new if needed)
        'settings' => [ // settings f.e. for TM6 API
            'db' => [ //for tm5
                'datasource' => 'Database/Mysql',
                'persistent' => false,
                'host' => 'host',
                'login' => 'login',
                'password' => 'pass',
                'database' => 'database',
                'prefix' => '',
            ],
            'api' => [ //for tm6
                'client' => 'Hochkönigin', //tm6 client slug
                'username' => 'api',
                'password' => 'Gast67tf6t77_f',
                'uri' => 'http://api.tourismail.net',
            ]
        ],
        'interests' => [
            'summer' => __d('fe', 'Summer'),
            'winter' => __d('fe', 'Winter'),
            'beauty' => __d('fe', 'Wellness & Beauty'),
        ],
    ],

    // brochure
    'brochure' => [
        'interests' => [
            'summer' => [
                'title' => __d('fe', 'Summer'),
                'rel' => false,
            ],
            'winter' => [
                'title' => __d('fe', 'Winter'),
                'rel' => false,
            ],
            'beauty' => [
                'title' => __d('fe', 'Wellness & Beauty'),
                'rel' => false,
            ],
        ]
    ],

    // captcha
    'captcha' => [
        'type' => 'math',
        'width' => 70,
        'height' => 40,
        'size' => 12,
        'angle' => 0,
        'font' => 'open-sans.regular.ttf',
        'background' => [93, 93, 93],
        'color' => [255,255,255]
    ],

    // styles
    'styles' => [
        [
            'title' => 'Headline (sans)',
            'block' => 'h2',
            'classes' => 'editor-headline-sans'
        ],
        [
            'title' => 'Headline (serif)',
            'block' => 'h2',
            'classes' => 'editor-headline-serif'
        ],
        [
            'title' => 'Rot',
            'inline' => 'span',
            'classes' => 'editor-red'
        ],
        [
            'title' => 'Blau',
            'inline' => 'span',
            'classes' => 'editor-green'
        ],
        [
            'title' => 'Türkis',
            'inline' => 'span',
            'classes' => 'editor-blue'
        ],
        [
            'title' => 'Grau',
            'inline' => 'span',
            'classes' => 'editor-light-grey'
        ],
        [
            'title' => 'Klein',
            'inline' => 'span',
            'classes' => 'editor-small'
        ],
        [
            'title' => 'Großgeschrieben',
            'inline' => 'span',
            'classes' => 'uppercase'
        ],
    ],

    // node settings
    'node-settings' => [],

    // config
    'config' => [

        // default config
        'default' => [
            'name' => __d('be', 'Default'),
            'fields' => [

                // address
                'hotel' => [
                    'fieldset' => __d('be', 'Footer'),
                    'required' => [
                        'rules' => [
                            'notempty' => __d('be', 'The hotel name is required'),
                        ]
                    ],
                    'attr' => [
                        'type' => 'text',
                        'label' => __d('be', 'Hotel name'),
                        'placeholder' => __d('be', 'Hotel name'),
                    ]
                ],
                'family' => [
                    'fieldset' => __d('be', 'Footer'),
                    'multi' => true,
                    'required' => [
                        'rules' => [
                            'notempty' => __d('be', 'The family name is required'),
                        ]
                    ],
                    'attr' => [
                        'type' => 'text',
                        'label' => __d('be', 'Family'),
                        'placeholder' => __d('be', 'Family'),
                    ]
                ],
                'street' => [
                    'fieldset' => __d('be', 'Footer'),
                    'multi' => true,
                    'required' => [
                        'rules' => [
                            'notempty' => __d('be', 'A street is required'),
                        ]
                    ],
                    'attr' => [
                        'type' => 'text',
                        'label' => __d('be', 'Street'),
                        'placeholder' => __d('be', 'Street'),
                    ]
                ],
                'city' => [
                    'fieldset' => __d('be', 'Footer'),
                    'multi' => true,
                    'required' => [
                        'rules' => [
                            'notempty' => __d('be', 'A city is required'),
                        ]
                    ],
                    'attr' => [
                        'type' => 'text',
                        'label' => __d('be', 'City'),
                        'placeholder' => __d('be', 'City'),
                    ]
                ],
                'zip' => [
                    'fieldset' => __d('be', 'Footer'),
                    'multi' => false,
                    'required' => [
                        'rules' => [
                            'notempty' => __d('be', 'ZIP'),
                        ]
                    ],
                    'attr' => [
                        'type' => 'text',
                        'label' => __d('be', 'ZIP'),
                        'placeholder' => '6020',
                    ]
                ],
                'email' => [
                    'fieldset' => __d('be', 'Footer'),
                    'required' => [
                        'rules' => [
                            'notempty' => __d('be', 'An email address is required'),
                            'url' => [
                                'rule' => 'email',
                                'message' => __d('be', 'Invalid email address'),
                                'last' => true,
                            ],
                        ]
                    ],
                    'attr' => [
                        'type' => 'text',
                        'label' => __d('be', 'E-Mail address'),
                        'placeholder' => __d('be', 'E-Mail address'),
                    ]
                ],
                'phone' => [
                    'fieldset' => __d('be', 'Footer'),
                    'required' => [
                        'rules' => [
                            'notempty' => __d('be', 'A phone number is required'),
                        ]
                    ],
                    'attr' => [
                        'type' => 'text',
                        'label' => __d('be', 'Phone number (formatted)'),
                        'placeholder' => __d('be', 'Phone number (formatted)'),
                    ]
                ],
                'phone-plain' => [
                    'fieldset' => __d('be', 'Footer'),
                    'required' => [
                        'rules' => [
                            'notempty' => __d('be', 'A phone number is required'),
                        ]
                    ],
                    'attr' => [
                        'type' => 'text',
                        'label' => __d('be', 'Phone number (plain)'),
                        'placeholder' => __d('be', 'Phone number (plain)'),
                    ]
                ],
                'domain' => [
                    'fieldset' => __d('be', 'Footer'),
                    'required' => [
                        'rules' => [
                            'notempty' => __d('be', 'The URL is required'),
                            'url' => [
                                'rule' => 'url',
                                'message' => __d('be', 'Invalid URL'),
                                'last' => true,
                            ],
                            'protocol' => [
                                'rule' => ['custom', '/^(http\:\/\/|https\:\/\/)/i'],
                                'message' => __d('be', 'Link without protocol'),
                            ]
                        ]
                    ],
                    'attr' => [
                        'type' => 'text',
                        'label' => __d('be', 'URL'),
                        'placeholder' => __d('be', 'URL'),
                    ]
                ],

                //geo
                'geo-region' => [
                    'fieldset' => __d('be', 'Geo'),
                    'multi' => false,
                    'required' => [
                        'rules' => [
                            'notempty' => __d('be', 'A region is required'),
                        ]
                    ],
                    'attr' => [
                        'type' => 'text',
                        'label' => __d('be', 'Region'),
                        'placeholder' => 'AT-5',
                    ]
                ],
                'geo-latitude' => [ //Breitengrad
                    'fieldset' => __d('be', 'Geo'),
                    'multi' => false,
                    'required' => [
                        'rules' => [
                            'notempty' => __d('be', 'A latitude is required'),
                        ]
                    ],
                    'attr' => [
                        'type' => 'text',
                        'label' => __d('be', 'Latitude'),
                        'placeholder' => '47.278091',
                    ]
                ],
                'geo-longitude' => [ //Längengrad
                    'fieldset' => __d('be', 'Geo'),
                    'multi' => false,
                    'required' => [
                        'rules' => [
                            'notempty' => __d('be', 'A longitude is required'),
                        ]
                    ],
                    'attr' => [
                        'type' => 'text',
                        'label' => __d('be', 'Longitude'),
                        'placeholder' => '11.433128',
                    ]
                ],

                // seasons
                'summer-start' => [
                    'fieldset' => __d('be', 'Seasons'),
                    'required' => [
                        'rules' => [
                            'notempty' => __d('be', 'A date is required'),
                        ]
                    ],
                    'attr' => [
                        'type' => 'text',
                        'label' => __d('be', 'Summer start date'),
                        'placeholder' => __d('be', 'Summer start date'),
                        'class' => 'date',
                        'data-date-daypicker' => 'true',
                    ]
                ],
                'winter-start' => [
                    'fieldset' => __d('be', 'Seasons'),
                    'required' => [
                        'rules' => [
                            'notempty' => __d('be', 'A date is required'),
                        ]
                    ],
                    'attr' => [
                        'type' => 'text',
                        'label' => __d('be', 'Winter start date'),
                        'placeholder' => __d('be', 'Winter start date'),
                        'class' => 'date',
                        'data-date-daypicker' => 'true',
                    ]
                ],

                // fixed pages
                'home' => [
                    'fieldset' => __d('be', 'Pages'),
                    'required' => [
                        'rules' => [
                            'notempty' => __d('be', 'A page is required'),
                        ]
                    ],
                    'attr' => [
                        'type' => 'text',
                        'label' => __d('be', 'Home'),
                        'class' => 'selector',
                        'data-selector-max' => 1,
                        'data-selector-node' => 'true',
                        'data-selector-text' => __d('be', 'Select page'),
                    ],
                    'details' => true,
                ],
                'request' => [
                    'fieldset' => __d('be', 'Pages'),
                    'required' => [
                        'rules' => [
                            'notempty' => __d('be', 'A page is required'),
                        ]
                    ],
                    'attr' => [
                        'type' => 'text',
                        'label' => __d('be', 'Booking request'),
                        'class' => 'selector',
                        'data-selector-max' => 1,
                        'data-selector-node' => 'true',
                        'data-selector-text' => __d('be', 'Select page'),
                    ],
                    'details' => false,
                ],
                'book' => [
                    'fieldset' => __d('be', 'Pages'),
                    'required' => [
                        'rules' => [
                            'notempty' => __d('be', 'A page is required'),
                        ]
                    ],
                    'attr' => [
                        'type' => 'text',
                        'label' => __d('be', 'Online booking'),
                        'class' => 'selector',
                        'data-selector-max' => 1,
                        'data-selector-node' => 'true',
                        'data-selector-text' => __d('be', 'Select page'),
                    ],
                    'details' => false,
                ],
                'imprint' => [
                    'fieldset' => __d('be', 'Pages'),
                    'required' => [
                        'rules' => [
                            'notempty' => __d('be', 'A page is required'),
                        ]
                    ],
                    'attr' => [
                        'type' => 'text',
                        'label' => __d('be', 'Imprint'),
                        'class' => 'selector',
                        'data-selector-max' => 1,
                        'data-selector-node' => 'true',
                        'data-selector-text' => __d('be', 'Select page'),
                    ],
                    'details' => false,
                ],
                'sitemap' => [
                    'fieldset' => __d('be', 'Pages'),
                    'required' => [
                        'rules' => [
                            'notempty' => __d('be', 'A page is required'),
                        ]
                    ],
                    'attr' => [
                        'type' => 'text',
                        'label' => __d('be', 'Sitemap'),
                        'class' => 'selector',
                        'data-selector-max' => 1,
                        'data-selector-node' => 'true',
                        'data-selector-text' => __d('be', 'Select page'),
                    ],
                    'details' => false,
                ],
                'map' => [
                    'fieldset' => __d('be', 'Pages'),
                    'required' => [
                        'rules' => [
                            'notempty' => __d('be', 'A page is required'),
                        ]
                    ],
                    'attr' => [
                        'type' => 'text',
                        'label' => __d('be', 'Routeplanner'),
                        'class' => 'selector',
                        'data-selector-max' => 1,
                        'data-selector-node' => 'true',
                        'data-selector-text' => __d('be', 'Select page'),
                    ],
                    'details' => false,
                ],
                'rating' => [
                    'fieldset' => __d('be', 'Pages'),
                    'required' => [
                        'rules' => [
                            'notempty' => __d('be', 'A page is required'),
                        ]
                    ],
                    'attr' => [
                        'type' => 'text',
                        'label' => __d('be', 'Hotel rating'),
                        'class' => 'selector',
                        'data-selector-max' => 1,
                        'data-selector-node' => 'true',
                        'data-selector-text' => __d('be', 'Select page'),
                    ],
                    'details' => false,
                ],
                'prospekt' => [
                    'fieldset' => __d('be', 'Pages'),
                    'required' => [
                        'rules' => [
                            'notempty' => __d('be', 'A page is required'),
                        ]
                    ],
                    'attr' => [
                        'type' => 'text',
                        'label' => __d('be', 'Hotel prospekt'),
                        'class' => 'selector',
                        'data-selector-max' => 1,
                        'data-selector-node' => 'true',
                        'data-selector-text' => __d('be', 'Select page'),
                    ],
                    'details' => false,
                ],
                'press' => [
                    'fieldset' => __d('be', 'Pages'),
                    'required' => [
                        'rules' => [
                            'notempty' => __d('be', 'A page is required'),
                        ]
                    ],
                    'attr' => [
                        'type' => 'text',
                        'label' => __d('be', 'Press'),
                        'class' => 'selector',
                        'data-selector-max' => 1,
                        'data-selector-node' => 'true',
                        'data-selector-text' => __d('be', 'Select page'),
                    ],
                    'details' => false,
                ],
                'weather' => [
                    'fieldset' => __d('be', 'Pages'),
                    'required' => [
                        'rules' => [
                            'notempty' => __d('be', 'A page is required'),
                        ]
                    ],
                    'attr' => [
                        'type' => 'text',
                        'label' => __d('be', 'Weather'),
                        'class' => 'selector',
                        'data-selector-max' => 1,
                        'data-selector-node' => 'true',
                        'data-selector-text' => __d('be', 'Select page'),
                    ],
                    'details' => false,
                ],
                'jobs' => [
                    'fieldset' => __d('be', 'Pages'),
                    'required' => [
                        'rules' => [
                            'notempty' => __d('be', 'A page is required'),
                        ]
                    ],
                    'attr' => [
                        'type' => 'text',
                        'label' => __d('be', 'Jobs'),
                        'class' => 'selector',
                        'data-selector-max' => 1,
                        'data-selector-node' => 'true',
                        'data-selector-text' => __d('be', 'Select page'),
                    ],
                    'details' => false,
                ],
                'treatments' => [
                    'fieldset' => __d('be', 'Pages'),
                    'required' => [
                        'rules' => [
                            'notempty' => __d('be', 'A page is required'),
                        ]
                    ],
                    'attr' => [
                        'type' => 'text',
                        'label' => __d('be', 'Treatments (overview)'),
                        'class' => 'selector',
                        'data-selector-max' => 1,
                        'data-selector-node' => 'true',
                        'data-selector-text' => __d('be', 'Select page'),
                    ],
                    'details' => false,
                ],
                'search' => [
                    'fieldset' => __d('be', 'Pages'),
                    'required' => [
                        'rules' => [
                            'notempty' => __d('be', 'A page is required'),
                        ]
                    ],
                    'attr' => [
                        'type' => 'text',
                        'label' => __d('be', 'Search'),
                        'class' => 'selector',
                        'data-selector-max' => 1,
                        'data-selector-node' => 'true',
                        'data-selector-text' => __d('be', 'Select page'),
                    ],
                    'details' => false,
                ],
                'privacy' => [
                    'fieldset' => __d('be', 'Pages'),
                    'required' => [
                        'rules' => [
                            'notempty' => __d('be', 'A page is required'),
                        ]
                    ],
                    'attr' => [
                        'type' => 'text',
                        'label' => __d('be', 'Privacy information'),
                        'class' => 'selector',
                        'data-selector-max' => 1,
                        'data-selector-node' => 'true',
                        'data-selector-text' => __d('be', 'Select page'),
                    ],
                    'details' => false,
                ],
                'gtc' => [
                    'fieldset' => __d('be', 'Pages'),
                    'required' => [
                        'rules' => [
                            'notempty' => __d('be', 'A page is required'),
                        ]
                    ],
                    'attr' => [
                        'type' => 'text',
                        'label' => __d('be', 'GTC'),
                        'class' => 'selector',
                        'data-selector-max' => 1,
                        'data-selector-node' => 'true',
                        'data-selector-text' => __d('be', 'Select page'),
                    ],
                    'details' => false,
                ],
                'cookie' => [
                    'fieldset' => __d('be', 'Pages'),
                    'required' => [
                        'rules' => [
                            'notempty' => __d('be', 'A page is required'),
                        ]
                    ],
                    'attr' => [
                        'type' => 'text',
                        'label' => __d('be', 'Cookies'),
                        'class' => 'selector',
                        'data-selector-max' => 1,
                        'data-selector-node' => 'true',
                        'data-selector-text' => __d('be', 'Select page'),
                    ],
                    'details' => false,
                ],
                'newsletter' => [
                    'fieldset' => __d('be', 'Pages'),
                    'required' => [
                        'rules' => [
                            'notempty' => __d('be', 'A page is required'),
                        ]
                    ],
                    'attr' => [
                        'type' => 'text',
                        'label' => __d('be', 'Newsletter'),
                        'class' => 'selector',
                        'data-selector-max' => 1,
                        'data-selector-node' => 'true',
                        'data-selector-text' => __d('be', 'Select page'),
                    ],
                    'details' => true,
                ],
                'error' => [
                    'fieldset' => __d('be', 'Pages'),
                    'required' => [
                        'rules' => [
                            'notempty' => __d('be', 'A page is required'),
                        ]
                    ],
                    'attr' => [
                        'type' => 'text',
                        'label' => __d('be', '404 errors'),
                        'class' => 'selector',
                        'data-selector-max' => 1,
                        'data-selector-node' => 'true',
                        'data-selector-text' => __d('be', 'Select page'),
                    ],
                    'details' => false,
                ],
                'services' => [
                    'fieldset' => __d('be', 'Pages'),
                    'required' => false,
                    'attr' => [
                        'type' => 'text',
                        'label' => __d('be', 'Incl. services'),
                        'class' => 'selector',
                        'data-selector-max' => 1,
                        'data-selector-node' => 'true',
                        'data-selector-text' => __d('be', 'Select page'),
                    ],
                    'details' => false,
                ],
                'room_overview' => [
                    'fieldset' => __d('be', 'Pages'),
                    'required' => false,
                    'attr' => [
                        'type' => 'text',
                        'label' => __d('be', 'Room overview'),
                        'class' => 'selector',
                        'data-selector-max' => 1,
                        'data-selector-node' => 'true',
                        'data-selector-text' => __d('be', 'Select page'),
                    ],
                    'details' => false,
                ],

            ]
        ],

        // fixed navigation links
        'header_elements' => [
            'name' => __d('be', 'Header elements'),
            'fields' => [
                'show' => [
                    'fieldset' => __d('be', 'Bubble'),
                    'required' => [
                        'rules' => [
                            'notempty' => __d('be', 'An option is required'),
                        ]
                    ],
                    'attr' => [
                        'type' => 'select',
                        'label' => __d('be', 'Show'),
                        'options' => [
                            1 => __d('be', 'On'),
                            0 => __d('be', 'Off'),
                        ]
                    ]
                ],
                'text' => [
                    'fieldset' => __d('be', 'Bubble'),
                    'multi' => false,
                    'required' => [
                        'rules' => [
                            'notempty' => __d('be', 'A text is required'),
                        ]
                    ],
                    'attr' => [
                        'type' => 'textarea',
                        'label' => __d('be', 'Text (front side)'),
                        'placeholder' => __d('be', 'Text (front side)'),
                        'class' => 'wysiwyg',
                        'data-config' => 'bubble',
                    ]
                ],
                'text_en' => [
                    'fieldset' => __d('be', 'Bubble'),
                    'multi' => false,
                    'required' => [
                        'rules' => [
                            'notempty' => __d('be', 'A text is required'),
                        ]
                    ],
                    'attr' => [
                        'type' => 'textarea',
                        'label' => __d('be', 'Text (front side)'),
                        'placeholder' => __d('be', 'Text (front side)'),
                        'class' => 'wysiwyg',
                        'data-config' => 'bubble',
                    ]
                ],
                'text_2' => [
                    'fieldset' => __d('be', 'Bubble'),
                    'multi' => false,
                    'required' => [
                        'rules' => [
                            'notempty' => __d('be', 'A text is required'),
                        ]
                    ],
                    'attr' => [
                        'type' => 'textarea',
                        'label' => __d('be', 'Text (back side)'),
                        'placeholder' => __d('be', 'Text (back side)'),
                        'class' => 'wysiwyg',
                        'data-config' => 'bubble',
                    ]
                ],
                'text_2_en' => [
                    'fieldset' => __d('be', 'Bubble'),
                    'multi' => false,
                    'required' => [
                        'rules' => [
                            'notempty' => __d('be', 'A text is required'),
                        ]
                    ],
                    'attr' => [
                        'type' => 'textarea',
                        'label' => __d('be', 'Text (back side)'),
                        'placeholder' => __d('be', 'Text (back side)'),
                        'class' => 'wysiwyg',
                        'data-config' => 'bubble',
                    ]
                ],
                'link' => [
                    'fieldset' => __d('be', 'Bubble'),
                    'attr' => [
                        'type' => 'text',
                        'label' => __d('be', 'Link'),
                        'class' => 'selector',
                        'data-selector-max' => 1,
                        'data-selector-node' => 'true',
                        'data-selector-text' => __d('be', 'Select page'),
                    ]
                ],
                'show_text' => [
                    'fieldset' => __d('be', 'Animated text'),
                    'required' => [
                        'rules' => [
                            'notempty' => __d('be', 'An option is required'),
                        ]
                    ],
                    'attr' => [
                        'type' => 'select',
                        'label' => __d('be', 'Show'),
                        'options' => [
                            1 => __d('be', 'On'),
                            0 => __d('be', 'Off'),
                        ]
                    ]
                ],
                'text_line_1' => [
                    'fieldset' => __d('be', 'Animated text'),
                    'multi' => false,
                    'required' => [
                        'rules' => [
                            'notempty' => __d('be', 'A text is required'),
                        ]
                    ],
                    'attr' => [
                        'type' => 'text',
                        'label' => __d('be', 'Text (line 1)'),
                        'placeholder' => __d('be', 'Text (line 1)'),
                        'class' => 'text',
                        'data-config' => 'bubble',
                    ]
                ],
                'text_line_1_en' => [
                    'fieldset' => __d('be', 'Animated text'),
                    'multi' => false,
                    'required' => [
                        'rules' => [
                            'notempty' => __d('be', 'A text is required'),
                        ]
                    ],
                    'attr' => [
                        'type' => 'text',
                        'label' => __d('be', 'Text (line 1)'),
                        'placeholder' => __d('be', 'Text (line 1)'),
                        'class' => 'text',
                        'data-config' => 'bubble',
                    ]
                ],
                'text_line_2' => [
                    'fieldset' => __d('be', 'Animated text'),
                    'multi' => false,
                    'required' => [
                        'rules' => [
                            'notempty' => __d('be', 'A text is required'),
                        ]
                    ],
                    'attr' => [
                        'type' => 'text',
                        'label' => __d('be', 'Text (line 2)'),
                        'placeholder' => __d('be', 'Text (line 2)'),
                        'class' => 'text',
                        'data-config' => 'bubble',
                    ]
                ],
                'text_line_2_en' => [
                    'fieldset' => __d('be', 'Animated text'),
                    'multi' => false,
                    'required' => [
                        'rules' => [
                            'notempty' => __d('be', 'A text is required'),
                        ]
                    ],
                    'attr' => [
                        'type' => 'text',
                        'label' => __d('be', 'Text (line 2)'),
                        'placeholder' => __d('be', 'Text (line 2)'),
                        'class' => 'text',
                        'data-config' => 'bubble',
                    ]
                ],
            ],
        ],

        // fixed navigation links
        'fixnav' => [
            'name' => __d('be', 'Navigation'),
            'fields' => [
                'first-left' => [
                    'fieldset' => __d('be', 'Fixed navigation links'),
                    'required' => [
                        'rules' => [
                            'notempty' => __d('be', 'A page is required'),
                        ]
                    ],
                    'attr' => [
                        'type' => 'text',
                        'label' => __d('be', 'First link on the left'),
                        'class' => 'selector',
                        'data-selector-max' => 1,
                        'data-selector-node' => 'true',
                        'data-selector-text' => __d('be', 'Select page'),
                    ],
                    'details' => true,
                ],
                'second-left' => [
                    'fieldset' => __d('be', 'Fixed navigation links'),
                    'required' => [
                        'rules' => [
                            'notempty' => __d('be', 'A page is required'),
                        ]
                    ],
                    'attr' => [
                        'type' => 'text',
                        'label' => __d('be', 'Second link on the left'),
                        'class' => 'selector',
                        'data-selector-max' => 1,
                        'data-selector-node' => 'true',
                        'data-selector-text' => __d('be', 'Select page'),
                    ],
                    'details' => true,
                ],
                'first-right' => [
                    'fieldset' => __d('be', 'Fixed navigation links'),
                    'required' => [
                        'rules' => [
                            'notempty' => __d('be', 'A page is required'),
                        ]
                    ],
                    'attr' => [
                        'type' => 'text',
                        'label' => __d('be', 'First link on the right'),
                        'class' => 'selector',
                        'data-selector-max' => 1,
                        'data-selector-node' => 'true',
                        'data-selector-text' => __d('be', 'Select page'),
                    ],
                    'details' => true,
                ],
                'second-right' => [
                    'fieldset' => __d('be', 'Fixed navigation links'),
                    'required' => [
                        'rules' => [
                            'notempty' => __d('be', 'A page is required'),
                        ]
                    ],
                    'attr' => [
                        'type' => 'text',
                        'label' => __d('be', 'Second link on the right'),
                        'class' => 'selector',
                        'data-selector-max' => 1,
                        'data-selector-node' => 'true',
                        'data-selector-text' => __d('be', 'Select page'),
                    ],
                    'details' => true,
                ],
                'footer-nav-items' => [
                    'fieldset' => __d('be', 'Footer navigation'),
                    'attr' => [
                        'type' => 'text',
                        'label' => __d('be', 'Menu items'),
                        'class' => 'selector',
                        'data-selector-max' => 5,
                        'data-selector-node' => 'true',
                        'data-selector-text' => __d('be', 'Select pages'),
                    ],
                    'details' => true,
                ],
                'footer-bottom-items' => [
                    'fieldset' => __d('be', 'Footer bottom navigation'),
                    'attr' => [
                        'type' => 'text',
                        'label' => __d('be', 'Bottom links'),
                        'class' => 'selector',
                        'data-selector-max' => 10,
                        'data-selector-node' => 'true',
                        'data-selector-text' => __d('be', 'Select pages'),
                    ],
                    'details' => true,
                ],
            ],
        ],

        // fixed links
        'links' => [
            'name' => __d('be', 'External'),
            'fields' => [
                'holidaycheck' => [
                    'fieldset' => __d('be', 'Partner'),
                    'multi' => true,
                    'required' => [
                        'rules' => [
                            'notempty' => __d('be', 'A link is required'),
                            'url' => [
                                'rule' => 'url',
                                'message' => __d('be', 'Invalid URL'),
                                'last' => true,
                            ],
                            'protocol' => [
                                'rule' => ['custom', '/^(http\:\/\/|https\:\/\/)/i'],
                                'message' => __d('be', 'Link without protocol'),
                            ]
                        ]
                    ],
                    'attr' => [
                        'type' => 'text',
                        'label' => __d('be', 'Holidaycheck'),
                        'placeholder' => __d('be', 'Holidaycheck'),
                    ]
                ],
                'tripadvisor' => [
                    'fieldset' => __d('be', 'Partner'),
                    'multi' => true,
                    'required' => [
                        'rules' => [
                            'notempty' => __d('be', 'A link is required'),
                            'url' => [
                                'rule' => 'url',
                                'message' => __d('be', 'Invalid URL'),
                                'last' => true,
                            ],
                            'protocol' => [
                                'rule' => ['custom', '/^(http\:\/\/|https\:\/\/)/i'],
                                'message' => __d('be', 'Link without protocol'),
                            ]
                        ]
                    ],
                    'attr' => [
                        'type' => 'text',
                        'label' => __d('be', 'Tripadvisor'),
                        'placeholder' => __d('be', 'Tripadvisor'),
                    ]
                ],

                // social
                'facebook' => [
                    'fieldset' => __d('be', 'Social'),
                    'required' => [
                        'rules' => [
                            'notempty' => __d('be', 'A link is required'),
                            'url' => [
                                'rule' => 'url',
                                'message' => __d('be', 'Invalid URL'),
                                'last' => true,
                            ],
                            'protocol' => [
                                'rule' => ['custom', '/^(http\:\/\/|https\:\/\/)/i'],
                                'message' => __d('be', 'Link without protocol'),
                            ]
                        ]
                    ],
                    'attr' => [
                        'type' => 'text',
                        'label' => __d('be', 'Facebook link'),
                        'placeholder' => __d('be', 'Facebook link'),
                    ]
                ],
                'facebook_check' => [
                    'fieldset' => __d('be', 'Social'),
                    'attr' => [
                        'type' => 'checkbox',
                        'label' => __d('be', 'Show Facebook lin'),
                    ]
                ],
                'instagram' => [
                    'fieldset' => __d('be', 'Social'),
                    'required' => [
                        'rules' => [
                            'notempty' => __d('be', 'A link is required'),
                            'url' => [
                                'rule' => 'url',
                                'message' => __d('be', 'Invalid URL'),
                                'last' => true,
                            ],
                            'protocol' => [
                                'rule' => ['custom', '/^(http\:\/\/|https\:\/\/)/i'],
                                'message' => __d('be', 'Link without protocol'),
                            ]
                        ]
                    ],
                    'attr' => [
                        'type' => 'text',
                        'label' => __d('be', 'Instagram link'),
                        'placeholder' => __d('be', 'Instagram link'),
                    ]
                ],
                'instagram_check' => [
                    'fieldset' => __d('be', 'Social'),
                    'attr' => [
                        'type' => 'checkbox',
                        'label' => __d('be', 'Show Instagram link'),
                    ]
                ],
                'twitter' => [
                    'fieldset' => __d('be', 'Social'),
                    'required' => [
                        'rules' => [
                            'notempty' => __d('be', 'A link is required'),
                            'url' => [
                                'rule' => 'url',
                                'message' => __d('be', 'Invalid URL'),
                                'last' => true,
                            ],
                            'protocol' => [
                                'rule' => ['custom', '/^(http\:\/\/|https\:\/\/)/i'],
                                'message' => __d('be', 'Link without protocol'),
                            ]
                        ]
                    ],
                    'attr' => [
                        'type' => 'text',
                        'label' => __d('be', 'Twitter link'),
                        'placeholder' => __d('be', 'Twitter link'),
                    ],
                ],
                'twitter_check' => [
                    'fieldset' => __d('be', 'Social'),
                    'attr' => [
                        'type' => 'checkbox',
                        'label' => __d('be', 'Show Twitter link'),
                    ]
                ],
                'youtube' => [
                    'fieldset' => __d('be', 'Social'),
                    'required' => [
                        'rules' => [
                            'notempty' => __d('be', 'A link is required'),
                            'url' => [
                                'rule' => 'url',
                                'message' => __d('be', 'Invalid URL'),
                                'last' => true,
                            ],
                            'protocol' => [
                                'rule' => ['custom', '/^(http\:\/\/|https\:\/\/)/i'],
                                'message' => __d('be', 'Link without protocol'),
                            ]
                        ]
                    ],
                    'attr' => [
                        'type' => 'text',
                        'label' => __d('be', 'YouTube link'),
                        'placeholder' => __d('be', 'YouTube link'),
                    ]
                ],
                'youtube_check' => [
                    'fieldset' => __d('be', 'Social'),
                    'attr' => [
                        'type' => 'checkbox',
                        'label' => __d('be', 'Show YouTube link'),
                    ]
                ],
            ]
        ],

        // tracking config
        'tracking' => [
            'name' => __d('be', 'Tracking & APIs'),
            'fields' => [
                'recapcha_site_key' => [
                    'fieldset' => __d('be', 'Recaptcha'),
                    'attr' => [
                        'type' => 'text',
                        'label' => __d('be', 'Site key'),
                        'placeholder' => '',
                        'templateVars' => ['help' => '<div class="help-message">' . sprintf(__d('be', 'Get this from %s'), ' <a href="https://www.google.com/recaptcha/admin#site">' . __d('be', 'Google') . '</a>') . '</div>'],
                    ]
                ],
                'recapcha_secret_key' => [
                    'fieldset' => __d('be', 'Recaptcha'),
                    'attr' => [
                        'type' => 'text',
                        'label' => __d('be', 'Secret key'),
                        'placeholder' => '',
                        'templateVars' => ['help' => '<div class="help-message">' . sprintf(__d('be', 'Get this from %s'), ' <a href="https://www.google.com/recaptcha/admin#site">' . __d('be', 'Google') . '</a>') . '</div>'],
                    ]
                ],

                // google analytics tracking id
                'ga-tracking-id' => [
                    'fieldset' => __d('be', 'Google'),
                    'attr' => [
                        'type' => 'text',
                        'label' => __d('be', 'Google Analytics - Tracking ID'),
                        'placeholder' => 'UA-XXXXXXXX-X',
                        'templateVars' => ['help' => '<div class="help-message">' . __d('be', 'Get this from Google Analytics / Manage / Property / Tracking-Information') . '</div>'],
                    ]
                ],
                'tagmanager-head' => [
                    'fieldset' => __d('be', 'Google'),
                    'attr' => [
                        'type' => 'textarea',
                        'label' => __d('be', 'Google Tagmanager - Head Code'),
                        'placeholder' => '<!-- Google Tag Manager --> ...',
                        'templateVars' => ['help' => '<div class="help-message">' . __d('be', 'Get this from Google Tagmanager / Manage / install Google Tagmanager') . '</div>'],
                    ]
                ],
                'tagmanager-body' => [
                    'fieldset' => __d('be', 'Google'),
                    'attr' => [
                        'type' => 'textarea',
                        'label' => __d('be', 'Google Tagmanager - Body Code'),
                        'placeholder' => '<!-- Google Tag Manager (noscript) --> ...',
                        'templateVars' => ['help' => '<div class="help-message">' . __d('be', 'Get this from Google Tagmanager / Manage / install Google Tagmanager') . '</div>'],
                    ]
                ],

                // google map
                'ga-map' => [
                    'fieldset' => __d('be', 'Google'),
                    'attr' => [
                        'type' => 'text',
                        'label' => __d('be', 'Google MAP API'),
                        'placeholder' => 'XXXXXXXX',
                    ]
                ],

                // facebook pixel
                'pixel-head' => [
                    'fieldset' => __d('be', 'Facebook Pixel'),
                    'attr' => [
                        'type' => 'textarea',
                        'label' => __d('be', 'Facebook Pixel - Head Code'),
                        'placeholder' => '<!-- Facebook Pixel --> ...',
                    ]
                ],

                // weather
                'weather-api' => [
                    'fieldset' => __d('be', 'Weather'),
                    'multi' => false,
                    'required' => [
                        'rules' => [
                            'notempty' => __d('be', 'API is required'),
                        ]
                    ],
                    'attr' => [
                        'type' => 'text',
                        'label' => __d('be', 'API'),
                        'placeholder' => 'API',
                    ]
                ],
                'weather-location' => [
                    'fieldset' => __d('be', 'Weather'),
                    'multi' => false,
                    'required' => [
                        'rules' => [
                            'notempty' => __d('be', 'A location is required'),
                        ]
                    ],
                    'attr' => [
                        'type' => 'text',
                        'label' => __d('be', 'Location'),
                        'placeholder' => 'location',
                    ]
                ],
                'weather-country' => [
                    'fieldset' => __d('be', 'Weather'),
                    'multi' => false,
                    'required' => [
                        'rules' => [
                            'notempty' => __d('be', 'A country is required'),
                        ]
                    ],
                    'attr' => [
                        'type' => 'text',
                        'label' => __d('be', 'Country'),
                        'placeholder' => 'at',
                    ]
                ],

            ]
        ],

        // development config
        // INFO: Do not change, just expand!
        'dev' => [
            'name' => __d('be', 'Development'),
            'fields' => [

                // debug
                'mode' => [
                    'fieldset' => __d('be', 'Debug'),
                    'required' => [
                        'rules' => [
                            'notempty' => __d('be', 'An option is required'),
                        ]
                    ],
                    'attr' => [
                        'type' => 'select',
                        'label' => __d('be', 'Status'),
                        'options' => [
                            1 => __d('be', 'On'),
                            0 => __d('be', 'Off'),
                        ]
                    ]
                ],
                'ip' => [
                    'fieldset' => __d('be', 'Debug'),
                    'attr' => [
                        'type' => 'text',
                        'label' => __d('be', 'IP Address(es)'),
                        'placeholder' => __d('be', 'IP Address(es)'),
                        'templateVars' => ['help' => '<div class="help-message">' . __d('be', 'Debug IP Addresses, seperat with ";"') . '</div>'],
                    ]
                ],
                'email' => [
                    'fieldset' => __d('be', 'Debug'),
                    'required' => [
                        'rules' => [
                            'notempty' => __d('be', 'An email address is required'),
                            'email' => [
                                'rule' => 'email',
                                'message' => __d('be', 'Invalid email address'),
                            ]
                        ]
                    ],
                    'attr' => [
                        'type' => 'text',
                        'label' => __d('be', 'E-Mail address'),
                        'placeholder' => __d('be', 'E-Mail address'),
                        'templateVars' => ['help' => '<div class="help-message">' . __d('be', 'Recipient for ALL emails when debug mode is ON!') . '</div>'],
                    ]
                ],

            ]
        ],

    ],

    // season "containers"
    'season-containers' => [
        //'spring' => __d('be', 'Spring'),
        'summer' => __d('be', 'Summer'),
        //'autumn' => __d('be', 'Autumn'),
        'winter' => __d('be', 'Winter'),
    ],

    // ssl
    'ssl' => false,

];

return $_global;
