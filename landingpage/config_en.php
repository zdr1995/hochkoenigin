<?php
//config file for medienjaeger Landingpage
//(C) Medienjaeger 2014

$cfg = array();


//Deutsch
$cfg['en'] = array(
	'general' => array(
		// 'form-recipient' => $_SERVER['REMOTE_ADDR'] == '83.175.88.51' ? 'coders@medienjaeger.at' : 'urlaub@hochkoenigin.com',
		'form-recipient' => 'urlaub@hochkoenigin.com',
		'htmltitle' => 'the HOCHK&Ouml;NIGIN - magic mountain resort',
		'phone' => array('plain' => '004365847447', 'nice' => '+43 6584 7447'),
		'email' => 'urlaub@hochkoenigin.com',
		'email_temporary' => 'urlaub@hochkoenigin.com',
		'website' => 'www.hochkoenigin.com',
		'company-name' => 'the Hochk&ouml;nigin',
		'company-street' => 'Hochk&ouml;nigstra&szlig;e 27',
		'company-city' => 'A-5761 Maria&nbsp;Alm',
		'company-country' => 'Austria',
		'name' => 'Family Marina & Manfred Sch&ouml;negger',
		'gmap' => 'https://www.google.com/maps/dir//die+HOCHK%C3%96NIGIN+Magic+Mountain+Resort,+Hochk%C3%B6nigstra%C3%9Fe+27,+5761+Maria+Alm/@47.4044595,12.9022839,21z/data=!4m8!4m7!1m0!1m5!1m1!1s0x4776e3c45de45c3b:0x963aa37920f93d1f!2m2!1d12.902192!2d47.404502?hl=en',
		'privacy' => '/privacy',
		'imprint' => '/imprint',
		'gtc' => '/gtc',
		'facebook' => 'https://www.facebook.com/hochkoenigin/',
		'instagram' => 'https://www.instagram.com/diehochkoenigin/',
	),
	'header' => array('title' => 'header', 'path' => '/img/header.png'),
	'images' => array(
		array('title' => 'logo', 'path' => '/img/logo.png'),
		array('title' => 'shadow', 'path' => '/img/shadow.png'),
		array('title' => 'comming_soon', 'path' => '/img/comming_soon_eng.png'),
		array('title' => 'icon', 'path' => '/img/icon.png'),
		array('title' => 'gold_line', 'path' => '/img/gold_line.png'),
		array('title' => 'pattern', 'path' => '/img/pattern.png'),
		array('title' => 'phone', 'path' => '/img/phone.svg'),
		array('title' => 'email', 'path' => '/img/email.svg'),
		array('title' => 'facebook', 'path' => '/img/facebook.jpg'),
		array('title' => 'instagram', 'path' => '/img/instagram.jpg'),
		array('title' => 'remove', 'path' => '/img/remove.svg'),
	),
	'content' => array(
		'headline_1' => '<h2 class="headline_1">the HOCHK&Ouml;NIGIN – </h2>',
		'headline_2' => '<h1 class="headline_2"><strong>Magic Mountain Resort</strong> – Coming&nbsp;soon</h1>',
		'text_1_l' => '<span class="text_1_l"><strong>New, exceptionally different, cheerful, colourful – simply magical: The &ldquo;HOCHK&Ouml;NIGIN&rdquo; will be enthroned in Maria Alm from December 2019, offering a royal reception for her guests.</strong> The Thalerhof will become something magisterial amongst hotels, where every guest can feel like royalty.</br></br> The Thalerhof will continue to retain its famous <strong>warmth and hospitality</strong> which will also become a large part of the &ldquo;HOCHK&Ouml;NIGIN&rdquo;.</span>',
		'text_1_r' => '<span class="text_1_r"><strong>40 new dreamlike suite rooms</strong> with beautiful loggias with a wonderful view, a <strong>SKY SPA</strong> with astonishing vistas, <strong>an event sauna, outdoor infinity pool, exciting activity programme, a park with magical relaxation areas in the countryside, outdoor show kitchen</strong> and much more will invite you to relax, feel good and leave your everyday life behind you beginning in December 2019. The exuberant joie de vivre and cheerfulness in the midst of dreamlike nature are contagious and guarantee an unforgettable holiday at the foot of the Hochk&ouml;nig in beautiful Salzburger Land. We look forward to welcoming you to the magical new &ldquo;HOCHK&Ouml;NIGIN&rdquo;!</span>',
		'text_2' => '<span class="text_2">We look forward to welcoming you to the magical new &ldquo;HOCHK&Ouml;NIGIN&rdquo;!</span>',
		'text_3' => '<span class="text_3">THE H&Ouml;RL FAMILY</span>',
	),
	'gallery' => array(
		array('title' => 'image_1', 'path' => '/img/gallery/bild_1.png'),
		array('title' => 'image_2', 'path' => '/img/gallery/bild_2.png'),
		array('title' => 'image_3', 'path' => '/img/gallery/bild_3.png'),
		array('title' => 'image_4', 'path' => '/img/gallery/bild_4.png'),
		array('title' => 'image_5', 'path' => '/img/gallery/bild_5.png'),
		array('title' => 'image_6', 'path' => '/img/gallery/bild_6.png'),
	),
	'offer' => [
		'headline' => '&ldquo;BE THE FIRST&rdquo; - UNIQUE OPENING BID!',
		'image' => '<img src="/img/offer_eng.png" alt="book now" />',
		'text' => '
			<p>Why not enjoy your holiday in a different way – more cheerful, colorful and magical? Get to know our Magic Mountain Resort ****s – die HOCHKÖNIGIN exclusively. Discover all novelties and benefit from our opening bid:</p>
			<ul>
				<li><strong>INDULGENCE BOARD PLUS</strong> from early morning to late evening – so much winter holiday pleasure included</li>
				<li>Calm down and enjoy the silence with your <strong>free wellness voucher</strong>, which is worth &euro; 50,- (redeemable until 13:00 o’clock)</li>
				<li>All <strong>included Magic Mountain services</strong></li>
				<li><strong>Free Hochkönig Card</strong>: look forward to magical winter moments!</li>
				<li><strong>2000 m&sup2; of pure wellness pleasure</strong>: Explore our NATURE SPA on three floors</li>
			</ul>
			<table align="center">
				<tr>
					<td><strong class="red">4 nights incl. half board</strong></td>
					<td></td>
					<td><strong class="red">from &euro; 524</strong></td>
				</tr>
				<tr>
					<td><strong class="red">7 nights incl. half board</strong></td>
					<td></td>
					<td><strong class="red">from &euro; 875</strong></td>
				</tr>
			</table>',
		'button' => 'REQUEST',
	],
	'prices' => [
		'headline' => 'Price list',
		'text' => '<a href="https://www.hochkoenigin.com/de/nhw/zimmer-preise.html" target="_blank"><img src="/img/translatable/prices_1_en.jpg" alt="Price list 1" /></a><br>
		<br>
		<a href="https://www.hochkoenigin.com/de/nhw/zimmer-preise.html" target="_blank"><img src="/img/translatable/prices_2_en.jpg" alt="Price list 2" /></a><br>
		<br>',
		'button' => '<a href="/files/pricelist.pdf" class="button" target="_blank">DOWNLOAD PRICE LIST</a>',
	],
	'404' => [
		'headline' => 'Website not found',
		'text' => 'This website can not be found.',
	],
	'imprint' => [
		'headline' => 'Imprint',
		'text' => '
			<strong>Hochk&ouml;nigin GmbH & Co KG</strong><br />
			According to &sect; 5 (1) of the Austrian eCommerce Act (ECG) and &sect; 24 Media Act, we hereby state that we are the operator of this website:<br><br>
			Hochk&ouml;nigin GmbH & Co KG&nbsp;<br/>
			<a href="https://www.google.com/maps/dir//Hochk%C3%B6nigstra%C3%9Fe+27,+5761+Alm/@47.4045275,12.9001605,17z/data=!4m8!4m7!1m0!1m5!1m1!1s0x4776e3c45e11e5ab:0x2d9d226b9d8da6b7!2m2!1d12.9023492!2d47.4045275" target="_blank">Hochk&ouml;nigstra&szlig;e 27</br>
			A-5761 Maria&nbsp;Alm</a><br/>
			Tel&period;&colon; <a href="004365847447-0">+43/ &lpar;0&rpar;6584/7447-0</a><br>
			Fax&colon; <a href="tel:004365847447-17">+43/ &lpar;0&rpar;6584/7447-17</a><br>
			<a href="https://www.hochkoenigin.com" target="_blank">www.hochkoenigin.com</a><br>
			<a href="mailto:urlaub@hochkoenigin.com">urlaub@hochkoenigin.com</a><br><br><br>
			<strong>Company name:</strong> Hochk&ouml;nigin GmbH & Co KG<br>
			<strong>Executive Director:</strong> H&ouml;rl Josef<br>
			<strong>Self-chosen company name:</strong> the HOCHK&Ouml;NIGIN<br>
			<strong>Supervisory authority:</strong> District Commission Zell am See <br>
			Member of the Salzburg Chamber of Commerce<br>
			<strong>Section:</strong> Hotel industry<br>
			<strong>Company registration number:</strong> FN: 28793<br>
			<strong>VAT ID:</strong> ATU 33420508<br>
			<h3>Concept, screen design and technical implementation:</h3>
			Advertising agency medien-h&auml;ger GmbH<br>
			E-Mail: <a href="mailto:office@medienjaeger.at">office@medienjaeger.at</a><br>
			<a href="https://www.medienjaeger.at" target="_blank">www.medienjaeger.at</a><br>
			<h3>Website contents</h3>
			Hochk&ouml;nigin GmbH & Co KG assumes no responsibility for the topicality, correctness, completeness or quality of the information provided. Liability claims against Hochk&ouml;nigin GmbH & Co KG which relate to damages of a material or immaterial nature caused by the use or non-use of the information provided or by the use of incorrect or incomplete information are excluded, unless there is demonstrably intentional or grossly negligent action by Hochk&ouml;nigin GmbH & Co KG. All offers are non-binding. Hochk&ouml;nigin GmbH & Co KG expressly reserves the right to change parts of or the entire offer without prior notice, add to, delete or cease publication temporarily or permanently.<br>
			<h3>References and links</h3>
			Regarding direct or indirect references to external websites, i.e. &ldquo;links&rdquo;, which lie outside the area of responsibility of Hochk&ouml;nigin GmbH & Co KG, a liability obligation would only come into force in the event that Hochk&ouml;nigin GmbH & Co KG has knowledge of the contents and it would at the same time be technically possible and reasonable to prevent use in the event of illegal content. Hochk&ouml;nigin GmbH & Co KG hereby expressly declares that at the time of linking the respective linked pages were free of illegal content. Hochk&ouml;nigin GmbH & Co KG has no influence on the current and future design, content or authorship of the linked or referred pages. Hochk&ouml;nigin GmbH & Co KG hereby expressly dissociates itself from all contents of all linked or referred pages that have been changed after the link has been set. This statement applies to all links and references within its own internet offer as well as to external entries in services provided by the company such as guest books, discussion forums, mailing lists and the like. For illegal, incorrect or incomplete contents and in particular for damages resulting from the use or non-use of such presented information, the provider of the page referred to, and not the one who only links to the respective page, is liable.<br>
			<h3>Copyright and trademark law</h3>
			<strong>Photo credits:</strong><br>
			Zuchna Visualization, Hochk&ouml;nigin GmbH & Co. KG, TVB Maria Alm, Salzburger Land Tourismus, fotolia.de, Archive Gro&szlig;glockner Hochalpenstra&szlig;en AG, Krimmler Waterfalls, Salzburger Land, Austria Advertising, Garger Wolfgang, Photography G&uuml;nter Standl <br>
			Huber Photography - Huber Michael<br><br>
			Hochk&ouml;nigin GmbH & Co KG endeavours to observe the copyrights for graphics, sound documents, video sequences and texts used in all publications, to use graphics, sound documents, video sequences and texts created by itself or to use unlicensed graphics, sound documents, video sequences or texts. All brand names and trademarks mentioned within the Internet offer and possibly protected by third parties are subject without restriction to the provisions of the respectively valid trademark law and the ownership rights of the respective registered owner. Mere mention is not sufficient to conclude that trademarks are not protected by rights of third parties! The copyright for published objects created by Hochk&ouml;nigin GmbH & Co KG remains solely with the owner of the website. Reproduction or use of such graphics, sound documents, video sequences or texts in other electronic or printed publications is not permitted without the express consent of Hochk&ouml;nigin GmbH & Co KG.<br>
			<h3>Copyright/Liability</h3>
			Given the technical characteristics of the Internet, no guarantee can be given for the authenticity, accuracy or completeness of the information made available on the Internet. There is no guarantee given for the availability or operation of the website and its contents. Any liability for direct, indirect or other damages, regardless of their causes, resulting from the use or unavailability of the data and information on this website is excluded to the extent legally permissible. The content of this website is protected by copyright. The information is intended for personal use only. Any further use, in particular storage in databases, duplication and any form of commercial use as well as passing on to third parties also in parts or in revised form without agreement of the respective organisation is forbidden. Any integration of individual pages of our offer in foreign frames is to be omitted.<br>
			<h3>Miscellaneous</h3>
			If sections or individual terms of this statement are not legal or correct, the content or validity of the other parts remain unaffected by this fact.<br>
			<h3>Online Dispute Resolution</h3>
			Since 9/1/2016, the EU Regulation on Online Dispute Resolution in Consumer Affairs (No 524/2013) applies. Disputes between consumers and merchants in connection with online sales contracts or online service contracts can be resolved through the following online platform. <a href="https://ec.europa.eu/consumers/odr/" target="_blank">https://ec.europa.eu/consumers/odr/</a><br><br>
			We care about the security of your data. We only use the data for the purpose for which it is earmarked and only pass it on to third parties provided that they contribute to the fulfilment the purpose as expressly commissioned by us. According to the EU General Data Protection Regulation (GDPR) and the Federal Law Gazette 2017/120 (LINK) of the Republic of Austria with final effect 25.5.2018, users have the right to obtain free information on request about the personal data that we have stored about them. In addition, each customer or user has the right at any time to correction of incorrect data, blocking and deletion of their personal data, as far as no statutory storage or reporting obligation.
		',
	],
	'privacy' => [
		'headline' => 'Privacy policy',
		'text' => '
			<h3>Declaration on information obligation</h3>
			The protection of your personal data is very important to us. Therefore we process your data exclusively on the basis of the legal regulations (GDPR, TKG 2003). In this privacy policy we inform you about the most important aspects of data processing within our website.<br><br>
			<h3>General</h3>
			By using this website, you consent to the use of personal and non-personal data as set forth in this privacy policy in the manner described below and for the purposes set forth below. You may withdraw your consent at any time (also in part) by e-mail <a href="mailto:urlaub@hochkoenigin.com" target="_blank">urlaub&commat;hochkoenigin.com</a>. This may affect the functionality of the content offered on this site, in particular because some data applications are required to deliver the service you requested. You also have the right to information about personal data concerning you, to data portability, to correction or deletion of personal data as well as to restriction or opposition to the processing of your personal data. You can exercise these rights by e-mail to <a href="mailto:urlaub@hochkoenigin.com" target="_blank">urlaub&commat;hochkoenigin.com</a>. Furthermore, you have the right to appeal to the Data Protection Authority<br> in Austria: <a href="https://www.dsb.gv.at" target="_blank">https://www.dsb.gv.at</a><br>
			in Germany: <a href="https://www.bfdi.bund.de/DE/Infothek/Anschriften_Links/anschriften_links-node.html" target="_blank">https://www.bfdi.bund.de/DE/Infothek/Anschriften_Links/anschriften_links-node.html</a><br>
			in Italy: <a href="https://www.garanteprivacy.it" target="_blank">https://www.garanteprivacy.it</a><br>
			National Data Protection Authorities: <a href="https://www.dsb.gv.at/links" target="_blank">https://www.dsb.gv.at/links</a>.<br>
			Personal data (e.g. name, e-mail address, telephone number) is processed and transmitted by HOCHK&Ouml;NIGIN GMBH & CO KG only in a lawful manner (in particular for fulfilment of contractual obligations or on the basis of your consent).<br><br>
			<h3>Contact</h3>
			If you contact HOCHK&Ouml;NIGIN GMBH & CO KG by e-mail and/or contact form, your personal data, in particular your name and e-mail address, will be stored for 3 years in order to process the inquiry and for any follow-up questions. This data processing is therefore required for the fulfilment of the contract in accordance with &sect; 8 (3) (4) DSG and Art. 6 (1) (b) GDPR. If you leave comments or other contributions in the boxes provided, your IP address will be stored for a maximum of six months. This storage has the purpose to be able to identify those responsible in case of any legal violations. It therefore lies in the primary interest of HOCHK&Ouml;NIGIN GMBH & CO KG according to &sect; 8 (1) Z 4 DSG and Art 6 (1) (f) GDPR. The data entered by you in the contact form remains with us until you ask us to delete it, you revoke your consent to the storage or the purpose for the data storage is omitted (e.g. after completion of your request). Mandatory statutory provisions – especially retention periods – remain unaffected.<br><br>
			<h3>Presentation of the website</h3>
			HOCHK&Ouml;NIGIN GMBH & CO KG saves the following data with each access to the website <a href="https://www.hochkoenigin.com" target="_blank">www.hochkoenigin.com</a>(hereinafter referred to as the &ldquo;website&rdquo;): Name of the website visited, requested file, date/time, amount of data transferred, message about successful retrieval, browser type/version, operating system, previously visited page and IP address. The short-term storage of the IP address of your device is required for the provision of the content of the website and thus in accordance with &sect; 8 (3) Z 4 DSG and in accordance with Art. 6 (1) (b) GDPR for the performance of the contract. Your IP address will be deleted after the use of the website. HOCHK&Ouml;NIGIN GMBH & CO KG uses the aforementioned data for statistical purposes only.<br><br>
			<h3>Cookies</h3>
			When you visit the website, one or more cookies (small text files) are stored on your device. The purpose of this is to improve the service of HOCHK&Ouml;NIGIN GMBH & CO KG by, for example, saving user settings. Use of the website is also possible without cookies. You can disable the storage of cookies in your browser, restrict them to certain websites, or set your browser to notify you before a cookie is stored. At any time, you can delete cookies from your computer’s hard drive using the privacy features of your browser. In this case, the functionality and usability of the website could be restricted.<br><br>
			<h3>Server log files</h3>
			The provider of the website automatically collects and stores information in so-called server log files, which your browser automatically transmits to us. This includes:<br>
			<ul>
			<li>Browser type and browser version</li>
			<li>Operating system used</li>
			<li>Referrer URL</li>
			<li>Host name of the accessing computer</li>
			<li>Time of the server request</li>
			<li>IP address</li>
			</ul><br>
			There is no merge of this data with other data sources. The basis for data processing is Art. 6 (1) (b) GDPR, which allows the processing of data for the fulfilment of a contract or precontractual measures.<br><br>
			<h3>SSL encryption</h3>
			This site uses SSL encryption for security reasons and to protect the transmission of confidential content, such as requests that you send to HOCHK&Ouml;NIGIN GMBH & CO KG. An encrypted connection is indicated by the browser’s address bar changing from &quot;http: //&quot; to &quot;https: //&quot; and the lock icon in your browser bar. If the SSL encryption is activated, the data that you send to HOCHK&Ouml;NIGIN GMBH & CO KG cannot be read by third parties. Data storage for online booking We point out that for the purpose of online booking and for subsequent contract processing by the online booking system, cookies and the IP data of the connection owner are stored, as well as the data provided by the booking person in the online booking. In addition all necessary data for the booking is stored with us for the purpose of fulfilling the contract. The data provided by you is required to fulfil the contract or to carry out pre-contractual measures. Without this data we cannot conclude the contract with you. Data transfer to third parties will not take place, with the exception of the corresponding channel manager, hotel programme or the transmission of credit card data to the processing bank/payment service provider for the purpose of debiting the online booking, as well as to our tax advisor to fulfil our tax obligations. When cancelling the online booking, the data stored with us will be deleted. In the event of a successful online booking, all data from the contractual relationship is stored until the end of the tax retention period (7 years). Data processing takes place on the basis of the statutory provisions of &sect; 96 (3) TKG as well as of Art. 6 (1) (a) (consent) and/or (b) (necessary for fulfilment of the contract) of the GDPR.<br><br>
			<h3>Google Analytics</h3>
			This website uses Google Analytics, a web analytics service provided by Google Inc. (&ldquo;Google&rdquo;), 1600 Amphitheater Parkway, Mountain View, CA 94043, USA. Google Analytics uses cookies that allow analysis of the use of the website. The data generated by the cookie about your use of this website (including your IP address) will be transmitted to a Google server in the USA and stored there for the duration of your visit to the website <a href="https://www.hochkoenigin.com" target="_blank">www.hochkoenigin.com</a>. For the transfer of personal data to the US, an adequacy decision (https://eur-lex.europa.eu/legal-content/DE/TXT/HTML/?uri=CELEX:32016D1250&from=DE ) of the European Commission exists. Google will use the data transmitted on behalf of HOCHK&Ouml;NIGIN GMBH & CO KG to evaluate your use of the website, to compile reports on the website activities for HOCHK&Ouml;NIGIN GMBH & CO KG and to provide other services related to website activity and Internet usage. Google will also transfer this data to third parties if required by law or if third parties process this data on behalf of Google. Google will never associate your IP address with other Google data. However, if IP anonymisation is enabled on this site, Google will shorten your IP address beforehand within the European Union or within the European Economic Area. Only in exceptional cases will the full IP address be sent to a Google server in the US and shortened there. IP anonymisation is active on this website. You can prevent the storage of cookies in the context of Google Analytics by a corresponding setting of your browser software. In this case, you may not be able to use all features of this website. You may also prevent the collection by Google of the data generated by the cookie and related to the use of the website (including your IP address) as well as the processing of this data by Google by downloading and installing the browser plug-in available under the following link: https://tools.google.com/dlpage/gaoptout?hl=en For more information about the Google Analytics terms of use and privacy policy, please visit <a href="https://www.google.com/analytics/terms/de.html" target="_blank">https://www.google.com/analytics/terms/de.html</a><br><br>
			<h3>Google AdWords Remarketing</h3>
			This website uses Google Remarketing. Google Remarketing is an advertising service provided by Google that allows targeted promotion to previous visitors to the site. Third parties, including Google, serve ads on websites on the Internet. Cookies are stored on your device and used to serve advertisements based on previous visits by a user to this website. The identification of the users takes place via cookies set in the web browser. With the help of the text files, the user behaviour can be analysed when visiting the website and then used for targeted product recommendations and interest-based advertising. If you do not want to receive interest-based advertising, you can use the Ads Preferences Manager to disable Google’s use of cookies for these purposes and to adjust ads on the Google Display Network by visiting Google at </a href="https://adssettings.google.com/?hl=de" target="_blank">https://adssettings.google.com/?hl=de</a>. <br><br>
			<h3>Google Maps</h3>
			The website of HOCHK&Ouml;NIGIN GMBH & CO KG uses the Google Maps API of Google Inc., based in the USA, to visually display geographical information. When using Google Maps features, Google collects, processes and uses data. For more information about the processing of data by Google, please refer to the privacy policy of Google: <a href="https://www.google.de/intl/de/policies/privacy" target="_blank">https://www.google.de/intl/de/policies/privacy</a><br><br>
			<h3>Google-Fonts</h3>
			We embed certain fonts through the Google Fonts service of Google Inc., 1600 Amphitheater Parkway, Mountain View, CA 94043 USA. This is necessary for the presentation of the used font. The integration of the font takes place via a call on the server of Google and is loaded into the browser cache. When downloading the font, your IP address, as well as the visited website will be transferred to Google. More information can be found here. Google Fonts: <a href="https://developers.google.com/fonts/faq Google terms of service: https://policies.google.com/privacy?hl=en" target="_blank">https://developers.google.com/fonts/faq Google terms of service: https://policies.google.com/privacy?hl=en</a><br><br>
			<h3>Facebook Custom Audiences</h3>
			HOCHK&Ouml;NIGIN GMBH & CO KG uses the website Facebook Custom Audiences. Facebook Custom Audiences is a promotional service provided by Facebook Inc., 1601 S California Ave., Palo Alto, CA, 94304, USA, which allows users to promote their website in a targeted manner. Facebook uses cookies to serve ads based on previous visits by a user to this site. The identification of the users takes place via cookies set in the web browser, which are stored there. With the help of cookies, the user behaviour can be analysed when visiting the website and then used for targeted product recommendations and interest-based advertising. If you do not want to receive interest-based advertising on Facebook, you can unsubscribe here (in English only): <a href="https://www.facebook.com/settings/?tab=ads" target="_blank">https://www.facebook.com/settings/?tab=ads</a><br><br>
			<h3>Social media plug-ins</h3>
			HOCHK&Ouml;NIGIN GMBH & CO KG uses so-called social media plug-ins (interfaces to social networks) on the website. When visiting the website, the system automatically connects to the respective social network due to the integration of social media plug-ins and transmits data (IP address, visit to the website, etc.). The data transmission takes place without the intervention of and outside the responsibility of HOCHK&Ouml;NIGIN GMBH & CO KG. HOCHK&Ouml;NIGIN GMBH & CO KG points out that you can prevent this data transmission by logging out of the respective social networks before visiting the website. Only in the &ldquo;logged in&rdquo; state can the social network assign specific data to your activity profile through automatic data transmission. The automatically transmitted data is used exclusively by the operators of the social networks and not by HOCHK&Ouml;NIGIN GMBH & CO KG. For more information, including on the content of social media data collection, please consult the website of the social network. You can also customise your privacy settings there. The social networks included on the website are:<br>
			<ul>
			<li>“Facebook”<br>
			Facebook Inc&period;&comma; 1601 S California Ave&comma; Palo Alto&comma; CA&comma; 94304&comma; USA<br>
			For more information, visit <a href="https://www.facebook.com/policy.php" target="_blank">https://www.facebook.com/policy.php</a></li>
			<li>“Twitter”<br>
			Twitter Inc&period;&comma; 795 Folsom St&period;&comma; Suite 600&comma; San Francisco&comma; CA 94107&comma; USA<br>
			For more information, visit <a href="" target="_blank">https://twitter.com/privacy?lang=en</a></li>
			<li>“Youtube”<br>
			YouTube&comma; LLC&comma; 901 Cherry Ave&period;&comma; San Bruno&comma; CA 94066&comma; USA<br>
			For more information, visit <a href="https://www.youtube.com/t/privacy_guidelines" target="_blank">https://www.youtube.com/t/privacy_guidelines</a></li>
			<li>“Google”<br>
			Google LLC &lpar;„Google“&rpar;&comma; Amphitheatre Parkway&comma; Mountain View&comma; CA 94043&comma; USA<br>
			For more information, visit <a href="https://policies.google.com/terms?hl=en" target="_blank">https://policies.google.com/terms?hl=en</a></li>
			</ul><br>
			<h3>Other third-party tools</h3>
			Our website uses functions, widgets or plugins from<br><ul>
			<li>Tripadvisor (rating widget): TripAdvisor LLC, 400 1st Avenue, Needham, MA 02494, United States</li>
			<li>Holidaycheck (rating widget): HolidayCheck AG, Bahnweg 8, CH-8598 Bottighofen</li>
			<li>Wunderground.com (weather report): The Weather Company, to IBM business<br>
			Attn: Privacy Office 1001 Summit Boulevard&comma; Floor 20 Brookhaven&comma; GA&comma; USA 30319</li>
			<li>Webcams: feratel media technologies AG, Maria-Theresien-Strasse 8, A-6020 Innsbruck</li>
			</ul>
			The IP address is sent to these third-party tools to ensure that they work. We have no control over whether the third-party providers store the IP address for statistical purposes or similar. More information can be found in the privacy policies of the respective services.<br><br>
			<h3>Newsletter</h3>
			If you want to subscribe to the free newsletter of HOCHK&Ouml;NIGIN GMBH & CO KG, you only have to enter the e-mail address to which the newsletter is to be sent. This data processing is therefore required for the fulfilment of the contract in accordance with &sect; 8 (3) (4) DSG and Art. 6 (1) (b) GDPR. Other personal information such as first and last name, interests ... are optional. This additional information about you serves only to personalise the newsletter. Registration uses the double opt-in procedure. After logging in, you will receive a confirmation and authorisation e-mail with the request to click on the link contained in the e-mail address provided by you. This ensures that only the authorised user of the given e-mail address can sign up for our newsletter mailing list. You can unsubscribe anytime. At the end of each newsletter there is a link where you can unsubscribe. Alternatively, you can also send a corresponding e-mail to <a href="mailto:urlaub@hochkoenigin.com" target="_blank">urlaub&commat;hochkoenigin.com</a>. Your personal data will then be deleted from the newsletter mailing list. If we have received the relevant information in the context of the contractual relationship with you, HOCHK&Ouml;NIGIN GMBH & CO KG reserves the right to permanently store for its own advertising purposes your first and last name, postal address, year of birth and your professional, branch or business name in summary lists and to use this information for the delivery of interesting offers and information on offers of HOCHK&Ouml;NIGIN GMBH & CO KG by mail. You can object to the processing of your data for this purpose at any time by sending a message to <a href="mailto:urlaub@hochkoenigin.com" target="_blank">urlaub&commat;hochkoenigin.com</a>.<br><br>
			<h3>Contests</h3>
			When participating in the contests offered on the website of HOCHK&Ouml;NIGIN GMBH & CO KG personal data, namely title, first name, last name, e-mail address, mailing address and the country, are collected. This data is required for the execution of the contest, in particular for the assignment of the participation requests to the respective participants as well as for the determination and notification of the winners – thus in accordance with to &sect; 8 (3) Z 4 DSG and Art. 6 (1) (b) GDPR for fulfilment of the contract. The personal data received will only be processed and used by HOCHK&Ouml;NIGIN GMBH & CO KG insofar as it is necessary for the execution of the contest. The personal data is stored for the duration of the contest and – for processing any winning and damage claims – for a maximum of three years thereafter and then deleted. By participating in the contest, you agree that your name will be published on this website and on the social media channels in the event of winning.<br><br>
			For questions about data processing you can reach us under the following contact details:<br>
			Hochk&ouml;nigin GmbH & Co KG&nbsp;<br/>
			<a href="https://www.google.com/maps/dir//Hochk%C3%B6nigstra%C3%9Fe+27,+5761+Alm/@47.4045275,12.9001605,17z/data=!4m8!4m7!1m0!1m5!1m1!1s0x4776e3c45e11e5ab:0x2d9d226b9d8da6b7!2m2!1d12.9023492!2d47.4045275" target="_blank">Hochk&ouml;nigstra&szlig;e 27</br>
			A-5761 Maria&nbsp;Alm</a><br/>
			Tel&period;&colon; <a href="004365847447-0">+43/ &lpar;0&rpar;6584/7447-0</a><br>
			Fax&colon; <a href="tel:004365847447-17">+43/ &lpar;0&rpar;6584/7447-17</a><br>
			<a href="https://www.hochkoenigin.com" target="_blank">www.hochkoenigin.com</a><br>
			<a href="mailto:urlaub@hochkoenigin.com">urlaub@hochkoenigin.com</a><br><br>
			<h3>Privacy policy for the use of ShareThis</h3><br>
			This website uses plugins of the ShareThis bookmarking service offered by ShareThis Inc. (&quot;ShareThis&quot;), 250 Cambridge Avenue, Palo Alto, CA 94306, USA. When accessing our website, ShareThis receives knowledge of your IP address and that you have visited the website of H&ouml;rl Thalerhof GmbH & Co KG with this IP address. ShareThis plug-ins allow users to bookmark webpages that are available on the Internet and share or post links to related social networking sites such as Twitter, Facebook, Xing, or Google&plus;. If a website visitor uses one of these functions and is also online with the corresponding service (for example, Twitter, Facebook or Google&plus;), the visit to our website is assigned to the respective user. Further information on the collection, analysis and processing of your data by ShareThis and related rights may be found in the ShareThis privacy policy at <a href="http://www.sharethis.com/legal/privacy" target="_blank">http://www.sharethis.com/legal/privacy</a>.
			<h3>Facebook Pixel</h3><br>
			Our website uses the remarketing function „Facebook Pixel” of Facebook Inc. (“Facebook”).<br/>
			The intention of this function is to present interest-related advertisements (“Facebook Ads”) to visitors of this website, as part of their visit of the social network Facebook. For this purpose, Facebook-Pixel has been implemented on our website. Facebook-Pixel establishes a direct connection to the Facebook servers when visiting the website. This information is transmitted to the Facebook server and Facebook assigns the information to your personal Facebook account. For more information about the collection and use of data through Facebook, as well as your rights and possibilities regarding your privacy, please have a look at the Privacy Statement of Facebook: <a href="https://www.facebook.com/about/privacy/" target="_blank">https://www.facebook.com/about/privacy/</a>.<br/>
			Alternatively, you can deactivate the remarking feature here: <a href="https://www.facebook.com/ads/preferences/?entry_product=ad_settings_screen#_=_" target="_blank">https://www.facebook.com/settings/?tab=ads#_=_</a>. In order to correctly deactivate this function, you must be logged in to Facebook.<br/>
		',
	],
	'gtc' => [
		'headline' => 'GTC',
		'headline_inner' => 'General terms and conditions',
		'text' => '
			<h3 class="hidden">General terms and conditions</h3>
			<a href="http&colon;//www.hotelverband.at/down/AGBH_061115.pdf" target="_blank" class="styled">Austrian Hotel Contract Conditions (&Ouml;HVB)</a><br/><br/>
			&lpar;Decided at the 93rd committee meeting of the Association of Hotel and Accommodation Companies on 23 September 1981&rpar;<br/><br/>
			<strong>Source&colon;</strong><br/>
			Owner, publisher and publisher&colon;<br/>
			Fachverband der Hotel- und Beherbergungsbetriebe, 1045 Vienna, Wiedner Hauptstra&szlig;e 63.<br/>
			Responsible for the content: Fachverband managing director Michael Raffling, 1045 Vienna, Wiedner Hauptstra&szlig;e 63.</br></br>
			<strong>&sect;1. General</strong></br>
			The &lpar;general&rpar; Austrian Hotel Contract Conditions &lpar;&Ouml;HVB&rpar; represent the contractual content according to which Austrian hoteliers usually conclude accommodation contracts with their guests. The Austrian Hotel Contract Conditions do not exclude special agreements.
			</br></br>
			<strong>&sect; 2 Contracting partner</strong></br>
			&lpar;1&rpar; In case of doubt, the purchaser of the accommodation provider is the contracting partner, even if they have ordered or co-ordered for other named persons.
			&lpar;2&rpar; The persons with the claim to accommodation are guests in the sense of the contract conditions.
			</br></br>
			<strong>&sect; 3 Contract conclusion, deposit</strong></br>
			&lpar;1&rpar; The accommodation contract is usually concluded by the acceptance of the guest’s written or verbal order by the accommodation provider.
			&lpar;2&rpar; It may be agreed that the guest makes a down payment.
			&lpar;3&rpar; The accommodation provider may also demand the advance payment of the entire agreed fee.
			</br></br>
			<strong>&sect; 4 Beginning and end of accommodation</strong></br>
			&lpar;1&rpar; The guest has the right to move into the rented rooms from 2 pm on the agreed day.
			&lpar;2&rpar; The accommodation provider has the right to withdraw from the contract in the event that the guest does not appear by 6 pm on the agreed arrival day, unless a later arrival time has been agreed.
			&lpar;3&rpar; If the guest has made a deposit, the room &lpar;s&rpar; will remain reserved until at the latest 12 pm the following day.
			&lpar;4&rpar; If a room is used for the first time before 6 am, the previous night counts as the first night.
			&lpar;5&rpar; The rented rooms are to be vacated by the guest on the day of departure by 12 pm.
			</br></br>
			<strong>&sect; 5 Withdrawal from the accommodation contract</strong></br>
			&lpar;1&rpar; Up to three months before the agreed date of arrival of the guest, the accommodation contract may be terminated without payment of a cancellation fee by both contracting partners by unilateral declaration. The cancellation must be in the hands of the contracting partner no later than three months before the agreed date of arrival of the guest.
			&lpar;2&rpar; At the latest one month before the agreed date of arrival of the guest the accommodation contract can be cancelled by both contracting partners by unilateral declaration; however a cancellation fee in the amount of the room price for three days is to be paid. The cancellation must be in the hands of the contracting partner no later than one month before the agreed date of arrival of the guest.
			&lpar;3&rpar; The accommodation provider has the right to withdraw from the contract in the event that the guest does not appear by 6 pm on the agreed arrival day, unless a later arrival time has been agreed. &lpar;4&rpar; If the guest has made a deposit, the room &lpar;s&rpar; will remain reserved until at the latest 12 pm the following day.
			&lpar;5&rpar; Even if the guest does not use the rooms ordered or the pension service, they are obliged to pay the agreed fee to the accommodation provider. However, the accommodation provider must deduct what it has saved as a result of non-use of its services or what it has received by renting the rooms in another manner. Experience has shown that in most cases, the savings of the operation due to the failure of performance is 20 percent of the room price and 30 percent of the meal price. &lpar;6&rpar; The accommodation provider is responsible, to seek to other rental of the unused rooms in accordance with the circumstances &lpar;&sect; 1107 ABGB&rpar;.
			</br></br>
			<strong>&sect; 6 Provision of substitute accommodation</strong></br>
			&lpar;1&rpar; The accommodation provider may provide the guest with adequate substitute accommodation if this is reasonable for the guest, in particular because the deviation is insignificant and objectively justified.
			&lpar;2&rpar; An objective justification is present, for example, if the room&lpar;s&rpar; have become unusable, guests already accommodated have extend their stay or other important operational measures necessitate this step.
			&lpar;3&rpar; Any additional expenditures for the substitute accommodation shall be at the expense of the accommodation provider.
			</br></br>
			<strong>&sect; 7 Rights of the guest</strong></br>
			&lpar;1&rpar; By concluding an accommodation contract, the guest acquires the right to the usual use of the rented rooms, the facilities of the tourist accommodation, which are usually accessible to the guests for use and without any special conditions, and the usual service.
			&lpar;2&rpar; The guest has the right to move into the rented rooms from 2 pm on the agreed day.
			&lpar;3&rpar; If full board or half board is agreed, the guest has the right to ask for adequate meals &lpar;lunch box&rpar; or a receipt for meals that they do not use, if they do so on time, that is by 6 pm of the previous day.
			&lpar;4&rpar; Otherwise, if the accommodation provider is willing to provide service, the guest will not be entitled to any compensation if they do not use the agreed meals within the usual time of day and in the rooms designated for this purpose.
			</br></br>
			<strong>&sect; 8 Obligations of the guest</strong></br>
			&lpar;1&rpar; Upon termination of the accommodation contract, the agreed fee is to be paid. Foreign currencies are accepted by the accommodation provider according to ability at the current rate in payment. The accommodation provider is not obliged to accept cashless means of payment such as checks, credit cards, vouchers and certificates. All costs necessary for the acceptance of these securities, such as for telegrams, inquiries, etc., shall be borne by the guest.
			&lpar;2&rpar; If food or drinks are available at the accommodation establishment, but are instead brought there and consumed in public areas, the accommodation provider is entitled to charge reasonable compensation &lpar;so-called &ldquo;corkage charge&rdquo; for drinks&rpar;.
			&lpar;3&rpar; Prior to commissioning of electrical appliances, which are brought by the guests and which are not part of the usual travel requirements, the consent of the accommodation provider must be obtained.
			&lpar;4&rpar; The damage caused by the guest is subject to the provisions of the law on damages. Therefore, the guest is liable for any damage or disadvantage suffered by the accommodation provider or third parties due to the guest’s fault or the fault of their companions or other persons for which they are responsible, even if the injured party is entitled to claim compensation directly from the accommodation provider.
			</br></br>
			<strong>&sect; 9 Rights of the accommodation provider</strong></br>
			&lpar;1&rpar; If the guest refuses to pay the compensation or if they are in arrears, the owner of the accommodation establishment has the right to retain the items brought in order to secure its claims arising from the accommodation and meals as well as its expenses for the guest. &lpar;&sect; 970 c ABGB legal right of retention.&rpar;
			&lpar;2&rpar; The accommodation provider has a lien right to the items brought in by the guest to secure the agreed remuneration. &lpar;&sect; 1101 ABGB legal lien of the accommodation provider.&rpar;
			&lpar;3&rpar; If the service is requested in the guest’s room or at extraordinary times of day, the accommodation provider is entitled to demand a special fee for this; however, this special fee is to be marked on the room price list. It can also refuse these services for operational reasons.
			</br></br>
			<strong>&sect; 10 Obligations of the accommodation provider</strong></br>
			&lpar;1&rpar; The accommodation provider is obliged to provide the agreed services in a standard scope.
			&lpar;2&rpar; Special accommodation to be provided by the accommodation provider which is not included in the accommodation charge: a&rpar; accommodation, which may be charged separately, such as the provision of salons, sauna and indoor pool, swimming pool, solarium, storey bath, garaging, etc.; b&rpar; a reduced price will be charged for the provision of extra beds or children’s cots.
			&lpar;3&rpar; The announced prices have to be inclusive prices.
			</br></br>
			<strong>&sect; 11 Liability of the accommodation provider for damages</strong></br>
			&lpar;1&rpar; The accommodation provider is liable for any damage suffered by a guest, if the damage occurred within the framework of the establishment and if its employees are at fault.
			&lpar;2&rpar; Liability for brought-in items. In addition, the accommodation provider is liable as the custodian for the objects brought in by the accommodated guests up to a maximum amount of EUR 1,100.00, unless it proves that the damage was neither caused by it or one of its employees nor by outside third parties entering and exiting the facilities. Under these circumstances, the accommodation provider is liable for valuables, money and securities up to a maximum of EUR 550.00; unless it has taken over these things in custody in knowledge of their nature or the damage was caused by itself or its employees and it therefore has unlimited liability. A rejection of liability due to attack is legally without effect. The safekeeping of valuables, money and securities can be denied if they are much more valuable items than guests of the holding in question usually leave in custody. Agreements under which liability is to be reduced below the level specified in the above paragraphs are ineffective. Things shall then be deemed to have been brought in if they are taken over by a person who is employed by the accommodation establishment or taken to a place designated by the accommodation provider. &lpar;In particular, &sect;&sect; 970 ff. ABGB.&rpar;
			</br></br>
			<strong>&sect; 12 Keeping of animals</strong></br>
			&lpar;1&rpar; Animals may only be brought to the accommodation after prior authorisation and, if necessary, against special remuneration. Animals must not stay in the salons, social and restaurant rooms.
			&lpar;2&rpar; The guest is liable for the damage done by accompanying animals, in accordance with the statutory provisions applicable to the pet owner &lpar;&sect; 1320 ABGB&rpar;.
			</br></br>
			<strong>&sect; 13 Extension of accommodation</strong></br>
			An extension of the stay by the guest requires the consent of the accommodation provider.
			</br></br>
			<strong>&sect; 14 Termination of accommodation</strong></br>
			&lpar;1&rpar; If the accommodation contract has been agreed upon for a certain period of time, it will end with the passage of this time. If the guest leaves prematurely, the accommodation provider is entitled to demand the full agreed fee. However, it is the responsibility of the accommodation provider to make an effort to rent the unused spaces in another manner in accordance with the circumstances. The provision in &sect; 5 &lpar;5&rpar; applies mutatis mutandis &lpar;deduction percentages&rpar; otherwise.
			&lpar;2&rpar; The death of a guest ends the contract with the accommodation provider.
			&lpar;3&rpar; If the accommodation contract has been concluded for an indefinite period, the contracting partners can cancel the contract at any time subject to a notice period of three days. The termination must reach the contracting partner before 10 am; otherwise, this day is not considered the first day of the notice period, but only the following day.
			&lpar;4&rpar; If the guest does not vacate their room by 12 pm, the accommodation provider is entitled to charge the room price for one more day. &lpar;5&rpar; The accommodation provider shall be entitled to dissolve the accommodation contract with immediate effect if the guest a&rpar; makes a materially disadvantageous use of the premises or through their reckless, offensive or otherwise grossly improper behaviour offends the other guests or the accommodation provider or is guilty of a punishable offence against property, morality or physical safety to the people or to a person staying in the accommodation; &lpar;b&rpar; is infected or is affected by or in need of care for an illness extending beyond the accommodation period; &lpar;c&rpar; fails to pay the invoice submitted to them on request within a reasonable period.
			</br></br>
			<strong>&sect; 15 Illness or death of the guest in the accommodation</strong></br>
			&lpar;1&rpar; If a guest falls ill during their stay at the accommodation, the accommodation provider has the obligation to provide medical care if this is necessary and the guest is unable to do so themselves. The accommodation provider has the following claim for compensation against the guest or, in the case of death, against their legal successor: a&rpar; any compensation from the guest for unpaid medical expenses; b&rpar; the required room disinfection, if ordered by the medical officer; c&rpar; replacement of any unusable laundry, bed linen and bed furniture, against the delivery of these items to the legal successor, otherwise for the disinfection or thorough cleaning of all these items; d&rpar; the restoration of walls, furnishings, carpets, etc., as far as they have been contaminated or damaged in connection with the illness or death; e&rpar; the room rent, as far as the room temporarily cannot be rented in connection with the illness or the death &lpar;at least three, at most seven days&rpar;.
			</br></br>
			<strong>&sect; 16 Place of performance and place of jurisdiction</strong></br>
			&lpar;1&rpar; The place of performance is the place where the tourist accommodation is located.
			&lpar;2&rpar; For all disputes arising from the accommodation contract, the factual and local court for the accommodation establishment is agreed upon, unless a&rpar; the guest as a consumer has employment or a residence located in the country; in this case the place of jurisdiction is the place announced by the guest in the registration; b&rpar; the guest as consumer has only one domestic place of employment; In this case, this is agreed as the place of jurisdiction. The cancellation fees listed in &sect; 5 numbers 1, 2 and 5 are entered in the cartel register, number 1 Kt 617 / 91-5, in accordance with &sect; 31 in conjunction with &sect; 32 of the Cartel Act as a non-binding association recommendation.
		',
	],
);
?>
