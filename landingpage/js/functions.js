//ON DOCUMENT READY
$(document).ready(function(){

    initdatepicker();
    initchildrenaction();

    // $('body').removeClass('loading');

    $(document).on('click', '.request-button', function(){
      $(this).hide();
      $(this).parents('#request').find('.request-content').slideToggle();
    });


    $('.bxSlider').bxSlider({
        randomStart: true,
        adaptiveHeight: true,
        responsive: true,
        keyboardEnabled: true,
        autoHover: true,
    });

    $.validate({
      lang: 'de',
      validateOnEvent : true
    });

    if(window.location.hash == '#requestform'){
      setTimeout(function(){
        $.smoothScroll({
          scrollElement: $('html,body'),
          scrollTarget: $('#request'),
          easing: 'swing',
          speed: 1000,
          offset: -100
        });
        $('.request-button').click();
      }, 1000);
    }
});


function selecotraction(e){
	var elementCount = $('.rooms-wrap .room-wrap').length;

	//add/remove room
    if($(e).hasClass('room-add')){
    	//add
    	var cloned = $('.rooms-wrap .room-wrap:first').clone(true);
    	cloned.addClass('fadedout');
    	cloned.find('input, select').val('');
    	cloned.find('.age').parents('.input').remove();
    	cloned.insertAfter($('.rooms-wrap .room-wrap:last'));
    	setTimeout(function() {
    		$('.rooms-wrap .room-wrap.fadedout').removeClass('fadedout');
        }, 10);
        elementCount++;
    } else if(!$(e).hasClass('room-nodelete')){
		//remove
		$(e).parents('.room-wrap').addClass('fadedout');
		setTimeout(function() {
			$(e).parents('.room-wrap').remove();
        }, 300);
        elementCount--;
    }

    //reorder
    setTimeout(function() {
	    $('.rooms-wrap .room-wrap').each(function(k,v){
	        var _name = 'rooms[' + k + ']';
	        // change nr
	        $(this).attr('data-room-key', k);
	        $(this).attr('id', 'room-' + k);
	        $(this).find('select.room-select').attr('id', 'rooms-' + k + '-room').attr('name', _name + '[room]');
	        $(this).find('select.package-select').attr('id', 'rooms-' + k + '-package').attr('name', _name + '[package]');
	        $(this).find('input.room-adults').attr('id', 'rooms-' + k + '-adults').attr('name', _name + '[adults]');
	        $(this).find('input.room-children').attr('id', 'rooms-' + k + '-children').attr('name', _name + '[children]');
	        $(this).find('input.age').each(function(k,v){
	       		$(this).attr('id', 'rooms-' + k + '-ages-' + k + '-age').attr('name', _name + '[ages][' + k + '][age]');
	        });
	    });

	    //show/hide delete button
	    if(elementCount <= 1){
	    	$('.room-wrap').find('.room-remove').addClass('room-nodelete');
	    } else{
	    	$('.room-wrap').find('.room-remove').removeClass('room-nodelete');
	    }
    }, 300);
}


function initchildrenaction(){

    $('input.room-children').keyup(function(event){
        childrenageaction(this);
    });

    $('input.room-children').change(function(event){
        childrenageaction(this);
    });

}

function childrenageaction(e){
	var room = $(e).parents('.room-wrap');
	var roomKey = room.attr('data-room-key');
	var childCount = $(e).val();
    var childAgeInputsCount = room.find('input.age').length;//size();

	//check childCount
	if(isNaN(childCount)){
        childCount = 0;
        $(e).val(0);
    }else if(childCount > 4){
        childCount = 4;
        $(e).val(4);
    }
    //check childAgeInputsCount
    if(isNaN(childAgeInputsCount) || typeof(childAgeInputsCount) == 'undefined'){
      childAgeInputsCount = 0;
    }

    if(childCount > childAgeInputsCount){

    	for(var x = childAgeInputsCount; x < childCount; x++){
            var __clone = $($(e).parent('div.input')).clone();
            __clone.addClass('room-child-age').addClass('fadedout');

            var _name = 'rooms[' + roomKey + '][ages][' + x + '][age]';

            // change nr
            $(__clone).find('label').text(__translations.childage).attr('for', 'room-' + roomKey + '-age-' + x);
            $(__clone).find('input').attr('id', 'room-' + roomKey + '-age-' + x);
            $(__clone).find('input').attr('name', _name);
            $(__clone).find('input').val('');
            $(__clone).find('input').removeClass('children');
            $(__clone).find('input').addClass('age');
            $(__clone).find('input').attr('placeholder', __translations.childage);

            // add
            $(e).parents('div.room-col').append(__clone);
            setTimeout(function() {
	    		$('.rooms-wrap .room-wrap .room-child-age.fadedout').removeClass('fadedout');
	        }, 1);
        }

    } else{
        for(var x = childAgeInputsCount; x >= childCount; x--){
    		room.find('#room-' + roomKey + '-age-' + x).parent('div.input.fadedout').addClass('fadedout');
        	room.find('#room-' + roomKey + '-age-' + x).parent('div.input').remove();
        }
    }
}

function initdatepicker(){
    $('input.date').each(function(k,v){
        if($(this).hasClass('picker__input') === false){

            // init
            var __min = $(this).data('date-min') ? new Date($(this).data('date-min')) : undefined;
            var __max = $(this).data('date-max') ? new Date($(this).data('date-max')) : undefined;
            var __range = $(this).data('date-range');
            var __years = $(this).data('date-years') == false ? false : true;
            var __months = $(this).data('date-months') == false ? false : true;
            var __format = $(this).data('date-format') != undefined ? $(this).data('date-format') : 'dd.mm.yyyy';
            var __hidden = $(this).data('date-hidden') != undefined ? $(this).data('date-hidden') : 'yyyy-mm-dd';
            var __callback = $(this).data('date-callback') != undefined ? $(this).data('date-callback') : false;

            // classes
            var __class_year = 'picker__year';
            var __class_today = 'picker__button--today';

            $(this).data('value', $(this).val());
            var $input = $(this).pickadate({
                selectYears: __years,
                selectMonths: __months,
                container: '#datepicker-container',
                format: __format,
                formatSubmit: __hidden,
                hiddenName: true,
                closeOnSelect: true,
                closeOnClear: false,
                max: __max,
                min: __min,
                klass: {
                    buttonToday: __class_today,
                    year: __class_year
                }
            });

            if(__range){
                var __clicked = $(this).hasClass('date-from') ? 'from' : 'to';
                var __opposite = __clicked == 'from' ? 'to' : 'from';
                $input.pickadate('picker').on('set', function(event) {
                    if ( event.select ) {
                        var __select = $('input.date.date-' + __clicked + '[data-date-range="' + __range + '"]').pickadate('picker').get('select');
                        var __related = $('input.date.date-' + __opposite + '[data-date-range="' + __range + '"]').pickadate('picker').get('select');
                        if(__opposite == 'to'){
                            __select.obj.setDate(__select.obj.getDate() + 1);
                            $('input.date.date-' + __opposite + '[data-date-range="' + __range + '"]').pickadate('picker').set('min', __select);
                            if(__related && __related.pick <= __select.pick){
                                $('input.date.date-' + __opposite + '[data-date-range="' + __range + '"]').pickadate('picker').set('select', __select);
                            }
                        }else{
                            __select.obj.setDate(__select.obj.getDate() - 1);
                            $('input.date.date-' + __opposite + '[data-date-range="' + __range + '"]').pickadate('picker').set('max', __select);
                            if(__related && __related.pick >= __select.pick){
                                $('input.date.date-' + __opposite + '[data-date-range="' + __range + '"]').pickadate('picker').set('select', __select);
                            }
                        }
                    }else if('clear' in event){
                        if(__opposite == 'from'){
                            $('input.date.date-' + __opposite + '[data-date-range="' + __range + '"]').pickadate('picker').set('max', false);
                        }else{
                            $('input.date.date-' + __opposite + '[data-date-range="' + __range + '"]').pickadate('picker').set('min', false);
                        }
                    }
                });
            }

            if (__callback == 'validateFlatrate') {
                $input.pickadate('picker').on('set', function(event) {
                    validateFlatrate();
                });
            }

        }
    });
}
