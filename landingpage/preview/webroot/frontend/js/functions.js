var __header;
var __scrolling = false;
var __done = false;



$(document).ready(function(){

    toggleNavigation();
    toggleInfoBox();
    acordionAnimation();
    backToTop();
    footerLanguageAnim();
    toggleFormFields();
    hideBottomMenu();
    switchLanguages();
    jump();
    // loadingAnimation();
    // tabletMenu();
    // mobileMenu();
    initdatepicker();
    initchildrenaction();
    privacyPolicy();
    bxSliders();
    toggleShareButtons();
    quickForm();
    videoJS();
    focusInForms();
    toggleJobsForm();
    toggleBrochureForm();
    toggleRoomInfoBox();

    // css variables support for IE
    cssVars({
      onlyVars: true
    });

    //copy to clipboard
	$(".share-btn-clipboard").on("click",  function(event) {
    	CopyLink();
    });

});

$(window).scroll(function() {

    elementScrolled();
    countUp();

    // menu toggle on scroll
    if ($(window).scrollTop() <= 0 && $("#header").length) {
        if (!$('div#recent-packages').hasClass('invisible')) {
            $('div#recent-packages').toggleClass('invisible');
        }
    } else {
        if ($('div#recent-packages').hasClass('invisible')) {
            $('div#recent-packages').toggleClass('invisible');
        }
    }


});


// FUNCTIONS

function elementScrolled(elem) {
    if (typeof(elem) != "undefined" && elem !== null) {
        var docViewTop = $(window).scrollTop();
        var docViewBottom = docViewTop + $(window).height();
        var elemTop = $(elem).offset().top;
        return ((elemTop <= docViewBottom) && (elemTop >= docViewTop));
    }
}

function scrollToContent(){
    if(__scrolling === false){
        content_position = $('a.anchor[name=content]').offset().top;
        if($(window).scrollTop() < content_position){
            __scrolling = true;
            $.smoothScroll({
                scrollElement: $('html,body'),
                scrollTarget: $('a.anchor[name=content]'),
		        easing: 'swing',
		        speed: 1000,
		        offset: -90,
                afterScroll: function() {
                    __scrolling = false;
                }
            });
        }
    }
}

function scrollToTop(){
    $.smoothScroll({
        scrollElement: $('html,body'),
        scrollTarget: $('#wrapper'),
        easing: 'swing',
        speed: 1000,
        offset: 0
    });
}

function onTop(){
    if($(this).scrollTop() < 50){
        $('body').addClass('ontop');
    }else{
        $('body').removeClass('ontop');
    }
}

function initdatepicker(){
    $('input.date').each(function(k,v){
        if($(this).hasClass('picker__input') === false){

            // init
            var __min = $(this).data('date-min') ? new Date($(this).data('date-min')) : undefined;
            var __max = $(this).data('date-max') ? new Date($(this).data('date-max')) : undefined;
            var __range = $(this).data('date-range');
            var __years = $(this).data('date-years') == false ? false : true;
            var __months = $(this).data('date-months') == false ? false : true;
            var __format = $(this).data('date-format') != undefined ? $(this).data('date-format') : 'dd.mm.yyyy';
            var __hidden = $(this).data('date-hidden') != undefined ? $(this).data('date-hidden') : 'yyyy-mm-dd';
            var __callback = $(this).data('date-callback') != undefined ? $(this).data('date-callback') : false;

            // classes
            var __class_year = 'picker__year';
            var __class_today = 'picker__button--today';

            $(this).data('value', $(this).val());
            var $input = $(this).pickadate({
                selectYears: __years,
                selectMonths: __months,
                container: '#datepicker-container',
                format: __format,
                formatSubmit: __hidden,
                hiddenName: true,
                closeOnSelect: true,
                closeOnClear: false,
                max: __max,
                min: __min,
                klass: {
                    buttonToday: __class_today,
                    year: __class_year
                }
            });

            if(__range){
                var __clicked = $(this).hasClass('date-from') ? 'from' : 'to';
                var __opposite = __clicked == 'from' ? 'to' : 'from';
                $input.pickadate('picker').on('set', function(event) {
                    if ( event.select ) {
                        var __select = $('input.date.date-' + __clicked + '[data-date-range="' + __range + '"]').pickadate('picker').get('select');
                        var __related = $('input.date.date-' + __opposite + '[data-date-range="' + __range + '"]').pickadate('picker').get('select');
                        if(__opposite == 'to'){
                            __select.obj.setDate(__select.obj.getDate() + 1);
                            $('input.date.date-' + __opposite + '[data-date-range="' + __range + '"]').pickadate('picker').set('min', __select);
                            if(__related && __related.pick <= __select.pick){
                                $('input.date.date-' + __opposite + '[data-date-range="' + __range + '"]').pickadate('picker').set('select', __select);
                            }
                        }else{
                            __select.obj.setDate(__select.obj.getDate() - 1);
                            $('input.date.date-' + __opposite + '[data-date-range="' + __range + '"]').pickadate('picker').set('max', __select);
                            if(__related && __related.pick >= __select.pick){
                                $('input.date.date-' + __opposite + '[data-date-range="' + __range + '"]').pickadate('picker').set('select', __select);
                            }
                        }
                    }else if('clear' in event){
                        if(__opposite == 'from'){
                            $('input.date.date-' + __opposite + '[data-date-range="' + __range + '"]').pickadate('picker').set('max', false);
                        }else{
                            $('input.date.date-' + __opposite + '[data-date-range="' + __range + '"]').pickadate('picker').set('min', false);
                        }
                    }
                });
            }

            if (__callback == 'validateFlatrate') {
                $input.pickadate('picker').on('set', function(event) {
                    validateFlatrate();
                });
            }

        }
    });
}

function selecotraction(e){
	var elementCount = $('.rooms-wrap .room-wrap').length;

	//add/remove room
    if($(e).hasClass('room-add')){
    	//add
    	var cloned = $('.rooms-wrap .room-wrap:first').clone(true);
    	cloned.addClass('fadedout');
    	cloned.find('input, select').val('');
    	cloned.find('.age').parents('.input').remove();
    	cloned.insertAfter($('.rooms-wrap .room-wrap:last'));
    	setTimeout(function() {
    		$('.rooms-wrap .room-wrap.fadedout').removeClass('fadedout');
        }, 10);
        elementCount++;
    } else if(!$(e).hasClass('room-nodelete')){
		//remove
		$(e).parents('.room-wrap').addClass('fadedout');
		setTimeout(function() {
			$(e).parents('.room-wrap').remove();
        }, 300);
        elementCount--;
    }

    //reorder
    setTimeout(function() {
	    $('.rooms-wrap .room-wrap').each(function(k,v){
	        var _name = 'rooms[' + k + ']';
	        // change nr
	        $(this).attr('data-room-key', k);
	        $(this).attr('id', 'room-' + k);
	        $(this).find('select.room-select').attr('id', 'rooms-' + k + '-room').attr('name', _name + '[room]');
	        $(this).find('select.package-select').attr('id', 'rooms-' + k + '-package').attr('name', _name + '[package]');
	        $(this).find('input.room-adults').attr('id', 'rooms-' + k + '-adults').attr('name', _name + '[adults]');
	        $(this).find('input.room-children').attr('id', 'rooms-' + k + '-children').attr('name', _name + '[children]');
	        $(this).find('input.age').each(function(k,v){
	       		$(this).attr('id', 'rooms-' + k + '-ages-' + k + '-age').attr('name', _name + '[ages][' + k + '][age]');
	        });
	    });

	    //show/hide delete button
	    if(elementCount <= 1){
	    	$('.rooms-wrap .room-wrap').find('.room-remove').addClass('room-nodelete');
	    } else{
	    	$('.rooms-wrap .room-wrap').find('.room-remove').removeClass('room-nodelete');
	    }
    }, 300);
}

function initchildrenaction(){

    $('input.room-children').keyup(function(event){
        childrenageaction(this);
    });

    $('input.room-children').change(function(event){
        childrenageaction(this);
    });

}

function childrenageaction(e){
	var room = $(e).parents('.room-wrap');
	var roomKey = room.attr('data-room-key');
	var childCount = $(e).val();
    var childAgeInputsCount = room.find('input.age').size();

	//check childCount
	if(isNaN(childCount)){
        childCount = 0;
        $(e).val(0);
    }else if(childCount > 4){
        childCount = 4;
        $(e).val(4);
    }

    if(childCount > childAgeInputsCount){

    	for(var x = childAgeInputsCount; x < childCount; x++){
            var __clone = $($(e).parent('div.input')).clone();
            __clone.addClass('room-child-age').addClass('fadedout');

            var _name = 'rooms[' + roomKey + '][ages][' + x + '][age]';

            // change nr
            $(__clone).find('label').text(__translations.childage).attr('for', 'room-' + roomKey + '-age-' + x);
            $(__clone).find('input').attr('id', 'room-' + roomKey + '-age-' + x);
            $(__clone).find('input').attr('name', _name);
            $(__clone).find('input').attr('max', 18);
            $(__clone).find('input').val('');
            $(__clone).find('input').removeClass('children');
            $(__clone).find('input').addClass('age');
            $(__clone).find('input').attr('placeholder', __translations.childage);


            // add
            $(e).parents('div.room-col').append(__clone);
            setTimeout(function() {
	    		$('.rooms-wrap .room-wrap .room-child-age.fadedout').removeClass('fadedout');
	        }, 1);
        }

    } else{
        for(var x = childAgeInputsCount; x >= childCount; x--){
    		room.find('#room-' + roomKey + '-age-' + x).parent('div.input.fadedout').addClass('fadedout');
        	room.find('#room-' + roomKey + '-age-' + x).parent('div.input').remove();
        }
    }
}

function copyTextToClipboard(text) {
	var textArea = document.createElement("textarea");

	// Place in top-left corner of screen regardless of scroll position.
	textArea.style.position = 'fixed';
	textArea.style.top = 0;
	textArea.style.left = 0;

	// Ensure it has a small width and height. Setting to 1px / 1em
	// doesn't work as this gives a negative w/h on some browsers.
	textArea.style.width = '2em';
	textArea.style.height = '2em';

	// We don't need padding, reducing the size if it does flash render.
	textArea.style.padding = 0;

	// Clean up any borders.
	textArea.style.border = 'none';
	textArea.style.outline = 'none';
	textArea.style.boxShadow = 'none';

	// Avoid flash of white box if rendered for any reason.
	textArea.style.background = 'transparent';

	textArea.value = text;
	document.body.appendChild(textArea);
	textArea.select();

	try {
		var successful = document.execCommand('copy');
	var msg = successful ? 'successful' : 'unsuccessful';
		// console.log('Copying text command was ' + msg);
	} catch (err) {
		// console.log('Oops, unable to copy');
	}
	document.body.removeChild(textArea);
}

function CopyLink() {
	copyTextToClipboard(location.href);
}

function validateFlatrate(){
    var __from = $('input.date.date-from[data-date-range="request"]').pickadate('picker').get('select');
    var __to = $('input.date.date-to[data-date-range="request"]').pickadate('picker').get('select');
    if(__from !== null && __to !== null){
        __from = __from.pick/1000;
        __to = __to.pick/1000;
        $('.rooms-wrap select.package-select').each(function(k,v){
            if(flatrateRanges[$(this).val()] !== undefined){
                var __valid = false;
                $.each(flatrateRanges[$(this).val()], function(k,v){
                    if(v.from <= __from && __to <= v.to){
                        __valid = true;
                    }
                });
                if(__valid === false){
                    alert(__translations['package'].replace(/%s/g, $(this).find('option:selected').text()));
                    $(this).val('');
                }
            }
        });
    }
}

function toggleNavigation() {
	$('a#menu-open').on('click',  function(event) {
        event.preventDefault();
    });
}

function toggleInfoBox() {
    $('.tabs-wrapper .buttons > a').on('click', function(){
        event.preventDefault();
        var rel = $(this).attr('rel');
        $(this).parents('.tabs-wrapper').find('.buttons > a').removeClass('active');
        $(this).parents('.tabs-wrapper').find('.content > .item').removeClass('active').addClass('hidden');
        $(this).parents('.tabs-wrapper').find('.link > .link-btn').removeClass('active').addClass('hidden');
        $(this).parents('.tabs-wrapper').find('.buttons > a[rel="' + rel + '"]').addClass('active');
        $(this).parents('.tabs-wrapper').find('.content > .item[rel="' + rel + '"]').addClass('active').removeClass('hidden');
        $(this).parents('.tabs-wrapper').find('.link > .link-btn[rel="' + rel + '"]').addClass('active').removeClass('hidden');
    });
}

function acordionAnimation() {
    $(".flex-slide").mouseenter(function(){
        $(".flex-slide").each(function(){
            $(this).removeClass('active');
        });
        $(this).toggleClass('active');
    });
}

function backToTop() {
    $(window).scroll(function() {
        if($(window).scrollTop() + $(window).height() == $(document).height()) {
            $('.scroll-to-top-btn').fadeIn(200);
        } else {
            $('.scroll-to-top-btn').fadeOut(200);
        }
    });
    $('.scroll-to-top-btn').click(function() {
        $('body,html').animate({
            scrollTop : 0
        }, 500);
    });
}

function footerLanguageAnim() {
    $(window).scroll(function() {
        if($(window).scrollTop() + $(window).height() == $(document).height()) {
            $('#foot-lang').fadeIn(200);
        } else {
            $('#foot-lang').fadeOut(200);
        }
    });
    $('#foot-lang').click(function() {
        $('body,html').animate({
            scrollTop : 0
        }, 500);
    });
}

function toggleRoomInfoBox() {
    $('.package-price-buttons').click(function(){
        event.preventDefault();
        var id = $(this).attr("data-id");

        $('#line-'+id).toggleClass('invisible visible');
        if (!$('[data-id="'+id+'"]').hasClass('clicked')) {
            $('[data-id="'+id+'"] svg').css({
                "transform": "rotate(180deg)",
                "-ms-transform": "rotate(180deg)",
                "-moz-transform": "rotate(180deg)",
                "-webkit-transform": "rotate(180deg)",
                "-o-transform": "rotate(180deg)",
                "-webkit-transition": "all 1s ease",
                "-moz-transition": "all 1s ease",
                "-o-transition": "all 1s ease",
                "transition": "all 1s ease",
            });
            $('[data-id="'+id+'"]').addClass('clicked');
        } else {
            $('[data-id="'+id+'"] svg').css({
                "transform": "rotate(0deg)",
                "-ms-transform": "rotate(0deg)",
                "-moz-transform": "rotate(0deg)",
                "-webkit-transform": "rotate(0deg)",
                "-o-transform": "rotate(360deg)",
            });
            $('[data-id="'+id+'"]').removeClass('clicked');
        }

        if ($('#box-'+id).hasClass('hidden')){
            $('#box-'+id).removeClass('hidden');
            $('#box-'+id+' td').toggleClass('invisible visible');
        } else {
            $('#box-'+id+' td').toggleClass('invisible visible');
            setTimeout(function() {
                $('#box-'+id).addClass('hidden');
            }, 250);
        }

    });
}

function toggleJobsForm() {
    $('#open-jobs-form').click(function(){
        $('#jobs-wrapper, .hide-toggle-2').toggleClass('hidden');
    });
}

function toggleBrochureForm() {
    $('#open-brochure-form').click(function(){
        $('#brochure-wrapper, .hide-toggle-3').toggleClass('hidden');
    });
}

function toggleFormFields() {
    $('#unfold-form-btn').click(function(){
        $('#form-field-fold>div, .hide-toggle').toggleClass('hidden');
        $(this).find('.svg-inline--fa').toggleClass('fa-plus').toggleClass('fa-minus');
    });
}

function hideBottomMenu() {
    // hide bottom menu if there is no header
    if (!$("#header").length && $('div#recent-packages').hasClass('invisible')) {
        $('div#recent-packages').removeClass('invisible');
    }
}

function switchLanguages() {
    $('div.languages > span').click(function(event){
        event.preventDefault();
        $(this).parent('div').toggleClass('open');
    });
}

function jump() {
    $('a.j2c').click(function(event){
        event.preventDefault();
        scrollToContent(false);
    });

    $('a.j2t').click(function(event){
        event.preventDefault();
        scrollToTop();
    });
}

function loadingAnimation() {
    //fadeout loading-animation
	var fadeoutTime = docRoute === homeRoute ? 300 : 0;
	setTimeout(function() {
		$('body').addClass('fully-loaded');
		setTimeout(function() {
			$('.loading-overlay').remove();
		}, 1000);
    }, fadeoutTime);
}

function tabletMenu() {
    $('nav a.m').click(function(event){
        var type = $(this).parents('nav').hasClass('mobile') ? 'mobile' : 'desktop';
        var state = $(this).parent('li').hasClass('open') ? 'open' : 'close';
        var drop = $(this).parent('li').hasClass('children-0') ? false : true;
        if(type == 'mobile'){
            event.preventDefault();
            if(state == 'close'){
                $(this).parent('li').addClass('open');
                $(this).nextAll('ul').animate({
                    maxHeight: '1000px'
                }, 1000, function() {
                    $(this).css('max-height', $(this).css('height'));
                });
            }else{
                $(this).parent('li').removeClass('open');
                $(this).nextAll('ul').animate({
                    maxHeight: 0
                }, 300, function() { });
            }
        }else{
            if(drop === true){
                event.preventDefault();
            }
            $('nav > ul > li').removeClass('open');
            if(state == 'close'){
                $(this).parent('li').addClass('open');
            }
        }
    });
}

function mobileMenu() {
    $('.mobile-menu > button').click(function(event){
        if($('nav.mobile').height() == 0){
            $('button.hamburger').addClass('is-active');
            $('nav.mobile').animate({
                maxHeight: $('nav.mobile > ul').css('height')
            }, 300, function() {
                $('nav.mobile').css('max-height', 'none');
            });
        }else{
            $('button.hamburger').removeClass('is-active');
            $('nav.mobile').css('max-height', $('nav.mobile').css('height'));
            $('nav.mobile').animate({
                maxHeight: 0
            }, 300, function() { });
        }
    });
}

function privacyPolicy() {
    $('a.cookie-hint-button').click(function(event){
        event.preventDefault();
        $('#cookie-hint').remove();
        $('body').removeClass('cookie-hint');

        var d = new Date();
        d.setTime(d.getTime() + (365*24*60*60*1000));
        var expires = "expires="+ d.toUTCString();
        document.cookie = "hint=false; " + expires + "; path=/";

    });
}

function bxSliders() {

    // header
    __header = $('header .bxslider').bxSlider({
        auto: $('header .bxslider .bxslide').length > 1 ? true : false,
        pager: false,
        controls: true,
        speed: 800,
        pause: 8000,
        nextText: '<img src="/frontend/img/uploaded/icons/Icons_Rechts-lang.svg" class="cal-icon icon">',
        prevText: '<img src="/frontend/img/uploaded/icons/Icons_Links-lang.svg" class="cal-icon icon">'
    });

    // impressions
    $('div.impressions .bxslider').bxSlider({
        auto: $('div.impressions .bxslider .bxslide').length > 1 ? true : false,
        pager: false,
        controls: true,
        randomStart: true,
        speed: 800,
        pause: 8000,
        nextText: '<img src="/frontend/img/uploaded/icons/Icons_Rechts-lang.svg" class="cal-icon icon invert">',
        prevText: '<img src="/frontend/img/uploaded/icons/Icons_Links-lang.svg" class="cal-icon icon invert">'
    });

    // gallery
    $('section.gallery .bxslider').bxSlider({
        auto: true,
        pager: true,
        controls: true,
        randomStart: true,
        speed: 800,
        pause: 8000,
        nextText: '<img src="/frontend/img/uploaded/icons/Icons_Rechts-lang.svg" class="cal-icon icon invert">',
        prevText: '<img src="/frontend/img/uploaded/icons/Icons_Links-lang.svg" class="cal-icon icon invert">',
        pagerCustom: '#bx-gallery-pager'
    });

    // pool
    $('section.pool .bxslider').each(function(k,v){
        $(this).bxSlider({
            auto: false,
            pager: false,
            controls: true,
            speed: 800,
            pause: 8000,
            randomStart: true,
            slideWidth: 336,
            maxSlides: 5,
            minSlides: 1,
            moveSlides: 3,
            nextText: '<img src="/frontend/img/uploaded/icons/Icons_Rechts-lang.svg" class="cal-icon icon invert">',
            prevText: '<img src="/frontend/img/uploaded/icons/Icons_Links-lang.svg" class="cal-icon icon invert">'
        });
    });

    // menu
    $('.menu-packages-slider.bxslider').each(function(k,v){
        $(this).bxSlider({
            auto: false,
            pager: true,
            controls: true,
            speed: 800,
            pause: 8000,
            pagerCustom: '.bx-pool-pager-' + $(this).parent('section.pool').data('pool-id'),
            nextText: '<img src="/frontend/img/uploaded/icons/Icons_Rechts-mittel.svg" class="cal-icon icon">',
            prevText: '<img src="/frontend/img/uploaded/icons/Icons_Links-mittel.svg" class="cal-icon icon">'
        });
    });
}

function toggleShareButtons() {
    $(".share-btns-toggle").on("click",  function(event) {
        $('.share-btns-wrap').addClass('open');
    });
    $(".share-btns-darken, .share-btns-close").on("click",  function(event) {
        $('.share-btns-wrap').removeClass('open');
    });
}

function quickForm() {
    $('section.top div.right span.button.request a').click(function(event){
        var windowWidth = $(window).width();
        if(windowWidth > 1024) {
            event.preventDefault();
            $(this).parent('span').toggleClass('open');
        }
    });
}

function videoJS() {
    $('video.video-js').each(function(){
		videojs($(this).attr('id'), {}, function(){
		  // Player (this) is initialized and ready.
		});
	});
}

function focusInForms() {
    $(".input > *").focus(function(){
		$(this).parents('.input').addClass("focused");
	}).blur(function(){
    	$(this).parents('.input').removeClass("focused");
    });
}

function countUp() {
    if (elementScrolled('.countUp') && __done == false) {
        $('#countUp1, #countUp2, #countUp3').data('countToOptions', {
            formatter: function (value, options) {
              return value.toFixed(options.decimals).replace(/\B(?=(?:\d{3})+(?!\d))/g, ',');
            }
        });
        $('.countUp').each(count);
        __done = true;
    }
}

function count(options) {
    var $this = $(this);
    options = $.extend({}, options || {}, $this.data('countToOptions') || {});
    $this.countTo(options);
}
