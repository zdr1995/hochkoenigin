<?php

$_setup = [

    // domains
    'plugin_i18n' => [
        'Backend' => ['name' => __d('be', 'Backend'), 'domain' => 'be', 'list' => 'languages', 'fetch' => true],
        'Frontend' => ['name' => __d('be', 'Website'), 'domain' => 'fe', 'list' => 'translations', 'fetch' => true],
        'Countries' => ['name' => __d('be', 'Countries'), 'domain' => 'country', 'list' => 'translations', 'fetch' => true],
        'Salutations' => ['name' => __d('be', 'Salutations'), 'domain' => 'salutation', 'list' => 'translations', 'fetch' => true],
    ],

    // technical contacts
    'contact' => [
        'admin' => [
            'name' => 'Medienjaeger Programmierung',
            'email' => 'coders@medienjaeger.at',
        ],
        'support' => [
            'name' => 'Ludwig Jaeger',
            'email' => 'support@medienjaeger.at',
        ],
    ],

    // currencies
    'currencies' => ['EUR' => '&euro;'],

    // image settings
    'images' => [
        'use_categories' => true, // true, false or element
        'focus' => true,
        'sizes' => [
            'auto' => [
                'thumbs' => ['width' => 300, 'height' => 200],
                'popup' => ['width' => 800, 'height' => 600],
            ],
            'ecard' => [
                'view' => ['width' => 1000, 'height' => 625],
                'gallery' => ['width' => 750, 'height' => 468],
                'thumbs' => ['width' => 235, 'height' => 146],
            ],
            'purposes' => [
                1 => ['name' => __d('be', 'Header'), 'width' => 3000, 'height' => 1500, 'editor' => false, 'thumbs' => [ // header & content
                    ['width' => 230, 'height' => 1000, 'folder' => 'small'],
                ]],
                2 => ['name' => __d('be', 'Room & Offer'), 'width' => 1800, 'height' => 954, 'thumbs' => [ // room & offer
                    ['width' => 1128, 'height' => 598, 'folder' => 'overview'],
                    ['width' => 2000, 'height' => 800, 'folder' => 'banner'],
                    ['width' => 549, 'height' => 320, 'folder' => 'plan'],
                    ['width' => 336, 'height' => 336, 'folder' => 'pool'],
                    ['width' => 170, 'height' => 110, 'folder' => 'menu'],
                ]],
                3 => ['name' => __d('be', 'Banner'), 'width' => 1800, 'height' => 780, 'thumbs' => false], // banner
                4 => ['name' => __d('be', 'Pool'), 'width' => 336, 'height' => 336, 'thumbs' => false], // pool
                5 => ['name' => __d('be', 'Teaser (Offers)'), 'width' => 915, 'height' => 780, 'thumbs' => [ // teaser offers
                    ['width' => 442, 'height' => 780, 'folder' => 'small'],
                ]],
                6 => ['name' => __d('be', 'Teaser (Beauty)'), 'width' => 744, 'height' => 480, 'thumbs' => [ // teaser beauty
                    ['width' => 414, 'height' => 414, 'folder' => 'small'],
                ]],
                7 => ['name' => __d('be', 'Teaser (Services)'), 'width' => 519, 'height' => 440, 'thumbs' => false], // teaser services
                8 => ['name' => __d('be', 'Banner #2'), 'width' => 885, 'height' => 580, 'thumbs' => false] // banner 2sp
            ],
        ],
        'quality' => [
            'png' => 0,
            'jpg' => 80,
        ]
    ],

    // seo
    'seo' => [
        'meta' => [
            'title' => ['min' => 0, 'max' => 55],
            'desc' => ['min' => 80, 'max' => 156]
        ],
        'images' => [
            'folder' => 'seo',
        ],
        'canonical' => []
    ],

    // editor links classes
    'editor' => [
        'links' => [
            'nolink' => __d('be', 'No link'),
            'phone' => __d('be', 'Phone'),
            'email' => __d('be', 'E-Mail'),
        ]
    ],

    // themes
    'themes' => [
        'website' => __d('be', 'Website'),
    ],

    // pagination
    'pagination' => [
        'limit' => 25
    ],

    // categories
    'categories' => [
        'code' => 'none', // change also in categories table!
    ],

    // allowed
    'allowed' => [],

];

return $_setup;
