<?php use Cake\Core\Configure; ?>
<?php if($_SERVER['REMOTE_ADDR'] == '83.175.88.51'){ ?>
    <!-- ######   TEASER   ###### -->
    <!-- ###### [Elements/Website/teaser.ctp] ###### -->
<? } ?>

<?php if(array_key_exists('details', $element_content) && is_array($element_content['details'])){ ?>
<section class="teaser">
	<div class="inner">
        <div class="headline uppercase strong center">
            <?= $element_content['details']['headline'] ?>
        </div>
        <div class="design-element center">
            <span class="design-lines left"></span>
            <img src="/frontend/img/uploaded/icons/3Punkte_weiss.svg" alt="design-element" class="icon invert">
            <span class="design-lines right"></span>
        </div>
        <div class="content">
            <?= $element_content['details']['content'] ?>
        </div>
    </div>
</section>
<?php } ?>
