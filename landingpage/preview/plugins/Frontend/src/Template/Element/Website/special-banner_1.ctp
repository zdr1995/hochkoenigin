<?php use Cake\Core\Configure; ?>
<?php if($_SERVER['REMOTE_ADDR'] == '83.175.88.51'){ ?>
    <!-- ######   BANNER #1   ###### -->
    <!-- ###### [Elements/Website/special-banner_1.ctp] ###### -->
<? } ?>

<?php if(array_key_exists('details', $special_element_content) && is_array($special_element_content['details']) && !empty($special_element_content['details'])){ ?>
    <section class="banner-wrapper">
        <a href="<?= $special_element_content['details']['link'][0]['details']['link'] ?>" target="<?= $special_element_content['details']['link'][0]['details']['link'] ?>">
            <?php if(array_key_exists('image', $special_element_content['details']) && !empty($special_element_content['details']['image'])){ ?>
                <figure class="object-fit-image">
                    <img src="<?= $special_element_content['details']['image'][0]['details']['seo'][3] ?>" alt="<?= $special_element_content['details']['image'][0]['details']['title'] ?>" style="object-position: <?= $special_element_content['details']['image'][0]['details']['focus'][3]['css'] ?>;">
                    <?php if(array_key_exists('headline', $special_element_content['details']) && !empty($special_element_content['details']['headline']) && array_key_exists('text_line', $special_element_content['details']) && !empty($special_element_content['details']['text_line'])){ ?>
                        <figcaption>
                            <div class="inner-wrapper">
                                <span class="headline center uppercase strong">
                                    <span class ="bg">
                                        <?= $special_element_content['details']['headline'] ?>
                                    </span>
                                </span>
                                <span class="content center strong">
                                    <?= $special_element_content['details']['text_line'] ?>
                                </span>
                            </div>
                        </figcaption>
                    <?php } ?>
                </figure>
            <?php } ?>
        </a>
    </section>
<?php } ?>
