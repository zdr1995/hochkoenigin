<?php use Cake\Core\Configure; ?>
<?php if($_SERVER['REMOTE_ADDR'] == '83.175.88.51'){ ?>
    <!-- ######   TOP-LINE   ###### -->
    <!-- ###### [Elements/Website/top-line.ctp] ###### -->
<? } ?>


<section class="top-line">
    <div class="left">

        <?= $this->element('Frontend.Website/language-switcher') ?>

    </div>
    <div class="right">
        <a href="tel:<?= Configure::read('config.default.phone-plain') ?>">
            <img src="/frontend/img/uploaded/icons/Icons_Telefon.svg" alt="phone" class="phone-icon icon">
            <span class="strong"><?= Configure::read('config.default.phone') ?></span>
        </a>
        <a href="mailto:<?= Configure::read('config.default.email') ?>">
            <img src="/frontend/img/uploaded/icons/Icons_Mail.svg" alt="email" class="mail-icon icon">
        </a>
        <a href="https://www.google.com/maps/dir//Hochk%C3%B6nigstra%C3%9Fe+27,+5761+Alm/@47.4045275,12.9001605,17z/data=!4m8!4m7!1m0!1m5!1m1!1s0x4776e3c45e11e5ab:0x2d9d226b9d8da6b7!2m2!1d12.9023492!2d47.4045275">
            <img src="/frontend/img/uploaded/icons/Icons_Lage.svg" alt="routeplaner" class="map-icon icon">
        </a>
    </div>
</section>

<a href="https://www.hochkoenigin.com/">
    <img src="/frontend/img/logo.png" alt="logo" class="header-logo">
</a>
