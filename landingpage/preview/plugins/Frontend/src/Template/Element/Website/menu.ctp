<?php use Cake\Core\Configure; ?>

<div class="menu">
    <a href="<?= $this->Url->build(['node' => 'node:' . Configure::read('config.fixnav.first-left.0.id'), 'language' => $this->request->params['language']]) ?>" target="_self" class="menu-item left strong first">
        <img src="/frontend/img/uploaded/icons/Icons_Gutschein.svg" alt="gift" class="gift-icon icon">
        <span class="uppercase">
            <?= Configure::read('config.fixnav.first-left.0.details.element.title') ?>
        </span>
    </a>
    <a href="<?= $this->Url->build(['node' => 'node:' . Configure::read('config.fixnav.second-left.0.id'), 'language' => $this->request->params['language']]) ?>" target="_self" class="menu-item left strong">
        <img src="/frontend/img/uploaded/icons/Icons_360.svg" alt="360grad" class="360-icon icon">
        <span class="uppercase">
            <?= Configure::read('config.fixnav.second-left.0.details.element.title') ?>
        </span>
    </a>
    <a href="#navi" id="menu-open" class="j2c menu-item nav-bttn strong">
        <img src="/frontend/img/uploaded/icons/Icons_Down.svg" alt="down" class="down-icon icon animated wobble infinite">
        <span class="uppercase">
            <?= __d('fe', 'menu') ?>
        </span>
    </a>
    <div id="recent-packages" class="menu-item invisible">
        <?= $this->element('Frontend.Website/special-recent-packages') ?>
    </div>
    <a href="<?= $this->Url->build(['node' => 'node:' . Configure::read('config.fixnav.first-right.0.id'), 'language' => $this->request->params['language']]) ?>" target="_self" class="menu-item right col-light strong">
        <img src="/frontend/img/uploaded/icons/Icons_Anfragen.svg" alt="calendar" class="cal-icon icon">
        <span class="uppercase">
            <?= Configure::read('config.fixnav.first-right.0.details.element.title') ?>
        </span>
    </a>
    <a href="<?= $this->Url->build(['node' => 'node:' . Configure::read('config.fixnav.second-right.0.id'), 'language' => $this->request->params['language']]) ?>" target="_self" class="menu-item right red strong last">
        <img src="/frontend/img/uploaded/icons/Icons_Buchen.svg" alt="booking" class="book-icon icon">
        <span class="uppercase">
            <?= Configure::read('config.fixnav.second-right.0.details.element.title') ?>
        </span>
    </a>
</div>
