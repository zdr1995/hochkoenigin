<?php use Cake\Core\Configure; ?>

<footer style="background-image:URL('/frontend/img/uploaded/icons/Pattern_outline_weiss.svg');">
    <div class="inner-wrapper">

        <!-- 2 Collumns -->
        <div class="collumns">
            <div class="left">
                <a href="https://www.hochkoenigin.com/" target="_self"class="footer-logo">
                    <img src="/frontend/img/logo.png" alt="footer-logo" class="footer-logo">
                </a>
                <div class="links">
                </div>
            </div>
            <div class="right">
                <a href="https://www.google.com/maps/dir//Hochk%C3%B6nigstra%C3%9Fe+27,+5761+Alm/@47.4045275,12.9001605,17z/data=!4m8!4m7!1m0!1m5!1m1!1s0x4776e3c45e11e5ab:0x2d9d226b9d8da6b7!2m2!1d12.9023492!2d47.4045275" target="_blank">
                    <img src="/frontend/img/uploaded/icons/Karte_tuerkis.svg" alt="map" class="footer-map">
                </a>
            </div>
        </div>

        <!-- Contact line -->
        <div class="contact uppercase center">
            <a href="https://www.google.com/maps/dir//Hochk%C3%B6nigstra%C3%9Fe+27,+5761+Alm/@47.4045275,12.9001605,17z/data=!4m8!4m7!1m0!1m5!1m1!1s0x4776e3c45e11e5ab:0x2d9d226b9d8da6b7!2m2!1d12.9023492!2d47.4045275" target="_blank">
                <span style="white-space:nowrap !important;">
                    <?= Configure::read('config.default.street-' . $this->request->params['language']) ?>
                </span>
                <span class="dot">&nbsp;&period;&nbsp;</span>
                <span style="white-space:nowrap !important;">
                    <?= Configure::read('config.default.zip') ?>&nbsp;
                    <?= Configure::read('config.default.city-' . $this->request->params['language']) ?>
                </span>
            </a>
            <span class="dot">&nbsp;&period;&nbsp;</span>
            <a href="tel:<?= Configure::read('config.default.phone-plain') ?>" target="_blank" style="white-space:nowrap !important;">
                <?= __d('fe', 'Tel.') ?>&nbsp;<?= Configure::read('config.default.phone') ?>
            </a>
            <span class="dot">&nbsp;&period;&nbsp;</span>
            <a href="mailto:<?= Configure::read('config.default.email') ?>" target="_blank" style="white-space:nowrap !important;">
                <?= Configure::read('config.default.email') ?>
            </a>
        </div>

        <!-- Social links -->
        <div class="social center">
            <?php $social_items = ['facebook', 'instagram', 'twitter', 'youtube'] ?>
            <?php foreach ($social_items as $_item) {
                if (Configure::read('config.links.' . $_item . '_check') == 1) { ?>
                    <a href="<?= Configure::read('config.links.' . $_item) ?>" target="_blank">
                        <img src="/frontend/img/uploaded/icons/<?= $_item ?>.svg" alt="<?= $_item ?>" class="icon">
                    </a>
                <? }
            } ?>
        </div>

        <!-- Bottom links -->
        <div class="bottom-links center">
            <?php $counter = 0; ?>
            <?php foreach (Configure::read('config.fixnav.footer-bottom-items') as $_k => $_item) { ?>
                <a href="<?= $this->Url->build(['node' => 'node:' . $_item['id'], 'language' => $this->request->params['language']]) ?>" target="_self">
                    <span class="capitalise"><?= $_item['details']['element']['title'] ?></span>
                </a>
                <?php if ($counter !== count(Configure::read('config.fixnav.footer-bottom-items')) - 1) { ?>
                    <span class="dot">&nbsp;&period;&nbsp;</span>
                <?php } ?>
                <?php $counter = $counter + 1; ?>
            <?php } ?>
        </div>

    </div>
</footer>
