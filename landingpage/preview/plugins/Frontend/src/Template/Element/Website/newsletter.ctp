<?php if($_SERVER['REMOTE_ADDR'] == '83.175.88.51'){ ?>
    <!-- ######   NEWSLETTER   ###### -->
    <!-- ###### [Elements/Website/newsletter.ctp] ###### -->
<? } ?>

<section class="newsletter hidden-print">
    <h3><?= __d('fe', 'Newsletter'); ?></h3>
    <div class="info"><?= __d('fe', 'Receive all the latest news and offers!'); ?></div>
    <?= $this->CustomForm->input('newsletter', ['type' => 'checkbox', 'label' => __d('fe', 'Yes, I want to receive a newsletter.'), 'id' => 'newsletter']); ?>
</section>
