<?php use Cake\Core\Configure; ?>

<nav id="mainnav" class="hidden-print">
    <a class="anchor" name="navi"></a>
    <a href="<?= $this->Url->build(['node' => 'node:' . Configure::read('config.default.home.0.id'), 'language' => $this->request->params['language']]); ?>">
        <img src="/frontend/img/logo_black.png" alt="logo" class="menu-logo">
    </a>
    <ul class="mainnav">
        <?php foreach($menu as $idx1 => $lvl1){ ?>
            <?php $lnk1 = $this->Url->build(['node' => 'node:' . $lvl1['id'], 'language' => $this->request->params['language']]); ?>
            <li class="<?php echo 'children-' . count($lvl1['children']); ?><?php echo $lvl1['highlight'] || $lvl1['active'] ? ' active' : ''; ?>">
                <a class="d center" href="<?= $lnk1; ?>">
                    <span class="wrapper">
                        <span class="uppercase strong title">
                            <?= $lvl1['content']; ?>
                        </span>
                        <span class="subtitle">
                            <?= $lvl1['element']['subtitle']['content']; ?>
                        </span>
                    </span>
                </a>
                <a class="m center" href="<?= $lnk1; ?>">
                    <i class="fas fa-chevron-down" aria-hidden="true"></i>
                    <span class="wrapper">
                        <span class="uppercase strong title">
                            <?= $lvl1['content']; ?>
                        </span>
                        <span class="subtitle">
                            <?= $lvl1['element']['subtitle']['content']; ?>
                        </span>
                    </span>
                </a>
                <?php if(count($lvl1['children']) > 0){ ?>
                <ul class="subnav<?php echo $idx1 > 2 ? ' rtl' : ''; ?> invisible">
                    <li class="m center">
                        <a href="<?= $lnk1; ?>">
                            <img src="/frontend/img/uploaded/icons/3Punkte_tuerkis.svg" alt="design" class="nav-list-icon icon">
                            &nbsp;
                            <span class="wrapper">
                                <span class="strong title">
                                    <?= $lvl1['content']; ?>
                                </span>
                                <span class="subtitle">
                                    <?= $lvl1['element']['subtitle']['content']; ?>
                                </span>
                            </span>
                        </a>
                    </li>
                    <?php foreach($lvl1['children'] as $idx2 => $lvl2){ ?>
                        <li class="<?php echo $idx2 == 0 ? 'first' : ''; ?><?php echo $lvl2['active'] ? ' active' : ''; ?>">
                            <a href="<?= $this->Url->build(['node' => 'node:' . $lvl2['id'], 'language' => $this->request->params['language']]); ?>" class="uppercase">
                                <img src="/frontend/img/uploaded/icons/3Punkte_tuerkis.svg" alt="design" class="nav-list-icon icon">
                                &nbsp;<?= $lvl2['content']; ?>
                            </a>
                        <?php if(count($lvl2['children']) > 0){ ?>
                        <ul class="subsubnav invisible">
                            <?php foreach($lvl2['children'] as $idx3 => $lvl3){ ?>
                                <li class="<?php echo $idx3 == 0 ? 'first' : ''; ?><?php echo $lvl3['active'] ? ' active' : ''; ?>">
                                    <a href="<?= $this->Url->build(['node' => 'node:' . $lvl3['id'], 'language' => $this->request->params['language']]); ?>" class="">
                                        <img src="/frontend/img/uploaded/icons/3Punkte_tuerkis.svg" alt="design" class="nav-list-icon icon">
                                        &nbsp;<?= $lvl3['content']; ?>
                                    </a>
                                </li>
                            <?php } ?>
                        </ul>
                        <?php } ?>
                        </li>
                    <?php } ?>
                </ul>
                <?php } ?>
            </li>
        <?php } ?>
    </ul>
    <div class="right-links">
        <a href="tel:<?= Configure::read('config.default.phone-plain') ?>">
            <img src="/frontend/img/uploaded/icons/Icons_Telefon.svg" alt="phone" class="phone-icon icon">
        </a>
        <a href="<?= Configure::read('config.default.email') ?>">
            <img src="/frontend/img/uploaded/icons/Icons_Mail.svg" alt="email" class="mail-icon icon">
        </a>
    </div>
    <div class="design-line" style="background: url('/frontend/img/uploaded/Design_Line.png');">
        <img src="/frontend/img/uploaded/Design_Line.png" alt="design-line" class="hidden">
    </div>
</nav>
