<?php if($_SERVER['REMOTE_ADDR'] == '83.175.88.51'){ ?>
    <!-- ######   DOWNLOADS   ###### -->
    <!-- ###### [Elements/Website/downloads.ctp] ###### -->
<? } ?>

<?php use Cake\Core\Configure; ?>
<?php
//init
$class = $style = $title = '';

if($element_content['type'] == 'image'){
	$class .= ' downloadable-image';
	$style = 'background-image: url(' . $element_content['details']['seo'][3] . '); background-position:' . $element_content['details']['focus'][3]['css'] . ';';
	$file = [
		'check' => $element_content['details']['paths']['original'],
		'href' => '/provide/image/de/' . $element_content['details']['id'],
	];
	$title = __d('fe', 'Download');
} else{
	$class .= ' download-' . $element_count;
	$style = 'background-image: url(' . $element_content['details']['image'][0]['details']['seo'][3] . '); background-position:' . $element_content['details']['image'][0]['details']['focus'][3]['css'] . ';';
    $file = $element_content['details']['file'];
	$file = [
		'check' => WWW_ROOT . '/files/' . $file['name'],
		'href' => '/files/' . $file['name'],
	];
	$title = $element_content['details']['title'];
}

?>
 <?php if(isset($file['check']) && !empty($file['check']) && file_exists($file['check'])){ ?>
	<a href="<?= $file['href'] ?>" class="download<?= $class ?> hvr-sweep-to-top hidden-print" style="<?= $style ?>">
		<div class="download-title">
			<i class="fas fa-arrow-down " aria-hidden="true"></i>
			<?= $title ?>
		</div>
	</a>
<?php } ?>
