<?php if($_SERVER['REMOTE_ADDR'] == '83.175.88.51'){ ?>
    <!-- ######   SOCIALWALL   ###### -->
    <!-- ###### [Elements/Website/special-socialwall.ctp] ###### -->
<? } ?>

<?php if (is_array($special_element_content) && array_key_exists('details', $special_element_content)) { ?>
    <h2 class="social-wall-headline"><?= __d('fe', 'Social Media'); ?></h2>
    <section class="social-wall">
        <?php
        $cnt = 0;
        $max = 9;
        ?>
        <?php foreach ($special_element_content['details']['_details'] as $post) { ?>
            <?php
            $infos = false;
            ?>
            <?php if ($max === false || $cnt < $max) { ?>
                <?php if ($post['__source'] == 'facebook') { ?>
                    <?php if (array_key_exists('attachments', $post)) { ?>
                        <?php
                        switch ($post['type']) {
                            case "photo":
                            case "video":
                            case "link":

                                // init
                                $infos = [
                                    'id' => $post['id'],
                                    'type' => $post['type'],
                                    'message' => array_key_exists('message', $post) ? $post['message'] : false,
                                    'images' => [],
                                    'link' => $post['link'],
                                    'source' => $post['__source'],
                                    'date' => $post['__created'],
                                    'icon' => 'fa-facebook',
                                ];

                                // images
                                $attachments = array_key_exists('subattachments', $post['attachments'][0]) ? $post['attachments'][0]['subattachments'] : $post['attachments'];
                                foreach ($attachments as $attachment) {
                                    switch ($attachment['type']) {
                                        case "photo":
                                        case "album":
                                        case "video_inline":
                                            if (array_key_exists('media', $attachment) && is_array($attachment['media']) && array_key_exists('image', $attachment['media']) && is_array($attachment['media']['image'])) {
                                                $infos['images'][] = $attachment['media']['image']['src'];
                                            }
                                            break;
                                        default:
                                            // echo "Unknown attachment '" . $attachment['type'] . "'"; exit;
                                            break;
                                    }
                                }

                                $cnt++;
                                break;
                            default:
                                break;
                        }
                        ?>
                    <?php } ?>
                <?php } else if ($post['__source'] == 'instagram') { ?>
                    <?php
                    // init
                    $infos = [
                        'id' => $post['id'],
                        'type' => $post['type'],
                        'message' => false,
                        'images' => [],
                        'link' => $post['link'],
                        'source' => $post['__source'],
                        'date' => $post['__created'],
                        'icon' => 'fa-instagram',
                    ];

                    // message
                    if (array_key_exists('caption', $post) && is_array($post['caption']) && array_key_exists('text', $post['caption'])) {
                        $infos['message'] = $post['caption']['text'];
                    }

                    // image
                    if (array_key_exists('images', $post) && is_array($post['images']) && array_key_exists('standard_resolution', $post['images'])) {
                        $infos['images'][] = $post['images']['standard_resolution']['url'];
                    }

                    $cnt++;
                    ?>
                <?php } ?>
                <?php if (is_array($infos) && (count($infos['images']) > 0 || !empty($infos['message']))) { ?>
                    <article class="brick <?= $infos['source']; ?> <?= $infos['type']; ?>">
                        <i class="fab <?= $infos['icon']; ?>" aria-hidden="true"></i>
                        <?php if (count($infos['images']) > 0) { ?>
                            <a href="<?= $infos['link']; ?>" target="_blank" class="img"><div style="background-image: url('<?= $infos['images'][0]; ?>')"></div></a>
                        <?php } ?>
                        <div class="date"><?= date("d.m.Y H:i:s", $infos['date']); ?></div>
                        <?php if (!empty($infos['message'])) { ?>
                            <div class="message">
                                <?= $infos['message']; ?>
                            </div>
                            <a class="more button" href="<?= $infos['link']; ?>"><?= __d('fe', 'more'); ?></a>
                        <?php } ?>
                    </article>
                <?php } ?>
            <?php } ?>
        <?php } ?>
    </section>
<?php } ?>
