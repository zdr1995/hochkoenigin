<?php if($_SERVER['REMOTE_ADDR'] == '83.175.88.51'){ ?>
    <!-- ######   POOL   ###### -->
    <!-- ###### [Elements/Website/pool.ctp] ###### -->
<? } ?>

<?php if(array_key_exists('details', $element_content) && is_array($element_content['details']) && array_key_exists('_details', $element_content['details']) && is_array($element_content['details']['_details']) && count($element_content['details']['_details']['infos']) > 0){ ?>
<a class="anchor" id="<?= $element_content['id']; ?>" name="<?= $element_content['id']; ?>"></a>
<section class="pool" data-pool-id="<?= $element_content['id']; ?>">
    <ul class="viewport bxslider">
    <?php foreach($element_content['details']['_details']['infos'] as $package){ ?>
        <?php

            // link
            $url = array_key_exists($package['id'], $element_content['details']['_details']['nodes']) ? $this->Url->build(['node' => 'node:' . $element_content['details']['_details']['nodes'][$package['id']], 'language' => $this->request->params['language']]) : false;

        ?>
        <li>
            <article class="teaser-50 right">
                <div class="img" style="background-position: <?= $package['images'][0]['details']['focus'][3]['css']; ?>; background-image: url('<?= $package['images'][0]['details']['seo'][3]; ?>');">
                    <?php if(array_key_exists($package['id'], $element_content['details']['_details']['prices']['values']) && count($element_content['details']['_details']['prices']['values'][$package['id']]) > 0){ ?>
                    <span class="min"><span class="draft"><?= $element_content['details']['_details']['prices']['drafts'][$element_content['details']['_details']['prices']['connections'][$package['id']]['ranges']['min']['draft']]['translations']['title']; ?></span><?= __d('fe', 'from € %s', number_format($element_content['details']['_details']['prices']['connections'][$package['id']]['ranges']['min']['value'], 2, ',', '.')); ?></span>
                    <?php } ?>
                </div>
                <div class="text-wrapper">
                    <div class="text">
                        <h2 class="h1-like"><?= $package['title']; ?></h2>
                        <?php if(count($package['valid_times']) > 0){ ?>
                        <span class="times">
                            <?php foreach($package['valid_times'] as $time){ ?>
                                <span class="time"><?= __d('fe', '%s to %s', $time['from'], $time['to']); ?></span>
                            <?php } ?>
                        </span>
                        <?php } ?>
                        <div><?= $package['teaser']; ?></div>
                        <?php if($url){ ?><a class="button" href="<?= $url; ?>"><?= __d('fe', 'show offer'); ?></a><?php } ?>
                    </div>
                    <div class="clear"></div>
                </div>
                <div class="clear"></div>
            </article>
        </li>
    <?php } ?>
    </ul>
    <?php $idx = 0; ?>
    <div class="bx-navi">
        <?php if(count($element_content['details']['_details']['infos']) > 1){ ?>
        <div class="bx-pool-pager bx-pool-pager-<?= $element_content['id']; ?>">
            <?php foreach($element_content['details']['_details']['infos'] as $package){ ?>
                <a data-slide-index="<?= $idx; ?>" href=""><i class="fas fa-circle" aria-hidden="true"></i><i class="fas fa-circle" aria-hidden="true"></i></a>
                <?php $idx++; ?>
            <?php } ?>
        </div>
        <?php } ?>
        <?php
            $url = $this->Url->build(['node' => $element_content['details']['link'][0]['org'], 'language' => $this->request->params['language']]);
            if(strlen($url) > 1){
        ?>
        <a class="more" href="<?= $url; ?>"><i class="fas fa-chevron-right" aria-hidden="true"></i>&nbsp;<?= $element_content['details']['linktext']; ?></a>
        <?php } ?>
        <div class="clear"></div>
    </div>
    <div class="clear"></div>
</section>
<?php } ?>
