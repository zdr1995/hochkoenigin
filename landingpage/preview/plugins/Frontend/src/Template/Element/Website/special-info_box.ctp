<?php if($_SERVER['REMOTE_ADDR'] == '83.175.88.51'){ ?>
    <!-- ######   INFO-BOX   ###### -->
    <!-- ###### [Elements/Website/special-info_box.ctp] ###### -->
<? } ?>

<?php $rounds = ['1', '2', '3'] ?>


<section class="info-box-wrapper">
	<div class="inner">
        <?php if(array_key_exists('details', $special_element_content) && is_array($special_element_content['details']) && !empty($special_element_content['details'])){ ?>

            <div class="buttons">
                <?php foreach ($rounds as $round) { ?>
                    <a id="info-box-<?= $round ?>" href="#" target="_self">
                        <span>
                            <?= $special_element_content['details']['ib_headline_' . $round] ?>
                        </span>
                    </a>
                <?php } ?>
            </div>

            <div class="content">
                <?php foreach ($rounds as $round) { ?>
                    <div class="item item-<?= $round ?>">

                        <div class="left">
                            <span>
                                <?= $special_element_content['details']['ib_textblock_' . $round] ?>
                            </span>
                        </div>

                        <div class="right">
                            <a href="<?= $special_element_content['details']['ib_link_' . $round][0]['details']['link'] ?>" target="<?= $special_element_content['details']['ib_link_' . $round][0]['details']['target'] ?>">
                                <img src="<?= $special_element_content['details']['ib_image_' . $round][0]['details']['seo'][7] ?>" alt="<?= $special_element_content['details']['ib_image_' . $round][0]['details']['title'] ?>" style="object-position: <?= $special_element_content['details']['ib_image_' . $round][0]['details']['focus'][7]['css'] ?>;">
                            </a>
                        </div>

                    </div>
                <?php } ?>
            </div>

            <div class="link">
                <?php foreach ($rounds as $round) { ?>
                    <a href="<?= $special_element_content['details']['ib_link_' . $round][0]['details']['link'] ?>" target="<?= $special_element_content['details']['ib_link_' . $round][0]['details']['target'] ?>">
                        <span>
                            <?= $special_element_content['details']['ib_linktxt_' . $round] ?>
                        </span>
                    </a>
                <?php } ?>
            </div>

        <?php } ?>
    </div>
</section>
