<?php use Cake\Core\Configure; ?>
<?php use Frontend\Controller\AppController; ?>
<?php if($_SERVER['REMOTE_ADDR'] == '83.175.88.51'){ ?>
    <!-- ######   RECENT PACKAGES   ###### -->
    <!-- ###### [Elements/Website/special-recent-packages.ctp] ###### -->
<? } ?>

    <ul class="menu-packages-slider viewport bxslider">
        <?php foreach ($recent_packages as $_k => $package) { ?>
            <li>
                <div class="bxslide">
                    <img src="<?= $package->images_with_details[0]['seo']['2_menu'] ?>" alt="<?= $package->images_with_details[0]['title'] ?>" class="menu-package-overview-img" style="object-position: <?= $package->images_with_details[0]['focus'][2]['css'] ?>;">
                    <div class="menu-package-overview-txt">
                        <span class="wrapper">
                            <span class="headline uppercase center strong">
                                <?= $package->title ?>
                            </span>
                            <span class="content center">
                                <?= $package->content ?>
                            </span>
                        </span>
                    </div>
                </div>
            </li>
        <?php } ?>
    </ul>
    <img src="<?= $package->images_with_details[0]['seo']['original'] ?>" alt="<?= $package->images_with_details[0]['title'] ?>" class="menu-package-overview-img">
