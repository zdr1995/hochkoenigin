<?php if($_SERVER['REMOTE_ADDR'] == '83.175.88.51'){ ?>
    <!-- ######   COUNTER BANNER   ###### -->
    <!-- ###### [Elements/Website/special-counter.ctp] ###### -->
<? } ?>

<?php if(array_key_exists('details', $special_element_content) && is_array($special_element_content['details']) && !empty($special_element_content['details'])){ ?>
    <section class="banner-wrapper counter">
        <a href="<?= $special_element_content['details']['link'][0]['details']['link'] ?>" target="<?= $special_element_content['details']['link'][0]['details']['link'] ?>">
            <div class="column col-1">
                <span class="design-element left"></span>
                <span class="content strong">
                    <?= $special_element_content['details']['wellness_line'] ?> m&sup2;
                </span>
                <span class="headline uppercase strong">
                    <?= __d('be', 'Wellness') ?>
                </span>
            </div>
            <div class="column col-2">
                <span class="content strong">
                    <?= $special_element_content['details']['pools_line'] ?>
                </span>
                <span class="headline uppercase strong">
                    <?= __d('be', 'Pools') ?>
                </span>
            </div>
            <div class="column col-3">
                <span class="design-element right"></span>
                <span class="content strong">
                    <?= $special_element_content['details']['saunas_line'] ?>
                </span>
                <span class="headline uppercase strong">
                    <?= __d('be', 'Saunas') ?>
                </span>
            </div>
        </a>
    </section>
<?php } ?>
