<?php use Cake\Core\Configure; ?>

<?php
    $_headers = [];

    if(array_key_exists('media', $content) && is_array($content['media']) && array_key_exists('header-' . Configure::read('config.default.season'), $content['media']) && is_array($content['media']['header-' . Configure::read('config.default.season')])){
        foreach($content['media']['header-' . Configure::read('config.default.season')] as $h){
            if(is_array($h) && array_key_exists('type', $h) && in_array($h['type'], ['image','element']) && is_array($h['details'])){
                if($h['type'] == 'image'){
                    $_headers[] = [
                        'type' => 'image',
                        'url' => $h['details']['seo'][1],
                        'focus' => $h['details']['focus'][1],
                        'alt' => $h['details']['title'],
                    ];
                }else if($h['type'] == 'element' && array_key_exists('code', $h['details']) && $h['details']['code'] == 'header-teaser'){
                    if(array_key_exists('image', $h['details']) && is_array($h['details']['image']) && array_key_exists(0, $h['details']['image']) && is_array($h['details']['image'][0]) && count($h['details']['image'][0]) > 0){
                        $_headers[] = [
                            'type' => 'teaser',
                            'url' => $h['details']['image'][0]['details']['seo'][1],
                            'focus' => $h['details']['image'][0]['details']['focus'][1],
                            'alt' => $h['details']['image'][0]['details']['title'],
                            'quote' => $h['details']['quote'],
                            'author' => $h['details']['author'],
                        ];
                    }
                }
            }
        }
    }

    //set default header
	if(count($_headers) <= 0){
		$_headers[] = array(
			'type' => 'image',
			'url' => Configure::read('config.default.default_header_' . Configure::read('config.default.season') . '.0.details.seo.1'),
			'focus' => Configure::read('config.default.default_header_' . Configure::read('config.default.season') . '.0.details.focus.1'),
			'alt' => __d('fe','Header image'),
		);
	}

?>

<?php if(count($_headers) > 0){ ?>
    <header id="header" class="images images-<?= count($_headers) ?> hidden-print">

        <?= $this->element('Frontend.Website/top-line') ?>

        <ul class="viewport bxslider">
            <?php foreach($_headers as $header){ ?>
                <li>
                    <div class="bxslide">

                        <figure class="object-fit-image">
                          <img src="<?= $header['url'] ?>" alt="<?= $header['alt'] ?>" style="object-position: <?= $header['focus']['css'] ?>;">
                        </figure>

                        <!-- <?php if($header['type'] == 'teaser'){ ?>
                        <div class="text">
                            <?php if(!empty($header['quote'])){ ?>
                            <div class="quote">&bdquo;<?= $header['quote']; ?>&ldquo;</div>
                            <?php } ?>
                            <?php if(!empty($header['author'])){ ?>
                            <div class="author"><?= $header['author']; ?></div>
                            <?php } ?>
                        </div>
                        <?php } ?> -->

                    </div>
                </li>
            <?php } ?>
        </ul>

    	<a class="j2c animated pulse infinite" href="#main">
            <span class="uppercase block"><?= __d('be', 'Scroll') ?></span>
            <img src="/frontend/img/uploaded/icons/Icons_Down_Double.svg" alt="double-down" class="icon">
        </a>
    </header>
<?php } ?>
