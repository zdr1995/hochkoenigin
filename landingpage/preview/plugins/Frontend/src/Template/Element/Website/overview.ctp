<?php if($_SERVER['REMOTE_ADDR'] == '83.175.88.51'){ ?>
    <!-- ######   OVERVIEW   ###### -->
    <!-- ###### [Elements/Website/overview.ctp] ###### -->
<? } ?>

<?php echo "<pre>" . print_r($element_content,1) . "</pre>"; exit; ?>

<?= $this->element('Frontend.Website/media', ['media' => $content['media'], 'position' => 'cans-pool', 'wrapper' => 'cans-pool']) ?>

<main class="inner">
    <div class="overview">
        <div class="content-box">
            <?php switch ($element_content['details']['type']) {
                case 'news': ?>
                    <div class="heading">
                        <div class="headline"><?= $content['headline'] ?></div>
                        <div class="teaser"><?= $content['content'] ?></div>
                    </div>
                    <?php
                    $news = $element_content['details']['_details'];
                    $news_ids = $element_content['details']['news']['0']['details']['contain'];
                    if(is_array($news_ids)){ ?>
                        <div class="news-slider-wrap">
                            <div class="news-slider">
                                <div class="news-slide">
                                    <?php $cnt = 0; ?>
                                    <?php foreach($news_ids as $news_id){ ?>
                                        <?php if(!isset($news['infos'][$news_id]['title']) || empty($news['infos'][$news_id]['title'])) continue; ?>
                                        <?php $node_id = isset($news['nodes'][$news_id]) ? $news['nodes'][$news_id] : false; ?>
                                        <?php if($cnt>0 && $cnt%3 == 0) echo '</div><div class="news-slide">'; ?>
                                        <div class="newsItemBox">
                                            <div class="item">
                                                <div class="image">
                                                    <img src="<?php
                                                     print_r($news['infos'][$news_id]['image']['0']['details']['urls']['4']) ?>" alt="<?php print_r($news['infos'][$news_id]['title']) ?>">
                                                </div>
                                                <div class="content">
                                                    <div class="headline">
                                                        <?php print_r($news['infos'][$news_id]['title']) ?>
                                                    </div>
                                                    <div class="teaser">
                                                        <?php print_r($news['infos'][$news_id]['subtitle']) ?>
                                                    </div>
                                                    <div class="text">
                                                        <?php print_r($news['infos'][$news_id]['content']) ?>
                                                    </div>
                                                    <!-- <div class="btn">
                                                        <a href="<?= $this->Url->build(['node' => 'node:' . $node_id, 'language' => $this->request->params['language']]); ?>" class="button"><?= __d('fe', 'read more') ?></a>
                                                    </div> -->
                                                </div>
                                            </div>
                                        </div>
                                        <?php $cnt++; ?>
                                    <?php } ?>
                                </div>
                            </div>
                        </div>
                    <?php } ?>
                    <?php
                    break;
                case 'can':
                    $content['content'] = '<div class="headline">' . $content['headline'] . '</div>' . $content['content'];
                    $sidebar = '<div class="sidebar">';
                        $cans = $element_content['details']['_details'];
                        if(is_array($cans)){
                            foreach($element_content['details']['_details']['infos'] as $_can_infos){
                                $can_id = $_can_infos['id'];
                                $node_id = $cans['nodes'][$can_id];
                                $sidebar .= '<div class="item">';
                                    $sidebar .= '<a href="' . $this->Url->build(['node' => 'node:' . $cans['infos'][$can_id]['node_id'], 'language' => $this->request->params['language']]) . '">';
                                        $sidebar .= '<img src="' . $cans['infos'][$can_id]['image']['0']['details']['urls']['original'] . '" alt="' . $cans['infos'][$can_id]['title'] . '">';
                                        $sidebar .= '<span>' . __d('fe', 'Details') . '</span><i class="fas fa-caret-right"></i>';
                                    $sidebar .= '</a>';
                                $sidebar .= '</div>';
                            }
                        }
                    $sidebar .= '</div>';
                    echo str_replace('{$sidebar}', $sidebar, $content['content']);
                    break;
            } ?>
        </div>
    </div>
</main>
