<section class="main package">
    <a class="anchor" name="content"></a>
    <div>
        <div class="content<?php echo array_key_exists($content['id'], $prices['values']) && count($prices['values'][$content['id']]) > 0 ? '' : ' np'; ?>">
            <table cellpadding="0" cellspacing="0">
                <tr>
                    <td>
                        <h1><?= $content['headline']; ?></h1>
                        <?php if(count($content['valid_times']) > 0){ ?>
                            <?php foreach($content['valid_times'] as $time){ ?>
                                <span class="time"><?= __d('fe', 'from %s to %s', $time['from'], $time['to']); ?></span>
                            <?php } ?>
                        <?php } ?>
                    </td>
                </tr>
            </table>
        </div>
        <?php if(array_key_exists($content['id'], $prices['values']) && count($prices['values'][$content['id']]) > 0){ ?>
            <span class="min"><span class="draft"><?= $prices['drafts'][$prices['connections'][$content['id']]['ranges']['min']['draft']]['translations']['title']; ?></span><?= __d('fe', 'from € %s', number_format($prices['connections'][$content['id']]['ranges']['min']['value'], 2, ',', '.')); ?></span>
        <?php } ?>
    </div>
</section>
<div class="impressions">
    <ul class="viewport bxslider">
        <?php foreach($content['images'] as $img){ ?>
            <li>
                <div class="bxslide" style="background-position: <?= $img['details']['focus'][3]['css']; ?>; background-image: url('<?= $img['details']['seo'][3]; ?>');"></div>
            </li>
        <?php } ?>
    </ul>
    <?php if(count($content['images']) > 1){ ?>
    <div id="bx-pager">
        <?php foreach($content['images'] as $k => $v){ ?>
            <a data-slide-index="<?= $k; ?>" href=""><i class="fas fa-circle" aria-hidden="true"></i><i class="fas fa-circle" aria-hidden="true"></i></a>
        <?php } ?>
    </div>
    <?php } ?>
</div>
<div class="clear"></div>
<main class="center">
    <?= $content['content']; ?>
</main>
<?php if(is_array($content['seperator']) && array_key_exists(0, $content['seperator'])){ ?>
<section class="seperator" style="background-position: <?= $content['seperator'][0]['details']['focus'][1]['css']; ?>; background-image: url('<?= $content['seperator'][0]['details']['seo'][1]; ?>');"></section>
<?php } ?>
<?php if(!empty($content['services_text'])){ ?>
<section class="main">
    <h2 class="h1-like"><?= $content['services_title']; ?></h2>
    <?php if(!empty($content['services_text'])){ ?>
        <?= $content['services_text']; ?>
    <?php } ?>
</section>
<?php } ?>
<?php if(array_key_exists($content['id'], $prices['values']) && count($prices['values'][$content['id']]) > 0){ ?>
<section class="package prices">
    <h2 class="h1-like"><?= __d('fe', 'Package prices per person'); ?></h2>
    <div class="price-table drafts-<?= count($prices['connections'][$content['id']]['used']['drafts']); ?>">

        <!-- headline -->
        <div class="headline line">
            <div class="element">&nbsp;</div>
            <div class="drafts">
                <?php foreach($prices['drafts'] as $draft){ ?>
                    <?php if(in_array($draft['id'], $prices['connections'][$content['id']]['used']['drafts'])){ ?>
                    <div class="draft"><?= $draft['translations']['title']; ?><span><?= $draft['translations']['caption']; ?></span></div>
                    <?php } ?>
                <?php } ?>
                <div class="clear"></div>
            </div>
            <div class="clear"></div>
        </div>

        <!-- prices -->

        <?php foreach($prices['elements'] as $id => $room){ ?>
            <?php if(in_array($id, $prices['connections'][$content['id']]['used']['elements'])){ ?>
            <div class="line">
                <div class="element"><?php echo $room['node'] ? '<a href="' . $this->Url->build(['node' => 'node:' . $room['node'], 'language' => $this->request->params['language']]) . '"><i class="fas fa-angle-right" aria-hidden="true"></i> '. $room['title'] . '</a>' : '<i class="fas fa-angle-right" aria-hidden="true"></i> ' . $room['title']; ?></div>
                <div class="drafts">
                    <?php if(array_key_exists($id, $prices['values'][$content['id']])){ ?>
                        <?php foreach($prices['drafts'] as $draft){ ?>
                            <?php if(in_array($draft['id'], $prices['connections'][$content['id']]['used']['drafts'])){ ?>
                            <div class="draft"><?php echo array_key_exists($draft['id'], $prices['values'][$content['id']][$id]) ? '<span>&euro;</span>' . number_format($prices['values'][$content['id']][$id][$draft['id']], 2, ',', '.') . '<div class="clear"></div>' : '--'; ?></div>
                            <?php } ?>
                        <?php } ?>
                    <?php } ?>
                    <div class="clear"></div>
                </div>
                <div class="clear"></div>
            </div>
            <?php } ?>
        <?php } ?>
    </div>
</section>
<?= $this->element('Frontend.Website/request-book-buttons'); ?>
<?php if(is_array($content['info']) && array_key_exists(0, $content['info']) && is_array($content['info'][0]) && array_key_exists('details', $content['info'][0])){ ?>
<section class="main footnote">
    <?= $content['info'][0]['details']['textblock']; ?>
</section>
<?php } ?>
<?php }else{ ?>
    <?= $this->element('Frontend.Website/request-book-buttons'); ?>
<?php } ?>
<div class="clear"></div>
<?= $this->element('Frontend.Website/media', ['media' => $content['media'], 'position' => 'media-top', 'wrapper' => 'media-top']) ?>
<?= $this->element('Frontend.Website/media', ['media' => $content['media'], 'position' => 'media-middle', 'wrapper' => 'media-middle']) ?>
<?= $this->element('Frontend.Website/media', ['media' => $content['media'], 'position' => 'media-bottom', 'wrapper' => 'media-bottom']) ?>
