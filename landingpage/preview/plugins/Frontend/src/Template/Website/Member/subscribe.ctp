<main>
    <a class="anchor" name="content"></a>
    <h1><?= $content['headline_signup']; ?></h1>

    <?php if($messages['show']){ ?>
        <div class="main-content">
        <?php if($messages['success'] === true){ ?>
            <div class="message success"><?= $messages['status']; ?></div>
        <?php }else if($messages['success'] === false){ ?>
            <div class="message error"><?= $messages['status']; ?></div>
        <?php } ?>
        </div>
    <?php }else{ ?>

        <div class="main-content"><?= $content['content_signup']; ?></div>

        <!--// Form Start //-->

        <?= $this->CustomForm->create($form); ?>
        <div class="col left">
            <?= $this->CustomForm->input('salutation', ['label' => __d('fe', 'Salutation'), 'empty' => __d('fe', '-- Please select --')]); ?>
            <?= $this->CustomForm->input('firstname', ['label' => __d('fe', 'Firstname')]); ?>
        </div>
        <div class="col right">
            <?= $this->CustomForm->input('lastname', ['label' => __d('fe', 'Lastname')]); ?>
            <?= $this->CustomForm->input('email', ['label' => __d('fe', 'E-Mail')]); ?>
        </div>
        <div class="clear"></div>

        <div class="col left">
            <?= $this->CustomForm->input('username', ['label' => __d('fe', 'Username')]); ?>
        </div>
        <div class="col right">
            <?= $this->CustomForm->input('password', ['type' => 'password', 'label' => __d('fe', 'Password')]); ?>
        </div>
        <div class="clear"></div>

        <?= $this->element('Frontend.Website/privacy') ?>
        <?= $this->element('Frontend.Website/captcha', ['text' => __d('fe', 'Register'), 'options' => [['text' => __d('fe', 'Login'), 'class' => 'option', 'type' => 'login', 'url' => $this->Url->build(['node' => 'node:' . $this->request->params['node']['id'], 'language' => $this->request->params['language']])]]]) ?>
        <?= $this->CustomForm->end(); ?>

        <!--// Form End //-->

    <?php } ?>
</main>
<?= $this->element('Frontend.Website/media', ['media' => $content['media'], 'position' => 'media-top', 'wrapper' => 'media-top']) ?>
<?= $this->element('Frontend.Website/media', ['media' => $content['media'], 'position' => 'media', 'wrapper' => 'media-middle']) ?>
<?= $this->element('Frontend.Website/media', ['media' => $content['media'], 'position' => 'media-bottom', 'wrapper' => 'media-bottom']) ?>
