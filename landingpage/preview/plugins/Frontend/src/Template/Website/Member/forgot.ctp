<main>
    <a class="anchor" name="content"></a>
    <h1><?= $content['headline_forgot']; ?></h1>
    
    <?php if($messages['show']){ ?>
        <div class="main-content">
        <?php if($messages['success'] === true){ ?>
            <div class="message success"><?= $messages['status']; ?></div>
        <?php }else if($messages['success'] === false){ ?>
            <div class="message error"><?= $messages['status']; ?></div>
        <?php } ?>
        </div>
    <?php }else{ ?>
    
        <div class="main-content"><?= $content['content_forgot']; ?></div>
        
        <!--// Form Start //-->
        
        <?= $this->CustomForm->create($form); ?>
        
        <div class="col left">
            <?= $this->CustomForm->input('email', ['label' => __d('fe', 'E-Mail')]); ?>
        </div>
        <div class="col right">
            <?= $this->CustomForm->input('password', ['type' => 'password', 'label' => __d('fe', 'New password')]); ?>
        </div>
        <div class="clear"></div>
        
        <?= $this->element('Frontend.Website/captcha', ['text' => __d('fe', 'Reset password'), 'options' => [['text' => __d('fe', 'Login'), 'class' => 'option', 'type' => 'login', 'url' => $this->Url->build(['node' => 'node:' . $this->request->params['node']['id'], 'language' => $this->request->params['language']])]]]) ?>
        <?= $this->CustomForm->end(); ?>
        
        <!--// Form End //-->
    
    <?php } ?>
</main>
<?= $this->element('Frontend.Website/media', ['media' => $content['media'], 'position' => 'media-top', 'wrapper' => 'media-top']) ?>
<?= $this->element('Frontend.Website/media', ['media' => $content['media'], 'position' => 'media', 'wrapper' => 'media-middle']) ?>
<?= $this->element('Frontend.Website/media', ['media' => $content['media'], 'position' => 'media-bottom', 'wrapper' => 'media-bottom']) ?>