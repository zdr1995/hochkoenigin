<?php if($_SERVER['REMOTE_ADDR'] == '83.175.88.51'){ ?>
    <!-- ######   REQUEST   ###### -->
    <!-- ###### [Website/Request/index.ctp] ###### -->
<? } ?>

<script>
    var flatrateRanges = <?= json_encode($ranges); ?>;
</script>

<?php if (!array_key_exists('check_box', $content) && empty($content['check_box'])) {
    $content['check_box'] = '0';
} ?>

<?php $_content = empty($content['content']) ? 'no-content' : ''; ?>

<main class="page inner" id="request-form">
    <a class="anchor" name="content"></a>
    <div class="content-wrapper <?= $_content ?>">
        <?php switch ($content['check_box']) {
            case '1':
                ?>
                <div class="blank">
                    <h1 class=""><?= $content['headline']; ?></h1>
                    <?php if (array_key_exists('sub_headline', $content) && !empty($content['sub_headline'])) { ?>
                        <h2 class="uppercase strong"><?= $content['sub_headline']; ?></h2>
                    <?php } ?>
                    <span class="blank">
                        <?= $content['content']; ?>
                    </span>
                </div>
                <?php
                break;
            default:
                ?>
                <div class="left">
                    <h1 class=""><?= $content['headline']; ?></h1>
                    <?php if (array_key_exists('sub_headline', $content) && !empty($content['sub_headline'])) { ?>
                        <h2 class="uppercase strong"><?= $content['sub_headline']; ?></h2>
                    <?php } ?>
                </div>
                <div class="right">
                    <?= $content['content']; ?>
                </div>
                <?php
                break;
        } ?>
    </div>
</main>

<?= $this->element('Frontend.Website/media', ['media' => $content['media'], 'position' => 'media-top', 'wrapper' => 'media-top']) ?>

<section class="request-wrapper inner">
    <a class="anchor" name="content"></a>
    <?php if($messages['show']){ ?>
        <?= $this->element('Frontend.Website/form-message', ['messages' => $messages]) ?>
    <?php }else{ ?>

        <!--// Form Start //-->
        <?= $this->CustomForm->create($form); ?>

        <div class="form-cols">
            <div class="col left">
                <?= $this->CustomForm->input('arrival', ['label' => __d('fe', 'Arrival'), 'value' => $arrival_val, 'class' => 'date date-from', 'data-date-callback' => 'validateFlatrate', 'data-date-range' => 'request', 'data-date-min' => date("Y-m-d")]); ?>
            </div>
            <div class="col right">
                <?= $this->CustomForm->input('departure', ['label' => __d('fe', 'Departure'), 'value' => $departure_val, 'class' => 'date date-to', 'data-date-callback' => 'validateFlatrate', 'data-date-range' => 'request', 'data-date-min' => date("Y-m-d")]); ?>
            </div>
        </div>
        <div class="clear"></div>

    	<!-- rooms -->
    	<div class="rooms-label label strong"><?= __d('fe', 'Rooms') ?></div>
    	<div class="rooms-wrap">
    	    <?php if(array_key_exists('rooms', $this->request->data) && is_array($this->request->data['rooms']) && count($this->request->data['rooms']) > 0){ ?>
    	        <?php foreach($this->request->data['rooms'] as $k => $v){ ?>
    	        	<?= $this->element('Frontend.Website/request-form-room', ['rooms' => $rooms, 'packages' => $packages, 'key' => $k, 'value' => $v]) ?>
    	        <?php } ?>
    	    <?php }else{ ?>
    	    	<?= $this->element('Frontend.Website/request-form-room', ['rooms' => $rooms, 'packages' => $packages, 'key' => 0, 'value' => false]) ?>
    	    <?php } ?>
    		<a href="javascript:void(0)" class="room-add button uppercase" onclick="selecotraction(this)"><i class="fas fa-plus"></i>&nbsp;<?= __d('fe', 'add room') ?></a>
        </div>
    	<div class="clear"></div>

        <!-- personal info -->
        <div class="form-cols">
            <div class="col left">
				<?= $this->CustomForm->input('firstname', ['label' => __d('fe', 'Firstname')]); ?>
                <?= $this->CustomForm->input('email', ['label' => __d('fe', 'E-Mail')]); ?>
			</div>
			<div class="col right">
				<?= $this->CustomForm->input('lastname', ['label' => __d('fe', 'Lastname')]); ?>
                <?= $this->CustomForm->input('salutation', ['label' => __d('fe', 'Salutation'), 'empty' => __d('fe', '-- Please select --')]); ?>
			</div>
		</div>

        <!-- extendable fields -->
        <div class="extendable" id="form-field-fold">
            <span class="more-fields"><a href="javascript:void(0)" class="button" id="unfold-form-btn"><i class="fa fa-plus"></i>
                <span class="hide-toggle uppercase"><?= __d('fe', 'more details') ?></span>
                <span class="hide-toggle hidden uppercase"><?= __d('fe', 'less details') ?></span>
            </a></span>
            <div class="form-cols hidden">
                <div class="col left">
                    <?= $this->CustomForm->input('title', ['label' => __d('fe', 'Title')]); ?>
                    <?= $this->CustomForm->input('phone', ['label' => __d('fe', 'Phone')]); ?>
                    <?= $this->CustomForm->input('address', ['label' => __d('fe', 'Address')]); ?>
                </div>
                <div class="col right">
                    <?= $this->CustomForm->input('zip', ['label' => __d('fe', 'ZIP')]); ?>
                    <?= $this->CustomForm->input('city', ['label' => __d('fe', 'City')]); ?>
                    <?= $this->CustomForm->input('country', ['label' => __d('fe', 'Country'), 'empty' => __d('fe', '-- Please select --')]); ?>
                </div>
            </div>
            <div class="hidden">
                <?= $this->CustomForm->input('message', ['label' => __d('fe', 'Message')]); ?>
            </div>
        </div>
        <div class="clear"></div>

        <?= $this->element('Frontend.Website/newsletter') ?>
        <?= $this->element('Frontend.Website/privacy') ?>
        <?= $this->element('Frontend.Website/captcha') ?>
        <?= $this->CustomForm->end(); ?>

        <!--// Form End //-->
    <?php } ?>

</section>

<?= $this->element('Frontend.Website/media', ['media' => $content['media'], 'position' => 'media-bottom', 'wrapper' => 'media-bottom']) ?>
