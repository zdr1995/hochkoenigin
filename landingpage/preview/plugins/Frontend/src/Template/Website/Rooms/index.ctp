<main class="room">

    <a class="anchor" name="content"></a>
    <div>
        <div class="content<?php echo array_key_exists($content['id'], $prices['values']) && count($prices['values'][$content['id']]) > 0 ? '' : ' np'; ?>">
            <table cellpadding="0" cellspacing="0">
                <tr>
                    <td>
                        <h1><?= $content['headline']; ?></h1>
                        <?= $content['content']; ?>
                    </td>
                </tr>
            </table>
        </div>
        <?php if(array_key_exists($content['id'], $prices['values']) && count($prices['values'][$content['id']]) > 0){ ?>
            <span class="min"><?= __d('fe', 'from € %s', number_format($prices['connections'][$content['id']]['ranges']['min']['value'], 2, ',', '.')); ?></span>
        <?php } ?>
    </div>
</main>
<div class="impressions">
    <ul class="viewport bxslider">
        <?php foreach($content['images'] as $img){ ?>
            <li>
                <div class="bxslide" style="background-position: <?= $img['details']['focus'][3]['css']; ?>; background-image: url('<?= $img['details']['seo'][3]; ?>');"></div>
            </li>
        <?php } ?>
    </ul>
    <?php if(count($content['images']) > 1){ ?>
    <div id="bx-pager">
        <?php foreach($content['images'] as $k => $v){ ?>
            <a data-slide-index="<?= $k; ?>" href=""><i class="fas fa-circle" aria-hidden="true"></i><i class="fas fa-circle" aria-hidden="true"></i></a>
        <?php } ?>
    </div>
    <?php } ?>
</div>
<div class="clear"></div>
<?= $this->element('Frontend.Website/request-book-buttons'); ?>
<?php if(array_key_exists($content['id'], $prices['values']) && count($prices['values'][$content['id']]) > 0){ ?>
<section class="room prices">
    <?php foreach($prices['drafts'] as $id => $draft){ ?>
        <?php if(in_array($id, $prices['connections'][$content['id']]['used']['drafts'])){ ?>
            <h2 class="h1-like"><?= $draft['translations']['title']; ?></h2>
            <div class="price-table options-<?= count($prices['drafts'][$id]['used']['options']); ?>">

                <!-- headline -->
                <div class="headline line">
                    <div class="season">&nbsp;</div>
                    <div class="options">
                        <?php foreach($prices['options'] as $option => $name){ ?>
                            <?php if(in_array($option, $prices['drafts'][$id]['used']['options'])){ ?>
                            <?php
                                switch($option){
                                    case "day":
                                        $title = __d('fe', 'Daily price');
                                        break;
                                    case "short":
                                        $title = __d('fe', 'Short Stay') . '*';
                                        break;
                                    case "week":
                                        $title = __d('fe', '7 days');
                                        break;
                                }
                            ?>
                            <div class="option"><?= $title; ?></div>
                            <?php } ?>
                        <?php } ?>
                        <div class="clear"></div>
                    </div>
                    <div class="clear"></div>
                </div>

                <!-- prices -->
                <?php foreach($prices['seasons'] as $season){ ?>
                    <?php if(array_key_exists($season['id'], $prices['values'][$content['id']]) && array_key_exists($id, $prices['values'][$content['id']][$season['id']])){ ?>
                    <div class="line">
                        <div class="season">
                            <?php foreach($season['times'] as $time){ ?>
                            <div class="time"><?= date("d.m.Y", $time['from']) . ' - ' . date("d.m.Y", $time['to']); ?></div>
                            <?php } ?>
                        </div>
                        <div class="options">
                            <?php foreach($prices['options'] as $option => $name){ ?>
                                <?php if(in_array($option, $prices['drafts'][$id]['used']['options'])){ ?>
                                <div class="option"><?php echo array_key_exists($option, $prices['values'][$content['id']][$season['id']][$id]) ? '<span>&euro;</span>' . number_format($prices['values'][$content['id']][$season['id']][$id][$option], 2, ',', '.') . '<div class="clear"></div>' : '--'; ?></div>
                                <?php } ?>
                            <?php } ?>
                            <div class="clear"></div>
                        </div>
                        <div class="clear"></div>
                    </div>
                    <?php } ?>
                <?php } ?>
            </div>
        <?php } ?>
    <?php } ?>
    <?php if(is_array($content['info']) && array_key_exists(0, $content['info']) && is_array($content['info'][0]) && array_key_exists('details', $content['info'][0])){ ?>
    <div class="footnote">
        <?= $content['info'][0]['details']['textblock']; ?>
    </div>
    <?php } ?>
</section>
<?= $this->element('Frontend.Website/special-children', ['content' => $content]) ?>
<?= $this->element('Frontend.Website/request-book-buttons'); ?>
<?php } ?>
<?= $this->element('Frontend.Website/media', ['media' => $content['media'], 'position' => 'media-top', 'wrapper' => 'media-top']) ?>
<?= $this->element('Frontend.Website/media', ['media' => $content['media'], 'position' => 'media-middle', 'wrapper' => 'media-middle']) ?>
<?= $this->element('Frontend.Website/media', ['media' => $content['media'], 'position' => 'media-bottom', 'wrapper' => 'media-bottom']) ?>
