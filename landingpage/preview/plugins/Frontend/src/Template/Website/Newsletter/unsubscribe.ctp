<main class="page">
	<?= $this->element('Frontend.Website/media', ['media' => $content['media'], 'position' => 'left', 'wrapper' => 'left']) ?>
	<div class="page-content-wrap">
		<a class="anchor" name="content"></a>
		<div class="page-content-headline">
			<div class="page-content-icon"></div>
		    <h1><?= $content['headline']; ?></h1>
		</div>
	    <div class="content">
	        <?php if($messages['success'] === true){ ?>
	            <div class="message success"><?= $messages['status']; ?></div>
	        <?php }else if($messages['success'] === false){ ?>
	            <div class="message error"><?= $messages['status']; ?></div>
	        <?php } ?>
	        <?= $content['content']; ?>
	        <div class="clear"></div>
	    </div>
	</div>
</main>

<div class="form-wrapper">
	<div class="inner">
		<?php if($messages['show']){ ?>
	        <?= $this->element('Frontend.Website/form-message', ['messages' => $messages]) ?>
	    <?php }else{ ?>
	        <!--// Form Start //-->
	        <?= $this->CustomForm->create($form); ?>
	        <?= $this->CustomForm->input('email', ['label' => __d('fe', 'E-Mail')]); ?>    
	        <?= $this->element('Frontend.Website/captcha', ['text' => __d('fe', 'Unsubscribe'), 'options' => [['text' => __d('fe', 'Subscribe'), 'class' => 'option', 'type' => 'unsubscribe', 'url' => $this->Url->build(['node' => 'node:' . $this->request->params['node']['id'], 'language' => $this->request->params['language']])]]]) ?>
	        <?= $this->CustomForm->end(); ?>
	        <!--// Form End //-->
		<?php } ?>
	</div>
</div>

<?= $this->element('Frontend.Website/media', ['media' => $content['media'], 'position' => 'bottom', 'wrapper' => 'bottom']) ?>