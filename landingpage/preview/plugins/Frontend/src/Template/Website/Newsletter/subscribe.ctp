<main class="page">
	<?= $this->element('Frontend.Website/media', ['media' => $content['media'], 'position' => 'left', 'wrapper' => 'left']) ?>
	<div class="page-content-wrap">
		<a class="anchor" name="content"></a>
		<div class="page-content-headline">
			<div class="page-content-icon"></div>
		    <h1><?= $content['headline']; ?></h1>
		</div>
	    <div class="content">
	        <?php if($messages['success'] === true){ ?>
	            <div class="message success"><?= $messages['status']; ?></div>
	        <?php }else if($messages['success'] === false){ ?>
	            <div class="message error"><?= $messages['status']; ?></div>
	        <?php } ?>
	        <?= $content['content']; ?>
	        <div class="clear"></div>
	    </div>
	</div>
</main>

<div class="form-wrapper">
	<div class="inner">
		<?php if($messages['show']){ ?>
	        <?= $this->element('Frontend.Website/form-message', ['messages' => $messages]) ?>
	    <?php }else{ ?>
	        <!--// Form Start //-->
	        <?= $this->CustomForm->create($form); ?>
	        <div class="col left">
	            <?= $this->CustomForm->input('salutation', ['label' => __d('fe', 'Salutation'), 'empty' => __d('fe', '-- Please select --')]); ?>
	            <?= $this->CustomForm->input('firstname', ['label' => __d('fe', 'Firstname')]); ?>
	        </div>
	        <div class="col right">
	            <?= $this->CustomForm->input('lastname', ['label' => __d('fe', 'Lastname')]); ?>
	            <?= $this->CustomForm->input('email', ['label' => __d('fe', 'E-Mail')]); ?>
	        </div>
	        <div class="clear"></div>
	        <?php if(count($interests) > 0){ ?>
                <?= $this->CustomForm->input('interests', ['templateVars' => ['cc' => 'multiple-checkbox'], 'label' => __d('fe', 'Interests'), 'multiple' => 'checkbox']); ?>
	            <div class="clear"></div>
	        <?php } ?>
            <?= $this->element('Frontend.Website/privacy') ?>
	        <?= $this->element('Frontend.Website/captcha', ['text' => __d('fe', 'Subscribe'), 'options' => [['text' => __d('fe', 'Unsubscribe'), 'class' => 'option', 'type' => 'subscribe', 'url' => $this->Url->build(['node' => 'node:' . $this->request->params['node']['id'], 'language' => $this->request->params['language'], 'extend' => [__d('fe', 'unsubscribe')]])]]]) ?>
	        <?= $this->CustomForm->end(); ?>
	        <!--// Form End //-->
		<?php } ?>
	</div>
</div>

<?= $this->element('Frontend.Website/media', ['media' => $content['media'], 'position' => 'bottom', 'wrapper' => 'bottom']) ?>
