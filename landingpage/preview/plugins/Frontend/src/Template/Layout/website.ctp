<?php use Cake\Core\Configure; ?>
<!doctype html>
<!--[if lt IE 9 ]><html version="HTML+RDFa 1.1" lang="<?php echo $this->request->params['language']; ?>" class="no-js ie"><![endif]-->
<!--[if IE 9 ]><html version="HTML+RDFa 1.1" lang="<?php echo $this->request->params['language']; ?>" class="no-js ie9"><![endif]-->
<!--[if (gt IE 9)|!(IE)]><!--><html version="HTML+RDFa 1.1" lang="<?php echo $this->request->params['language']; ?>" class="no-js"><!--<![endif]-->
    <head>
        <meta name="viewport" content="width=device-width,initial-scale=1.0,maximum-scale=1">
        <?= $this->Html->charset() ?>
        <title><?= isset($title) ? strip_tags($title) : $this->fetch('title') ?></title>
        <?= $this->element('Frontend.Website/meta', ['seo' => $seo, 'content' => $content]) ?>
        <?= $this->fetch('meta') ?>

        <?= $this->Html->css('Frontend.reset.css') ?>
        <?= $this->Html->css('Frontend.animate.css') ?>
        <?= $this->Html->css('Frontend.video-js.css') ?>
        <?= $this->Html->css('Frontend.styles.css') ?>
        <?= $this->Html->css('Frontend.mobile.css') ?>
        <?= $this->fetch('css') ?>

        <link href="https://fonts.googleapis.com/css?family=PT+Sans:400,400i,700,700i&display=swap&subset=latin-ext" rel="stylesheet">
        <link rel="stylesheet" type="text/css" href="/frontend/css/weather-icons.min.css" />

        <script type="text/javascript">
            if( /Android|webOS|iPhone|iPod|iPad|BlackBerry/i.test(navigator.userAgent))
                 document.write('<link rel="stylesheet" type="text/css" media="only screen and (max-device-width: 1200px)" href="/frontend/css/mobile.css" />');
        </script>

        <link rel="stylesheet" href="https://pro.fontawesome.com/releases/v5.3.1/css/all.css" integrity="sha384-9ralMzdK1QYsk4yBY680hmsb4/hJ98xK3w0TIaJ3ll4POWpWUYaA2bRjGGujGT8w" crossorigin="anonymous">  <!-- you have to add page domain to our fontawesome account untill the version goes full-online -->

        <script>var dataLayer = []</script>
    	<?= /* Tagmanager - head-code */ Configure::read('config.tracking.tagmanager-head'); ?>
        <script src='https://www.google.com/recaptcha/api.js'></script>

    </head>

    <?php $body_class = isset($this->request->params['route']) && $this->request->params['route'] == Configure::read('config.default.home.0.details.node.route') ? 'home' : 'not-home'; ?>

    <body class="<?= isset($hint) && $hint === true ? 'cookie-hint ' : '' ?><?= $body_class . ' ' . $this->request->params['language']; ?>">

    	<?= /* Tagmanager - body-code */ Configure::read('config.tracking.tagmanager-body'); ?>

    	<!-- <div class="loading-overlay">
    		<div class="loading-overlay-inner">
    			<?php if(isset($this->request->params['route']) && $this->request->params['route'] == Configure::read('config.default.home.0.details.node.route')){ ?>
    				<div class="loading-logo"></div>
    			<?php } ?>
    			<div class="loading-spinner"></div>
    		</div>
    	</div> -->

        <div id="wrapper">
            <?= $this->element('Frontend.Website/cookie', ['hint' => $hint]) ?>
            <?= $this->element('Frontend.Website/header', ['menu' => $menu]) ?>
            <?= $this->fetch('content') ?>
            <?= $this->element('Frontend.Website/footer') ?>
        </div>

        <div id="datepicker-container"></div>

        <?= $this->Html->css('Frontend.pickadate/default.css') ?>
        <?= $this->Html->css('Frontend.pickadate/default.date.css') ?>
        <?= $this->element('Frontend.Website/js', ['tracking' => isset($tracking) ? $tracking : false]) ?>
        <?= $this->element('Frontend.slideshow') ?>

        <script>
            //scroll to content
            <?php if(isset($this->request->params['node']) && array_key_exists('jump', $this->request->params['node']) && $this->request->params['node']['jump'] == 1){ ?>
            	$(window).load(function(){
            		scrollToContent();
            	});
            <?php } ?>

            //Browser warning
    		var $buoop = {vs:{i:13,f:-4,o:-4,s:8,c:-4},api:4};
    		function $buo_f(){
    		 var e = document.createElement("script");
    		 e.src = "//browser-update.org/update.min.js";
    		 document.body.appendChild(e);
    		};
    		try {document.addEventListener("DOMContentLoaded", $buo_f,false)}
    		catch(e){window.attachEvent("onload", $buo_f)}
		</script>

    </body>
</html>
