<?php
// ini_set('display_errors', 1);
// ini_set('display_startup_errors', 1);
// error_reporting(E_ALL);
include('vendor/recaptcha/src/autoload.php');
$recaptcha = new \ReCaptcha\ReCaptcha('6LcW-58UAAAAACrNgoWi7maTW2ZT6mVIKMknomhe');
$resp = $recaptcha->verify($_POST['response'], $_POST['remoteip']);
if ($resp->isSuccess()) {
	$return = array(
		'success' => true,
		'recaptchaConfirm' => 'all-clear',
	);
	echo json_encode($return);
	exit;
} else {
	$errors = $resp->getErrorCodes();
	$return = array(
		'success' => false,
		'errors' => $errors,
	);
	echo json_encode($return);
	exit;
}
