<?php
//config file for medienjaeger Landingpage
//(C) Medienjaeger 2014

$cfg = array();


//Deutsch
$cfg['de'] = array(
	'general' => array(
		'form-recipient' => $_SERVER['REMOTE_ADDR'] == '83.175.88.51' ? 'coders@medienjaeger.at' : 'urlaub@hochkoenigin.com',
		'htmltitle' => 'die HOCHK&Ouml;NIGIN - magic mountain resort',
		'phone' => array('plain' => '004365847447', 'nice' => '+43 6584 7447'),
		'email' => 'urlaub@hochkoenigin.com',
		'email_temporary' => 'urlaub@hochkoenigin.com',
		'website' => 'www.hochkoenigin.com',
		'company-name' => 'die Hochk&ouml;nigin',
		'company-street' => 'Hochk&ouml;nigstra&szlig;e 27',
		'company-city' => 'A-5761 Maria&nbsp;Alm',
		'company-country' => '&Ouml;STERREICH',
		'name' => 'Familie Marina & Manfred Sch&ouml;negger',
		'gmap' => 'https://www.google.com/maps/dir//die+HOCHK%C3%96NIGIN+Magic+Mountain+Resort,+Hochk%C3%B6nigstra%C3%9Fe+27,+5761+Maria+Alm/@47.4044595,12.9022839,21z/data=!4m8!4m7!1m0!1m5!1m1!1s0x4776e3c45de45c3b:0x963aa37920f93d1f!2m2!1d12.902192!2d47.404502?hl=de',
		'privacy' => '/privacy',
		'imprint' => '/imprint',
		'gtc' => '/gtc',
		'facebook' => 'https://www.facebook.com/hochkoenigin/',
		'instagram' => 'https://www.instagram.com/diehochkoenigin/',
	),
	'header' => array('title' => 'header', 'path' => '/img/header.png'),
	'images' => array(
		array('title' => 'logo', 'path' => '/img/logo.png'),
		array('title' => 'shadow', 'path' => '/img/shadow.png'),
		array('title' => 'comming_soon', 'path' => '/img/comming_soon.png'),
		array('title' => 'icon', 'path' => '/img/icon.png'),
		array('title' => 'gold_line', 'path' => '/img/gold_line.png'),
		array('title' => 'pattern', 'path' => '/img/pattern.png'),
		array('title' => 'phone', 'path' => '/img/phone.svg'),
		array('title' => 'email', 'path' => '/img/email.svg'),
		array('title' => 'facebook', 'path' => '/img/facebook.jpg'),
		array('title' => 'instagram', 'path' => '/img/instagram.jpg'),
		array('title' => 'remove', 'path' => '/img/remove.svg'),
	),
	'content' => array(
		'headline_1' => '<h2 class="headline_1">die&nbsp;HOCHK&Ouml;NIGIN – </h2>',
		'headline_2' => '<h1 class="headline_2"><strong>Magic Mountain Resort</strong> – Coming&nbsp;soon</h1>',
		'text_1_l' => '<span class="text_1_l"><strong>Neu, au&szlig;ergew&ouml;hnlich anders, lebensfroh, bunt - einfach magisch so wird „die HOCHK&Ouml;NIGIN“ ab Dezember 2019 in Maria Alm thronen und ihren G&auml;sten einen k&ouml;niglichen Empfang bereiten.</strong> Aus dem Thalerhof wird eine Majest&auml;t unter den Hotels, in der sich jeder Gast als K&ouml;nig f&uuml;hlt.</br></br> Die ber&uuml;hmte <strong>Herzlichkeit und Gastgeber-Qualit&auml;ten des ehemaligen Thalerhofs bleiben selbstverst&auml;ndlich bestehen</strong> und werden Sie auch in der „HOCHK&Ouml;NIGIN“ begeistern.</span>',
		'text_1_r' => '<span class="text_1_r"><strong>40 neue und traumhafte Suite-Zimmer</strong> mit wundersch&ouml;nen und „aussichtsreichen“ Loggias, ein <strong>SKY-SPA</strong>, dessen Ausblick staunen l&auml;sst, eine finnische <strong>Eventsauna sowie einer BIO-Sauna mit Panorama-"Meer"-Blick, einem Outdoor-Infinitypool und</strong> Whirlpool auf einer Gesamtl&auml;nge von 22 m mit gigantischem Blick, Ski IN & OUT in einer der gr&ouml;&szlig;ten Skiregion Europas – WINTERPARADIES HOCHK&Ouml;NIG, ein <strong>spannendes Aktivprogramm, ein Park mit magischen Relaxzonen im Gr&uuml;nen</strong> und vieles mehr laden ab Dezember 2019 zum Entspannen, Wohlf&uuml;hlen und den Alltag hinter sich lassen ein.',
		'text_2' => '<span class="text_2">Wir freuen uns, Sie in der magischen, neuen „HOCHK&Ouml;NIGIN“ willkommen zu hei&szlig;en!</span>',
		'text_3' => '<span class="text_3">Ihre Familie H&ouml;rl</span>',
	),
	'gallery' => array(
		array('title' => 'image_1', 'path' => '/img/gallery/bild_1.png'),
		array('title' => 'image_2', 'path' => '/img/gallery/bild_2.png'),
		array('title' => 'image_3', 'path' => '/img/gallery/bild_3.png'),
		array('title' => 'image_4', 'path' => '/img/gallery/bild_4.png'),
		array('title' => 'image_5', 'path' => '/img/gallery/bild_5.png'),
		array('title' => 'image_6', 'path' => '/img/gallery/bild_6.png'),
	),
	'offer' => [
		'headline' => '"BE THE FIRST" - EINMALIGES NEUERÖFFNUNGS-ANGEBOT!',
		'image' => '<img src="/img/offer.png" alt="gleich buchen" />',
		'text' => '
			<p>Genießen Sie Ihren Urlaub doch mal anders: Etwas lebensfroher, bunter und magischer! Lernen Sie unser magic mountain resort****s ganz exklusiv kennen, entdecken Sie alles Neue und profitieren Sie vom Eröffnungsangebot:</p>
			<ul>
				<li><strong>GENUSSPENSION PLUS inklusive:</strong> Schlemmen & Genießen – egal wann. Genießen Sie unser reichhaltiges Schlemmer-Frühstücksbuffet, Late Lunch inkl. Suppe & feinen Salaten und Salzburger Jause, unsere Süße Verführung sowie die Feinschmeckermenüs am Abend. So viel Urlaubsfreude im Preis inklusive!</li>
				<li>Starten Sie ganz entspannt in Ihren Urlaub mit einem <strong>Wellnessgutschein im Wert von € 50,-</strong></li>
				<li>Alle <strong>magic mountain Inklusivleistungen</strong></li>
				<li><strong>KOSTENLOSE HOCHK&Ouml;NIG CARD:</strong> Diese Card beinhaltet einen gratis GoPro Verleih, kostenlosen Eintritt in verschiedene Museen, sowie zahlreiche Ermäßigungen auf diverse Aktivitäten in der Region</li>
				<li><strong>2.000 m² pures Wellnessvergn&uuml;gen</strong> im dreistöckigen Nature SPA mit Infinity Outdoor-Pool, Relax Whirlpool, Outdoor Living Room mit Outdoor Cinema, Finnische Eventsauna, ...</li>
			</ul>
			<table align="center">
				<tr>
					<td><strong class="red">4 Nächte inkl. Halbpension</strong></td>
					<td><strong class="red">ab &euro; 524,-</strong></td>
				</tr>
				<tr>
					<td><strong class="red">7 Nächte inkl. Halbpension</strong></td>
					<td><strong class="red">ab &euro; 875,-</strong></td>
				</tr>
			</table>',
		'button' => 'ANFRAGEN',
	],
	'prices' => [
		'headline' => 'Preisliste',
		'text' => '<a href="/files/pricelist.pdf" target="_blank"><img src="/img/translatable/prices_1_de.jpg" alt="Preisliste 1" /></a><br>
		<br>
		<a href="/files/pricelist.pdf" target="_blank"><img src="/img/translatable/prices_2_de.jpg" alt="Preisliste 2" /></a><br>
		<br>',
		'button' => '<a href="/files/pricelist.pdf" class="button" target="_blank">Preisliste herunterladen</a>',
	],
	'404' => [
		'headline' => 'Seite nicht gefunden',
		'text' => 'Diese Seite wurde nicht gefunden.',
	],
	'imprint' => [
		'headline' => 'Impressum',
		'text' => '
			<strong>Hochk&ouml;nigin GmbH & Co KG</strong><br />
			Nach &sect; 5 Abs. 1 des &ouml;sterreichischen eCommerce-Gesetz &lpar;ECG&rpar; und &sect; 24 Mediengesetz geben wir hiermit uns als den Betreiber dieser Internetseite bekannt:<br><br>
			Hochk&ouml;nigin GmbH & Co KG&nbsp;<br/>
			<a href="https://www.google.com/maps/dir//Hochk%C3%B6nigstra%C3%9Fe+27,+5761+Alm/@47.4045275,12.9001605,17z/data=!4m8!4m7!1m0!1m5!1m1!1s0x4776e3c45e11e5ab:0x2d9d226b9d8da6b7!2m2!1d12.9023492!2d47.4045275" target="_blank">Hochk&ouml;nigstra&szlig;e 27</br>
			A-5761 Maria&nbsp;Alm</a><br/>
			Tel&period;&colon; <a href="004365847447-0">+43/ &lpar;0&rpar;6584/7447-0</a><br>
			Fax&colon; <a href="tel:004365847447-17">+43/ &lpar;0&rpar;6584/7447-17</a><br>
			<a href="https://www.hochkoenigin.com" target="_blank">www.hochkoenigin.com</a><br>
			<a href="mailto:urlaub@hochkoenigin.com">urlaub@hochkoenigin.com</a><br><br><br>
			<strong>Firmenname:</strong> Hochk&ouml;nigin GmbH & Co KG<br>
			<strong>Gesch&auml;ftsf&uuml;hrer:</strong> H&ouml;rl Josef<br>
			<strong>Selbst gew&auml;hlte Unternehmensbezeichnung:</strong> die HOCHK&Ouml;NIGIN<br>
			<strong>Aufsichtsbeh&ouml;rde:</strong> Bezirkshauptmannschaft Zell am See<br>
			Mitglied der Wirtschaftskammer Salzburg<br>
			<strong>Fachgruppe:</strong> Hotellerie<br>
			<strong>Firmenbuchnummer:</strong> FN: 28793<br>
			<strong>UID:</strong> ATU 33420508<br>
			<h3>Konzept, Screendesign und technische Umsetzung:</h3>
			Werbeagentur medien-j&auml;ger GmbH<br>
			E-Mail: <a href="mailto:office@medienjaeger.at">office@medienjaeger.at</a><br>
			<a href="https://www.medienjaeger.at" target="_blank">www.medienjaeger.at</a><br>
			<h3>Inhalt des Onlineangebotes</h3>
			Hochk&ouml;nigin GmbH & Co KG &uuml;bernimmt keinerlei Gew&auml;hr f&uuml;r die Aktualit&auml;t, Korrektheit, Vollst&auml;ndigkeit oder Qualit&auml;t der bereitgestellten Informationen. Haftungsanspr&uuml;che gegen Hochk&ouml;nigin GmbH & Co KG welche sich auf Sch&auml;den materieller oder ideeller Art beziehen, die durch die Nutzung oder Nichtnutzung der dargebotenen Informationen bzw. durch die Nutzung fehlerhafter und unvollst&auml;ndiger Informationen verursacht wurden, sind grunds&auml;tzlich ausgeschlossen, sofern seitens Hochk&ouml;nigin GmbH & Co KG kein nachweislich vors&auml;tzliches oder grob fahrl&auml;ssiges Verschulden vorliegt. Alle Angebote sind freibleibend und unverbindlich. Hochk&ouml;nigin GmbH & Co KG beh&auml;lt es sich ausdr&uuml;cklich vor, Teile der Seiten oder das gesamte Angebot ohne gesonderte Ank&uuml;ndigung zu ver&auml;ndern, zu erg&auml;nzen, zu l&ouml;schen oder die Ver&ouml;ffentlichung zeitweise oder endg&uuml;ltig einzustellen.<br>
			<h3>Verweise und Links</h3>
			Bei direkten oder indirekten Verweisen auf fremde Internetseiten, sogenannten "Links", die au&szlig;erhalb des Verantwortungsbereiches des Hochk&ouml;nigin GmbH & Co KG liegen, w&uuml;rde eine Haftungsverpflichtung ausschlie&szlig;lich in dem Fall in Kraft treten, in dem Hochk&ouml;nigin GmbH & Co KG von den Inhalten Kenntnis hat und selbiger technisch m&ouml;glich und zumutbar w&auml;re, die Nutzung im Falle rechtswidriger Inhalte zu verhindern. Hochk&ouml;nigin GmbH & Co KG erkl&auml;rt hiermit ausdr&uuml;cklich, dass zum Zeitpunkt der Linksetzung die entsprechenden verlinkten Seiten frei von illegalen Inhalten waren. Auf die aktuelle und zuk&uuml;nftige Gestaltung, die Inhalte oder die Urheberschaft der gelinkten, bzw. verkn&uuml;pften Seiten hat Hochk&ouml;nigin GmbH & Co KG keinerlei Einfluss. Deshalb distanziert sich Hochk&ouml;nigin GmbH & Co KG hiermit ausdr&uuml;cklich von allen Inhalten aller gelinkten, bzw. verkn&uuml;pften Seiten, die nach der Linksetzung ver&auml;ndert wurden. Diese Feststellung gilt f&uuml;r alle innerhalb des eigenen Internetangebotes gesetzten Links und Verweise sowie f&uuml;r Fremdeintr&auml;ge in von dem Unternehmen eingerichteten Services wie G&auml;steb&uuml;chern, Diskussionsforen, Mailinglisten und &auml;hnlichem. F&uuml;r illegale, fehlerhafte oder unvollst&auml;ndige Inhalte und insbesondere f&uuml;r Sch&auml;den, die aus der Nutzung oder Nichtnutzung solcherart dargebotener Informationen entstehen, haftet allein der Anbieter der Seite, auf welche verwiesen wurde, nicht derjenige, der &uuml;ber Links auf die jeweilige Ver&ouml;ffentlichung lediglich verweist.<br>
			<h3>Urheber- und Kennzeichenrecht</h3>
			<strong>Bildnachweis:</strong><br>
			Zuchna Visualisierung, Hochk&ouml;nigin GmbH & Co KG, TVB Maria Alm, Salzburger Land Tourismus, fotolia.de, Archiv Gro&szlig;glockner Hochalpenstra&szlig;en AG, Krimmler Wasserf&auml;lle, Salzburger Land, &Ouml;sterreich Werbung, Garger Wolfgang, Fotografie G&uuml;nter Standl<br>
			Huber Fotografie - Huber Michael<br><br>
			Hochk&ouml;nigin GmbH & Co KG ist bestrebt, in allen Publikationen die Urheberrechte der verwendeten Grafiken, Tondokumente, Videosequenzen und Texte zu beachten, von ihr selbst erstellte Grafiken, Tondokumente, Videosequenzen und Texte zu nutzen oder auf lizenzfreie Grafiken, Tondokumente, Videosequenzen und Texte zur&uuml;ckzugreifen. Alle innerhalb des Internetangebotes genannten und ggf. durch Dritte gesch&uuml;tzten Marken- und Warenzeichen unterliegen uneingeschr&auml;nkt den Bestimmungen des jeweils g&uuml;ltigen Kennzeichenrechts und den Besitzrechten der jeweiligen eingetragenen Eigent&uuml;mer. Allein aufgrund der blo&szlig;en Nennung ist nicht der Schluss zu ziehen, dass Markenzeichen nicht durch Rechte Dritter gesch&uuml;tzt sind! Das Copyright f&uuml;r ver&ouml;ffentlichte, von Hochk&ouml;nigin GmbH & Co KG selbst erstellte Objekte bleibt allein beim Besitzer der Seiten. Eine Vervielf&auml;ltigung oder Verwendung solcher Grafiken, Tondokumente, Videosequenzen und Texte in anderen elektronischen oder gedruckten Publikationen ist ohne ausdr&uuml;ckliche Zustimmung seitens Hochk&ouml;nigin GmbH & Co KG nicht gestattet.<br>
			<h3>Copyright/Haftung</h3>
			Im Hinblick auf die technischen Eigenschaften des Internet kann keine Gew&auml;hr f&uuml;r die Authentizit&auml;t, Richtigkeit und Vollst&auml;ndigkeit, der im Internet zur Verf&uuml;gung gestellten Informationen &uuml;bernommen werden. Es wird auch keine Gew&auml;hr f&uuml;r die Verf&uuml;gbarkeit oder den Betrieb der gegenst&auml;ndlichen Website und ihrer Inhalte &uuml;bernommen. Jede Haftung f&uuml;r unmittelbare, mittelbare oder sonstige Sch&auml;den, unabh&auml;ngig von deren Ursachen, die aus der Benutzung oder Nichtverf&uuml;gbarkeit der Daten und Informationen dieser Website erwachsen, wird, soweit rechtlich zul&auml;ssig, ausgeschlossen. Der Inhalt dieser Website ist urheberrechtlich gesch&uuml;tzt. Die Informationen sind nur f&uuml;r die pers&ouml;nliche Verwendung bestimmt. Jede weitergehende Nutzung insbesondere die Speicherung in Datenbanken, Vervielf&auml;ltigung und jede Form von gewerblicher Nutzung sowie die Weitergabe an Dritte auch in Teilen oder in &uuml;berarbeiteter Form ohne Zustimmung der jeweiligen Organisation ist untersagt. Jede Einbindung einzelner Seiten unseres Angebotes in fremde Frames ist zu unterlassen.<br>
			<h3>Sonstiges</h3>
			Sofern Teile oder einzelne Formulierungen dieses Textes der geltenden Rechtslage nicht, nicht mehr oder nicht vollst&auml;ndig entsprechen sollten, bleiben die &uuml;brigen Teile des Dokumentes in ihrem Inhalt und ihrer G&uuml;ltigkeit davon unber&uuml;hrt.<br>
			<h3>Online-Streitbeilegung</h3>
			Seit dem 9.1.2016 gilt die EU-Verordnung &uuml;ber Online-Streitbeilegung in Verbraucherangelegenheiten &lpar;Nr. 524/2013&rpar;. Streitigkeiten zwischen Verbrauchern und H&auml;ndlern im Zusammenhang von Online-Kaufvertr&auml;gen oder Online-Dienstleistungsvertr&auml;gen k&ouml;nnen &uuml;ber folgende Online-Plattform beigelegt werden. <a href="https://ec.europa.eu/consumers/odr/" target="_blank">https://ec.europa.eu/consumers/odr/</a><br><br>
			Uns ist die Sicherheit Ihrer Daten wichtig. Wir verwenden sie ausschlie&szlig;lich zweckgebunden und geben sie nur dann an Dritte weiter, sofern diese in unserem ausdr&uuml;cklichen Auftrag zur Zweckerf&uuml;llung beitragen. Nach EU-Datenschutz-Grundverordnung &lpar;DSGVO&rpar; und nach dem Bundesgesetzblatt 2017/120 &lpar;LINK&rpar; der Republik &Ouml;sterreich mit endg&uuml;ltiger Wirksamkeit 25.5.2018 haben Nutzer das Recht, auf Antrag unentgeltlich Auskunft &uuml;ber die personenbezogenen Daten zu erhalten, die wir &uuml;ber sie gespeichert haben. Zus&auml;tzlich hat jeder Kunde bzw. Nutzer jederzeit das Recht auf Berichtigung unrichtiger Daten, Sperrung und L&ouml;schung seiner personenbezogenen Daten, soweit dem keine gesetzliche Aufbewahrungs- oder Meldepflichtpflicht entgegensteht.
		',
	],
	'privacy' => [
		'headline' => 'Datenschutz',
		'text' => '
			<h3>Erkl&auml;rung zur Informationspflicht</h3>
			Der Schutz Ihrer pers&ouml;nlichen Daten ist uns ein besonderes Anliegen&period; Wir verarbeiten Ihre Daten daher ausschlie&szlig;lich auf Grundlage der gesetzlichen Bestimmungen &lpar;DSGVO&comma; TKG 2003&rpar;&period; In diesen Datenschutzinformationen informieren wir Sie &uuml;ber die wichtigsten Aspekte der Datenverarbeitung im Rahmen unserer Website&period;<br><br>
			<h3>Allgemeines</h3>
			Durch die Nutzung dieser Website erkl&auml;ren Sie sich mit der in dieser Datenschutzerkl&auml;rung dargelegten Verwendung personenbezogener und nichtpersonenbezogener Daten in der nachstehend beschriebenen Art und Weise und zu den nachstehend benannten Zwecken einverstanden&period;
			Sie k&ouml;nnen Ihre Einwilligung jederzeit &lpar;auch teilweise&rpar; durch eine E-Mail an <a href="mailto:urlaub@hochkoenigin.com">urlaub@hochkoenigin.com</a> widerrufen&period; Dies kann die Funktionalit&auml;t der auf dieser Website angebotenen Inhalte beeintr&auml;chtigen&comma; insbesondere weil manche Datenanwendungen zur Erbringung der von Ihnen angeforderten Leistung erforderlich sind&period;
			Des Weiteren haben Sie ein Recht auf Auskunft &uuml;ber die Sie betreffenden personenbezogenen Daten&comma; auf Daten&uuml;bertragbarkeit&comma; auf Berichtigung oder L&ouml;schung personenbezogener Daten sowie auf Einschr&auml;nkung der oder Widerspruch gegen die Verarbeitung Ihrer personenbezogenen Daten&period; Sie k&ouml;nnen diese Rechte durch eine E-Mail an <a href="mailto:urlaub@hochkoenigin.com">urlaub@hochkoenigin.com</a> aus&uuml;ben&period;
			Weiters haben Sie ein Recht auf Beschwerde bei der Datenschutzbeh&ouml;rde<br>
			in &Ouml;sterreich: <a href="https://www.dsb.gv.at" target="_blank">https://www.dsb.gv.at</a><br>
			in Deutschland: <a href="https://www.bfdi.bund.de/DE/Infothek/Anschriften_Links/anschriften_links-node.html" target="_blank">https://www.bfdi.bund.de/DE/Infothek/Anschriften_Links/anschriften_links-node.html</a><br>
			in Italien: <a href="https://www.garanteprivacy.it" target="_blank">https://www.garanteprivacy.it</a><br>
			Nationale Datenschutzbeh&ouml;rden: <a href="https://www.dsb.gv.at/links" target="_blank">https://www.dsb.gv.at/links</a><br>
			Personenbezogene Daten &lpar;z&period;B&period; Name&comma; E-Mail-Adresse&comma; Telefonnummer&rpar; werden vom HOCHK&Ouml;NIGIN GMBH & CO KG nur in rechtm&auml;&szlig;iger Weise &lpar;insbesondere zur Erf&uuml;llung vertraglicher Pflichten oder auf Basis Ihrer Einwilligung&rpar; verarbeitet und &uuml;bermittelt&period;<br><br>
			<h3>Kontaktaufnahme</h3>
			Nehmen Sie mit dem HOCHK&Ouml;NIGIN GMBH & CO KG per E-Mail und/oder Kontaktformular Kontakt auf&comma; werden Ihre dort eingegebenen personenbezogenen Daten&comma; insbesondere Name und E-Mail-Adresse&comma; zwecks Bearbeitung der Anfrage sowie f&uuml;r allf&auml;llige Anschlussfragen 3 Jahre gespeichert&period; Diese Datenverarbeitung ist somit gem&auml;&szlig; § 8 Abs 3 Z 4 DSG sowie gem&auml;&szlig; Art 6 Abs 1 lit b DSGVO f&uuml;r die Vertragserf&uuml;llung erforderlich&period;
			Wenn Sie in den daf&uuml;r vorgesehenen Feldern Kommentare oder sonstige Beitr&auml;ge hinterlassen&comma; wird Ihre IP-Adresse f&uuml;r maximal sechs Monate gespeichert&period; Diese Speicherung hat den Zweck&comma; bei etwaigen Rechtsverst&ouml;&szlig;en den Verantwortlichen identifizieren zu k&ouml;nnen&period; Sie liegt daher gem&auml;&szlig; § 8 Abs 1 Z 4 DSG sowie Art 6 Abs 1 lit f DSGVO im &uuml;berwiegenden Interesse des HOCHK&Ouml;NIGIN GMBH & CO KG&period;
			Die von Ihnen im Kontaktformular eingegebenen Daten verbleiben bei uns&comma; bis Sie uns zur L&ouml;schung auffordern&comma; Ihre Einwilligung zur Speicherung widerrufen oder der Zweck f&uuml;r die Datenspeicherung entf&auml;llt &lpar;z&period;B&period; nach abgeschlossener Bearbeitung Ihrer Anfrage&rpar;&period; Zwingende gesetzliche Bestimmungen – insbesondere Aufbewahrungsfristen – bleiben unber&uuml;hrt&period;<br><br>
			<h3>Darstellung der Website</h3>
			Das HOCHK&Ouml;NIGIN GMBH & CO KG speichert bei jedem Zugriff auf die Website <a href="https://www.hochkoenigin.com" target="_blank">www.hochkoenigin.com</a> &lpar;nachfolgend „Website“ genannt&rpar; folgende Daten: Name der aufgerufenen Website&comma; angeforderte Datei&comma; Datum/Uhrzeit&comma; &uuml;bertragene Datenmenge&comma; Meldung &uuml;ber erfolgreichen Abruf&comma; Browsertyp/Version&comma; Betriebssystem&comma; zuvor besuchte Seite und IP-Adresse&period;
			Die kurzfristige Speicherung der IP-Adresse Ihres Endger&auml;ts ist f&uuml;r die Zurverf&uuml;gungstellung der Inhalte der Website und somit gem&auml;&szlig; § 8 Abs 3 Z 4 DSG sowie gem&auml;&szlig; Art 6 Abs 1 lit b DSGVO f&uuml;r die Vertragserf&uuml;llung erforderlich&period; Ihre IP-Adresse wird nach Beendigung der Nutzung der Website gel&ouml;scht&period;
			Das HOCHK&Ouml;NIGIN GMBH & CO KG verwendet die vorgenannten Daten lediglich f&uuml;r statistische Zwecke&period;<br><br>
			<h3>Cookies</h3>
			Wenn Sie die Website aufrufen&comma; werden ein oder mehrere Cookies &lpar;kleine Textdateien&rpar; auf Ihrem Endger&auml;t gespeichert&period; Dies hat den Zweck&comma; den Service des HOCHK&Ouml;NIGIN GMBH & CO KG dadurch zu verbessern&comma; dass z&period;B&period; Nutzereinstellungen gespeichert werden&period;
			Eine Nutzung der Website ist auch ohne Cookies m&ouml;glich&period; Sie k&ouml;nnen in Ihrem Browser das Speichern von Cookies deaktivieren&comma; auf bestimmte Websites beschr&auml;nken oder Ihren Browser so einstellen&comma; dass er Sie benachrichtigt&comma; bevor ein Cookie gespeichert wird&period; Sie k&ouml;nnen die Cookies &uuml;ber die Datenschutzfunktionen Ihres Browsers jederzeit von der Festplatte Ihres Rechners l&ouml;schen&period; In diesem Fall k&ouml;nnten die Funktionen und die Benutzerfreundlichkeit der Website eingeschr&auml;nkt werden&period;<br><br>
			<h3>Server-Log-Dateien</h3>
			Der Provider der Website erhebt und speichert automatisch Informationen in so genannten Server-Log-Dateien&comma; die Ihr Browser automatisch an uns &uuml;bermittelt&period; Dies sind:<br>
			<ul>
			<li>Browsertyp und Browserversion</li>
			<li>verwendetes Betriebssystem</li>
			<li>Referrer URL</li>
			<li>Hostname des zugreifenden Rechners</li>
			<li>Uhrzeit der Serveranfrage</li>
			<li>IP-Adresse</li>
			</ul><br>
			Eine Zusammenf&uuml;hrung dieser Daten mit anderen Datenquellen wird nicht vorgenommen&period; Grundlage f&uuml;r die Datenverarbeitung ist Art&period; 6 Abs&period; 1 lit&period; b DSGVO&comma; der die Verarbeitung von Daten zur Erf&uuml;llung eines Vertrags oder vorvertraglicher Ma&szlig;nahmen gestattet&period;<br><br>
			<h3>SSLVerschl&uuml;sselung</h3>
			Diese Seite nutzt aus Gr&uuml;nden der Sicherheit und zum Schutz der &Uuml;bertragung vertraulicher Inhalte&comma; wie zum Beispiel der Anfragen&comma; die Sie an das HOCHK&Ouml;NIGIN GMBH & CO KG senden&comma; eine SSL-Verschl&uuml;sselung&period; Eine verschl&uuml;sselte Verbindung erkennen Sie daran&comma; dass die Adresszeile des Browsers von "http://" auf "https://" wechselt und an dem Schloss-Symbol in Ihrer Browserzeile&period; Wenn die SSL Verschl&uuml;sselung aktiviert ist&comma; k&ouml;nnen die Daten&comma; die Sie an das HOCHK&Ouml;NIGIN GMBH & CO KG &uuml;bermitteln&comma; nicht von Dritten mitgelesen werden&period;
			Datenspeicherung bei der Online Buchung.<br>
			Wir weisen darauf hin&comma; dass zum Zweck der Online-Buchung und zur sp&auml;teren Vertragsabwicklung vom Online-Buchungssystem Cookies und die IP Daten des Anschlussinhabers gespeichert werden&comma; ebenso wie die bei der Online-Buchung bereitgestellten Daten der buchenden Person&period;
			Dar&uuml;ber hinaus werden zum Zweck der Vertragsabwicklung alle ben&ouml;tigten Daten f&uuml;r die Buchung bei uns gespeichert&period; Die von Ihnen bereit gestellten Daten sind zur Vertragserf&uuml;llung bzw&period; zur Durchf&uuml;hrung vorvertraglicher Ma&szlig;nahmen erforderlich&period; Ohne diese Daten k&ouml;nnen wir den Vertrag mit Ihnen nicht abschlie&szlig;en&period; Eine Daten&uuml;bermittlung an Dritte erfolgt nicht&comma; mit Ausnahme an den entsprechenden Channel-Manager&comma; Hotelprogramm bzw&period; der &Uuml;bermittlung der Kreditkartendaten an die abwickelnden Bankinstitute/Zahlungsdienstleister zum Zwecke der Abbuchung der Online-Buchung&comma; sowie an unseren Steuerberater zur Erf&uuml;llung unserer steuerrechtlichen Verpflichtungen&period;
			Bei Abbruch der Online-Buchung werden die bei uns gespeicherten Daten gel&ouml;scht&period; Im Falle einer erfolgreichen Online-Buchung werden s&auml;mtliche Daten aus dem Vertragsverh&auml;ltnis bis zum Ablauf der steuerrechtlichen Aufbewahrungsfrist &lpar;7 Jahre&rpar; gespeichert&period;
			Die Datenverarbeitung erfolgt auf Basis der gesetzlichen Bestimmungen des § 96 Abs&period; 3 TKG sowie des Art&period; 6&comma; Abs&period; 1 lit a &lpar;Einwilligung&rpar; und/oder lit b &lpar;notwendig zur Vertragserf&uuml;llung&rpar; der DSGVO&period;<br><br>
			<h3>Google Analytics</h3>
			Diese Website benutzt Google Analytics&comma; einen Webanalysedienst der Google Inc&period; &lpar;„Google“&rpar;&comma; 1600 Amphitheatre Parkway&comma; Mountain View&comma; CA 94043&comma; USA&period; Google Analytics verwendet Cookies&comma; die eine Analyse der Benutzung der Website durch Sie erm&ouml;glichen&period; Die durch den Cookie erzeugten Daten &uuml;ber Ihre Benutzung dieser Website &lpar;einschlie&szlig;lich Ihrer IP-Adresse&rpar; werden an einen Server von Google in den USA &uuml;bertragen und dort f&uuml;r die Dauer des Besuchs der Website <a href="https://www.hochkoenigin.com" target="_blank">www.hochkoenigin.com</a> gespeichert&period; F&uuml;r die &Uuml;bertragung personenbezogener Daten in die U&period;S&period;A&period; liegt ein Angemessenheitsbeschluss &lpar;<a href="https://eur-lex.europa.eu/legal-content/DE/TXT/HTML/?uri=CELEX:32016D1250&from=DE" target="_blank">https://eur-lex.europa.eu/legal-content/DE/TXT/HTML/?uri=CELEX:32016D1250&from=DE</a> &rpar; der Europ&auml;ischen Kommission vor&period;
			Google wird die &uuml;bermittelten Daten im Auftrag des HOCHK&Ouml;NIGIN GMBH & CO KG benutzen&comma; um Ihre Nutzung der Website auszuwerten&comma; um Reports &uuml;ber die Websiteaktivit&auml;ten f&uuml;r das HOCHK&Ouml;NIGIN GMBH & CO KG zusammenzustellen und um weitere mit der Websitenutzung und der Internetnutzung verbundene Dienstleistungen zu erbringen&period; Auch wird Google diese Daten gegebenenfalls an Dritte &uuml;bertragen&comma; sofern dies gesetzlich vorgeschrieben oder soweit Dritte diese Daten im Auftrag von Google verarbeiten&period; Google wird in keinem Fall Ihre IP-Adresse mit anderen Daten von Google in Verbindung bringen&period;
			Im Falle der Aktivierung der IP-Anonymisierung auf dieser Website wird Ihre IP-Adresse von Google jedoch innerhalb der Europ&auml;ischen Union oder im Europ&auml;ischen Wirtschaftsraum zuvor gek&uuml;rzt&period; Nur in Ausnahmef&auml;llen wird die volle IP-Adresse an einen Server von Google in den USA &uuml;bertragen und dort gek&uuml;rzt&period; Die IP-Anonymisierung ist auf dieser Website aktiv&period;
			Sie k&ouml;nnen die Speicherung der Cookies im Rahmen von Google Analytics durch eine entsprechende Einstellung Ihrer Browser-Software verhindern&period; In diesem Fall k&ouml;nnen Sie aber gegebenenfalls nicht s&auml;mtliche Funktionen dieser Website vollumf&auml;nglich nutzen&period; Sie k&ouml;nnen dar&uuml;ber hinaus die Erfassung der durch das Cookie erzeugten und auf die Nutzung der Website bezogenen Daten &lpar;inkl&period; Ihrer IP-Adresse&rpar; an Google sowie die Verarbeitung dieser Daten durch Google verhindern&comma; indem Sie das unter dem folgenden Link verf&uuml;gbare Browser-Plug-In herunterladen und installieren: https://tools&period;google&period;com/dlpage/gaoptout?hl=de
			N&auml;here Informationen zu den Nutzungsbedingungen und zum Datenschutz bez&uuml;glich Google Analytics finden Sie unter <a href="https://www.google.com/analytics/terms/de.html" target="_blank">https://www.google.com/analytics/terms/de.html</a><br><br>
			<h3>Google AdWords Remarketing</h3>
			Diese Webseite verwendet Google Remarketing&period; Google Remarketing ist ein Werbedienst von Google&comma; mit dem vorherige Besucher der Website zielgerichtet beworben werden k&ouml;nnen&period;
			Drittanbieter&comma; einschlie&szlig;lich Google&comma; schalten Anzeigen auf Websites im Internet&period; Dazu werden Cookies auf Ihrem Endger&auml;t gespeichert und zum Schalten von Anzeigen auf der Grundlage vorheriger Besuche eines Nutzers auf dieser Website verwendet&period; Die Identifikation der Nutzer erfolgt dabei &uuml;ber im Webbrowser gesetzte Cookies&period; Mit Hilfe der Textdateien kann das Nutzerverhalten beim Besuch der Website analysiert und anschlie&szlig;end f&uuml;r gezielte Produktempfehlungen und interessenbasierte Werbung genutzt werden&period;
			Wenn Sie keine interessenbasierte Werbung erhalten m&ouml;chten&comma; k&ouml;nnen Sie mit dem Anzeigevorgaben-Manager die Verwendung von Cookies durch Google f&uuml;r diese Zwecke deaktivieren und Anzeigen im Google Display-Netzwerk anpassen&comma; indem sie Google unter <a href="https://adssettings&period;google&period;com/?hl=de" target="_blank">https://adssettings&period;google&period;com/?hl=de</a> aufrufen&period;<br><br>
			<h3>Google Maps</h3>
			Die Webseite des HOCHK&Ouml;NIGIN GMBH & CO KG verwendet die Google Maps API der Google Inc&period; mit Sitz in den USA&comma; um geographische Informationen visuell darzustellen&period; Bei der Nutzung von Google Maps-Funktionen werden von Google Daten erhoben&comma; verarbeitet und genutzt&period; N&auml;here Informationen &uuml;ber die Datenverarbeitung durch Google entnehmen Sie den Datenschutzhinweisen von Google: <a href="https://www.google.de/intl/de/policies/privacy" target="_blank">https://www.google.de/intl/de/policies/privacy</a><br><br>
			<h3>Google-Fonts</h3>
			Wir binden gewisse Schriftarten &uuml;ber den Google-Fonts Dienst der Firma Google Inc&period;&comma; 1600 Amphitheatre Parkway&comma; Mountain View&comma; CA 94043 USA ein&period; F&uuml;r die Darstellung der verwendeten Schriftart ist dies notwendig&period; Die Einbindung der Schriftart erfolgt &uuml;ber einen Aufruf am Server von Google und wird in den Browsercache geladen&period; Beim Download der Schriftart wird Ihre IP-Adresse&comma; sowie die besuchte Webseite an Google &uuml;bertragen&period; Mehr Informationen finden Sie hier&period; Google-Fonts: <a href="https://developers&period;google&period;com/fonts/faq" target="_blank">https://developers&period;google&period;com/fonts/faq</a>
			Nutzungsbedingungen von Google: <a href="https://policies&period;google&period;com/privacy?hl=de" target="_blank">https://policies&period;google&period;com/privacy?hl=de</a><br><br>
			<h3>Facebook Custom Audiences</h3>
			Das HOCHK&Ouml;NIGIN GMBH & CO KG verwendet auf der Website Facebook Custom Audiences&period; Facebook Custom Audiences ist ein Werbedienst der Facebook Inc&period;&comma; 1601 S California Ave&comma; Palo Alto&comma; CA&comma; 94304&comma; USA&comma; mit denen vorherige Besucher der Website zielgerichtet beworben werden k&ouml;nnen&period;
			Facebook verwendet Cookies zum Schalten von Anzeigen auf der Grundlage vorheriger Besuche eines Nutzers auf dieser Website&period; Die Identifikation der Nutzer erfolgt dabei &uuml;ber im Webbrowser gesetzte Cookies&comma; die dort gespeichert werden&period; Mit Hilfe der Cookies kann das Nutzerverhalten beim Besuch der Website analysiert und anschlie&szlig;end f&uuml;r gezielte Produktempfehlungen und interessenbasierte Werbung genutzt werden&period;
			Wenn Sie keine interessenbasierte Werbung auf Facebook erhalten m&ouml;chten&comma; k&ouml;nnen Sie sich hier davon abmelden &lpar;nur auf Englisch&rpar;: <a href="https://www.facebook.com/settings/?tab=ads target="_blank">https://www.facebook.com/settings/?tab=ads</a><br><br>
			<h3>Social Media Plug-In</h3>
			Das HOCHK&Ouml;NIGIN GMBH & CO KG verwendet auf der Website sogenannte Social Media Plug-Ins &lpar;Schnittstellen zu sozialen Netzwerken&rpar;&period; Beim Besuch der Website stellt das System aufgrund der Einbindung der Social Media Plug-Ins automatisch eine Verbindung mit dem jeweiligen sozialen Netzwerk her und &uuml;bertr&auml;gt Daten &lpar;IP-Adresse&comma; Besuch der Website etc&period;&rpar;&period;
			Die Daten&uuml;bertragung geschieht ohne das Zutun und au&szlig;erhalb der Verantwortung des HOCHK&Ouml;NIGIN GMBH & CO KG&period; Das HOCHK&Ouml;NIGIN GMBH & CO KG weist darauf hin&comma; dass Sie diese Daten&uuml;bertragung verhindern k&ouml;nnen&comma; indem Sie sich vor dem Besuch der Website bei den jeweiligen sozialen Netzwerken ausloggen&period; Nur im „eingeloggten“ Zustand kann das soziale Netzwerk spezifische Daten Ihrem Aktivit&auml;ten-Profil durch automatische Daten&uuml;bertragung zuordnen&period;
			Die automatisch &uuml;bertragenen Daten werden ausschlie&szlig;lich von den Betreibern der sozialen Netzwerke genutzt und nicht durch das HOCHK&Ouml;NIGIN GMBH & CO KG&period;
			Weitere Informationen dazu&comma; unter anderem auch zum Inhalt der Datenerhebung durch die sozialen Netzwerke&comma; entnehmen Sie bitte direkt der Internetseite des entsprechenden sozialen Netzwerks&period; Dort k&ouml;nnen Sie auch Ihre Privatsph&auml;re-Einstellungen anpassen&period;
			Die auf der Website eingebundenen sozialen Netzwerke sind:<br>
			<ul>
			<li>“Facebook”<br>
			Facebook Inc&period;&comma; 1601 S California Ave&comma; Palo Alto&comma; CA&comma; 94304&comma; USA<br>
			N&auml;here Informationen unter <a href="https://de-de&period;facebook&period;com/policy&period;php" target="_blank">https://de-de&period;facebook&period;com/policy&period;php</a></li>
			<li>“Twitter”<br>
			Twitter Inc&period;&comma; 795 Folsom St&period;&comma; Suite 600&comma; San Francisco&comma; CA 94107&comma; USA<br>
			N&auml;here Informationen unter <a href="" target="_blank">https://twitter&period;com/privacy?lang=de</a></li>
			<li>“Youtube”<br>
			YouTube&comma; LLC&comma; 901 Cherry Ave&period;&comma; San Bruno&comma; CA 94066&comma; USA<br>
			N&auml;here Informationen unter <a href="https://www.youtube.com/t/privacy_guidelines" target="_blank">https://www.youtube.com/t/privacy_guidelines</a></li>
			<li>“Google”<br>
			Google LLC &lpar;„Google“&rpar;&comma; Amphitheatre Parkway&comma; Mountain View&comma; CA 94043&comma; USA<br>
			N&auml;here Informationen unter <a href="" target="_blank">https://policies&period;google&period;com/terms?hl=de</a></li>
			</ul><br>
			<h3>Weitere Dritt-Anbieter Tools</h3>
			Unsere Website verwendet Funktionen&comma; Widgets bzw&period; Plugins von<br><ul>
			<li>Tripadvisor &lpar;Bewertungswidget&rpar;: TripAdvisor LLC&comma; 400 1st Avenue&comma; Needham&comma; MA 02494&comma; USA</li>
			<li>Holidaycheck &lpar;Bewertungswidget&rpar;: HolidayCheck AG&comma; Bahnweg 8&comma; CH-8598 Bottighofen</li>
			<li>Wunderground&period;com &lpar;Wetteranzeige&rpar;: The Weather Company&comma; an IBM business<br>
			Attn: Privacy Office 1001 Summit Boulevard&comma; Floor 20 Brookhaven&comma; GA&comma; USA 30319</li>
			<li>Webcams: feratel media technologies AG&comma; Maria-Theresien-Stra&szlig;e 8&comma; A-6020 Innsbruck</li>
			</ul>
			Diesen Dritt-Anbieter-Tools wird die IP-Adresse &uuml;bermittelt&comma; um deren Funktionsweise zu gew&auml;hrleisten&period; Wir haben keinen Einfluss darauf&comma; falls die Dritt-Anbieter die IP-Adresse f&uuml;r statistische Zwecke oder &auml;hnliches speichern&period; Mehr Informationen dazu finden Sie in den Datenschutzerkl&auml;rungen der jeweiligen Dienste&period;<br><br>
			<h3>Newsletter</h3>
			Wenn Sie den kostenlosen Newsletter des HOCHK&Ouml;NIGIN GMBH & CO KG abonnieren m&ouml;chten&comma; m&uuml;ssen Sie lediglich Ihre E-Mail-Adresse angeben&comma; an die der Newsletter gesendet werden soll&period; Diese Datenverarbeitung ist gem&auml;&szlig; § 8 Abs 3 Z 4 DSG sowie gem&auml;&szlig; Art 6 Abs 1 lit b DSGVO f&uuml;r die Vertragserf&uuml;llung erforderlich&period; Weitere personenbezogene Angaben wie Vor- und Nachname&comma; Interessen … sind optional&period; Diese zus&auml;tzlichen Angaben zu ihrer Person dienen lediglich dazu&comma; um den Newsletter zu personalisieren&period;
			F&uuml;r die Registrierung wird das Double-Opt-In-Verfahren verwendet&period; Nach der Anmeldung erhalten Sie auf die von Ihnen angegebene E-Mail-Adresse eine Best&auml;tigungs- und Autorisierungs-E-Mail mit der Aufforderung&comma; den darin enthaltenen Link anzuklicken&period; Damit wird sichergestellt&comma; dass sich nur der berechtigte Nutzer der angegeben E-Mail-Adresse f&uuml;r unseren Newsletter-Verteiler anmelden kann&period;
			Sie k&ouml;nnen den Newsletter jederzeit abbestellen&period; Am Ende jedes Newsletters befindet sich ein Link&comma; &uuml;ber den Sie die Abbestellung vornehmen k&ouml;nnen&period; Alternativ k&ouml;nnen Sie auch eine entsprechende E-Mail an <a href="mailto:urlaub@hochkoenigin.com" target="_blank">urlaub@hochkoenigin&period;com</a> senden&period; Ihre personenbezogenen Daten werden anschlie&szlig;end aus dem Newsletter-Verteiler gel&ouml;scht&period;
			Sofern wir die entsprechenden Angaben im Rahmen der Vertragsbeziehung mit Ihnen erhalten haben&comma; beh&auml;lt sich das HOCHK&Ouml;NIGIN GMBH & CO KG f&uuml;r eigene Werbezwecke vor&comma; Ihren Vor- und Nachnamen&comma; Ihre Postanschrift&comma; Ihr Geburtsjahr und Ihre Berufs-&comma; Branchen- oder Gesch&auml;ftsbezeichnung in zusammengefassten Listen dauerhaft zu speichern und f&uuml;r die Zusendung von interessanten Angeboten und Informationen zu Angeboten des HOCHK&Ouml;NIGIN GMBH & CO KG per Briefpost zu nutzen&period; Sie k&ouml;nnen der Verarbeitung Ihrer Daten zu diesem Zweck jederzeit durch eine Nachricht an <a href="mailto:urlaub@hochkoenigin.com" target="_blank">urlaub@hochkoenigin&period;com</a> widersprechen&period;<br><br>
			<h3>Gewinnspiel</h3>
			Bei einer Teilnahme an auf der Website des HOCHK&Ouml;NIGIN GMBH & CO KG angebotenen Gewinnspielen werden personenbezogene Daten&comma; n&auml;mlich Titel&comma; Vorname&comma; Nachname&comma; E-Mail-Adresse&comma; Postanschrift und das Land&comma; erhoben&period; Diese Daten sind f&uuml;r die Durchf&uuml;hrung des Gewinnspiels&comma; insbesondere f&uuml;r die Zuordnung der Teilnahmeantr&auml;ge zu den jeweiligen Teilnehmern sowie f&uuml;r die Ermittlung und Benachrichtigung der Gewinner – somit gem&auml;&szlig; § 8 Abs 3 Z 4 DSG sowie gem&auml;&szlig; Art 6 Abs 1 lit b DSGVO zur Vertragserf&uuml;llung – erforderlich&period;
			Die erhaltenen personenbezogenen Daten werden vom HOCHK&Ouml;NIGIN GMBH & CO KG nur soweit verarbeitet und benutzt&comma; als sie f&uuml;r die Durchf&uuml;hrung des Gewinnspiels erforderlich sind&period; Die personenbezogenen Daten werden f&uuml;r die Dauer des Gewinnspiels und – zur Bearbeitung allf&auml;lliger Gewinn- und Schadenersatzanspr&uuml;che – f&uuml;r maximal drei Jahre danach gespeichert und anschlie&szlig;end gel&ouml;scht&period;
			Durch Ihre Teilnahme am Gewinnspiel erkl&auml;ren Sie sich damit einverstanden&comma; dass Ihr Name im Falle des Gewinns auf dieser Website sowie auf den Social Media Kan&auml;len ver&ouml;ffentlicht wird&period;<br><br>
			Bei Fragen zur Datenverarbeitung erreichen Sie uns unter folgenden Kontaktdaten:<br>
			Hochk&ouml;nigin GmbH & Co KG&nbsp;<br/>
			<a href="https://www.google.com/maps/dir//Hochk%C3%B6nigstra%C3%9Fe+27,+5761+Alm/@47.4045275,12.9001605,17z/data=!4m8!4m7!1m0!1m5!1m1!1s0x4776e3c45e11e5ab:0x2d9d226b9d8da6b7!2m2!1d12.9023492!2d47.4045275" target="_blank">Hochk&ouml;nigstra&szlig;e 27</br>
			A-5761 Maria&nbsp;Alm</a><br/>
			Tel&period;&colon; <a href="004365847447-0">+43/ &lpar;0&rpar;6584/7447-0</a><br>
			Fax&colon; <a href="tel:004365847447-17">+43/ &lpar;0&rpar;6584/7447-17</a><br>
			<a href="https://www.hochkoenigin.com" target="_blank">www.hochkoenigin.com</a><br>
			<a href="mailto:urlaub@hochkoenigin.com">urlaub@hochkoenigin.com</a><br><br>
			<h3>Datenschutzerkl&auml;rung f&uuml;r die Nutzung von ShareThis</h3><br>
			Diese Website benutzt Plugins des Bookmarking Dienstes ShareThis&comma; der angeboten wird von ShareThis Inc&period; &lpar;"ShareThis"&rpar;&comma; 250 Cambridge Avenue&comma; Palo Alto&comma; CA 94306&comma; USA&period; Beim Aufruf unserer Website erh&auml;lt ShareThis Kenntnis von Ihrer IP-Adresse und davon&comma; dass Sie mit dieser IP-Adresse die Website des H&ouml;rl Thalerhof GmbH & Co KG besucht haben&period; Mithilfe der ShareThis-Plugins k&ouml;nnen Nutzer im Internet verf&uuml;gbare Lesezeichen zu Webseiten setzen und Links zu entsprechenden Webseiten in sozialen Netzwerken wie Twitter&comma; Facebook&comma; Xing oder Google+ teilen oder posten bzw&period; die dortigen Inhalte empfehlen&period; Wenn ein Website-Besucher eine dieser Funktionen nutzt und gleichzeitig auch bei dem entsprechenden Dienst &lpar;beispielsweise Twitter&comma; Facebook oder Google+&rpar; online ist&comma; dann wird der Besuch unserer Webseite dort dem jeweiligen Nutzer zugeordnet&period; Weitere Informationen zur Datenerhebung&comma; Auswertung und Verarbeitung Ihrer Daten durch ShareThis sowie ihre darauf bezogenen Rechte k&ouml;nnen in der Datenschutzerkl&auml;rung von ShareThis unter <a href="https://www.sharethis.com/privacy/" target="_blank">http://www.sharethis.com/legal/privacy</a> aufgerufen werden&period;<br><br>
			<h3>Facebook Pixel</h3><br>
			Unsere Website verwendet die Remarketing-Funktion „Facebook-Pixel“ der Facebook Inc. („Facebook“). Diese Funktion dient dazu, Besuchern dieser Webseite im Rahmen des Besuchs des sozialen Netzwerkes <a href="https://www.trafficmaxx.de/social-media-marketing" target="_blank">Facebook interessenbezogene Werbeanzeigen</a> („Facebook-Ads“) zu pr&auml;sentieren. Hierzu wurde auf unserer Website Facebook-Pixel implementiert. &Uuml;ber Facebook-Pixel wird beim Besuch der Webseite eine direkte Verbindung zu den Facebook-Servern hergestellt. Dabei wird an den Facebook-Server &uuml;bermittelt, dass Sie diese Website besucht haben und Facebook ordnet diese Information Ihrem pers&ouml;nlichen Facebook-Benutzerkonto zu.<br/>
			N&auml;here Informationen zur Erhebung und Nutzung der Daten durch Facebook sowie &uuml;ber Ihre diesbez&uuml;glichen Rechte und M&ouml;glichkeiten zum Schutz Ihrer Privatsph&auml;re finden Sie in den Datenschutzhinweisen von Facebook unter <a href="https://www.facebook.com/about/privacy/" target="_blank">https://www.facebook.com/about/privacy/</a>.<br/>
			Alternativ k&ouml;nnen Sie die Remarketing-Funktion von Facebook unter <a href="https://www.facebook.com/ads/preferences/?entry_product=ad_settings_screen#_=_" target="_blank">https://www.facebook.com/settings/?tab=ads#_=_</a> deaktivieren. Hierf&uuml;r m&uuml;ssen Sie bei Facebook angemeldet sein.<br/>
		',
	],
	'gtc' => [
		'headline' => 'AGB',
		'headline_inner' => 'Allgemeine Gesch&auml;ftsbedingungen',
		'text' => '
			<h3 class="hidden">Allgemeine Gesch&auml;ftsbedingungen</h3>
			<a href="http&colon;//www.hotelverband.at/down/AGBH_061115.pdf" target="_blank" class="styled">&Ouml;sterreichische Hotelvertragsbedingungen &lpar;&Ouml;HVB&rpar;</a><br/><br/>
			&lpar;Beschlossen bei der 93&period; Ausschusssitzung des Fachverbandes der Hotel- und Beherbergungsbetriebe am 23&period; September 1981&rpar;<br/><br/>
			<strong>Quelle&colon;</strong><br/>
			Eigent&uuml;mer&comma; Herausgeber und Verleger&colon;<br/>
			Fachverband der Hotel- und Beherbergungsbetriebe&comma; 1045 Wien&comma; Wiedner Hauptstra&szlig;e 63&period;<br/>
			F&uuml;r den Inhalt verantwortlich&colon; Fachverbandsgesch&auml;ftsf&uuml;hrer Dr&period; Michael Raffling&comma; 1045 Wien&comma; Wiedner Hauptstra&szlig;e 63&period;<br/><br/>
			<strong>&sect;1 Allgemeines</strong><br/>
			Die &lpar;allgemeinen&rpar; &Ouml;sterreichischen Hotelvertragsbedingungen stellen jenen Vertragsinhalt dar&comma; zu welchem die &ouml;sterreichischen Beherberger &uuml;blicherweise mit ihren G&auml;sten Beherbergungsvertr&auml;ge abschlie&szlig;en&period; Die &Ouml;sterreichischen Hotelvertragsbedingungen schlie&szlig;en Sondervereinbarungen nicht aus&period;<br/><br/>
			<strong>&sect; 2 Vertragspartner</strong><br/>
			&lpar;1&rpar; Als Vertragspartner des Beherbergers gilt im Zweifelsfalle der Besteller&comma; auch wenn er f&uuml;r andere namentlich genannte Personen bestellt oder mitbestellt hat&period;<br/>
			&lpar;2&rpar; Die Beherbergung in Anspruch nehmende Personen sind G&auml;ste im Sinne der Vertragsbedingungen&period;<br/><br/>
			<strong>&sect; 3 Vertragsabschluss&comma; Anzahlung</strong><br/>
			&lpar;1&rpar; Der Beherbergungsvertrag kommt in der Regel durch die Annahme der schriftlichen oder m&uuml;ndlichen Bestellung des Gastes durch den Beherberger zustande&period;<br/>
			&lpar;2&rpar; Es kann vereinbart werden&comma; dass der Gast eine Anzahlung leistet&period;<br/>
			&lpar;3&rpar; Der Beherberger kann auch die Vorauszahlung des gesamten vereinbarten Entgeltes verlangen&period;<br/><br/>
			<strong>&sect; 4 Beginn und Ende der Beherbergung</strong><br/>
			&lpar;1&rpar; Der Gast hat das Recht&comma; die gemieteten R&auml;ume ab 14 Uhr des vereinbarten Tages zu beziehen&period;<br/>
			&lpar;2&rpar; Der Beherberger hat das Recht&comma; f&uuml;r den Fall&comma; dass der Gast bis 18 Uhr des vereinbarten Ankunftstages nicht erscheint&comma; vom Vertrag zur&uuml;ckzutreten&comma; es sei denn&comma; dass ein sp&auml;terer Ankunftszeitpunkt vereinbart wurde&period;<br/>
			&lpar;3&rpar; Hat der Gast eine Anzahlung geleistet&comma; so bleibt &lpar;bleiben&rpar; dagegen der Raum &lpar;die R&auml;ume&rpar; bis sp&auml;testens 12 Uhr des folgenden Tages reserviert&period;<br/>
			&lpar;4&rpar; Wird ein Zimmer erstmalig vor 6 Uhr fr&uuml;h in Anspruch genommen&comma; so z&auml;hlt die vorhergegangene Nacht als erste &Uuml;bernachtung&period;<br/>
			&lpar;5&rpar; Die gemieteten R&auml;ume sind durch den Gast am Tag der Abreise bis 12 Uhr freizumachen&period;<br/><br/>
			<strong>&sect; 5 R&uuml;cktritt vom Beherbergungsvertrag</strong><br/>
			&lpar;1&rpar; Bis sp&auml;testens drei Monate vor dem vereinbarten Ankunftstag des Gastes kann der Beherbergungsvertrag ohne Entrichtung einer Stornogeb&uuml;hr von beiden Vertragspartnern durch einseitige Erkl&auml;rung aufgel&ouml;st werden&period; Die Stornoerkl&auml;rung muss bis sp&auml;testens drei Monate vor dem vereinbarten Ankunftstag des Gastes in den H&auml;nden des Vertragspartners sein&period;<br/> &lpar;2&rpar; Bis sp&auml;testens einen Monat vor dem vereinbarten Ankunftstag des Gastes kann der Beherbergungsvertrag von beiden Vertragspartnern durch einseitige Erkl&auml;rung aufgel&ouml;st werden&comma; es ist jedoch eine Stornogeb&uuml;hr im Ausma&szlig; des Zimmerpreises f&uuml;r drei Tage zu bezahlen&period; Die Stornoerkl&auml;rung muss bis sp&auml;testens einen Monat vor dem vereinbarten Ankunftstag des Gastes in den H&auml;nden des Vertragspartners sein&period;<br/> &lpar;3&rpar; Der Beherberger hat das Recht&comma; f&uuml;r den Fall&comma; da&szlig; der Gast bis 18 Uhr des vereinbarten Ankunftstages nicht erscheint&comma; vom Vertrag zur&uuml;ckzutreten&comma; es sein denn&comma; dass ein sp&auml;terer Ankunftszeitpunkt vereinbart wurde&period;
			&lpar;4&rpar; Hat der Gast eine Anzahlung geleistet&comma; so bleibt &lpar;bleiben&rpar; dagegen der Raum &lpar;die R&auml;ume&rpar; bis sp&auml;testens 12 Uhr des folgenden Tages reserviert&period;<br/>
			&lpar;5&rpar; Auch wenn der Gast die bestellten R&auml;ume bzw&period; die Pensionsleistung nicht in Anspruch nimmt&comma; ist er dem Beherberger gegen&uuml;ber zur Bezahlung des vereinbarten Entgeltes verpflichtet&period; Der Beherberger muss jedoch in Abzug bringen&comma; was er sich infolge Nichtinanspruchnahme seines Leistungsangebots erspart oder was er durch anderweitige Vermietung der bestellten R&auml;ume erhalten hat&period; Erfahrungsgem&auml;&szlig; werden in den meisten F&auml;llen die Ersparungen des Betriebes infolge des Unterbleibens der Leistung 20 Prozent des Zimmerpreises sowie 30 Prozent des Verpflegungspreises betragen&period;&lpar;6&rpar; Dem Beherberger obliegt es&comma; sich um eine anderweitige Vermietung der nicht in Anspruch genommenen R&auml;ume den Umst&auml;nden entsprechend zu bem&uuml;hen &lpar;&sect; 1107 ABGB&rpar;&period;<br/><br/>
			<strong>&sect; 6 Beistellung einer Ersatzunterkunft</strong><br/>
			&lpar;1&rpar; Der Beherberger kann dem Gast eine ad&auml;quate Ersatzunterkunft zur Verf&uuml;gung stellen&comma; wenn dies dem Gast zumutbar ist&comma; besonders weil die Abweichung geringf&uuml;gig und sachlich gerechtfertigt ist&period;<br/> &lpar;2&rpar; Eine sachliche Rechtfertigung ist beispielsweise dann gegeben&comma; wenn der Raum &lpar;die R&auml;ume&rpar; unben&uuml;tzbar geworden sind&comma; bereits einquartierte G&auml;ste ihren Aufenthalt verl&auml;ngern oder sonstige wichtige betriebliche Ma&szlig;nahmen diesen Schritt bedingen&period;<br/>
			&lpar;3&rpar; Allf&auml;llige Mehraufwendungen f&uuml;r das Ersatzquartier gehen auf Kosten des Beherbergers&period;<br/><br/>
			<strong>&sect; 7 Rechte des Gastes</strong><br/>
			&lpar;1&rpar; Durch den Abschluss eines Beherbergungsvertrages erwirbt der Gast das Recht auf den &uuml;blichen Gebrauch der gemieteten R&auml;ume&comma; der Einrichtungen des Beherbergungsbetriebes&comma; die &uuml;blicherweise und ohne besondere Bedingungen den G&auml;sten zur Ben&uuml;tzung zug&auml;nglich sind&comma; und auf die &uuml;bliche Bedienung&period;<br/>
			&lpar;2&rpar; Der Gast hat das Recht&comma; die gemieteten R&auml;ume ab 14 Uhr des vereinbarten Tages zu beziehen&period;<br/> &lpar;3&rpar; Ist Vollpension oder Halbpension vereinbart&comma; so hat der Gast das Recht&comma; f&uuml;r Mahlzeiten&comma; die er nicht in Anspruch nimmt&comma; eine angemessene Ersatzverpflegung &lpar;Lunchpaket&rpar; oder einen Bon zu verlangen&comma; sofern er dies rechtzeitig&comma; das ist bis 18 Uhr des Vortages&comma; gemeldet hat&period;<br/>
			&lpar;4&rpar; Sonst hat der Gast bei Leistungsbereitschaft des Beherbergers&comma; wenn er die vereinbarten Mahlzeiten nicht innerhalb der &uuml;blichen Tageszeiten und in den hief&uuml;r bestimmten R&auml;umlichkeiten in Anspruch nimmt&comma; keinen Ersatzanspruch&period;<br/><br/>
			<strong>&sect; 8 Pflichten des Gastes</strong><br/>
			&lpar;1&rpar; Bei Beendigung des Beherbergungsvertrages ist das vereinbarte Entgelt zu bezahlen&period; Fremdw&auml;hrungen werden vom Beherberger nach Tunlichkeit zum Tageskurs in Zahlung genommen&period; Der Beherberger ist nicht verpflichtet&comma; bargeldlose Zahlungsmittel wie Schecks&comma; Kreditkarten&comma; Bons&comma; Vouchers usw&period; anzunehmen&period; Alle bei Annahme dieser Wertpapiere notwendigen Kosten&comma; etwa f&uuml;r Telegramme&comma; Erkundigungen usw&period;&comma; gehen zu Lasten des Gastes&period;<br/> &lpar;2&rpar; Wenn Nahrungsmittel oder Getr&auml;nke im Beherbergungsbetrieb erh&auml;ltlich sind&comma; aber dorthin mitgebracht und in &ouml;ffentlichen R&auml;umen verzehrt werden&comma; so ist der Beherberger berechtigt&comma; eine angemessene Entsch&auml;digung in Rechnung zu stellen &lpar;sogenanntes "Stoppelgeld" bei Getr&auml;nken&rpar;&period;<br/>
			&lpar;3&rpar; Vor Inbetriebnahme von elektrischen Ger&auml;ten&comma; welche von den G&auml;sten mitgebracht und welche nicht zum &uuml;blichen Reisebedarf geh&ouml;ren&comma; ist die Zustimmung des Beherbergers einzuholen&period;<br/>
			&lpar;4&rpar; F&uuml;r den vom Gast verursachten Schaden gelten die Vorschriften des Schadenersatzrechtes&period; Daher haftet der Gast f&uuml;r jeden Schaden und Nachteil&comma; den der Beherberger oder dritte Personen durch sein Verschulden oder durch das Verschulden seiner Begleiter oder anderer Personen&comma; f&uuml;r die er verantwortlich ist&comma; erleidet&comma; und zwar auch dann&comma; wenn der Gesch&auml;digte berechtigt ist&comma; zur Schadenersatzleistung direkt den Beherberger in Anspruch zu nehmen&period;<br/><br/>
			<strong>&sect; 9 Rechte des Beherbergers</strong><br/>
			&lpar;1&rpar; Verweigert der Gast die Zahlung des bedungenen Entgelts oder ist er damit im R&uuml;ckstand&comma; so steht dem Inhaber des Beherbergungsbetriebes das Recht zu&comma; zur Sicherung seiner Forderung aus der Beherbergung und Verpflegung sowie seiner Auslagen f&uuml;r den Gast&comma; die eingebrachten Sachen zur&uuml;ckzubehalten&period; &lpar;&sect; 970 c ABGB gesetzliches Zur&uuml;ckbehaltungsrecht&period;&rpar;<br/> &lpar;2&rpar; Der Beherberger hat zur Sicherstellung des vereinbarten Entgelts das Pfandrecht an den vom Gast eingebrachten Gegenst&auml;nden&period; &lpar;&sect; 1101 ABGB gesetzliches Pfandrecht des Beherbergers&period;&rpar;<br/>
			&lpar;3&rpar; Wird das Service im Zimmer des Gastes oder zu au&szlig;ergew&ouml;hnlichen Tageszeiten verlangt&comma; so ist der Beherberger berechtigt&comma; daf&uuml;r ein Sonderentgelt zu verlangen; dieses Sonderentgelt ist jedoch auf der Zimmerpreistafel auszuzeichnen&period; Er kann diese Leistungen aus betrieblichen Gr&uuml;nden auch ablehnen&period;<br/><br/>
			<strong>&sect; 10 Pflichten des Beherbergers</strong><br/>
			&lpar;1&rpar; der Beherberger ist verpflichtet&comma; die vereinbarten Leistungen in einem Standard entsprechenden Umfang zu erbringen&period;<br/> &lpar;2&rpar; Auszeichnungspflichtige Sonderleistungen des Beherbergers&comma; die nicht im Beherbergungsentgelt inbegriffen sind&colon;
			a&rpar; Sonderleistungen der Beherbergung&comma; die gesondert in Rechnung gestellt werden k&ouml;nnen&comma; wie die Bereitstellung von Salons&comma; Sauna und Hallenbad&comma; Schwimmbad&comma; Solarium&comma; Stockwerkbad&comma; Garagierung usw&period;;
			b&rpar; f&uuml;r die Bereitstellung von Zusatz- bzw&period; Kinderbetten wird ein erm&auml;&szlig;igter Preis berechnet&period;<br/>
			&lpar;3&rpar; Die ausgezeichneten Preise haben Inklusivpreise zu sein&period;<br/><br/>
			<strong>&sect; 11 Haftung des Beherbergers f&uuml;r Sch&auml;den</strong><br/>
			&lpar;1&rpar; Der Beherberger haftet f&uuml;r Sch&auml;den&comma; die ein Gast erleidet&comma; wenn sich der Schaden im Rahmen des Betriebes ereignet hat und ihn oder seine Dienstnehmer ein Verschulden trifft&period;<br/>
			&lpar;2&rpar; Haftung f&uuml;r eingebrachte Gegenst&auml;nde&period; Dar&uuml;ber hinaus haftet der Beherberger als Verwahrer f&uuml;r die von den aufgenommenen G&auml;sten eingebrachten Sachen bis zu einem H&ouml;chstbetrag von EUR 1&period;100&comma;00&comma; sofern er nicht beweist&comma; dass der Schaden weder durch ihn oder einen seiner Dienstnehmer verschuldet noch durch fremde&comma; im Haus aus- und eingehende Personen verursacht wurde&period; Unter diesen Umst&auml;nden haftet der Beherberger f&uuml;r Kostbarkeiten&comma; Geld und Wertpapiere bis zu einem H&ouml;chstbetrag von EUR 550&comma;00; es sei denn&comma; dass er diese Sachen in Kenntnis ihrer Beschaffenheit in Verwahrung &uuml;bernommen hat oder dass der Schaden von ihm selbst oder seinen Dienstnehmern verschuldet wurde und er daher unbeschr&auml;nkt haftet&period; Eine Ablehnung der Haftung durch Anschlag ist rechtlich ohne Wirkung&period; Die Verwahrung von Kostbarkeiten&comma; Geld und Wertpapieren kann verweigert werden&comma; wenn es sich um wesentlich wertvollere Gegenst&auml;nde handelt&comma; als G&auml;ste des betreffenden Betriebes gew&ouml;hnlich in Verwahrung geben&period; Vereinbarungen&comma; durch welche die Haftung unter das in den obigen Abs&auml;tzen genannte Ma&szlig; herabgesetzt werden soll&comma; sind unwirksam&period; Sachen gelten dann als eingebracht&comma; wenn sie von einer im Dienst des Beherbergungsbetriebes stehenden Person &uuml;bernommen oder an einen von dieser zugewiesenen&comma; hief&uuml;r bestimmten Platz gebracht werden&period; &lpar;Insbesondere &sect;&sect; 970 ff&period; ABGB&period;&rpar;<br/><br/>
			<strong>&sect; 12 Tierhaltung</strong><br/>
			&lpar;1&rpar; Tiere d&uuml;rfen nur nach vorheriger Bewilligung und allenfalls gegen eine besondere Verg&uuml;tung in den Beherbergungsbetrieb gebracht werden&period; In den Salons&comma; Gesellschafts- und Restaurantr&auml;umen d&uuml;rfen sich Tiere nicht aufhalten&period;<br/>
			&lpar;2&rpar; Der Gast haftet f&uuml;r den Schaden&comma; den mitgebrachte Tiere anrichten&comma; entsprechend den f&uuml;r den Tierhalter geltenden gesetzlichen Vorschriften &lpar;&sect; 1320 ABGB&rpar;&period;<br/><br/>
			<strong>&sect; 13 Verl&auml;ngerung der Beherbergung</strong><br/>
			Eine Verl&auml;ngerung des Aufenthaltes durch den Gast erfordert die Zustimmung des Beherbergers&period;<br/><br/>
			<strong>&sect; 14 Beendigung der Beherbergung</strong><br/>
			&lpar;1&rpar;Wurde der Beherbergungsvertrag auf bestimmte Zeit vereinbart&comma; so endet er mit dem Zeitablauf&period; Reist der Gast vorzeitig ab&comma; so ist der Beherberger berechtigt&comma; das volle vereinbarte Entgelt zu verlangen&period; Dem Beherberger obliegt es jedoch&comma; sich um eine anderweitige Vermietung der nicht in Anspruch genommenen R&auml;ume&comma; den Umst&auml;nden entsprechend&comma; zu bem&uuml;hen&period;
			Im &uuml;brigen gilt die Regelung in &sect; 5 &lpar;5&rpar; sinngem&auml;&szlig; &lpar;Abzugprozente&rpar;&period;<br/>
			&lpar;2&rpar; Durch den Tod eines Gastes endet der Vertrag mit dem Beherberger&period;<br/>
			&lpar;3&rpar; Wurde der Beherbergungsvertrag auf unbestimmte Zeit abgeschlossen&comma; so k&ouml;nnen die Vertragspartner den Vertrag bei Einhaltung einer K&uuml;ndigungsfrist von drei Tagen jederzeit l&ouml;sen&period; Die K&uuml;ndigung mu&szlig; den Vertragspartner vor 10 Uhr erreichen&comma; ansonsten gilt dieser Tag nicht als erster Tag der K&uuml;ndigungsfrist&comma; sondern erst der darauffolgende Tag&period;<br/>
			&lpar;4&rpar; Wenn der Gast sein Zimmer nicht bis 12 Uhr r&auml;umt&comma; ist der Beherberger berechtigt&comma; den Zimmerpreis f&uuml;r einen weiteren Tag in Rechnung zu stellen&period; &lpar;5&rpar; Der Beherberger ist berechtigt&comma; den Beherbergungsvertrag mit sofortiger Wirkung aufzul&ouml;sen&comma; wenn der Gast
			a&rpar; von den R&auml;umlichkeiten einen erheblich nachteiligen Gebrauch macht oder durch sein r&uuml;cksichtsloses&comma; anst&ouml;&szlig;iges oder sonst grob ungeh&ouml;riges Verhalten den &uuml;brigen Mitbewohnern das Zusammenwohnen verleidet oder sich gegen&uuml;ber dem Beherberger und seinen Leuten oder einer im Beherbergungsbetrieb wohnenden Person einer mit Strafe bedrohten Handlung gegen das Eigentum&comma; die Sittlichkeit oder die k&ouml;rperliche Sicherheit schuldig macht;
			b&rpar; von einer ansteckenden oder die Beherbergungsdauer &uuml;bersteigenden Krankheit befallen oder pflegebed&uuml;rftig wird;
			c&rpar; die ihm vorgelegte Rechnung &uuml;ber Aufforderung in einer zumutbar gesetzten Frist nicht bezahlt&period;<br/><br/>
			<strong>&sect; 15 Erkrankung oder Tod des Gastes im Beherbergungsbetrieb</strong><br/>
			&lpar;1&rpar; Erkrankt ein Gast w&auml;hrend seines Aufenthaltes im Beherbergungsbetrieb&comma; so hat der Beherberger die Pflicht&comma; f&uuml;r &auml;rztliche Betreuung zu sorgen&comma; wenn dies notwendig ist und der Gast hiezu selbst nicht in der Lage ist&period; Der Beherberger hat folgenden Kostenersatzanspruch gegen&uuml;ber dem Gast bzw&period; bei Todesfall gegen seinen Rechtsnachfolger&colon;
			a&rpar; allf&auml;lliger Ersatz vom Gast noch nicht beglichener Arztkosten;
			b&rpar; f&uuml;r die erforderliche Raumdesinfektion&comma; wenn diese vom Amtsarzt angeordnet wird;
			c&rpar; allenfalls Ersatz f&uuml;r die unbrauchbar gewordene W&auml;sche&comma; Bettw&auml;sche und Betteinrichtung&comma; gegen Ausfolgung dieser Gegenst&auml;nde an den Rechtsnachfolger&comma; andernfalls f&uuml;r die Desinfektion oder gr&uuml;ndliche Reinigung aller dieser Gegenst&auml;nde;
			d&rpar; f&uuml;r die Wiederherstellung von W&auml;nden&comma; Einrichtungsgegenst&auml;nden&comma; Teppichen usw&period;&comma; soweit diese in Zusammenhang mit der Erkrankung oder dem Todesfall verunreinigt oder besch&auml;digt wurden;
			e&rpar; f&uuml;r die Zimmermiete&comma; soweit sie in Zusammenhang mit der Erkrankung oder dem Todesfall durch zeitweise Unverwendbarkeit der R&auml;ume ausf&auml;llt &lpar;mindestens drei&comma; h&ouml;chstens sieben 
			Tage&rpar;&period;<br/><br/>
			<strong>&sect; 16 Erf&uuml;llungsort und Gerichtsstand</strong><br/>
			&lpar;1&rpar; Erf&uuml;llungsort ist der Ort&comma; in dem der Beherbergungsbetrieb gelegen ist&period;<br/>
			&lpar;2&rpar; F&uuml;r alle Streitigkeiten aus dem Beherbergungsvertrag wird das f&uuml;r den Beherbergungsbetrieb sachlich und &ouml;rtlich zust&auml;ndige Gericht vereinbart&comma; au&szlig;er
			a&rpar; der Gast hat als Verbraucher einen im Inland gelegenen Besch&auml;ftigungsort oder Wohnsitz; in diesem Fall wird als Gerichtsstand jener Ort&comma; der vom Gast in der Anmeldung bekannt gegeben wurde&comma; vereinbart;
			b&rpar; der Gast hat als Verbraucher nur einen inl&auml;ndischen Besch&auml;ftigungsort; in diesem Fall wird dieser als Gerichtsstand vereinbart&period;
			Die im &sect; 5 Ziffer 1&comma;2 und 5 angef&uuml;hrten Stornogeb&uuml;hren sind gem&auml;&szlig; &sect; 31 in Verbindung mit &sect; 32 Kartellgesetz als unverbindliche Verbandsempfehlung in das Kartellregister&comma; Zahl 1 Kt 617&sol;91-5&comma; eingetragen&period;
		',
	],
);
?>
