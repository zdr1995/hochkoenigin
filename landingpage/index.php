<?php

$lang = isset($_GET['lang']) && !empty($_GET['lang']) ? $_GET['lang'] : 'de';
require_once ('config_' . $lang . '.php');
$cfg = $cfg[$lang];

//router
$include_path = 'includes/home' . '.php';
list($parse_url, $query_string) = explode('?', $_SERVER['REQUEST_URI']);
$url_parts = explode('/', $parse_url);
if(isset($url_parts[1]) && !empty($url_parts[1]) && strpos($url_parts[1], '.') === false){
        if(file_exists($_SERVER['DOCUMENT_ROOT'] . '/includes/' . $url_parts[1] . '.php')){
        $include_path = 'includes/' . $url_parts[1] . '.php';
    } else if(count($url_parts) <= 2){
        header($_SERVER["SERVER_PROTOCOL"]." 404 Not Found", true, 404);
        $include_path = 'includes/404_' . $lang . '.php';
    }
}

 ?>
 
<!DOCTYPE html>
<html lang="<?= $lang ?>" style="background-image: url(<?= $cfg['images'][5]['path'] ?>)">
    <head>

        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta http-equiv="content-language" content="<?= $lang ?>">
        <meta name="robots" content="index,follow">
        <meta http-equiv="cache-control" content="max-age=3600">
        <meta name="description" content="" />

    	<title><?php echo $cfg['general']['htmltitle'] ?></title>

        <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.1.1/jquery.min.js"></script>
        <link href="https://fonts.googleapis.com/css?family=PT+Sans:400,700" rel="stylesheet">

        <link rel="stylesheet" type="text/css" href="/fonts/styles.css" />
        <link rel="stylesheet" type="text/css" href="/css/styles.css" />
        <link rel="stylesheet" type="text/css" href="/css/responsive.css" />
        <link rel="stylesheet" href="/css/pickadate.css">
        <link rel="stylesheet" href="/css/pickadate.date.css">

        <link rel="apple-touch-icon" sizes="180x180" href="favicon/apple-touch-icon.png">
        <link rel="icon" type="image/png" sizes="32x32" href="favicon/favicon-32x32.png">
        <link rel="icon" type="image/png" sizes="16x16" href="favicon/favicon-16x16.png">
        <link rel="manifest" href="favicon/site.webmanifest">
        <link rel="mask-icon" href="favicon/safari-pinned-tab.svg" color="#5bbad5">
        <meta name="msapplication-TileColor" content="#ffffff">
        <meta name="theme-color" content="#ffffff">

        <link rel="stylesheet" href="https://cdn.jsdelivr.net/bxslider/4.2.12/jquery.bxslider.css">
        <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.1.1/jquery.min.js"></script>
        <script src="//cdnjs.cloudflare.com/ajax/libs/jquery-form-validator/2.3.26/jquery.form-validator.min.js"></script>
        <script src="https://cdn.jsdelivr.net/bxslider/4.2.12/jquery.bxslider.min.js"></script>
        <script src="/js/smoothscroll.js"></script>
        <script src="/js/pickadate.js"></script>
        <script src="/js/pickadate.date.js"></script>
        <script src="/js/functions.js" type="text/javascript"></script>

        <script type="text/javascript">
          var __translations = {
            'childage' : 'Kinderalter'
          };
        </script>
         <script src='https://www.google.com/recaptcha/api.js'></script>

        <!-- Google Tag Manager -->
        <script>(function(w,d,s,l,i){w[l]=w[l]||[];w[l].push({'gtm.start':
        new Date().getTime(),event:'gtm.js'});var f=d.getElementsByTagName(s)[0],
        j=d.createElement(s),dl=l!='dataLayer'?'&l='+l:'';j.async=true;j.src=
        'https://www.googletagmanager.com/gtm.js?id='+i+dl;f.parentNode.insertBefore(j,f);
        })(window,document,'script','dataLayer','GTM-W7L2M4T');</script>
        <!-- End Google Tag Manager -->
        
        <!-- Facebook Pixel Code -->
        <script>
          !function(f,b,e,v,n,t,s)
          {if(f.fbq)return;n=f.fbq=function(){n.callMethod?
          n.callMethod.apply(n,arguments):n.queue.push(arguments)};
          if(!f._fbq)f._fbq=n;n.push=n;n.loaded=!0;n.version='2.0';
          n.queue=[];t=b.createElement(e);t.async=!0;
          t.src=v;s=b.getElementsByTagName(e)[0];
          s.parentNode.insertBefore(t,s)}(window, document,'script',
          'https://connect.facebook.net/en_US/fbevents.js');
          fbq('init', '404256540128763');
          fbq('track', 'PageView');
        </script>
        <noscript><img height="1" width="1" style="display:none"
          src="https://www.facebook.com/tr?id=404256540128763&ev=PageView&noscript=1"
        /></noscript>
        <!-- End Facebook Pixel Code -->

    </head>

    <body class="<?php echo $lang; ?>" style="background-image: url(<?= $cfg['images'][5]['path'] ?>)">

        <!-- Google Tag Manager (noscript) -->
        <noscript><iframe src="https://www.googletagmanager.com/ns.html?id=GTM-W7L2M4T"
        height="0" width="0" style="display:none;visibility:hidden"></iframe></noscript>
        <!-- End Google Tag Manager (noscript) -->

        <header style="background-image: url(<?= $cfg['header']['path'] ?>)">
            <div id="shadow">
                <img src="<?= $cfg['images'][1]['path'] ?>" alt="<?= $cfg['images'][1]['title'] ?>">
            </div>
            <div id="first-line">
                <div class="left">
                    <span class="only-desktop">TEL.&nbsp;<a href="tel:<?= $cfg['general']['phone']['plain'] ?>"><?= $cfg['general']['phone']['nice'] ?></a></span>
                    <span class="only-mobile"><a href="tel:<?= $cfg['general']['phone']['plain'] ?>">
                        <img src="<?= $cfg['images'][6]['path'] ?>" alt="<?= $cfg['images'][6]['title'] ?>">
                        <img src="<?= $cfg['images'][7]['path'] ?>" alt="<?= $cfg['images'][7]['title'] ?>">
                    </a></span>
                </div>
                <div class="right">
                    <span class="only-desktop"><a href="mailto:<?= $cfg['general']['email_temporary'] ?>"><?= $cfg['general']['email'] ?></a></span>
                    <span class="only-mobile"><a href="mailto:<?= $cfg['general']['email_temporary'] ?>">
                        <div id="language" class="<?= $lang ?> only-mobile">
                            <div class="language-wrapper">
                                <a href="https://www.hochkoenigin.com" class="">de</a>
                                <span style="opacity:0.5;">|</span>
                                <a href="https://www.hochkoenigin.com/en" class="active">en</a>
                            </div>
                        </div>
                    </a></span>
                </div>
                <!-- LANGUAGE SWITCHER -->
                <div id="language" class="<?= $lang ?> only-desktop">
                    <div class="language-wrapper">
                        <a href="https://www.hochkoenigin.com" class="">de</a>
                        <span style="opacity:0.5;">|</span>
                        <a href="https://www.hochkoenigin.com/en" class="active">en</a>
                    </div>
                </div>
            </div>
            <div id="logo">
                <img src="<?= $cfg['images'][0]['path'] ?>" alt="<?= $cfg['images'][0]['title'] ?>">
            </div>
        </header>
        
        <div id="comming_soon">
            <img src="<?= $cfg['images'][2]['path'] ?>" alt="<?= $cfg['images'][2]['title'] ?>">
        </div>

        <!-- CONTENT -->
        <?php include($include_path); ?>

        <footer>
            <div id="icon">
                <img src="<?= $cfg['images'][3]['path'] ?>" alt="<?= $cfg['images'][3]['title'] ?>">
            </div>
            <div id="links">
                <a href="<?= $cfg['general']['gmap'] ?>" target="_blank">
                    <span><?= $cfg['general']['company-street'] ?>&nbsp;</span>
                    &middot;
                    <span>&nbsp;<?= $cfg['general']['company-city'] ?>&nbsp;</span>
                </a>
                <span class="hide-m-dot">&middot;</span>
                <span>&nbsp;TEL.&nbsp;<a href="tel:<?= $cfg['general']['phone']['plain'] ?>"><?= $cfg['general']['phone']['nice'] ?></a>&nbsp;</span>
                <span class="hide-m-dot-2">&middot;</span>
                <span>&nbsp;<a href="mailto:<?= $cfg['general']['email_temporary'] ?>"><?= $cfg['general']['email'] ?></a></span>
                <div class="social">
                    <a href="<?= $cfg['general']['facebook'] ?>" target="_blank">
                        <img src="<?= $cfg['images'][8]['path'] ?>" alt="<?= $cfg['images'][8]['title'] ?>">
                    </a>
                    <a href="<?= $cfg['general']['instagram'] ?>" target="_blank">
                        <img src="<?= $cfg['images'][9]['path'] ?>" alt="<?= $cfg['images'][9]['title'] ?>">
                    </a>
                </div>
            </div>
            <div class="small-links">
                <span><a href="<?= $cfg['general']['imprint'] . '?lang=' . $lang ?>"><?= $cfg['imprint']['headline'] ?></a></span>
                <span>&nbsp;&middot;&nbsp;</span>
                <span><a href="<?= $cfg['general']['privacy'] . '?lang=' . $lang ?>"><?= $cfg['privacy']['headline'] ?></a></span>
                <span>&nbsp;&middot;&nbsp;</span>
                <span><a href="<?= $cfg['general']['gtc'] . '?lang=' . $lang ?>"><?= $cfg['gtc']['headline'] ?></a></span>
            </div>
            <!-- <div class="small-links">
                <span><a href="http://preview.hochkoenigin.com/<?= $lang ?>/zs6/impressum.html"><?= $cfg['imprint']['headline'] ?></a></span>
                <span>&nbsp;&middot;&nbsp;</span>
                <span><a href="http://preview.hochkoenigin.com/<?= $lang ?>/3q1/datenschutz.html"><?= $cfg['privacy']['headline'] ?></a></span>
                <span>&nbsp;&middot;&nbsp;</span>
                <span><a href="http://preview.hochkoenigin.com/<?= $lang ?>/kq1/abg.html"><?= $cfg['gtc']['headline'] ?></a></span>
            </div> -->
        </footer>

        <div id="datepicker-container"></div>
    </body>
</html>
