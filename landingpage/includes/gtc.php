<section>
    <div id="gold_line" style="background-image: url(<?= $cfg['images'][4]['path'] ?>)"></div>
    <div id="text" class="inner">
        <h1><?= $cfg['gtc']['headline_inner'] ?></h1>
        <p><?= $cfg['gtc']['text'] ?></p>
    </div>
</section>
