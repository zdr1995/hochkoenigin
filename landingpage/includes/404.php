<section>
    <div id="gold_line" style="background-image: url(<?= $cfg['images'][4]['path'] ?>)"></div>
    <div id="text" class="inner">
        <h1><?= $cfg['404']['headline'] ?></h1>
        <p><?= $cfg['404']['text'] ?></p>
    </div>
</section>
