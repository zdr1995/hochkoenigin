<?php $lang = isset($_GET['lang']) && !empty($_GET['lang']) ? $_GET['lang'] : 'de'; ?>

<section>
  <div id="gold_line" style="background-image: url(<?= $cfg['images'][4]['path'] ?>)"></div>
  <div id="text" class="inner">
    <?= $cfg['content']['headline_1'] ?>
    <?= $cfg['content']['headline_2'] ?>
    <div class="collumns">
      <?= $cfg['content']['text_1_l'] ?>
      <?= $cfg['content']['text_1_r'] ?>
    </div>
    <?= $cfg['content']['text_2'] ?>
    <?= $cfg['content']['text_3'] ?>
  </div>
</section>

<section>
  <div  id="slider" class="inner">
    <div class="bxSlider">
      <?php foreach ($cfg['gallery'] as $_s => $slide) { ?>
        <div class="slide">
          <img src="<?= $slide['path'] ?>" alt="<?= $slide['title'] ?>">
        </div>
      <?php } ?>
    </div>
  </div>
</section>

<section>
  <div id="offer" class="inner">
    <h3 class="red"><?= $cfg['offer']['headline'] ?></h3>
    <?= $cfg['offer']['image'] ?>
    <p><?= $cfg['offer']['text'] ?></p>
  </div>
</section>

<section>
  <div id="request" class="inner">
    <div class="center">
        <!-- <a href="http://preview.hochkoenigin.com/de/moz/anfragen#request-form" class="button request-button hide-<?= $lang == 'en' ? $lang : '' ?>"><?= $cfg['offer']['button'] ?></a>
        <a href="http://preview.hochkoenigin.com/en/moz/request#request-form" class="button request-button hide-<?= $lang == 'de' ? $lang : '' ?>"><?= $cfg['offer']['button'] ?></a> -->
        <a href="https://www.hochkoenigin.com/de/nv7/anfragen.html" class="button request-button hide-<?= $lang == 'en' ? $lang : '' ?>"><?= $cfg['offer']['button'] ?></a>
        <a href="https://www.hochkoenigin.com/de/nv7/anfragen.html" class="button request-button hide-<?= $lang == 'de' ? $lang : '' ?>"><?= $cfg['offer']['button'] ?></a>
    </div>
  </div>
</section>

<section id="prices">
  <!-- <div class="inner">
    <h3 class="green"><?= $cfg['prices']['headline'] ?></h3>
  </div> -->
  <div class="full-inner">
    <?= $cfg['prices']['text'] ?>
  </div>
  <!-- <div class="inner center">
    <?= $cfg['prices']['button'] ?>
  </div> -->
</section>
