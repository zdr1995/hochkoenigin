<section>
    <div id="gold_line" style="background-image: url(<?= $cfg['images'][4]['path'] ?>)"></div>
    <div id="text" class="inner">
        <h1><?= $cfg['imprint']['headline'] ?></h1>
        <p><?= $cfg['imprint']['text'] ?></p>
    </div>
</section>
