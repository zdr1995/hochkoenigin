<section>
    <div id="gold_line" style="background-image: url(<?= $cfg['images'][4]['path'] ?>)"></div>
    <div id="text" class="inner">
        <h1><?= $cfg['privacy']['headline'] ?></h1>
        <p><?= $cfg['privacy']['text'] ?></p>
    </div>
</section>
