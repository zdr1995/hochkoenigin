<?php
$valid = true;
$classes = [
	'email' => '',
	'firstname' => '',
	'lastname' => '',
	'arrival' => '',
	'departure' => '',
];
if(!isset($_POST['email']) || empty($_POST['email'])){
	$valid = false;
	$classes['email'] = ' error';
}
if(!isset($_POST['firstname']) || empty($_POST['firstname'])){
	$valid = false;
	$classes['firstname'] = ' error';
}
if(!isset($_POST['lastname']) || empty($_POST['lastname'])){
	$valid = false;
	$classes['lastname'] = ' error';
}
if(!isset($_POST['arrival']) || empty($_POST['arrival'])){
	$valid = false;
	$classes['arrival'] = ' error';
}
if(!isset($_POST['departure']) || empty($_POST['departure'])){
	$valid = false;
	$classes['departure'] = ' error';
}

if($valid === true){
	if(!isset($_POST['c-info']) || !isset($_POST['h-info']) || $_POST['c-info'] != 'all-clear' || !empty($_POST['h-info'])){
		// the form was send with js deactivated or with the honeypot filled out.
		// we will not send it, but let the bot belive so. ?>
		<div id="requestform_de" class="msg success bot">
			Ihre Anfrage wurde erfolgreich abgesendet. Wir werden uns bei Ihnen melden.
		</div>
	<?php } else{

		$table = '';
		$alternate = 0;
		foreach($_POST as $key => $value){
			$alternate = $alternate == 1 ? 2 : 1;
			$tr_attr = $alternate%2 == 1 ? ' style="background: #ddd;"' : '';
			if(in_array($key, ['g-recaptcha-response', 'h-info', 'c-info'])) continue;
			switch ($key) {
				case 'arrival':
				case 'departure':
					switch ($key) {
						case 'arrival':
							$key = 'Anreise';
							break;
						case 'departure':
							$key = 'Abreise';
							break;
					}
					$table .= '<tr' . $tr_attr . '>
						<td valign="top"><strong>' . $key . ': </strong></td>
						<td valign="top">&nbsp;</td>
						<td valign="top">' . date('d.m.Y', strtotime($value)) . '</td>
					</tr>';
					break;
				case 'rooms':
					$key = 'Zimmer';
					if(is_array($value) && count($value) > 0){
						$table .= '<tr' . $tr_attr . '>
							<td valign="top"><strong>' . $key . ': </strong></td>
							<td valign="top">&nbsp;</td>
							<td valign="top"><ul>';
							foreach($value as $room){
								if(isset($room['room']) && !empty($room['room'])){
									$table .= '<li>';
									$table .= '<strong>' . $room['room'] . '</strong>';
									if(isset($room['package']) && !empty($room['package'])){
										$table .= '<br /><i>' . $room['package'] . '</i>';
									}
									$personstring = $and = '';
									if(isset($room['adults']) && !empty($room['adults'])){
										if($room['adults'] == 1){
											$personstring .= $room['adults'] . ' Erwachsener';
										} else{
											$personstring .= $room['adults'] . ' Erwachsene';
										}
										$and = ' und ';
									}
									if(isset($room['children']) && !empty($room['children'])){
										if($room['children'] == 1){
											$personstring .= $and . $room['children'] . ' Kind';
										} else{
											$personstring .= $and . $room['children'] . ' Kinder';
										}
										if(isset($room['ages']) && is_array($room['ages'])){
											$age_string = $glue = '';
											$cnt = 1;
											foreach($room['ages'] as $age){
												if($cnt > 1 && $cnt >= count($room['ages'])){
													$glue = ' und ';
												}
												$age_string .= $glue . $age['age'];
												$glue = ', ';
												$cnt++;
											}
											if(!empty($age_string)){
												$personstring .= ' (im Alter von ' . $age_string . ')';
											}
										}
									}
									if(!empty($personstring)){
										$table .= '<br /><span>' . $personstring . '</span>';
									}
									$table .= '</li>';
								}
							}
						$table .= '</ul></td>
						</tr>';
					}
					break;
				default:
					switch ($key) {
						case 'salutation':
							$key = 'Anrede';
							break;
						case 'title':
							$key = 'Titel';
							break;
						case 'firstname':
							$key = 'Vorname';
							break;
						case 'lastname':
							$key = 'Nachname';
							break;
						case 'email':
							$key = 'E-Mail';
							break;
						case 'address':
							$key = 'Adresse';
							break;
						case 'zip':
							$key = 'PLZ';
							break;
						case 'city':
							$key = 'Stadt';
							break;
						case 'country':
							$key = 'Land';
							break;
						case 'phone':
							$key = 'Telefonnummer';
							break;
						case 'message':
							$key = 'Nachricht';
							break;
					}
					if(is_array($value)){
						$table .= '<tr' . $tr_attr . '>
							<td valign="top"><strong>' . $key . ': </strong></td>
							<td valign="top">&nbsp;</td>
							<td valign="top"><ul>';
							foreach($value as $v){
								$table .= '<li>' . $v . '</li>';
							}
						$table .= '</ul></td>
						</tr>';
					} else{
						$table .= '<tr' . $tr_attr . '>
							<td valign="top"><strong>' . $key . ': </strong></td>
							<td valign="top">&nbsp;</td>
							<td valign="top">' . $value . '</td>
						</tr>';
					}
					break;
			}
		}
		if(!empty($table)){
			$table = '<table width="100%" cellpadding="5" cellspacing="0" border="0">' . $table . '</table>';
		}
		// die($table);
		// show errors for debugging
		// ini_set('display_errors', 1);
		// ini_set('display_startup_errors', 1);
		// error_reporting(E_ALL);
		$betreff = 'Anfrage über hochkönigin.com';
		$nachricht = 'Folgende Anfrage ist über hochkönigin.com eingegangen:<br /><br />' . $table . '<br /><br />Bitte melden Sie sich beim Gast.';
		$header = 'MIME-Version: 1.0' . "\r\n" .
				'Content-type: text/html; charset=utf-8' . "\r\n" .
				'From: webmaster@hochkoenigin.com' . "\r\n" .
		    'Reply-To: urlaub@hochkoenigin.com' . "\r\n" .
		    'X-Mailer: PHP/' . phpversion();

		if(mail($cfg['general']['form-recipient'], $betreff, $nachricht, $header)){ ?>
			<div id="requestform" class="msg success">
				Ihre Anfrage wurde erfolgreich abgesendet. Wir werden uns bei Ihnen melden.
			</div>
			<script>
				var dataObject = {
					'event': 'Anfrageformular',
					'category': 'gesendet',
					// 'label': 'foo'
				};
				if(typeof dataLayer != 'undefined'){
					dataLayer.push(dataObject);
				}
			</script>
		<?php } else{ ?>
			<div id="requestform" class="msg error">
				Ihre Anfrage konnte nicht abgeschickt werden. Bisste versuchen Sie es später erneut. Gerne können Sie sich auch telefonisch bei uns melden.
			</div>
		<?php } ?>
	<?php } ?>
<?php } else{ ?>

	<script>
		function onFormSubmit(token) {
			//verify token
			$.post( "/verifyReCaptcha.php", {
				'response': token,
				'remoteip': '<?= $_SERVER['REMOTE_ADDR'] ?>',
			})
			.always(function(response) {
				var response = $.parseJSON(response);
				if(response.success === true){
					$('#FormC-info').attr('value', response.recaptchaConfirm);
						document.getElementById("requestform").submit();
				} else{
					$('.captcha-error').fadeIn();
				}
			});
		}
	</script>

	<form id="requestform" method="post" action="#requestform">
		<input type="hidden" name="c-info" id="FormC-info" />
		<input type="hidden" name="h-info" id="FormH-info" />
		<div class="form-cols">
			<div class="col left">
	      <!-- ARRIVAL -->
					<div class="input  text required<?= $classes['arrival'] ?>">
	          <label for="arrival">Anreise<span class="asterisks">*</span></label>
						<?php $value = isset($_POST['arrival']) && !empty($_POST['arrival']) ? $_POST['arrival'] : ''; ?>
	          <input data-validation="required" type="text" name="arrival" class="date date-from mobile-placeholder" data-date-range="request" data-date-min="2019-04-24" required="required" placeholder="Anreise" id="arrival" value="<?= $value ?>">
	        </div>
			</div>
			<div class="col right">
	      <!-- DEPARTURE -->
					<div class="input  text required<?= $classes['departure'] ?>">
	          <label for="departure">Abreise<span class="asterisks">*</span></label>
						<?php $value = isset($_POST['departure']) && !empty($_POST['departure']) ? $_POST['departure'] : ''; ?>
	          <input data-validation="required" type="text" name="departure" class="date date-to mobile-placeholder" data-date-range="request" data-date-min="2019-04-24" required="required" placeholder="Abreise" id="departure" value="<?= $value ?>">
	        </div>
			</div>
		</div>

		<!-- rooms -->
		<div class="rooms-label label">Zimmer</div>
		<div class="rooms-wrap">
			<?php if(!isset($_POST['rooms']) || !is_array($_POST['rooms'])){
				$_POST['rooms'] = [
					[
						'room' => '',
            'adults' => '',
            'package' => '',
            'children' => '',
            'ages' => [
							[
								'age' => ''
							]
						]
					]
				];
			} ?>
			<?php foreach($_POST['rooms'] as $room_key => $room){ ?>
				<div class="room-wrap" id="room-<?= $room_key ?>" data-room-key="<?= $room_key ?>">
		      <a href="javascript:void(0)" onclick="selecotraction(this)" class="room-remove room-nodelete" title="Zimmer entfernen"><i class="fa fa-times"></i></a>
		      <!-- ROOM CATEGORY -->
		        <div class="input  select">
		          <label for="rooms-<?= $room_key ?>-room">Zimmerkategorie</label>
							<?php $value = isset($_POST['rooms'][$room_key]['room']) && !empty($_POST['rooms'][$room_key]['room']) ? $_POST['rooms'][$room_key]['room'] : ''; ?>
		          <select name="rooms[<?= $room_key ?>][room]" class="room-select" id="rooms-<?= $room_key ?>-room">
		            <option value="" <?= $value == '' ? 'selected="selected"' : '' ?>>- Bitte wählen Sie ein Zimmer -</option>
		            <option value="Comfort Suite Hochkönigin" <?= $value == 'Comfort Suite Hochkönigin' ? 'selected="selected"' : '' ?>>Comfort Suite Hochkönigin</option>
		            <option value="Premium Suite Hochkönigin" <?= $value == 'Premium Suite Hochkönigin' ? 'selected="selected"' : '' ?>>Premium Suite Hochkönigin"</option>
		            <option value="Star Suite Hochkönigin" <?= $value == 'Star Suite Hochkönigin' ? 'selected="selected"' : '' ?>>Star Suite Hochkönigin "</option>
		            <option value="Penthouse Suite Hochkönigin" <?= $value == 'Penthouse Suite Hochkönigin' ? 'selected="selected"' : '' ?>>Penthouse Suite Hochkönigin"</option>
		            <option value="Penthouse Spa Suite Hochkönigin" <?= $value == 'Penthouse Spa Suite Hochkönigin' ? 'selected="selected"' : '' ?>>Penthouse Spa Suite Hochkönigin "</option>
		            <option value="Comfort Studio Maria Alm" <?= $value == 'Comfort Studio Maria Alm' ? 'selected="selected"' : '' ?>>Comfort Studio Maria Alm "</option>
		            <option value="Classic Comfort Studio Maria Alm" <?= $value == 'Classic Comfort Studio Maria Alm' ? 'selected="selected"' : '' ?>>Classic Comfort Studio Maria Alm "</option>
		            <option value="Comfort Double Room Maria Alm" <?= $value == 'Comfort Double Room Maria Alm' ? 'selected="selected"' : '' ?>>Comfort Double Room Maria Alm "</option>
		          </select>
		        </div>
					<div class="form-cols">
			      <div class="room-col room-col-1 col">
			        <!-- ADULTS -->
			          <div class="input  number">
									<?php $value = isset($_POST['rooms'][$room_key]['adults']) && !empty($_POST['rooms'][$room_key]['adults']) ? $_POST['rooms'][$room_key]['adults'] : ''; ?>
			            <label for="rooms-<?= $room_key ?>-adults">Erwachsene</label>
			            <input type="number" name="rooms[<?= $room_key ?>][adults]" class="room-adults" placeholder="Erwachsene" step="1" min="0" max="10" id="rooms-<?= $room_key ?>-adults" value="<?= $value ?>">
			          </div>
			        <!-- PACKAGE -->
			          <div class="input  select">
			            <label for="rooms-<?= $room_key ?>-package">Paket</label>
									<?php $value = isset($_POST['rooms'][$room_key]['package']) && !empty($_POST['rooms'][$room_key]['package']) ? $_POST['rooms'][$room_key]['package'] : ''; ?>
			            <select name="rooms[<?= $room_key ?>][package]" class="package-select" id="rooms-<?= $room_key ?>-package">
			              <option value="" <?= $value == '' ? 'selected="selected"' : '' ?>>- Bitte Paket auswählen -</option>
			              <option value="Salzburger Advent" <?= $value == 'Salzburger Advent' ? 'selected="selected"' : '' ?>>Salzburger Advent</option>
			              <option value="Weihnachten" <?= $value == 'Weihnachten' ? 'selected="selected"' : '' ?>>Weihnachten</option>
			              <option value="Pulverschnee Wochen" <?= $value == 'Pulverschnee Wochen' ? 'selected="selected"' : '' ?>>Pulverschnee Wochen</option>
			              <option value="Firnschnee Woche" <?= $value == 'Firnschnee Woche' ? 'selected="selected"' : '' ?>>Firnschnee Woche</option>
			              <option value="Winterzauber" <?= $value == 'Winterzauber' ? 'selected="selected"' : '' ?>>Winterzauber</option>
			              <option value="Fasching" <?= $value == 'Fasching' ? 'selected="selected"' : '' ?>>Fasching</option>
			              <option value="Silvester" <?= $value == 'Silvester' ? 'selected="selected"' : '' ?>>Silvester</option>
			            </select>
			          </div>
			      </div>
			      <div class="room-col room-col-2 col">
			        <!-- CHILDREN -->
			          <div class="input  number">
			            <label for="rooms-<?= $room_key ?>-children">Kinder</label>
									<?php $value = isset($_POST['rooms'][$room_key]['children']) && !empty($_POST['rooms'][$room_key]['children']) ? $_POST['rooms'][$room_key]['children'] : ''; ?>
			            <input type="number" name="rooms[<?= $room_key ?>][children]" class="room-children" placeholder="Kinder" step="1" min="0" id="rooms-<?= $room_key ?>-children" value="<?= $value ?>">
			          </div>
								<?php if(isset($_POST['rooms'][$room_key]['ages']) && is_array($_POST['rooms'][$room_key]['ages'])){ ?>
									<?php foreach($_POST['rooms'][$room_key]['ages'] as $age_key => $age){ ?>
										<div class="input number room-child-age">
				            	<label for="room-<?= $room_key ?>-age-<?= $age_key ?>">Kinderalter</label>
					            <input type="number" name="rooms[<?= $room_key ?>][ages][<?= $age_key ?>][age]" class="room-children age" placeholder="Kinderalter" step="1" min="0" id="room-<?= $room_key ?>-age-<?= $age_key ?>" value="<?= $age['age'] ?>">
		          			</div>
									<?php } ?>
								<?php } ?>
			      </div>
					</div>
		    </div>
			<?php } ?>
	    <a href="javascript:void(0)" class="button red room-add" onclick="selecotraction(this)"><i class="fas fa-plus"></i>Zimmer hinzufügen</a>
		</div>

		<div class="form-cols">
			<div class="col left">
	      <!-- SALUTATION -->
	  			<div class="input  select">
	          <label for="salutation">Anrede</label>
						<?php $value = isset($_POST['salutation']) && !empty($_POST['salutation']) ? $_POST['salutation'] : ''; ?>
	          <select name="salutation" id="salutation">
	            <option value="" <?= $value == '' ? 'selected="selected"' : '' ?>>-- Bitte wählen --</option>
							<option value="Frau" <?= $value == 'Frau' ? 'selected="selected"' : '' ?>>Frau</option>
	            <option value="Herr" <?= $value == 'Herr' ? 'selected="selected"' : '' ?>>Herr</option>
	          </select>
	        </div>
	      <!-- TITLE -->
	        <div class="input  text">
	          <label for="title">Titel</label>
						<?php $value = isset($_POST['title']) && !empty($_POST['title']) ? $_POST['title'] : ''; ?>
	          <input type="text" name="title" placeholder="Titel" class="mobile-placeholder" id="title" value="<?= $value ?>">
	        </div>
	      <!-- FIRSTNAME -->
	        <div class="input  text required<?= $classes['firstname'] ?>">
	          <label for="firstname">Vorname<span class="asterisks">*</span></label>
						<?php $value = isset($_POST['firstname']) && !empty($_POST['firstname']) ? $_POST['firstname'] : ''; ?>
	          <input data-validation="required" type="text" name="firstname" required="required" placeholder="Vorname" class="mobile-placeholder" id="firstname" value="<?= $value ?>">
	        </div>
	      <!-- LASTNAME -->
	        <div class="input  text required<?= $classes['lastname'] ?>">
	          <label for="lastname">Nachname<span class="asterisks">*</span></label>
						<?php $value = isset($_POST['lastname']) && !empty($_POST['lastname']) ? $_POST['lastname'] : ''; ?>
	          <input data-validation="required" type="text" name="lastname" required="required" placeholder="Nachname" class="mobile-placeholder" id="lastname" value="<?= $value ?>">
	        </div>
	      <!-- EMAIL -->
	        <div class="input  email required<?= $classes['email'] ?>">
	          <label for="email">E-Mail<span class="asterisks">*</span></label>
						<?php $value = isset($_POST['email']) && !empty($_POST['email']) ? $_POST['email'] : ''; ?>
	          <input data-validation="email" type="email" name="email" required="required" placeholder="E-Mail" class="mobile-placeholder" id="email" value="<?= $value ?>">
	        </div>
			</div>
			<div class="col right">
	      <!-- ADDRESS -->
	  			<div class="input  text">
	          <label for="address">Adresse</label>
						<?php $value = isset($_POST['address']) && !empty($_POST['address']) ? $_POST['address'] : ''; ?>
	          <input type="text" name="address" placeholder="Adresse" class="mobile-placeholder" id="address" value="<?= $value ?>">
	        </div>
	      <!-- ZIP -->
	        <div class="input  text">
	          <label for="zip">PLZ</label>
						<?php $value = isset($_POST['zip']) && !empty($_POST['zip']) ? $_POST['zip'] : ''; ?>
	          <input type="text" name="zip" placeholder="PLZ" class="mobile-placeholder" id="zip" value="<?= $value ?>">
	        </div>
	      <!-- CITY -->
	        <div class="input  text">
	          <label for="city">Ort</label>
						<?php $value = isset($_POST['city']) && !empty($_POST['city']) ? $_POST['city'] : ''; ?>
	          <input type="text" name="city" placeholder="Ort" class="mobile-placeholder" id="city" value="<?= $value ?>">
	        </div>
	      <!-- COUNTRY -->
	        <div class="input  select">
	          <label for="country">Land</label>
						<?php $countries = [
							'Deutschland',
							'Italien',
							'Schweiz',
							'Österreich',
							'Afghanistan',
							'Albanien',
							'Algerien',
							'Amerikanische Samoa-Inseln',
							'Andorra',
							'Angola',
							'Anguilla',
							'Antarktis',
							'Antigua and Barbuda',
							'Argentinien',
							'Armenia',
							'Aruba',
							'Australien',
							'Azerbaijan',
							'Bahamas',
							'Bahrain',
							'Bangladesh',
							'Barbados',
							'Belarus',
							'Belgien',
							'Belize',
							'Benin',
							'Bermuda',
							'Bhutan',
							'Bolivia',
							'Bosnia and Herzegovina',
							'Botswana',
							'Bouvet Island',
							'Brasilien',
							'British Indian Ocean Territory',
							'Brunei Darussalam',
							'Bulgarien',
							'Burkina Faso',
							'Burundi',
							'Cameroon',
							'Cape Verde',
							'Cayman Islands',
							'Central African Republic',
							'Chad',
							'Chile',
							'China',
							'Christmas Island',
							'Cocos Islands',
							'Comoros',
							'Cook Islands',
							'Costa Rica',
							'Côte DIvoire',
							'Denmark',
							'Djibouti',
							'Dominica',
							'Dominican Republic',
							'Ecuador',
							'El Salvador',
							'Equatorial Guinea',
							'Eritrea',
							'Estonia',
							'Ethiopia',
							'Falkland Islands',
							'Faroe Islands',
							'Fiji',
							'Finnland',
							'Frankreich',
							'French Guiana',
							'French Polynesia',
							'French Southern Territories',
							'Gabon',
							'Gambia',
							'Georgia',
							'Ghana',
							'Gibraltar',
							'Greenland',
							'Grenada',
							'Griechenland',
							'Guadeloupe',
							'Guam',
							'Guatemala',
							'Guernsey',
							'Guinea',
							'Guinea-Bissau',
							'Guyana',
							'Haiti',
							'Heard Island and McDonald Islands',
							'Honduras',
							'Hong Kong',
							'Indien',
							'Indonesia',
							'Iran',
							'Iraq',
							'Irland',
							'Island',
							'Isle of Man',
							'Israel',
							'Jamaica',
							'Japan',
							'Jersey',
							'Jordan',
							'Kambodscha',
							'Kanada',
							'Kazakhstan',
							'Kenya',
							'Kiribati',
							'Kolumbien',
							'Kongo',
							'Kroatien',
							'Kuba',
							'Kuwait',
							'Kyrgyzstan',
							'Laos',
							'Latvia',
							'Lebanon',
							'Lesotho',
							'Liberia',
							'Libyan Arab Jamahiriya',
							'Liechtenstein',
							'Lithuania',
							'Luxembourg',
							'Macao',
							'Macedonia',
							'Madagascar',
							'Malawi',
							'Malaysia',
							'Maldives',
							'Mali',
							'Malta',
							'Marshall Islands',
							'Martinique',
							'Mauritania',
							'Mauritius',
							'Mayotte',
							'Mexiko',
							'Micronesia',
							'Moldova',
							'Monaco',
							'Mongolia',
							'Montenegro',
							'Montserrat',
							'Morocco',
							'Mozambique',
							'Myanmar',
							'Namibia',
							'Nauru',
							'Nepal',
							'Netherlands Antilles',
							'Neuseeland',
							'New Caledonia',
							'Nicaragua',
							'Niederlande',
							'Niger',
							'Nigeria',
							'Niue',
							'Norfolk Island',
							'North Korea',
							'Northern Mariana Islands',
							'Norwegen',
							'Oman',
							'Pakistan',
							'Palau',
							'Panama',
							'Papua New Guinea',
							'Paraguay',
							'Peru',
							'Philippines',
							'Pitcairn',
							'Polen',
							'Portugal',
							'Puerto Rico',
							'Qatar',
							'Reunion',
							'Rumänien',
							'Russland',
							'Rwanda',
							'Saint Barthélemy',
							'Saint Helena',
							'Saint Kitts and Nevis',
							'Saint Lucia',
							'Saint Martin',
							'Saint Pierre and Miquelon',
							'Saint Vincent and the Grenadines',
							'Samoa',
							'San Marino',
							'Sao Tome and Principe',
							'Saudi Arabia',
							'Schweden',
							'Senegal',
							'Serbia',
							'Seychelles',
							'Sierra Leone',
							'Singapore',
							'Slowakei',
							'Slowenien',
							'Solomon Islands',
							'Somalia',
							'South Africa',
							'South Georgia and the South Sandwich Islands',
							'South Korea',
							'Spanien',
							'Sri Lanka',
							'Sudan',
							'Suriname',
							'Svalbard and Jan Mayen',
							'Swaziland',
							'Syrian Arab Republic',
							'Taiwan',
							'Tajikistan',
							'Tanzania',
							'Thailand',
							'Timor-Leste',
							'Togo',
							'Tokelau',
							'Tonga',
							'Trinidad and Tobago',
							'Tschechien',
							'Tunisia',
							'Turkey',
							'Turkmenistan',
							'Turks and Caicos Islands',
							'Tuvalu',
							'USA',
							'Uganda',
							'Ukraine',
							'Ungarn',
							'United Arab Emirates',
							'Uruguay',
							'Uzbekistan',
							'Vanuatu',
							'Vatican City',
							'Venezuela',
							'Vereinigtes Königreich',
							'Viet Nam',
							'Virgin Islands, British',
							'Virgin Islands, U.S.',
							'Wallis And Futuna',
							'Western Sahara',
							'Yemen',
							'Zambia',
							'Zimbabwe',
							'Zypern',
							'Ägypten',
							'Åland Islands',
						]; ?>
						<?php $value = isset($_POST['country']) && !empty($_POST['country']) ? $_POST['country'] : ''; ?>
	          <select name="country" id="country">
	            <option <?= $value == '' ? 'selected="selected"' : '' ?> value="">-- Bitte wählen --</option>
							<?php foreach($countries as $country){ ?>
		            <option <?= $value == $country ? 'selected="selected"' : '' ?> value="<?= $country ?>"><?= $country ?></option>
							<?php } ?>
	          </select>
	        </div>
	      <!-- PHONE -->
	        <div class="input tel">
	          <label for="phone">Telefon</label>
						<?php $value = isset($_POST['phone']) && !empty($_POST['phone']) ? $_POST['phone'] : ''; ?>
	          <input type="tel" name="phone" placeholder="Telefon" class="mobile-placeholder" id="phone" value="<?= $value ?>">
	        </div>
			</div>
		</div>

	  <!-- MESSAGE -->
	  	<div class="input  textarea">
	      <label for="message">Nachricht</label>
				<?php $value = isset($_POST['message']) && !empty($_POST['message']) ? $_POST['message'] : ''; ?>
	      <textarea name="message" placeholder="Nachricht" class="mobile-placeholder" id="message" rows="5"><?= $value ?></textarea>
	    </div>

	  <section class="captcha hidden-print">
      <div class="input msg error captcha-error" style="display:none;">
				Das Formular konnte nicht abgeschickt werden weil die Captcha-Überprüfung fehlschlug.
	    </div>
	    <!-- SUBMIT BUTTON -->
	      <div class="submit">
					<button class="g-recaptcha button" data-sitekey="6LcW-58UAAAAAFWfd4ZcEAGSrtbXuTRECOHzoAD7" data-callback="onFormSubmit">Anfragen</button>
	      </div>
	  </section>

	</form>
<?php } ?>
