<?php
namespace Frontend\Controller;

use Frontend\Controller\AppController;
use Cake\Controller\Controller;
use Cake\Cache\Cache;

class RoomsController extends AppController {

    public function index($id = false) {
        
		//get code
		$code = $this->getCode($id);
		
        // get content
        if (($content = Cache::read('room_content')) === false && Cache::read('room_content_ids') === $id) {
            $content = $this->getContent($id, $code);
            Cache::write('room_content', $content);
            Cache::write('room_content_ids', $id);
        } else {
            $content = $this->getContent($id, $code);
            Cache::write('room_content_ids', $id);
        }
        
        // get prices
        if (($prices = Cache::read('room_prices')) === false && Cache::read('room_prices_ids') === $id) {
            $prices = $this->getPrices($id, $code);
            Cache::write('room_prices', $prices);
        } else {
            $prices = $this->getPrices($id, $code);
            Cache::write('room_prices_ids', $id);
        }
        
        // set vars
        $this->set('title', $content['html']);
        $this->set('content', $content);
        $this->set('prices', $prices);

        // render
        $this->render(DS . ucfirst($this->request->params['structure']['theme']) . DS . $this->name . DS . 'index' . DS);
    }

}
