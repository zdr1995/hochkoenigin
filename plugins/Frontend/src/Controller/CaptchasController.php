<?php
/**
 * CakePHP(tm) : Rapid Development Framework (http://cakephp.org)
 * Copyright (c) Cake Software Foundation, Inc. (http://cakefoundation.org)
 *
 * Licensed under The MIT License
 * For full copyright and license information, please see the LICENSE.txt
 * Redistributions of files must retain the above copyright notice.
 *
 * @copyright Copyright (c) Cake Software Foundation, Inc. (http://cakefoundation.org)
 * @link      http://cakephp.org CakePHP(tm) Project
 * @since     0.2.9
 * @license   http://www.opensource.org/licenses/mit-license.php MIT License
 */
namespace Frontend\Controller;

use Frontend\Controller\AppController;
use Cake\Controller\Controller;
use Cake\Cache\Cache;
use Cake\Network\Exception\NotFoundException;
use Cake\View\Exception\MissingTemplateException;
use Cake\Core\Configure;

/**
 * Static content controller
 *
 * This controller will render views from Template/Pages/
 *
 * @link http://book.cakephp.org/3.0/en/controllers/pages-controller.html
 */
class CaptchasController extends AppController
{

//    function beforeFilter() {
//        $this->Auth->allow('math','text');
//        return parent::beforeFilter();
//    }
    public function makeCaptcha($type= 'text')
    {
        if($type== 'text')
            $this->text();
        else $this->math();
    }
    public function math() {

        // init
        $operations = array('+');
        $infos = array(
            'type' => 'math',
            'value1' => rand(1,9),
            'value2' => rand(1,9),
            'operation' => $operations[array_rand($operations)],
        );
        $text = $infos['value1'] . ' ' . $infos['operation'] . ' ' . $infos['value2'] . ' =';
        // save values in session
        $session= $this->request->session();
//        print_r($infos['value1']+$infos['value2']);die();
        $session->write('captcha', strval($infos['value1']+$infos['value2']));

        // show captcha
        $this->show($text);

    }

    public function text() {

        // init
        $length = 5;
        $infos = array(
            'type' => 'text',
            'text' => substr(str_shuffle("0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ"), 0, $length),
        );
        $text = $infos['text'];

        // save values in session
        $session= $this->request->session();
        $session->write('captcha', $infos);

        // show captcha
        $this->show($text);

    }

    public function show($text){

        // init
        $settings = Configure::read('Config.captcha');
        $font = WWW_ROOT . 'frontend' . DS . 'fonts' . DS . $settings['font'];
        // create captcha
        $captcha = imagecreatetruecolor($settings['width'], $settings['height']);
        imagesavealpha($captcha, true);
        imageantialias($captcha, true);
        $background = imagecolorallocate($captcha, $settings['background'][0], $settings['background'][1], $settings['background'][2]);
        $color = imagecolorallocate($captcha, $settings['color'][0], $settings['color'][1], $settings['color'][2]);
        imagefilledrectangle($captcha, 0, 0, $settings['width'], $settings['height'], $background);

        // calculate
        $box = imagettfbbox($settings['size'], $settings['angle'], $font, $text);

        $min_x = min( array( $box[0], $box[2], $box[4], $box[6] ) );
        $max_x = max( array( $box[0], $box[2], $box[4], $box[6] ) );
        $min_y = min( array( $box[1], $box[3], $box[5], $box[7] ) );
        $max_y = max( array( $box[1], $box[3], $box[5], $box[7] ) );

        $box = array(
            'left' => ( $min_x >= -1 ) ? -abs( $min_x + 1 ) : abs( $min_x + 2 ),
            'top' => abs( $min_y ),
            'width' => $max_x - $min_x,
            'height' => $max_y - $min_y,
            'box' => $box
        );

        $top  = ($settings['height']/2) - ($box['height']/2) + $box['top'];
        $left = ($settings['width']/2) - ($box['width']/2) + $box['left'];
        imagettftext($captcha, $settings['size'], $settings['angle'], $left, $top, $color, $font, $text);
        header("Content-type: image/png");
        imagepng($captcha);
        exit;

    }

    public function checkCaptcha()
    {
        if(array_key_exists('REQUEST_METHOD',$_SERVER) && $_SERVER['REQUEST_METHOD']==='POST' && array_key_exists('value', $this->request->data))
        {
            $value= $this->request->data['value'];
            $session= $this->request->session();
            $captcha_text= $session->read('captcha');
            if(array_key_exists('text', $captcha_text))
            {
                //  for text captcha
                $captcha_text= $captcha_text['text'];
                $session->destroy('captcha');
//                print_r($captcha_text);die();
                echo json_encode(['success'=> strval($value)==strval($captcha_text)]);
                die();

            }else if(!empty($captcha_text)) {
                // for numeric captcha
                $session->destroy('captcha');
                echo json_encode(['success'=> strval($value)==strval($captcha_text)]);
                die();
            } else {
                echo json_encode(['success'=>false]);
                die();
            }

        }else {
            echo json_encode(['success'=>false]);
            die();
        }
    }

    public function test()
    {
        print_r('tu si');die();
    }
}
