<?php
namespace Frontend\Controller;

use Frontend\Controller\AppController;
use Cake\Controller\Controller;
use Cake\Cache\Cache;

class PackagesController extends AppController {

    public function index($id = false) {

		//get code
        $code = $this->getCode($id);


        // get content
        if (($content = Cache::read('packages_contents')) === false && Cache::read('packages_contents_ids') === $id) {
            $content = $this->getContent($id, $code);
            Cache::write('packages_contents', $content);
            Cache::write('packages_contents_ids', $id);
        } else {
            $content = $this->getContent($id, $code);
            Cache::write('packages_contents_ids', $id);
        }
//        echo '<pre>'.var_export($content).'</pre>';die();
          
        // get rooms
        // old condition: ($__rooms_content = Cache::read('packages_rooms')) === false
        if (true == 1) {
            foreach ($this->getRooms() as $_k => $_v) {
                $__rooms_content[$this->getContent($_k, 'room')['id']] = $this->getContent($_k, 'room');
            }
            Cache::write('packages_rooms', $__rooms_content);
        }

        // get prices
        if (($prices = Cache::read('packages_prices')) === false && Cache::read('packages_prices_ids') === $id) {
            $prices = $this->getPrices($id, $code);
            Cache::write('packages_prices', $prices);
            Cache::write('packages_prices_ids', $id);
        } else {
            $prices = $this->getPrices($id, $code);
            Cache::write('packages_prices_ids', $id);
        }
            
        // set vars
        $this->set('title', $content['html']);
        $this->set('content', $content);
        $this->set('prices', $prices);
        $this->set('_rooms', $__rooms_content);

        // render
        $this->render(DS . ucfirst($this->request->params['structure']['theme']) . DS . $this->name . DS . 'index' . DS);

    }

}
