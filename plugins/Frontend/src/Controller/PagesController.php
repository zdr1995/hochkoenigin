<?php
namespace Frontend\Controller;

use Frontend\Controller\AppController;
use Frontend\Model\Table\FormsTable;
use Cake\Controller\Controller;
use Cake\Core\Configure;
use Cake\ORM\TableRegistry;
use Frontend\Form\FrontendForm;
use Cake\Cache\Cache;
use Cake\Mailer\MailerAwareTrait;
use Cake\Utility\Text;


class PagesController extends AppController {
    use MailerAwareTrait;


    public function index($id = false) {


        $this->Forms = new FormsTable(['connection' => $this->connection]);

        $post_sent = false;

        // Process LastMinute Form
        // get content
        if (($content = Cache::read('pages_contents')) === false && Cache::read('pages_contents_ids') === $id) {
            $content = $this->getContent($id, $this->getCode($id));
            Cache::write('pages_contents', $content);
            Cache::write('pages_contents_ids', $id);
        } else {
            $content = $this->getContent($id, $this->getCode($id));
            Cache::write('pages_contents_ids', $id);
        }

        // salutations
        Configure::load('salutations');
        $salutations = [];
        foreach(Configure::read('salutations') as $k => $v){
            $salutations[$k] = $v['short'];
        }

        // countries
        Configure::load('countries');
        $countries = [];
        foreach(Configure::read('countries') as $k => $v){
            $countries[$k] = $v['name'];
        }

        $type = array_key_exists('view', $content) ? $content['view'] : '';
        $form = new FrontendForm($type.ucfirst('lastminute'), $this->request->session()->read('Captcha.' . $this->request->params['route']));

        if($this->request->is('post') && (strpos( $this->request->url, 'last-minute-angebote') !== false) ) {
            $post_sent = $this->processLastminuteForm($form, $content);
        }



        // set vars
        $this->set('title', $content['html']);
        $this->set('content', $content);
        if($_SERVER['REMOTE_ADDR'] == '176.104.105.50' || $_SERVER['REMOTE_ADDR'] == '213.182.253.2') {

            if(array_key_exists('internal', $content) && $content['internal'] == 'Last-Minute-Angebote')
            {
                $rooms = $this->getRooms();
                $last_minutes_id = $content['media'];
                if(array_key_exists('media-bottom', $last_minutes_id))
                    $last_minutes_id = $last_minutes_id['media-bottom'];
                $ids = [];

//                if(count($ids))
//                {
                    $query = <<< 'EOQ'
                        SELECT *
                        FROM elements
                        WHERE code like 'lastminute'
                        AND elements.active = 1
                    EOQ;
                    $l_minutes = $this->connection->execute($query)->fetchAll('assoc');
                    $last_minutes = $this->parseContent($l_minutes);
                    foreach ($last_minutes as &$last_minute) {
                        $details = @json_decode($last_minute['fields'], JSON_FORCE_ARRAY);
                        foreach($details as $k => $detail) 
                            $last_minute[$k] = $detail;
                        

                        if(array_key_exists('image', $last_minute) && !empty($last_minute['image']))
                        {
                            $image_id = @explode(':', $last_minute['image']);
                            if(array_key_exists(1, $image_id))
                            {
                                    $images = $this->Images
                                        ->find()
                                        ->where(['Images.id' => $image_id[1]])
                                        ->limit(1)
                                        ->formatResults(function ($results) {
                                            return $results->map(function ($row) {
                                                return $this->Images->afterFind($row);
                                            });
                                        })->toArray();
                                    $last_minute['c_image'] = $images[0];
                                    $last_minute['image'] = $images[0];
                            }
                        }
                            // fetch rooms
                        $rooms = [];
                        $rooms_ids = [];
                        if(is_array($last_minute['room'])) {
                            foreach($last_minute['room'] as $room)
                                $room_id = @explode('element:', $room)[1];
                        }else {
                            $room_id = @explode('element:',$last_minute['room'])[1];
                            $q = <<< 'EOQ'
                                SELECT * 
                                FROM elements
                                WHERE code='room'
                                AND id = :id
                            EOQ;
                            $rooms = $this->connection->execute($q, ['id' =>$room_id])->fetchAll('assoc');
                            foreach ($rooms as &$room) {
                                $room['fields'] = @json_decode($room['fields'],JSON_FORCE_ARRAY);
                                $q = <<< 'EOQ'
                                    SELECT *
                                    FROM images
                                    WHERE id = :id
                                EOQ;
                                $room['images'] = $this->connection->execute($q, ['id' => @explode(':', $room['fields']['images'])[1]])->fetchAll('assoc');
                                if(array_key_exists(0, $room['images']))
                                {
                                    $id = $room['images'][0]['id'];
                                    $room['images']['url'] = '';
                                    $urls = [];
                                    exec("find . -name '$id.jpg' -print | head -n 1",$urls);
                                    if(array_key_exists(0, $urls))
                                        $room['images']['url'] = substr($urls[0],1);
                                }
                                $room['images_small'] = $this->connection->execute($q, ['id' => @explode(':', $room['fields']['images_small'])[1]])->fetchAll('assoc');

                                if(array_key_exists('media', $room))
                                    $room['media'] = @json_decode($room['media'], JSON_FORCE_ARRAY);

                                // Fetch nodes
                                $q = <<< 'EOQ'
                                    SELECT *
                                    FROM nodes
                                    WHERE foreign_id = :room_id
                                    LIMIT 1;
                                EOQ;
                                $node = $this->connection->execute($q, ['room_id' => $room_id])->fetchAll('assoc');
                                if($node!=null && $node[0] != null)
                                    $room['node'] = $node;
                            }
                            $last_minute['room'] = $rooms;
                        }
                    }
                    array_walk_recursive($last_minutes, function(&$value, $key) {

                        if((in_array($key, ['fields', 'media']) )) {
                            $data=[];
                            if(is_string($value)) {
                                $data = @json_decode($value,JSON_FORCE_ARRAY);
                                $value = $data;
                            }else if(is_array($value)) {
                                $data= $value;
                            }
                            // Parse content if exists
                            if(is_array($data))
                            {

                                foreach($data as $k=> $val) {
                                    $multiple = @explode(";", $val);
                                    foreach($multiple as $single)
                                    {
                                        $str = @explode(":",$single);
                                        if(in_array($str[0],['node', 'image', 'images', 'images_small', 'element', 'media']))
                                        {
                                            $str[0] .= 's';

                                            if($str != null && (count($str) == 2)) {
                                                $query = <<< 'EOQ'
                                                    SELECT *
                                                    FROM %s
                                                    WHERE id = :id
                                                EOQ;
                                                $query = sprintf($query, $str[0]);
                                                $additional_data = $this->connection->execute($query, ['id' => $str[1]])->fetchAll('assoc');
                                                if($additional_data != null)
                                                    $value[$str[0]] = $additional_data;
                                            }
                                        }

                                    }
                                }
                            }
                        }
                    });

                    foreach ($last_minutes as &$last_minute)
                    {
                        $q = <<< EOQ
                            SELECT *
                            FROM i18n
                            WHERE foreign_key=:f_key
                            AND locale=:locale
                        EOQ;
                        $tr = $this->connection->execute($q, ['f_key' => $last_minute['id'], 'locale' => $this->request->language])->fetchAll('assoc');

                        foreach($tr as $t)
                        {
                            if(!array_key_exists('field', $t))
                                continue;

                            if($t['field'] == 'content')
                                $last_minute['content'] = $t['content'];

                            if($t['field'] == 'headline')
                                $last_minute['headline'] = $t['content'];

                            if($t['field'] == 'price_desc')
                                $last_minute['price_desc'] = $t['content'];
                        }
                    }

                    $last_minutes = $this->parseContent($last_minutes);

                    $this->set('content', $content);
                    $content['last_minute_offers']= $last_minutes;

                    // Parse ranges
                    foreach($last_minutes as &$last_minute)
                    {
                        $ranges = array_filter(explode("|", $last_minute['ranges']));
                        $valid_ranges = [];
                        foreach($ranges as $range){
                            list($_f, $_t) = explode(":", $range, 2);
                            if(strtotime($_t) > time()){
                                $valid_ranges[] = [
                                    'from' => strtotime($_f),
                                    'to' => strtotime($_t),
                                ];
                            }
                        }
                        $last_minute['ranges'] = $valid_ranges;
                    }

                $this->set('salutations', $salutations);
                $this->set('countries', $countries);
                $this->set('last_minute_offers', $last_minutes);
                $this->set('rooms', $rooms);
                $content['view'] = '';
                $messages = ['show' => false, 'success' => null, 'newsletter' => null];
                $messages['show'] = false;
                $this->set('messages', $messages);
                $this->set('form', $form);
                $this->set("room_details", $rooms);
//                echo "<pre>";
////                $this->vd($rooms);
//                echo"</pre>";

                $lang = $this->request->language;

                // If we sent mail
                if($post_sent == true)
                {
                    if($lang = 'de')
                    {
                        $this->set('messages', 'Email wurde erfolgreich Versendet.');
                    }else {
                        $this->set('messages', 'Email success sen\'t.');
                    }
                }
                $this->render(DS . ucfirst($this->request->params['structure']['theme']) . DS . 'Lastminute' . DS . 'index' . DS);

            }else {
                $this->render(DS . ucfirst($this->request->params['structure']['theme']) . DS . $this->name . DS . 'index' . DS);
            }
        }

        // render
        else $this->render(DS . ucfirst($this->request->params['structure']['theme']) . DS . $this->name . DS . 'index' . DS);
    }

    public function sitemap(){

        $sitemap = [];
        foreach(Configure::read('translations') as $k => $v){
            if($v['active']){
                $sitemap[$k] = $this->__crawl('', $this->request->params['structure']['id'], $k, false, false, false, false);
            }
        }

        $this->set('sitemap', $sitemap);

        $this->set('_serialize', false);
        $this->render('/Sitemaps/xml');
    }

    public function lastminute() {
        if($_SERVER["REMOTE_ADDR"] == '176.104.105.50') {
            var_dump('tu si');
            die();
        }
        var_dump('tu si');
        die();
    }

    public function fetchRoomsLastminute($content)
    {
        $rooms = $this->getRooms();
        $room_details = [];
        $iterator = $content['media']['media-bottom'];
        foreach ($iterator as $iter) {
            $_rooms = [];
            if(array_key_exists('details', $iter) && array_key_exists('lastminutes', $iter['details']))
            {
                $data = $iter['details']['lastminutes'];
                foreach($data as $row) {

                    // finally fetch ids
                    if(array_key_exists('details',$row) && array_key_exists('contain', $row['details']))
                    {
                        $ids = $row['details']['contain'];
                        foreach($ids as $lmo)
                        {
                            $tmp = $this->mediaElementDetails($lmo, ['image']);
//                            $this->vd($tmp);
                            if ((!array_key_exists('quota', $tmp) || $tmp['quota'] > 0) && array_key_exists('room', $tmp) && !empty($tmp['room'])) {

                                $valid_ranges = [];
                                $skip = false;

                                // ranges
                                if (array_key_exists('ranges', $tmp) && !empty($tmp['ranges'])) {
                                    $ranges = array_filter(explode("|", $tmp['ranges']));
                                    foreach ($ranges as $range) {
                                        list($_f, $_t) = explode(":", $range, 2);
                                        if (strtotime($_t) > time()) {
                                            $valid_ranges[] = [
                                                'from' => strtotime($_f),
                                                'to' => strtotime($_t),
                                            ];
                                        }
                                    }

                                    if (count($valid_ranges) < 1) {
                                        $skip = true;
                                    } else {
                                        usort($valid_ranges, function ($a, $b) {
                                            if ($a['from'] == $b['from']) return 0;
                                            return ($a['from'] < $b['from']) ? -1 : 1;
                                        });
                                        $tmp['ranges'] = $valid_ranges;
                                    }

                                } else {
                                    $tmp['ranges'] = [];
                                }

                                if ($skip === false) {
                                    list($_code, $_id) = explode(":", $tmp['room'], 2);
                                    if (!in_array($_id, $_rooms)) $_rooms[] = $_id;
                                    $tmp['room'] = $_id;
                                    $last_minute_offers[] = $tmp;
                                }
                            }
                        }
                    }

                }
            }

            foreach($_rooms as $v){
                $room_details[$v] = [
                    'id' => $v,
                    'details' => [
                        'code' => 'room',
                    ],
                ];
                $room_details[$v]['_details'] = $this->getFurtherDetails('element', $room_details[$v], ['prices' => false]);
            }
        }
//        if(array_key_exists('last_minute_offers', $content) && is_array($content['last_minute_offers']) && array_key_exists(0, $content['last_minute_offers']) && is_array($content['last_minute_offers'][0]) && array_key_exists('id', $content['last_minute_offers'][0])){
//            $_rooms = [];
//            foreach($content['last_minute_offers'][0]['details']['contain'] as $lmo){
//                $tmp = $this->mediaElementDetails($lmo, ['image']);
//                if((!array_key_exists('quota', $tmp) || $tmp['quota'] > 0) && array_key_exists('room', $tmp) && !empty($tmp['room'])){
//
//                    $valid_ranges = [];
//                    $skip = false;
//
//                    // ranges
//                    if(array_key_exists('ranges', $tmp) && !empty($tmp['ranges'])){
//                        $ranges = array_filter(explode("|", $tmp['ranges']));
//                        foreach($ranges as $range){
//                            list($_f, $_t) = explode(":", $range, 2);
//                            if(strtotime($_t) > time()){
//                                $valid_ranges[] = [
//                                    'from' => strtotime($_f),
//                                    'to' => strtotime($_t),
//                                ];
//                            }
//                        }
//
//                        if(count($valid_ranges) < 1){
//                            $skip = true;
//                        }else{
//                            usort($valid_ranges, function($a, $b){
//                                if($a['from'] == $b['from']) return 0;
//                                return ($a['from'] < $b['from']) ? -1 : 1;
//                            });
//                            $tmp['ranges'] = $valid_ranges;
//                        }
//
//                    }else{
//                        $tmp['ranges'] = [];
//                    }
//
//                    if($skip === false){
//                        list($_code, $_id) = explode(":", $tmp['room'], 2);
//                        if(!in_array($_id, $_rooms)) $_rooms[] = $_id;
//                        $tmp['room'] = $_id;
//                        $last_minute_offers[] = $tmp;
//                    }
//                }
//            }
//            foreach($_rooms as $v){
//                $room_details[$v] = [
//                    'id' => $v,
//                    'details' => [
//                        'code' => 'room',
//                    ],
//                ];
//                $room_details[$v]['_details'] = $this->getFurtherDetails('element', $room_details[$v], ['prices' => false]);
//            }
//        }
        return $room_details;
    }

    /*
     * Searches for $needle in the multidimensional array $haystack.
     *
     * @param mixed $needle The item to search for
     * @param array $haystack The array to search
     * @return array|bool The indices of $needle in $haystack across the
     *  various dimensions. FALSE if $needle was not found.
     */
    public function recursive_array_search($needle,$haystack) {
        foreach($haystack as $key=>$value) {
            if($needle===$key && is_array($value) && count($value)) {
                return $value;
            } else if (is_array($value) && $subkey = $this->recursive_array_search($needle,$value)) {
                array_unshift($subkey, $key);
                return $subkey;
            }
        }
    }

    public function processLastminuteForm(&$form,$content)
    {
        if ($form->execute($this->request->data)) {

            $this->Forms->reduceLastMinuteQuota($this->request->data['id']);
            // Save into db
            $entity = $this->Forms->newEntity([
                'id' => Text::uuid(),
                'type' => $content['view'] ?? '',
                'action' => 'lastminute',
                'theme' => $this->request->params['structure']['id'],
                'data' => json_encode($this->request->data),
                'request' => json_encode($this->request->params),
                'sent' => time(),
            ]);

            if ($this->Forms->save($entity)) {
                $saved = true;
            }else{
                $saved = false;
                return;
            }

            if(array_key_exists('view', $content))
                $type = $content['view'];
            else $type = '';

            Configure::load('countries');
            $nc = $ic = [];
            foreach(Configure::read('countries') as $k => $v){
                if(array_key_exists('important', $v) && $v['important']){
                    $ic[$k] = $v['name'];
                }else{
                    $nc[$k] = $v['name'];
                }
            }
            asort($ic);
            asort($nc);
            $countries = array_merge($ic, $nc);
            $rooms = $this->getRooms();
            $packages = $this->getPackages();

            $interests = [];
            // interests
            foreach($interests as $k => $v){
                if(is_array($v) && array_key_exists('title', $v)){
                    $interests[$k] = $v['title'];
                }
            }

            // salutations
            Configure::load('salutations');
            $salutations = [];
            foreach(Configure::read('salutations') as $k => $v){
                $salutations[$k] = $v['short'];
            }

            $sender = Configure::read('sender');
            $content['recipient'] = $sender['email'];
            $content['recipient'] = $this->request->data['email'];


            $content['email_subject'] = ($this->request->language == 'de') ? 'Lastminute angebote' : 'Lastminute offer';
            // Send mail
            $this->getMailer('Frontend.Frontend')->send($content['view'] ?? 'lastminute', [$this->request, $content, $countries, $salutations, $interests, $rooms, $packages, Configure::read('sender'), Configure::read('config.dev'), $this->Forms->map()]);
            return true;
        }else {
            $this->set('errors', $form->errors());
            return false;
        }
    }

}
