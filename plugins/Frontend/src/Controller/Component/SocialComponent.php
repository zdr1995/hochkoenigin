<?php

namespace Frontend\Controller\Component;

use Cake\Controller\Component;

class SocialComponent extends Component
{
    
    var $config;
    var $poss = ['facebook', 'instagram', 'youtube'];
    
    public function initialize(array $config)
    {
        $this->config = $config;
    }
    
    public function wall()
    {
        
        $wall = [];
        
        // check
        if(is_array($this->config)){
        
            // get wall
            foreach($this->poss as $type){
                if(array_key_exists($type, $this->config) && $this->config[$type] !== false){
                    
                    // require
                    require_once(ROOT . DS .  'vendor' . DS  . 'social' . DS . $type . DS . $type . '.class.php');
                    
                    // fetch
                    $class = ucfirst($type);
                    ${$class} = new $class($this->config[$type]);
                    $posts = ${$class}->fetch();
                    
                    // set source
                    if(is_array($posts)){
                        foreach($posts as $post){
                            $created = $this->getCreationDateTime($type, $post);
                            $wall[] = array_merge(['__source' => $type, '__created' => $this->getCreationDateTime($type, $post), '__date' => date("Y-m-d H:i:s", $created)], $post);
                        }
                    }
                
                }
            }
        
        }
        
        // sort
        usort($wall, function($a, $b){
            if ($a['__created'] == $b['__created']){
                return 0;
            } else if ($a['__created'] > $b['__created']) {
                return -1;
            } else {
                return 1;
            }
        });
        
        return $wall;
    }
    
    private function getCreationDateTime($type, $post){
        
        // init
        $uxt = 0;
        
        // get uxt
        if(is_array($post)){
            switch ($type){
                case "facebook":
                    if(array_key_exists('created_time', $post)){
                        if(is_object($post['created_time'])){
                            $uxt = strtotime($post['created_time']->date);
                        }else if(is_array($post['created_time'])){
                            $uxt = strtotime($post['created_time']['date']);
                        }
                    }
                    break;
                case "instagram":
                    if(array_key_exists('created_time', $post) && strlen($post['created_time']) == 10){
                        $uxt = $post['created_time'];
                    }
                    break;
                case "youtube":
                    if(array_key_exists('snippet', $post) && is_array($post['snippet']) && array_key_exists('publishedAt', $post['snippet'])){
                        $uxt = strtotime($post['snippet']['publishedAt']);
                    }
                    break;
            }
        }
        
        return $uxt;
    }
    
}
