<?php
use Cake\Core\Configure;
use ScssPhp\ScssPhp\Compiler;
?>

<!doctype html>

<!--[if lt IE 9 ]><html version="HTML+RDFa 1.1" lang="<?php echo $this->request
  ->params['language']; ?>" class="no-js ie"><![endif]-->
<!--[if IE 9 ]><html version="HTML+RDFa 1.1" lang="<?php echo $this->request
  ->params['language']; ?>" class="no-js ie9"><![endif]-->
<!--[if (gt IE 9)|!(IE)]><!--><html version="HTML+RDFa 1.1" lang="<?php echo $this
  ->request->params['language']; ?>" class="no-js"><!--<![endif]-->

    <head>

    <!-- eta & Co -->
        <meta name="viewport" content="width=device-width,initial-scale=1.0,maximum-scale=1">
        <?= $this->Html->charset() ?>
        <title><?= isset($title) ? strip_tags($title) : $this->fetch('title') ?></title>
        <?= $this->element('Frontend.Website/meta', [ 'seo' => $seo, 'content' => $content ]) ?>
        <?= $this->fetch('meta') ?>

    <!-- Css - BASIC + DEBUG -->
        <!-- /css/bundle takes multiple files (also scss files) and combines them into a minified bundled file -->
        <!-- if you want to add files to the bundle go to the method cssBundle in plugins\Frontend\src\Controller\AppController.php -->
        <?php if (Configure::read('cssconfig.debug-on-our-ip') == true) {
          $scss = new Compiler();
          $scss->setImportPaths('frontend/css/');
          foreach (Configure::read('cssconfig.files') as $_css_file) { ?>
                <?php if (strpos($_css_file, '.scss') !== false) { ?>
                    <?php $css_content = $scss->compile(
                      file_get_contents(
                        $_SERVER['DOCUMENT_ROOT'] . '/webroot' . $_css_file
                      )
                    ); ?>
                    <?php file_put_contents(
                      str_replace(
                        '.scss',
                        '.css',
                        $_SERVER['DOCUMENT_ROOT'] . '/webroot' . $_css_file
                      ),
                      $css_content
                    ); ?>
                    <link data-info="from-scss" href="<?= str_replace(
                      '.scss',
                      '.css',
                      $_css_file
                    ) ?>" rel="stylesheet">
                <?php } else { ?>
                    <link rel="stylesheet" href="<?= $_css_file ?>">
                <?php } ?>
            <?php }
        } else {
           ?>
            <link rel="stylesheet" type="text/css" href="/frontend/css/bundle_basic.min.css">
        <?php
        } ?>

    <!-- other -->
        <script async defer>var dataLayer = [];</script>

      <!-- Tagmanager - head-code -->
    	<?= Configure::read('config.tracking.tagmanager-head') ?>

      

    </head>

    <?php $body_class =
      isset($this->request->params['route']) &&
      $this->request->params['route'] ==
        Configure::read('config.default.home.0.details.node.route')
        ? 'home'
        : 'not-home'; ?>

    <body lang="<?= $this->request->params['language'] ?>" class="<?= isset(
  $hint
) && $hint === true
  ? 'cookie-hint '
  : '' ?>
<?= $body_class . ' ' . $this->request->params['language'] ?> invisible">

    	<?= /* Tagmanager - body-code */ Configure::read(
         'config.tracking.tagmanager-body'
       ) ?>

        <div id="wrapper">
            <?= $this->element('Frontend.Website/cookie', ['hint' => $hint]) ?>
            <?= $this->element('Frontend.Website/menu') ?>
            <?= $this->element('Frontend.Website/header', ['menu' => $menu]) ?>
            <?= $this->element('Frontend.Website/navigation', [
              'menu' => $menu
            ]) ?>
            <?= $this->element('Frontend.Website/navigation_mobile', [
              'menu' => $menu
            ]) ?>
            <?= $this->element('Frontend.Website/breadcrumbs', [
              'breadcrumbs' => $breadcrumbs
            ]) ?>
            <a class="anchor" name="content"></a>
            <section id="master-wrapper">
                <?= $this->fetch('content') ?>
                <?= $this->element('Frontend.Website/footer') ?>
            </section>
        </div>

        <!-- JS MAIN -->
        <?php if($_SERVER['REMOTE_ADDR'] == '83.175.88.51' && Configure::read('jsconfig.debug-on-our-ip') !== true){ ?>
            <script src='/frontend/js/bundle.min.js' async defer></script>
        <?php } ?>

        <!-- Css MAIN -->
        <?php if(Configure::read('cssconfig.debug-on-our-ip') !== true){ ?>
          <link rel="stylesheet" type="text/css" href="/frontend/css/bundle.min.css">
        <?php } ?>

        <?= $this->element('Frontend.Website/js', [
          'tracking' => isset($tracking) ? $tracking : false
        ]) ?>

        <script async defer>
            //scroll to content
            <?php if (
              isset($this->request->params['node']) &&
              array_key_exists('jump', $this->request->params['node']) &&
              $this->request->params['node']['jump'] == 1
            ) { ?>
              $(window).on('load', function() {
                scrollToContent();
              });
            <?php } ?>

            //Browser warning
            var $buoop = {vs:{i:13,f:-4,o:-4,s:8,c:-4},api:4};
            function $buo_f(){
              var e = document.createElement("script");
              e.src = "//browser-update.org/update.min.js";
                document.body.appendChild(e);
              };

            try {document.addEventListener("DOMContentLoaded", $buo_f,false)}
            catch(e){window.attachEvent("onload", $buo_f)}
            <?php if(isset($_GET['slideshow'])){ ?>
              $(document).ready(function(){
                  slideshow(false);
              });
            <?php } ?>
        </script>

        <!-- Google ReCaptcha -->
        <script src='https://www.google.com/recaptcha/api.js' async defer></script>

        <div id="datepicker-container"></div>
    </body>
</html>
