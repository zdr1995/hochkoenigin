<?php use Cake\Core\Configure; ?>
<?php if($_SERVER['REMOTE_ADDR'] == '83.175.88.51'){ ?>
    <!-- ######   INFO-BOX   ###### -->
    <!-- ###### [Elements/Website/special-info_box.ctp] ###### -->
<?php } ?>

<?php $rounds = ['1', '2'] ?>
<?php $active_round = Configure::read('config.default.season') == 'su' ? '1' : '2'; ?>

<!-- desktop -->
<section class="info-box-wrapper two-tabs">
	<div class="tabs-wrapper inner">
        <?php if(array_key_exists('details', $special_element_content) && is_array($special_element_content['details']) && !empty($special_element_content['details'])){ ?>
            <!-- Buttons -->
            <div class="buttons switch-buttons">
                <?php foreach ($rounds as $round) { ?>
                    <a id="info-box-<?= $round ?>" href="#" target="_self" rel="<?= $round ?>" class="<?= $round == $active_round ? 'active' : '' ?> strong">
                        <div class="positioner">
                            <span class="center">
                                <?= $special_element_content['details']['ib_headline_' . $round] ?>
                            </span>
                        </div>
                    </a>
                <?php } ?>
            </div>
            <!-- Content -->
            <div class="content">
                <?php foreach ($rounds as $round) { ?>
                    <div class="item item-<?= $round ?><?= $round == $active_round ? ' active' : ' hidden beforeHidden' ?> " rel="<?= $round ?>">
                        <div class="padding">
                            <div class="left">
                                <span>
                                    <?= $special_element_content['details']['ib_textblock_' . $round] ?>
                                </span>
                            </div>
                            <div class="right">
                                <a href="<?= array_key_exists('link', $special_element_content['details']['ib_link_' . $round][0]['details']) ? $special_element_content['details']['ib_link_' . $round][0]['details']['link'] : $this->Url->build(['node' => $special_element_content['details']['ib_link_' . $round][0]['org'], 'language' => $this->request->params['language']]) ?>" target="<?= array_key_exists('target', $special_element_content['details']['ib_link_' . $round][0]['details']) ? $special_element_content['details']['ib_link_' . $round][0]['details']['target'] : '_self' ?>">
                                    <figure class="object-fit-image">
                                        <img src="data:image/png;base64,R0lGODlhAQABAAD/ACwAAAAAAQABAAACADs=" data-src="<?= $special_element_content['details']['ib_image_' . $round][0]['details']['seo']['4'] ?>" alt="<?= $special_element_content['details']['ib_image_' . $round][0]['details']['title'] ?>" style="object-position: <?= $special_element_content['details']['ib_image_' . $round][0]['details']['focus'][4]['css'] ?>;">
                                    </figure>
                                </a>
                            </div>
                        </div>
                    </div>
                <?php } ?>
            </div>
            <!-- Link button -->
            <div class="link center">
                <?php foreach ($rounds as $round) { ?>
                    <a href="<?= array_key_exists('link', $special_element_content['details']['ib_link_' . $round][0]['details']) ? $special_element_content['details']['ib_link_' . $round][0]['details']['link'] : $this->Url->build(['node' => $special_element_content['details']['ib_link_' . $round][0]['org'], 'language' => $this->request->params['language']]) ?>" target="<?= array_key_exists('target', $special_element_content['details']['ib_link_' . $round][0]['details']) ? $special_element_content['details']['ib_link_' . $round][0]['details']['target'] : '_self' ?>" class="link-btn link-btn-<?= $round ?><?= $round == $active_round ? ' active' : ' hidden' ?> uppercase strong" rel="<?= $round ?>">
                        <span class="design-element"></span>
                        <span class="content">
                            <?= $special_element_content['details']['ib_linktxt_' . $round] ?>
                        </span>
                    </a>
                <?php } ?>
            </div>
        <?php } ?>
    </div>
</section>
