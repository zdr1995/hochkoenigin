<!-- favicon //-->
<link rel="apple-touch-icon" sizes="180x180" href="/frontend/favicon/apple-touch-icon.png">
<link rel="icon" type="image/png" sizes="32x32" href="/frontend/favicon/favicon-32x32.png">
<link rel="icon" type="image/png" sizes="16x16" href="/frontend/favicon/favicon-16x16.png">
<link rel="manifest" href="/frontend/favicon/site.webmanifest">
<link rel="mask-icon" href="/frontend/favicon/safari-pinned-tab.svg" color="#5bbad5">
<meta name="msapplication-TileColor" content="#ffffff">
<meta name="theme-color" content="#ffffff">

<link rel="manifest" href="/manifest.json">

<?php if(isset($content) && array_key_exists('meta', $content)){ ?>
<!-- description //-->
<meta name="description" content="<?= $content['meta']; ?>" />
<?php } ?>

<?php if(isset($seo) && array_key_exists('robots', $seo)){ ?>
<!-- robots //-->
<meta name="robots" content="<?= $seo['robots']; ?>" />
<?php } ?>

<?php if(isset($seo) && array_key_exists('canonical', $seo)){ ?>
<!-- canonical //-->
<link rel="canonical" href="<?= $seo['canonical']; ?>" />
<?php } ?>

<!-- HSTS controls //-->
<meta http-equiv="Strict-Transport-Security" content="max-age=31536000">
<meta http-equiv="Strict-Transport-Security" content="includeSubDomains">
<meta http-equiv="Strict-Transport-Security" content="preload">

<!-- SNIFF controls //-->
<meta http-equiv="X-Content-Type-Options" content="nosniff">

<!-- Geo Tagging //-->
<?= $this->element('Frontend.Website/geotagging', ['seo' => $seo, 'content' => $content]) ?>
