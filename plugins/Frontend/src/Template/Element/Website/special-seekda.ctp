<?php if($_SERVER['REMOTE_ADDR'] == '83.175.88.51'){ ?>
    <!-- ######   SEEKDA   ###### -->
    <!-- ###### [Elements/Website/special-seekda.ctp] ###### -->
<?php } ?>

<?php
//preload image
$preload_img = 'https://static.seekda.com/assets/images/skd_spinner.gif';
if(isset($special_element_content['details']['preloadImage']) && !empty($special_element_content['details']['preloadImage'])){
	$preload_img = $special_element_content['details']['preloadImage'];
}

//hotel_id
$hotel_id = false;
if(isset($special_element_content['details']['hotelId']) && !empty($special_element_content['details']['hotelId'])){
	$hotel_id = $special_element_content['details']['hotelId'];
}

//api key
$api_key = false;
if(isset($special_element_content['details']['seekdaApiKey']) && !empty($special_element_content['details']['seekdaApiKey'])){
	$api_key = $special_element_content['details']['seekdaApiKey'];
}
?>

<?php if($hotel_id !== false && $api_key !== false){ ?>
	<!-- Script-Tag -->
	<script src="https://cloud.seekda.com/w/w-dynamic-shop/hotel:<?= $hotel_id ?>/<?= $api_key ?>.js"  async defer ></script>
	
	<section class="seekda main" id="seekda">
	    <div class="inner">
	    
	    	<!-- Check Availability -->
	        <div class="skd-widget" data-skd-widget="check-availability" data-skd-language-code="<?= $this->request->params['language']; ?>" data-skd-show-language="false" data-skd-show-currency="false" data-skd-show-header="false" data-skd-is-themeable="true" data-skd-auto-search="true" data-skd-show-roombox="false" data-skd-children-min="0" data-skd-auto-scroll="true" data-skd-send-to-groups="A" data-skd-listen-to-groups="B" ><div style="width:100%; min-height:50px; text-align:center;"><img src="<?= $preload_img ?>" alt="spinner" /><noscript>Your browser doesn't support JavaScript or it has been disabled. To use the booking engine, please make sure to get JavaScript running.</noscript></div></div>
	        
	        <!-- Offer List -->
	        <div class="skd-widget" data-skd-widget="offer-list" data-skd-packages-first="true" data-skd-language-code="<?= $this->request->params['language']; ?>" data-skd-is-themeable="true" data-skd-sort-order="price" data-skd-hide-packages="false" data-skd-hide-rates="false" data-skd-cheapest-offer-only="false" data-skd-show-services="true" data-skd-send-to-groups="B" data-skd-listen-to-groups="A"><div style="width:100%; min-height:100px; text-align:center;"><img src="<?= $preload_img ?>" alt="spinner" /><noscript>Your browser doesn't support JavaScript or it has been disabled. To use the booking engine, please make sure to get JavaScript running.</noscript></div></div>
	   
	    </div>
	</section>
	
	<!-- <script type="text/javascript" async defer >
	
	    var _skd = window._skd || {};
	
	    _skd.callbacks = _skd.callbacks || {};
	    _skd.callbacks.dsr = _skd.callbacks.dsr || {};
			
			fbq('track', 'BookingStart');

	    _skd.callbacks.dsr.searchBtnClick = function(data) {
    		dataLayer.push({'event': 'seekdaAction', 'seekdaAction': 'Suche'});
				fbq('track', 'Search');
	    }
	
	    _skd.callbacks.dsr.viewOffers = function(data) {
    		dataLayer.push({'event': 'seekdaAction', 'seekdaAction': 'Übersicht'});
	    }
	
	    _skd.callbacks.dsr.viewRoom = function(data) {
				dataLayer.push({'event': 'seekdaAction', 'seekdaAction': 'Zimmerauswahl'});
	    }
	
	    _skd.callbacks.dsr.viewPackage = function(data) {
				dataLayer.push({'event': 'seekdaAction', 'seekdaAction': 'Pauschalauswahl'});
	    }
	
	    _skd.callbacks.dsr.viewOfferDetails = function(data) {
				dataLayer.push({'event': 'seekdaAction', 'seekdaAction': 'Details'});
	    }
	
	    _skd.callbacks.dsr.viewPersInfo = function(data) {
    		dataLayer.push({'event': 'seekdaAction', 'seekdaAction': 'Personeninfos'});
				fbq('track', 'InitiateCheckout');
	    }
			
	    _skd.callbacks.dsr.viewReview = function(data) {
				dataLayer.push({'event': 'seekdaAction', 'seekdaAction': 'Zusammenfassung'});
				fbq('track', 'AddToCart', {value: data.total, currency: 'EUR'});
	    }  
	
	    _skd.callbacks.dsr.viewConfirmation = function(data) {
				dataLayer.push({'event': 'seekdaAction', 'seekdaAction': 'Buchung'});
				fbq('track', 'Purchase', {value: data.total, currency: 'EUR'});
				//adwords tracking could be like this (BUT WITH RIGHT ID)
		    // var ct=document.createElement('IMG');
		    // ct.src='https://www.googleadservices.com/pagead/conversion/123456/?value='+data.total+'&label=XXXXXXXXXXXXX&script=0';
		    // ct.width=1;
		    // ct.height=1;
		    // document.body.appendChild(ct);
			}
	</script> -->


	<script>
		window.onhashchange = function() { 
     if(document.URL.indexOf("/skd-personal-info") >= 0){ 
					var button;
					window.dataLayer = window.dataLayer || [];
				setTimeout(function(){ 

					button = document.getElementById('btnBookNow');
						button.onclick = function(){
							var rev = document.getElementsByClassName('skd-book-final')[0].getElementsByTagName('span')[0].innerText;
							var revNo = rev.replace( /^\D+/g, '');
							console.log('GTM PUSH:::BOOKNOW' );
					  		dataLayer.push({'event': 'gtm.click','Revenue': revNo});
						};
				}, 3000);			
		}
}
		
	</script>
	
<?php } ?>