<?php use Cake\Core\Configure; ?>
<?php if($_SERVER['REMOTE_ADDR'] == '83.175.88.51'){ ?>
    <!-- ######   POOL   ###### -->
    <!-- ###### [Elements/Website/pool.ctp] ###### -->
<?php } ?>

<?php if(array_key_exists('details', $element_content) && is_array($element_content['details']) && array_key_exists('_details', $element_content['details']) && is_array($element_content['details']['_details']) && (count($element_content['details']['_details']['infos']) > 0 || $element_content['details']['type'] == 'treatment')){ ?>

    <?php switch ($element_content['details']['type']) {

        case 'category_rooms': ?>
            <section class="pool room-cat pool-to-animate invisible" data-pool-id="<?= $element_content['id']; ?>">
                <a class="anchor" id="<?= $element_content['id']; ?>" name="<?= $element_content['id']; ?>"></a>
                <ul class="viewport bxslider">
                    <?php foreach($element_content['details']['_details']['infos'] as $_item){ ?>

                        <!-- link -->
                        <?php $url = array_key_exists($_item['id'], $element_content['details']['_details']['nodes']) ? $this->Url->build(['node' => 'node:' . $element_content['details']['_details']['nodes'][$_item['id']], 'language' => $this->request->params['language']]) : false; ?>

                        <li>
                            <div class="wrapper">
                                <a href="<?= $url ?>" target="_self">
                                    <figure class="object-fit-image">
                                        <img src="data:image/png;base64,R0lGODlhAQABAAD/ACwAAAAAAQABAAACADs=" data-src="<?= $_item['images'][0]['details']['seo']['4_topslider'] ?>" alt="<?= $_item['images'][0]['details']['title'] ?>" style="object-position: <?= $_item['images'][0]['details']['focus'][4]['css'] ?>;">

                                        <?php if(array_key_exists('headline', $_item) && !empty($_item['headline']) && array_key_exists('subheadline', $_item) && !empty($_item['subheadline'])){ ?>
                                            <figcaption>
                                                <div class="inner-wrapper">
                                                    <span class="headline uppercase strong">
                                                        <span class ="bg">
                                                            <?= $_item['headline'] ?>
                                                        </span>
                                                    </span>
                                                    <span class="content">
                                                        <?= $_item['subheadline'] ?>
                                                    </span>
                                                </div>
                                            </figcaption>
                                        <?php } ?>

                                    </figure>
                                </a>
                            </div>
                        </li>

                    <?php } ?>
                </ul>
            </section>
            <?php break;

        // case 'custom_rooms': ?>
            <!-- // code... -->
            <?php // break;

        case 'category_packages': ?>
            <section class="pool package-cat pool-to-animate invisible" data-pool-id="<?= $element_content['id']; ?>">
                <a class="anchor" id="<?= $element_content['id']; ?>" name="<?= $element_content['id']; ?>"></a>
                <ul class="viewport bxslider">
                    <?php foreach($element_content['details']['_details']['infos'] as $_item){ ?>

                        <!-- link -->
                        <?php $url = array_key_exists($_item['id'], $element_content['details']['_details']['nodes']) ? $this->Url->build(['node' => 'node:' . $element_content['details']['_details']['nodes'][$_item['id']], 'language' => $this->request->params['language']]) : false; ?>

                        <li>
                            <div class="wrapper">
                                <a href="<?= $url ?>" target="_self">
                                    <figure class="object-fit-image">
                                        <img src="data:image/png;base64,R0lGODlhAQABAAD/ACwAAAAAAQABAAACADs=" data-src="<?= $_item['images'][0]['details']['seo']['4_topslider'] ?>" alt="<?= $_item['images'][0]['details']['title'] ?>" style="object-position: <?= $_item['images'][0]['details']['focus'][4]['css'] ?>;">

                                        <?php if(array_key_exists('title', $_item) && !empty($_item['title'])){ ?>
                                            <figcaption>
                                                <div class="inner-wrapper">
                                                    <span class="headline strong">
                                                        <span class ="bg">
                                                            <?= $_item['title'] ?>
                                                        </span>
                                                    </span>
                                                    <span class="content">
                                                        <span class="left">
                                                            <img src="data:image/png;base64,R0lGODlhAQABAAD/ACwAAAAAAQABAAACADs=" data-src="/frontend/img/uploaded/icons/Icons_Anfragen.svg" alt="time" class="time-icon icon invert">
                                                            <span class="">
                                                                <?= $_item['from_date'] ?>
                                                            </span>
                                                        </span>
                                                        <span class="right">
                                                            <span class="">
                                                                <?php $_item['from_price'] = str_replace(',', '.', $_item['from_price']); ?>
                                                                <?= __d('fe', 'from EUR') ?>&nbsp;<?= number_format($_item['from_price'], 2, ',', '.') ?>
                                                            </span>
                                                        </span>
                                                    </span>
                                                </div>
                                            </figcaption>
                                        <?php } ?>

                                    </figure>
                                </a>
                            </div>
                        </li>

                    <?php } ?>
                </ul>
            </section>
            <?php break;

        // case 'custom_packages': ?>
            <!-- // code... -->
            <?php // break;

        default: ?>
            <section class="pool treatment pool-to-animate invisible" data-pool-id="<?= $element_content['id']; ?>">
                <a class="anchor" id="<?= $element_content['id']; ?>" name="<?= $element_content['id']; ?>"></a>
                <ul class="viewport bxslider">
                    <?php foreach($element_content['details']['treatment'] as $_item){ ?>

                        <!-- link -->
                        <?php $url = $this->Url->build(['node' => 'node:' . Configure::read('config.default.treatments.0.id'), 'language' => $this->request->params['language']]) . '#' . $_item['id'] ?>

                        <li>
                            <div class="wrapper">
                                <a href="<?= $url ?>" target="_self">
                                    <figure class="object-fit-image">
                                        <img src="data:image/png;base64,R0lGODlhAQABAAD/ACwAAAAAAQABAAACADs=" data-src="<?= $_item['details']['image'][0]['details']['seo']['4_topslider'] ?>" alt="<?= $_item['details']['image'][0]['details']['title'] ?>" style="object-position: <?= $_item['details']['image'][0]['details']['focus'][4]['css'] ?>;">

                                        <?php if(array_key_exists('title', $_item['details']) && !empty($_item['details']['title']) && array_key_exists('time', $_item['details']) && !empty($_item['details']['time']) && array_key_exists('price', $_item['details']) && !empty($_item['details']['price'])){ ?>
                                            <figcaption>
                                                <div class="inner-wrapper">
                                                    <span class="headline strong">
                                                        <span class ="bg">
                                                            <?= $_item['details']['title'] ?>
                                                        </span>
                                                    </span>
                                                    <span class="content">
                                                        <span class="left">
                                                            <img src="data:image/png;base64,R0lGODlhAQABAAD/ACwAAAAAAQABAAACADs=" data-src="/frontend/img/uploaded/icons/Icons_Zeit.svg" alt="time" class="time-icon icon">
                                                            <span class="">
                                                                <?= $_item['details']['time'] ?>&nbsp;<?= __d('fe', 'min.') ?>
                                                            </span>
                                                        </span>
                                                        <span class="right">
                                                            <span class="">
                                                                <?= __d('be', 'ab EUR') ?>&nbsp;<?= number_format($_item['details']['price'], 2, ',', '.') ?>
                                                            </span>
                                                        </span>
                                                    </span>
                                                </div>
                                            </figcaption>
                                        <?php } ?>

                                    </figure>
                                </a>
                            </div>
                        </li>

                    <?php } ?>
                </ul>
            </section>
            <?php break;
    } ?>
<?php } ?>
