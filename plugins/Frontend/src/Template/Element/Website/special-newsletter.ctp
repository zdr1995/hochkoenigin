<?php use Cake\Core\Configure; ?>
<?php if($_SERVER['REMOTE_ADDR'] == '83.175.88.51'){ ?>
    <!-- ######   NEWSLETTER   ###### -->
    <!-- ###### [Elements/Website/special-newsletter.ctp] ###### -->
<?php } ?>

<section class="main newsletter">
	<div class="inner">
        <?php if(array_key_exists('details', $special_element_content) && is_array($special_element_content['details']) && !empty($special_element_content['details']) && array_key_exists('headline', $special_element_content['details']) && !empty($special_element_content['details']['headline'])){ ?>
            <div class="headline  strong center">
                <?= $special_element_content['details']['headline'] ?>
            </div>
            <div class="design-element center">
                <span class="design-lines left"></span>
                <img src="data:image/png;base64,R0lGODlhAQABAAD/ACwAAAAAAQABAAACADs=" data-src="/frontend/img/uploaded/icons/3Punkte_weiss.svg" alt="design-element" class="icon">
                <span class="design-lines right"></span>
            </div>
        <?php } ?>
	    <form action="<?= $this->Url->build(['node' => Configure::read('config.default.newsletter.0.org'), 'language' => $this->request->params['language']]); ?>" method="GET">
	        <input type="text" name="firstname" value="<?= array_key_exists('firstname', $_GET) ? $_GET['firstname'] : '' ?>" placeholder="<?= __d('fe','Firstname') ?>" />
	        <input type="text" name="lastname" value="<?= array_key_exists('lastname', $_GET) ? $_GET['lastname'] : '' ?>" placeholder="<?= __d('fe','Lastname') ?>" />
	        <input type="text" name="email" value="<?= array_key_exists('email', $_GET) ? $_GET['email'] : '' ?>" placeholder="<?= __d('fe','Email') ?>" />
	        <button class="button uppercase strong">
                <?= __d('fe','Subsrcribe for the newsletter') ?>
            </button>
	    </form>
	</div>
</section>
