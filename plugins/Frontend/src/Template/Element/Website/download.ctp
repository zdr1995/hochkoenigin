<?php use Cake\Core\Configure; ?>
<?php if($_SERVER['REMOTE_ADDR'] == '83.175.88.51'){ ?>
    <!-- ######   DOWNLOADS   ###### -->
    <!-- ###### [Elements/Website/downloads.ctp] ###### -->
<?php } ?>

<!-- init -->
<?php
    $class = $image = $title = '';
    if($element_content['type'] == 'image'){

        $class .= ' downloadable-image';
        $image_url = $element_content['details']['seo'][3];
        $image_style = 'background-position:' . $element_content['details']['focus'][3]['css'] . ';';
        $image_dimensions = getimagesize($_SERVER['REQUEST_SCHEME'] . '://' . $_SERVER['HTTP_HOST'] . $image_url);
        $file = [
            'check' => $element_content['details']['paths']['original'],
            'href' => '/provide/image/de/' . $element_content['details']['id'],
        ];
        $title = __d('fe', 'Download');

    } else{

        $class .= ' download-' . $element_count;
        $image_url = $element_content['details']['image'][0]['details']['seo'][3];
        $image_style = 'background-position:' . $element_content['details']['image'][0]['details']['focus'][3]['css'] . ';';
        $image_dimensions = getimagesize($_SERVER['REQUEST_SCHEME'] . '://' . $_SERVER['HTTP_HOST'] . $image_url);
        $file = $element_content['details']['file'];
        $file = [
            'check' => WWW_ROOT . '/files/' . $file['name'],
            'href' => '/files/' . $file['name'],
        ];
        $title = $element_content['details']['title'];

    }
?>

<?php if(isset($file['check']) && !empty($file['check']) && file_exists($file['check'])){ ?>
    <div class="download-wrapper inner center" style="<?= $image_dimensions[1] >= '350' ? 'margin-bottom: 160px;' : 'margin-bottom: 110px; margin-top: -50px;' ; ?>">
        <figure class="object-fit-image">
            <img src="data:image/png;base64,R0lGODlhAQABAAD/ACwAAAAAAQABAAACADs=" data-src="<?= $image_url ?>" alt="<?= $title ?>" style="<?= $image_style ?>">
        </figure>
        <div class="content">
            <div class="headline strong">
                <span>
                    <?= $title ?>
                </span>
            </div>
            <div class="positioner">
                <div class="buttons-wrapper">
                    <!-- <a href="<?= $file['href'] ?>" class="open button">
                        <span class="strong uppercase">
                            <?= __d('fe', 'Preview') ?>
                        </span>
                    </a> -->
                    <a href="<?= $element_content['details']['link'][0]['details']['link'] ?>" class="open button">
                        <span class="strong uppercase">
                            <?= __d('fe', 'e-Brochure') ?>
                        </span>
                    </a>
                    <a href="<?= $file['href'] ?>" class="download button" download="<?= $title ?>">
                        <img src="data:image/png;base64,R0lGODlhAQABAAD/ACwAAAAAAQABAAACADs=" data-src="/frontend/img/uploaded/icons/Icons_Download_blau.svg" alt="Download" class="download-icon icon">
                        <span class="strong">
                            <?= __d('fe', 'Download') ?>
                        </span>
                    </a>
                </div>
            </div>
        </div>
    </div>
<?php } ?>
