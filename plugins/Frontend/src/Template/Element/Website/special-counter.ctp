<?php if($_SERVER['REMOTE_ADDR'] == '83.175.88.51'){ ?>
    <!-- ######   COUNTER BANNER   ###### -->
    <!-- ###### [Elements/Website/special-counter.ctp] ###### -->
<?php } ?>

<?php if(array_key_exists('details', $special_element_content) && is_array($special_element_content['details']) && !empty($special_element_content['details'])){ ?>
    <section class="banner-wrapper counter">
        <a href="<?= array_key_exists('link', $special_element_content['details']['link'][0]['details']) ? $special_element_content['details']['link'][0]['details']['link'] : $this->Url->build(['node' => $special_element_content['details']['link'][0]['org'], 'language' => $this->request->params['language']]) ?>" target="<?= array_key_exists('target', $special_element_content['details']['link'][0]['details']) ? $special_element_content['details']['link'][0]['details']['target'] : '_self' ?>">
            <div class="column col-2">
                <span id="countUp2" class="content strong countUp" data-from="0" data-to="<?= $special_element_content['details']['pools_line'] ?>" data-speed="1500" data-decimals="0"></span>
                <span class="headline uppercase strong">
                    <?= __d('be', 'Pools') ?>
                </span>
            </div>
            <div class="column col-1">
                <span class="design-element left"></span>
                <span class="content strong">
                    <span id="countUp1" class="countUp" data-from="0" data-to="<?= $special_element_content['details']['wellness_line'] ?>" data-speed="2500" data-decimals="3"></span>&nbspm&sup2;
                </span>
                <span class="headline uppercase strong">
                    <?= __d('be', 'Wellness') ?>
                </span>
            </div>
            <div class="column col-3">
                <span class="design-element right"></span>
                <span id="countUp3" class="content strong countUp" data-from="0" data-to="<?= $special_element_content['details']['saunas_line'] ?>" data-speed="1500" data-decimals="0"></span>
                <span class="headline uppercase strong">
                    <?= __d('fe', 'Saunas') ?>
                </span>
            </div>
        </a>
    </section>
<?php } ?>
