<?php if($_SERVER['REMOTE_ADDR'] == '83.175.88.51'){ ?>
    <!-- ######   TEXTBLOCK   ###### -->
    <!-- ###### [Elements/Website/special-textblock.ctp] ###### -->
<?php } ?>

<?php if (!array_key_exists('select_mobile', $special_element_content['details']) && empty($special_element_content['details']['select_mobile'])) {
    $special_element_content['details']['select_mobile'] = 'default';
} ?>

<?php if(is_array($special_element_content) && array_key_exists('details', $special_element_content)){ ?>
<section class="main textblock inner <?= $special_element_content['details']['select_mobile'] == 'default' ? 'default' : 'only-desktop' ?>">
    <?= $special_element_content['details']['textblock']; ?>
</section>
<?php } ?>
