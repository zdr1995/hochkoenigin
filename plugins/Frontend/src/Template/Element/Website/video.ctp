<?php if($_SERVER['REMOTE_ADDR'] == '83.175.88.51'){ ?>
    <!-- ######   VIDEO   ###### -->
    <!-- ###### [Elements/Website/special-video.ctp] ###### -->
<?php } ?>

<?php if (isset($content['auto_video']) && !empty($content['auto_video'])) {
	$vda = $content['auto_video'] == 1 ? ' autoplay' : '';
} else {
	$vda = '';
}
?>

<?php if((isset($element_content['details']['mp4']['name']) && !empty($element_content['details']['mp4']['name'])) || (isset($element_content['details']['youtube']) && !empty($element_content['details']['youtube']))){ ?>
	<section class="video-container main hidden-print">
		<div class="inner">
			<?php if(isset($element_content['details']['youtube']) && !empty($element_content['details']['youtube'])){ ?>
				<div class="youtube-video-container">
					<iframe id="video-<?php echo $element_content['id']; ?>" name="video" src="https://www.youtube-nocookie.com/embed/<?= $element_content['details']['youtube'] ?>?rel=0&showinfo=0&mute=1&loop=1" frameborder="0" allow="autoplay; encrypted-media" allowfullscreen class="vid-pl vid-yt<?= $vda ?>" enablejsapi="1"></iframe>
				</div>
			<?php } else{ ?>
			    <video loop name="video" id="video-<?php echo $element_content['id']; ?>" class="video-js vjs-default-skin vid-pl<?= $vda ?>" controls="controls" preload="auto" width="1000px" height="563px" muted>
			        <source src="/files/<?php echo $element_content['details']['mp4']['name']; ?>" type='<?php echo $element_content['details']['mp4']['type']; ?>' />
			    </video>
			<?php } ?>
	    </div>
	</section>
<?php } ?>

<script type="text/javascript">
    // window.onload = function() {
    //     var videos = document.getElementsByTagName("video"), fraction = 0.8;
    //     function checkScroll() {
    //     	for (var i = 0; i < videos.length; i++) {
    //     		var video = videos[i];
    //     		var x = video.offsetLeft,
    //     			y = video.offsetTop,
    //     			w = video.offsetWidth,
    //     			h = video.offsetHeight,
    //     			r = x + w, //right
    //     			b = y + h, //bottom
    //     			visibleX, visibleY, visible;
	//
    //     		visibleX = Math.max(0, Math.min(w, window.pageXOffset + window.innerWidth - x, r - window.pageXOffset));
    //     		visibleY = Math.max(0, Math.min(h, window.pageYOffset + window.innerHeight - y, b - window.pageYOffset));
    //     		visible = visibleX * visibleY / (w * h);
	//
    //     		if (visible > fraction) {
    //     			video.play();
    //     		} else {
    //     			video.pause();
    //     		}
    //     	}
    //     }
    //     window.addEventListener('scroll', checkScroll, false);
    //     window.addEventListener('resize', checkScroll, false);
    // }
</script>
