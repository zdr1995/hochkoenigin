<?php use Cake\Core\Configure; ?>
<?php if($_SERVER['REMOTE_ADDR'] == '83.175.88.51'){ ?>
    <!-- ######   BREADCRUMBS   ###### -->
    <!-- ###### [Elements/Website/breadcrumbs.ctp] ###### -->
<?php } ?>

<?php $body_class = isset($this->request->params['route']) && $this->request->params['route'] == Configure::read('config.default.home.0.details.node.route') ? 'home' : 'not-home'; ?>

<?php if(isset($breadcrumbs) && is_array($breadcrumbs) && count($breadcrumbs) > 0 && $body_class == 'not-home'){ ?>
<?php $pos = 1; ?>
<?php $counter = 0; ?>
<section class="breadcrumbs hidden-print">
    <ol itemscope itemtype="http://schema.org/BreadcrumbList">
        <?php foreach($breadcrumbs as $breadcrumb){ ?>
            <?php if($pos > 1){ ?>
            <span>&nbsp;&middot;&nbsp;</span>
            <?php } ?>
            <li itemprop="itemListElement" itemscope itemtype="http://schema.org/ListItem">
                <?php if ($counter == count($breadcrumbs) - 1) { ?>
                    <span itemprop="name"><?= $breadcrumb['content']; ?></span>
                    <meta itemprop="position" content="<?= $pos; ?>" />
                <?php } else { ?>
                    <a itemscope itemtype="http://schema.org/Thing" itemprop="item" href="<?= $this->Url->build(['node' => 'node:' . $breadcrumb['id'], 'language' => $this->request->params['language']], true); ?>">
                        <span itemprop="name"><?= $breadcrumb['content']; ?></span>
                    </a>
                    <meta itemprop="position" content="<?= $pos; ?>" />
                <?php } ?>
            </li>
            <?php $pos++; ?>
            <?php $counter = $counter + 1; ?>
        <?php } ?>
    </ol>
</section>
<?php } ?>
