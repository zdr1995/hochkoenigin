<?php if($_SERVER['REMOTE_ADDR'] == '83.175.88.51'){ ?>
    <!-- ######   BACK BUTTON   ###### -->
    <!-- ###### [Elements/Website/special-back.ctp] ###### -->
<?php } ?>

<section class="link-back">
    <a href="javascript:history.back(1);" target="_self">
        <img src="data:image/png;base64,R0lGODlhAQABAAD/ACwAAAAAAQABAAACADs=" data-src="/frontend/img/uploaded/icons/Icons_Links-lang.svg" class="cal-icon icon invert">
        <span class="uppercase strong">
            <?= $special_element_content['details']['back_txt'] ?>
        </span>
    </a>
</section>
