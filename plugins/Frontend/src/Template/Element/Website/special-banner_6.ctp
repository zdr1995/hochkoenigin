<?php if($_SERVER['REMOTE_ADDR'] == '83.175.88.51'){ ?>
    <!-- ######   BANNER #6 (Text7Image)   ###### -->
    <!-- ###### [Elements/Website/special-banner_6.ctp] ###### -->
<?php } ?>

<?php if(array_key_exists('details', $special_element_content) && is_array($special_element_content['details']) && !empty($special_element_content['details'])){ ?>
    <section class="banner-6-wrapper <?= $special_element_content['details']['select_orientation'] ?>">
        <?php switch ($special_element_content['details']['select_orientation']) {

            case 'left': ?>
                <div class="left">
                    <a href="<?= array_key_exists('link', $special_element_content['details']['link'][0]['details']) ? $special_element_content['details']['link'][0]['details']['link'] : $this->Url->build(['node' => $special_element_content['details']['link'][0]['org'], 'language' => $this->request->params['language']]) ?>" target="<?= array_key_exists('target', $special_element_content['details']['link'][0]['details']) ? $special_element_content['details']['link'][0]['details']['target'] : '_self' ?>">
                        <?php if(array_key_exists('image', $special_element_content['details']) && !empty($special_element_content['details']['image'])){ ?>
                            <figure class="object-fit-image">
                                <img src="data:image/png;base64,R0lGODlhAQABAAD/ACwAAAAAAQABAAACADs=" data-src="<?= $special_element_content['details']['image'][0]['details']['seo']['1_middle'] ?>" alt="<?= $special_element_content['details']['image'][0]['details']['title'] ?>" style="object-position: <?= $special_element_content['details']['image'][0]['details']['focus'][1]['css'] ?>;">
                            </figure>
                        <?php } ?>
                    </a>
                </div>
                <div class="right <?= $special_element_content['details']['select_color'] == 'red' ? 'red' : 'green' ?>">
                    <div class="content-wrapper right b6-to-animate-right invisible">
                        <span class="content">
                            <?= $special_element_content['details']['textblock'] ?>
                        </span>
                    </div>
                </div>
            <?php break;

            case 'right';
            default: ?>
                <div class="left <?= $special_element_content['details']['select_color'] == 'red' ? 'red' : 'green' ?>">
                    <div class="content-wrapper left b6-to-animate-left invisible">
                        <span class="content">
                            <?= $special_element_content['details']['textblock'] ?>
                        </span>
                    </div>
                </div>
                <div class="right">
                    <a href="<?= array_key_exists('link', $special_element_content['details']['link'][0]['details']) ? $special_element_content['details']['link'][0]['details']['link'] : $this->Url->build(['node' => $special_element_content['details']['link'][0]['org'], 'language' => $this->request->params['language']]) ?>" target="<?= array_key_exists('target', $special_element_content['details']['link'][0]['details']) ? $special_element_content['details']['link'][0]['details']['target'] : '_self' ?>">
                        <?php if(array_key_exists('image', $special_element_content['details']) && !empty($special_element_content['details']['image'])){ ?>
                            <figure class="object-fit-image">
                                <img src="data:image/png;base64,R0lGODlhAQABAAD/ACwAAAAAAQABAAACADs=" data-src="<?= $special_element_content['details']['image'][0]['details']['seo']['1_middle'] ?>" alt="<?= $special_element_content['details']['image'][0]['details']['title'] ?>" style="object-position: <?= $special_element_content['details']['image'][0]['details']['focus'][1]['css'] ?>;">
                            </figure>
                        <?php } ?>
                    </a>
                </div>
            <?php break;
        } ?>
    </section>
<?php } ?>
