<?php use Cake\Core\Configure; ?>

<footer style="background-image:URL('/frontend/img/uploaded/Pattern_outline_weiss_1.png');">

    <!-- BACK-TO-TOP button -->
    <div class="scroll-to-top-btn">
        <img src="data:image/png;base64,R0lGODlhAQABAAD/ACwAAAAAAQABAAACADs=" data-src="/frontend/img/uploaded/icons/Icons_Up.svg" alt="up" class="up-icon icon">
        <span class="strong">
            <?= __d('fe','up') ?>
        </span>
    </div>

    <div class="inner-wrapper">

        <!-- 2 Collumns -->
        <div class="collumns">
            <div class="left">
                <a href="<?= $this->Url->build(['node' => 'node:' . Configure::read('config.default.home.0.id'), 'language' => $this->request->params['language']]); ?>" target="_self"class="footer-logo">
                    <img src="data:image/png;base64,R0lGODlhAQABAAD/ACwAAAAAAQABAAACADs=" data-src="/frontend/img/logo.svg" alt="footer-logo" class="footer-logo">
                </a>
                <div class="links">
                    <?php foreach (Configure::read('config.fixnav.footer-nav-items') as $_k => $_item) { ?>
                        <a href="<?= $this->Url->build(['node' => 'node:' . $_item['id'], 'language' => $this->request->params['language']]) ?>" target="_self">
                            <img src="data:image/png;base64,R0lGODlhAQABAAD/ACwAAAAAAQABAAACADs=" data-src="/frontend/img/uploaded/icons/3Punkte_weiss.svg" alt="list-design-icon" class="icon">
                            <span class="uppercase"><?= $_item['details']['element']['title'] ?></span>
                        </a>
                    <?php } ?>
                </div>
            </div>
            <div class="right">
                <a href="<?= $this->Url->build(['node' => 'node:' . Configure::read('config.default.map.0.id'), 'language' => $this->request->params['language']]); ?>" target="_blank">
                    <img src="data:image/png;base64,R0lGODlhAQABAAD/ACwAAAAAAQABAAACADs="" data-src="/frontend/img/uploaded/icons/karte.svg" alt="map" class="footer-map">
                </a>
            </div>
        </div>

        <!-- Contact line -->
        <div class="contact uppercase center">
            <a href="<?= $this->Url->build(['node' => 'node:' . Configure::read('config.default.map.0.id'), 'language' => $this->request->params['language']]); ?>" target="_blank">
                <span>
                    <?= Configure::read('config.default.street-' . $this->request->params['language']) ?>
                </span>
                <span class="dot">&nbsp;&period;&nbsp;</span>
                <span>
                    <?= Configure::read('config.default.zip') ?>&nbsp;
                    <?= Configure::read('config.default.city-' . $this->request->params['language']) ?>
                </span>
            </a>
            <span class="dot">&nbsp;&period;&nbsp;</span>
            <a href="tel:<?= Configure::read('config.default.phone-plain') ?>" target="_blank">
                <?= __d('fe', 'Tel.') ?>&nbsp;<?= Configure::read('config.default.phone') ?>
            </a>
            <span class="dot">&nbsp;&period;&nbsp;</span>
            <a href="mailto:<?= Configure::read('config.default.email') ?>" target="_blank">
                <?= Configure::read('config.default.email') ?>
            </a>
        </div>

        <!-- Social links -->
        <div class="social center">
            <?php $social_items = ['facebook', 'instagram', 'twitter', 'youtube'] ?>
            <?php foreach ($social_items as $_item) {
                if (Configure::read('config.links.' . $_item . '_check') == 1) { ?>
                    <a href="<?= Configure::read('config.links.' . $_item) ?>" target="_blank">
                        <img src="data:image/png;base64,R0lGODlhAQABAAD/ACwAAAAAAQABAAACADs=" data-src="/frontend/img/uploaded/icons/<?= $_item ?>.svg" alt="<?= $_item ?>" class="icon">
                    </a>
                <? }
            } ?>
        </div>

        <!-- Search field -->
        <div class="search center">
            <form id="quick-search-form" action="<?= $this->Url->build(['node' => Configure::read('config.default.search.0.org'), 'language' => $this->request->params['language']]); ?>" method="GET" class="center">
                <?= $this->CustomForm->input('s', ['label' => false, 'placeholder' => __d('fe', 'Search'), 'class' => 'footer-search-input']); ?>
                <span onclick="document.getElementById('quick-search-form').submit()" class="sear-bttn">
                    <img src="data:image/png;base64,R0lGODlhAQABAAD/ACwAAAAAAQABAAACADs=" data-src="/frontend/img/uploaded/icons/Icons_Suche.svg" alt="search" class="icon">
                </span>
            </form>
        </div>

        <!-- Bottom links -->
        <div class="bottom-links center">
            <?php $counter = 0; ?>
            <?php foreach (Configure::read('config.fixnav.footer-bottom-items') as $_k => $_item) { ?>
                <a href="<?= $this->Url->build(['node' => 'node:' . $_item['id'], 'language' => $this->request->params['language']]) ?>" target="_self">
                    <span class="capitalise"><?= $_item['details']['element']['title'] ?></span>
                </a>
                <?php if ($counter !== count(Configure::read('config.fixnav.footer-bottom-items')) - 1) { ?>
                    <span class="dot">&nbsp;&period;&nbsp;</span>
                <?php } ?>
                <?php $counter = $counter + 1; ?>
            <?php } ?>
        </div>

    </div>

    <!-- language switcher -->
    <section id="foot-lang">
        <?= $this->element('Frontend.Website/language-switcher-footer') ?>
    </section>

</footer>
