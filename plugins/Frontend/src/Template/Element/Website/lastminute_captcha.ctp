<?php if($_SERVER['REMOTE_ADDR'] == '176.104.105.50') { ?>
    <!-- ######   LASTMINUTE CAPTCHA   ###### -->
    <!-- ###### [Website/Elements/lastminute_captcha.ctp] ###### -->

<?php } ?>

<?php use Cake\Core\Configure; ?>
<?php
$show = isset($show) ? (bool) $show : true;
$text = isset($text) ? $text : __d('fe','Send');
$opt = [];

if(isset($options) && is_array($options)){
    foreach($options as $option){
        if(array_key_exists('type', $option) && array_key_exists('text', $option) && array_key_exists('class', $option) && array_key_exists('url', $option)){
            $opt[] = $option;
        }
    }
}
?>

<!-- ERROR MESSAGE -->
<div class="captcha-error">
    <?= __d('fe','Beim Senden ist ein Fehler aufgetreten. Bitte aktualisieren Sie die Seite und versuchen Sie es erneut.') ?>
</div>

<?php if($show){ ?>
    <div class="captch-input" style="width:100%">
        <div style="display: flex;flex-direction: row;align-items: center;">
            <img src="/captcha/number" width="90" height="36"/>
            <input type="text" class="captcha_field" style="margin-left: 10px;">
        </div>
    </div>
    <section class="captcha hidden-print" style="display: flex;flex-direction: row-reverse;">
        <div class="input required captcha">
            <button
                type="submit"
                class="g-recaptcha button"
                id="frm_submit_btn"
                style="right: 0;width: 200px"
            > <?= $text ?> </button>
            <div class="clear"></div>
        </div>
        <div class="clear"></div>
        <div class="clear"></div>

    </section>
<?php } ?>


<?php if(count($opt) > 0){ ?>
    <section class="send-option hidden-print">
        <?php foreach($opt as $o){ ?>
            <a href="<?= $o['url']; ?>" class="<?= $o['class']; ?> <?= $o['type']; ?>"><?= $o['text']; ?></a>
            <div class="clear"></div>
        <?php } ?>
    </section>
<?php } ?>

<div>
<?php
    //check if captcha is used
    echo $this->Form->input('c-info', array('label' => false, 'type' => 'hidden'));
    //honeypot
    echo '<div class="hidden">';
    echo $this->Form->input('h-info', array('label' => false, 'type' => 'text'));
    echo '</div>';
?>
</div>

<script>


    function onFormSubmit(form) {
        return new Promise(function (res,rej) {
            let token = '';
            console.log($(form).find('.captcha_field').val());
            // console.log($(el).parents('.hidden-print'));
            // return;
            //verify token
            let data= {
                value: $(form).find('.captcha_field').val(),
                token: token,
                remoteip: '<?= $_SERVER['REMOTE_ADDR'] ?>'
            }
            //     validateForm(form);
            //     console.log('Valid '  + $(form).valid());
            // console.log('Valida2:');
            if(data.value != '')
            {
                $.post( "/captcha/check",data)
                    .always(function(response) {
                        var response = $.parseJSON(response);
                        if (response && response !== undefined && response.success === true) {
                            $('input#c-info').attr('value', 'all-clear');
                            res(true);
                            // $('button.g-recaptcha').parents('form').submit();
                        } else {
                            $('.captcha-error').fadeIn();
                            rej(false);
                        }
                    });
            }else {
                $('.captcha-error').fadeIn();
            }
        })

    }

    function validateForm(form) {
        // let form = $(el).closest('form').get(0);
        console.log(form);
        return $(form).validate({
            rules: {
                id: {
                  required: true
                },
                room: {
                    required: true
                },
                salutation: {
                    required: true
                },
                title: {
                    required: true
                },
                firstname: {
                    required: true
                },
                lastname: {
                    required: true
                },
                email: {
                    required: true,
                    type: 'email'
                },
                address: {
                    required: true
                },
                message: {
                    required: true
                },
                privacy: {
                    required: true
                },
                captcha: {
                    required: true
                },
                country: {
                    required: true
                },
                city: {
                    required: true
                },
                zip: {
                    required: true
                }

            },
            messages: {
                id: "<?= __d('fe', 'An offer is required'); ?>",
                firstname: "<?= __d('fe', 'A firstname is required'); ?>",
                lastname: "<?= __d('fe', 'A lastname is required'); ?>",
                email: "<?= __d('fe', 'An email address is required'); ?>",
                message: "<?= __d('fe', 'A message is required'); ?>",
                country: "<?= __d('fe', 'Please select a country'); ?>",
                city: "<?= __d('fe', 'A city is required'); ?>",
                zip: "<?= __d('fe', 'A zip code is required'); ?>"
            },
            submitHandler: function(form) {
                onFormSubmit(form)
                .then(function(res) {
                    console.log(res);
                    if(res)
                        form.submit();
                })
                .catch(function(err) {
                    console.log('blah');
                })

            }
        })
        // return false;
    }
</script>

