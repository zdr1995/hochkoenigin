<?php use Cake\Core\Configure; ?>

<?php if($_SERVER['REMOTE_ADDR'] == '83.175.88.51' && Configure::read('jsconfig.debug-on-our-ip') === true){
    foreach(Configure::read('jsconfig.basic.files') as $_js_file){ ?>
        <script src="<?= $_js_file ?>"></script>
    <?php }
    foreach(Configure::read('jsconfig.extended.files') as $_js_file){ ?>
        <script src="<?= $_js_file ?>"></script>
    <?php }
} else{ ?>
    <script src='/frontend/js/bundle_basic.min.js' async></script>
<?php } ?>

<!-- Lazy image load -->
<script>
    function init() {
        var imgDefer = document.getElementsByTagName('img');
        for (var i=0; i<imgDefer.length; i++) {
            if(imgDefer[i].getAttribute('data-src')) {
                imgDefer[i].setAttribute('src',imgDefer[i].getAttribute('data-src'));
            }
        }
    }
    window.onload = init;
</script>

<!-- FontAwesome -->
<script src="https://kit.fontawesome.com/b251571473.js"></script>

<!-- Video.js -->
<script src="//vjs.zencdn.net/7.3.0/video.min.js" async defer></script>

<script async defer>

    var __translations = {
    	'childage': '<?= __d('fe', 'childage') ?>',
        'package': '<?= __d('fe', 'The package "%s" is not valid at the selected time!'); ?>',
    };

    var __system = {
        'locale': '<?= $this->request->params['language']; ?>',
        'jump': <?php echo !array_key_exists('jump', $this->request->params['node']) || $this->request->params['node']['jump'] ? 'true' : 'false'; ?>
    };

    // tagmanager stuff
	var gaTrackingId = '<?= Configure::read('config.tracking.ga-tracking-id'); ?>';
    var docLang = '<?= $this->request->params['language']; ?>';
    var docRoute = '<?php echo array_key_exists('route', $this->request->params) ? $this->request->params['route'] : 'false'; ?>';
    var homeRoute = '<?= Configure::read('config.default.home.0.details.node.route') ?>';
    var docSeason = '<?= Configure::read('config.default.season'); ?>';
    var docStatusCode = <?= http_response_code(); ?>;
    <?php if(isset($tracking) && $tracking){ ?>var formSent = true;<?php } ?>

    FontAwesomeConfig = { searchPseudoElements: true };
</script>