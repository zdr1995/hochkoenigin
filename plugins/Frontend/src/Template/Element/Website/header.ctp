<?php use Cake\Core\Configure; ?>

<?php
    $_headers = [];

    if(array_key_exists('media', $content) && is_array($content['media']) && array_key_exists('header-' . Configure::read('config.default.season'), $content['media']) && is_array($content['media']['header-' . Configure::read('config.default.season')])){
        if (Configure::read('config.default.season') == 'wi' && !empty($content['media']['header-wi'])) {
            $_season = 'wi';
        } else {
            $_season = 'su';
        }
        foreach($content['media']['header-' . $_season] as $h){
            if(is_array($h) && array_key_exists('type', $h) && in_array($h['type'], ['image','element']) && is_array($h['details'])){
                if($h['type'] == 'image'){
                    $_headers[] = [
                        'type' => 'image',
                        'url' => $h['details']['seo'][1],
                        'focus' => $h['details']['focus'][1],
                        'alt' => $h['details']['title'],
                    ];
                }else if($h['type'] == 'element' && array_key_exists('code', $h['details']) && $h['details']['code'] == 'header-teaser'){
                    if(array_key_exists('image', $h['details']) && is_array($h['details']['image']) && array_key_exists(0, $h['details']['image']) && is_array($h['details']['image'][0]) && count($h['details']['image'][0]) > 0){
                        $_headers[] = [
                            'type' => 'teaser',
                            'url' => $h['details']['image'][0]['details']['seo'][1],
                            'focus' => $h['details']['image'][0]['details']['focus'][1],
                            'alt' => $h['details']['image'][0]['details']['title'],
                            'quote' => $h['details']['quote'],
                            'author' => $h['details']['author'],
                        ];
                    }
                }
            }
        }
    }

    if (array_key_exists('header-mo', $content['media'])) {
        foreach($content['media']['header-mo'] as $_h){
            if(is_array($_h) && array_key_exists('type', $_h) && in_array($_h['type'], ['image','element']) && is_array($_h['details'])){
                if($_h['type'] == 'image'){
                    $_headers_mo[] = [
                        'type' => 'image',
                        'url' => $_h['details']['seo'][1],
                        'focus' => $_h['details']['focus'][1],
                        'alt' => $_h['details']['title'],
                    ];
                }else if($_h['type'] == 'element' && array_key_exists('code', $_h['details']) && $_h['details']['code'] == 'header-teaser'){
                    if(array_key_exists('image', $_h['details']) && is_array($_h['details']['image']) && array_key_exists(0, $_h['details']['image']) && is_array($_h['details']['image'][0]) && count($_h['details']['image'][0]) > 0){
                        $_headers_mo[] = [
                            'type' => 'teaser',
                            'url' => $_h['details']['image'][0]['details']['seo'][1],
                            'focus' => $_h['details']['image'][0]['details']['focus'][1],
                            'alt' => $_h['details']['image'][0]['details']['title'],
                            'quote' => $_h['details']['quote'],
                            'author' => $_h['details']['author'],
                        ];
                    }
                }
            }
        }
    }

?>

<!-- main headers -->
<?php if(count($_headers) > 0){ ?>
    <header id="header" class="images images-<?= count($_headers) ?> hidden-print not-scrolled">

        <!-- header slider -->
        <ul class="viewport bxslider">
            <?php foreach($_headers as $header){ ?>
                <li>
                    <div class="bxslide">

                        <figure class="object-fit-image">
                          <img src="<?= $header['url'] ?>" alt="<?= $header['alt'] ?>" style="object-position: <?= $header['focus']['css'] ?>;">
                        </figure>
                        <figcaption>
                            <div class="placeholder"></div>
                        </figcaption>

                        <!-- <?php if($header['type'] == 'teaser'){ ?>
                        <div class="text">
                            <?php if(!empty($header['quote'])){ ?>
                            <div class="quote">&bdquo;<?= $header['quote']; ?>&ldquo;</div>
                            <?php } ?>
                            <?php if(!empty($header['author'])){ ?>
                            <div class="author"><?= $header['author']; ?></div>
                            <?php } ?>
                        </div>
                        <?php } ?> -->

                    </div>
                </li>
            <?php } ?>
        </ul>

        <?= $this->element('Frontend.Website/top-line') ?>

        <!-- bubble -->
        <?php if(!empty(Configure::read('config.header_elements.show')) && Configure::read('config.header_elements.show') == 1 && !empty(Configure::read('config.header_elements.text'))){ ?>
            <div class="bubble">
                <a href="<?= $this->Url->build(['node' => Configure::read('config.header_elements.link.0.org'), 'language' => $this->request->params['language']]); ?>" target="_self">
                    <figure class="red-dot-frame object-fit-image invisible header-red-dot-frame">
                        <img src="/frontend/img/uploaded/Button-Outline.png" alt="design-element">
                        <figcaption class="header-bubble">
                            <figure class="red-dot object-fit-image header-red-dot header-bubble-side header-bubble-side-front">
                                <img src="/frontend/img/uploaded/Button-Pink_1.png" alt="design-element">
                                <figcaption>
                                    <?= $this->request->params['language'] == 'en' ? Configure::read('config.header_elements.text_en') : Configure::read('config.header_elements.text') ?>
                                </figcaption>
                            </figure>
                            <figure class="green-dot object-fit-image header-red-dot header-bubble-side header-bubble-side-back">
                                <img src="/frontend/img/uploaded/Button-Tuerkis-dunkler_1.png" alt="design-element">
                                <figcaption>
                                    <?= $this->request->params['language'] == 'en' ? Configure::read('config.header_elements.text_2_en') :Configure::read('config.header_elements.text_2') ?>
                                </figcaption>
                            </figure>
                        </figcaption>
                    </figure>

                </a>
            </div>
        <?php } ?>

        <!-- animated text -->
        <?php if(!empty(Configure::read('config.header_elements.show_text')) && Configure::read('config.header_elements.show_text') == 1 && !empty(Configure::read('config.header_elements.text_line_1')) && !empty(Configure::read('config.header_elements.text_line_2'))){ ?>
            <div class="animation center animated-slow zoomIn only-desktop">
                <span class="first-animated-line">
                    <?= $this->request->params['language'] == 'en' ? Configure::read('config.header_elements.text_line_1_en') : Configure::read('config.header_elements.text_line_1') ?>
                </span>
                <span class="second-animated-line">
                    <?= $this->request->params['language'] == 'en' ? Configure::read('config.header_elements.text_line_2_en') : Configure::read('config.header_elements.text_line_2') ?>
                </span>
            </div>
        <?php } ?>

    </header>

    <!-- scroll down button -->
    <a class="header j2c animated pulse infinite only-desktop" href="#mainnav">
        <span class="uppercase block"><?= __d('be', 'Scroll') ?></span>
        <img src="/frontend/img/uploaded/icons/Icons_Down_Double.svg" alt="double-down" class="icon">
    </a>
<?php } ?>

<!-- only mobile headers -->
<?php if(isset($_headers_mo) && count($_headers_mo) > 0 && count($_headers) <= 0){ ?>
    <header id="header-mobile" class="images images-<?= count($_headers_mo) ?> hidden-print not-scrolled only-mobile">

        <?= $this->element('Frontend.Website/top-line') ?>

        <!-- header slider -->
        <ul class="viewport bxslider">
            <?php foreach($_headers_mo as $header){ ?>
                <li>
                    <div class="bxslide">

                        <figure class="object-fit-image">
                          <img src="<?= $header['url'] ?>" alt="<?= $header['alt'] ?>" style="object-position: <?= $header['focus']['css'] ?>;">
                        </figure>

                        <!-- <?php if($header['type'] == 'teaser'){ ?>
                        <div class="text">
                            <?php if(!empty($header['quote'])){ ?>
                            <div class="quote">&bdquo;<?= $header['quote']; ?>&ldquo;</div>
                            <?php } ?>
                            <?php if(!empty($header['author'])){ ?>
                            <div class="author"><?= $header['author']; ?></div>
                            <?php } ?>
                        </div>
                        <?php } ?> -->

                    </div>
                </li>
            <?php } ?>
        </ul>

        <!-- bubble -->
        <?php if(!empty(Configure::read('config.header_elements.show')) && Configure::read('config.header_elements.show') == 1 && !empty(Configure::read('config.header_elements.text'))){ ?>
            <div class="bubble">
                <a href="<?= $this->Url->build(['node' => Configure::read('config.header_elements.link.0.org'), 'language' => $this->request->params['language']]); ?>" target="_self">
                    <figure class="red-dot object-fit-image invisible">
                        <img src="/frontend/img/uploaded/Button_Pink.svg" alt="design-element">
                        <figcaption>
                            <?= Configure::read('config.header_elements.text') ?>
                        </figcaption>
                    </figure>
                </a>
            </div>
        <?php } ?>

    </header>
<?php } ?>
