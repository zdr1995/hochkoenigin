<?php use Cake\Core\Configure; ?>
<?php if($_SERVER['REMOTE_ADDR'] == '83.175.88.51'){ ?>
    <!-- ######   OVERVIEW   ###### -->
    <!-- ###### [Elements/Website/overview.ctp] ###### -->
<?php } ?>

<?php if(array_key_exists('details', $element_content) && is_array($element_content['details']) && array_key_exists('_details', $element_content['details']) && is_array($element_content['details']['_details'])){ ?>
    <?php switch ($element_content['details']['type']) {

        // ROOMS
        case 'room': ?>

            <?php if(array_key_exists('details', $element_content) && is_array($element_content['details']) && !empty($element_content['details']) && array_key_exists('headline', $element_content['details']) && !empty($element_content['details']['headline'])){ ?>
                <div class="overview-headline strong center inner uppercase">
                    <?= $element_content['details']['headline'] ?>
                </div>
                <div class="overview-healdine-design-element design-element center inner">
                    <span class="design-lines left"></span>
                    <img src="data:image/png;base64,R0lGODlhAQABAAD/ACwAAAAAAQABAAACADs=" data-src="/frontend/img/uploaded/icons/3Punkte_weiss.svg" alt="design-element" class="icon invert">
                    <span class="design-lines right"></span>
                </div>
            <?php } ?>

            <section class="main overview <?= $element_content['details']['type']; ?>">
                <div class="room-overview-wrapper inner">
                    <?php $rooms = array(); ?>
                    <?php foreach($element_content['details']['_details']['prices']['connections'] as $room_id => $connection){ ?>
                        <?php if(array_key_exists($room_id, $element_content['details']['_details']['infos'])){ ?>
                            <?php $rooms[] = $element_content['details']['_details']['infos'][$room_id]; ?>
                        <?php } ?>
                    <?php } ?>

                    <!-- sort by CMS -->
                    <?php
                        uasort($rooms, function($a, $b){
                            $first_from_a = $a['sort'];
                            $first_from_b = $b['sort'];
                            if ($first_from_a == $first_from_b) {
                                return 0;
                            }
                            return ($first_from_a < $first_from_b) ? -1 : 1;
                        });
                    ?>

                    <?php foreach ($rooms as $key => $room) { ?>
                        <div class="room room-<?= $key ?> fadeIn animated">
                            <a href="<?= $this->Url->build(['node' => 'node:' . $element_content['details']['_details']['nodes'][$room['id']], 'language' => $this->request->params['language']]); ?>" target="_self">
                                <figure class="healine-img object-fit-image">
                                    <img src="data:image/png;base64,R0lGODlhAQABAAD/ACwAAAAAAQABAAACADs=" data-src="<?= $room['images'][0]['details']['seo']['2_overview'] ?>" alt="<?= $room['images'][0]['details']['title'] ?>" style="object-position: <?= $room['images'][0]['details']['focus']['2']['css'] ?>;">
                                </figure>
                            </a>
                            <div class="content-wrapper center">
                                <div class="left">
                                    <a href="<?= $this->Url->build(['node' => 'node:' . $element_content['details']['_details']['nodes'][$room['id']], 'language' => $this->request->params['language']]); ?>" target="_self">
                                        <?php if (array_key_exists('headline', $room) && isset($room['headline']) && !empty($room['headline'])) { ?>
                                            <span class="headline strong">
                                                <?= $room['headline'] ?>
                                            </span>
                                        <?php } ?>
                                        <?php if (array_key_exists('subheadline', $room) && isset($room['subheadline']) && !empty($room['subheadline'])) { ?>
                                            <span class="subheadline uppercase">
                                                <?= $room['subheadline'] ?>
                                            </span>
                                        <?php } ?>
                                    </a>
                                </div>
                                <div class="right">
                                    <div class="top">
                                        <div class="left">
                                            <?php if (array_key_exists('ic_line_1', $room) && isset($room['ic_line_1']) && !empty($room['ic_line_1'])) { ?>
                                                <span class="line-1">
                                                    <img src="data:image/png;base64,R0lGODlhAQABAAD/ACwAAAAAAQABAAACADs=" data-src="/frontend/img/uploaded/icons/Icons_Groesse.svg" alt="Calculator" class="calculator-icon icon">
                                                    <span class="text">
                                                        <?= $room['ic_line_1'] ?>
                                                    </span>
                                                </span>
                                            <?php } ?>
                                            <?php if (array_key_exists('ic_line_2', $room) && isset($room['ic_line_2']) && !empty($room['ic_line_2'])) { ?>
                                                <span class="line-2">
                                                    <img src="data:image/png;base64,R0lGODlhAQABAAD/ACwAAAAAAQABAAACADs=" data-src="/frontend/img/uploaded/icons/Icons_Personen.svg" alt="person" class="person-icon icon">
                                                    <span class="text">
                                                        <?= $room['ic_line_2'] ?>
                                                    </span>
                                                </span>
                                            <?php } ?>
                                            <?php if (array_key_exists('ic_line_3', $room) && isset($room['ic_line_3']) && !empty($room['ic_line_3'])) { ?>
                                                <span class="line-3">
                                                    <?php if ($element_content['details']['internal'] == 'Zimmertyp Stammhaus Maria Alm') { ?>
                                                        <img src="data:image/png;base64,R0lGODlhAQABAAD/ACwAAAAAAQABAAACADs=" data-src="/frontend/img/uploaded/icons/Icons_Stammhaus.svg" alt="house" class="house-icon icon">
                                                    <?php } else { ?>
                                                        <img src="data:image/png;base64,R0lGODlhAQABAAD/ACwAAAAAAQABAAACADs=" data-src="/frontend/img/uploaded/icons/Icons_Lodge.svg" alt="house" class="house-icon icon">
                                                    <?php }?>
                                                    <span class="text">
                                                        <?= $room['ic_line_3'] ?>
                                                    </span>
                                                </span>
                                            <?php } ?>
                                        </div>
                                        <div class="right">
                                            <a href="<?= $this->Url->build(['node' => 'node:' . $element_content['details']['_details']['nodes'][$room['id']], 'language' => $this->request->params['language']]); ?>" target="_self">
                                                <span class="price">
                                                    <span>
                                                        <?= isset($element_content['details']['_details']['prices']['connections'][$room['id']]['ranges']['_global']['min']['value']) ? sprintf(__d('fe', 'from')) : '-' ?>
                                                    </span>
                                                    <span>
                                                        <?= isset($element_content['details']['_details']['prices']['connections'][$room['id']]['ranges']['_global']['min']['value']) ? sprintf(__d('fe', 'EUR %s'), number_format($element_content['details']['_details']['prices']['connections'][$room['id']]['ranges']['_global']['min']['value'], 2, ',', '.')) : '-' ?>
                                                    </span>
                                                </span>
                                                <span class="text">
                                                        <?= __d('fe','per person/night') ?>
                                                </span>
                                            </a>
                                        </div>
                                    </div>
                                    <div class="bottom">
                                        <div class="left bttn">
                                            <a href="<?= $this->Url->build(['node' => 'node:' . $element_content['details']['_details']['nodes'][$room['id']], 'language' => $this->request->params['language']]); ?>" target="_self">
                                                <span class="uppercase strong">
                                                    <?= __d('fe','Details') ?>
                                                </span>
                                            </a>
                                        </div>
                                        <div class="right bttn">
                                            <a href="<?= $this->Url->build(['node' => 'node:' . Configure::read('config.fixnav.second-right.0.id'), 'language' => $this->request->params['language']]) ?>" target="_self" class="center">
                                                <img src="data:image/png;base64,R0lGODlhAQABAAD/ACwAAAAAAQABAAACADs=" data-src="/frontend/img/uploaded/icons/Icons_Buchen.svg" alt="booking" class="book-icon icon">
                                                <span class="uppercase strong">
                                                    <?= __d('fe','Book') ?>
                                                </span>
                                            </a>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    <?php } ?>

                </div>
            </section>
        <?php break;

        // PACKAGES
        case 'package': ?>
            <section class="main overview <?= $element_content['details']['type']; ?> bg">
           		<div class="package-overview-table inner">

                    <!-- sorting by CMS -->
                    <?php $packages = array(); ?>

                    <?php foreach($element_content['details']['_details']['prices']['connections'] as $package_id => $connection){ ?>
                        <?php if(array_key_exists($package_id, $element_content['details']['_details']['infos'])){ ?>
                            <?php $packages[] = $element_content['details']['_details']['infos'][$package_id]; ?>
                        <?php } ?>
                    <?php } ?>

                    <?php 
                    uasort($packages, function($a, $b){
                        $first_from_a = $a['sort'];
                        $first_from_b = $b['sort'];
                        if ($first_from_a == $first_from_b) {
                            return 0;
                        }
                        return ($first_from_a < $first_from_b) ? -1 : 1;
                    }); 
                    ?>

                    <!-- sorting by date -->
                    <?php
                    // uasort($packages, function($a, $b){
    	            //     if ($a['valid_times'] == $b['valid_times']) {
    	            //         return strcmp($a['id'], $b['id']); //does nor rly matter
    	            //     }
    	            //     return ($a['valid_times'] < $b['valid_times']) ? -1 : 1;
                    // });
                    ?>

                    <?php $packages = array_values($packages); ?>
                    <?php foreach ($packages as $key => $package) { ?>

                        <div class="package-wrapper <?= $key%2 != 0 ? 'right' : 'left'  ?> fadeInUp animated">
                            <a href="<?= $this->Url->build(['node' => 'node:' . $element_content['details']['_details']['nodes'][$package['id']], 'language' => $this->request->params['language']]); ?>" target="_self">
                                <figure class="healine-img object-fit-image">
                                    <img src="data:image/png;base64,R0lGODlhAQABAAD/ACwAAAAAAQABAAACADs=" data-src="<?= $package['images'][0]['details']['seo']['2_overview'] ?>" alt="<?= $package['images'][0]['details']['title'] ?>" style="object-position: <?= $package['images'][0]['details']['focus']['2']['css'] ?>;">
                                </figure>
                            </a>
                            <div class="package-info">

                                <!-- headline -->
                                <?php if (array_key_exists('title', $package) && isset($package['title']) && !empty($package['title'])) { ?>
                                    <span class="title strong">
                                        <a href="<?= $this->Url->build(['node' => 'node:' . $element_content['details']['_details']['nodes'][$package['id']], 'language' => $this->request->params['language']]); ?>" target="_self">
                                            <?= $package['title'] ?>
                                        </a>
                                    </span>
                                <?php } ?>

                                <!-- info-box -->
                                <?php $_style = 'style="font-size:87%;"' ?>
                                <span class="line-1">
                                    <img src="data:image/png;base64,R0lGODlhAQABAAD/ACwAAAAAAQABAAACADs=" data-src="/frontend/img/uploaded/icons/Icons_Anfragen.svg" alt="calendar" class="cal-icon icon invert">
                                    <span class="text" <?= count($package['valid_times']) > 1 ? $_style : ''; ?>>
                                        <?php $__counter = 0; ?>
                                        <?php $glue = '&nbsp;&middot;&nbsp;' ?>
                                        <?php foreach ($package['valid_times'] as $key => $_v_times) { ?>
                                            <?= $_v_times['from'] . ' - ' . $_v_times['to'] ?>
                                            <?php if ((count($package['valid_times']) > 1) && (count($package['valid_times']) != $__counter+1 )) {
                                                echo $glue;
                                            } ?>
                                            <?php $__counter++; ?>
                                        <?php } ?>
                                    </span>
                                </span>
                                <span class="line-2">
                                    <img src="data:image/png;base64,R0lGODlhAQABAAD/ACwAAAAAAQABAAACADs=" data-src="/frontend/img/uploaded/icons/Icons_Bett.svg" alt="bed" class="bed-icon icon">
                                    <span class="text">
                                        <?= $package['nights'] ?>&nbsp;<?= __dn('fe', 'Night', 'Nights', $package['nights']) ?>
                                    </span>
                                </span>
                                <span class="line-3 <?= array_key_exists('ski', $package) && isset($package['ski']) && !empty($package['ski']) ? '' : 'invisible' ?>">
                                    <img src="data:image/png;base64,R0lGODlhAQABAAD/ACwAAAAAAQABAAACADs=" data-src="/frontend/img/uploaded/icons/Icons_Ski.svg" alt="ski" class="ski-icon icon">
                                    <span class="text">
                                        <?= __d('fe','Optional ski pass') ?>
                                    </span>
                                </span>

                                <!-- price -->
                                <a href="<?= $this->Url->build(['node' => 'node:' . $element_content['details']['_details']['nodes'][$package['id']], 'language' => $this->request->params['language']]); ?>" target="_self">
                                    <?php $option_min = $element_content['details']['_details']['prices']['connections'][$package['id']]['ranges']['_global']['min']; ?>
                                    <?php if(isset($option_min['value']) && !empty($option_min['value'])){ ?>
                                        <span class="price">
                                            <span>
                                                <?= isset($option_min['value']) ? sprintf(__d('fe', 'from')) : '-' ?>
                                            </span>
                                            <span>
                                                <?= isset($option_min['value']) && !empty($option_min['value']) ? sprintf(__d('fe', ' € %s'), str_replace(',00', ',-', number_format($option_min['value'], 2, ',', '.'))) : ' - ' ?>
                                            </span>
                                        </span>
                                        <span class="text">
                                                <?= __d('fe','per person/stay') ?>
                                        </span>
                                    <?php } ?>
                                </a>

                                <!-- button -->
                                <a href="<?= $this->Url->build(['node' => 'node:' . $element_content['details']['_details']['nodes'][$package['id']], 'language' => $this->request->params['language']]); ?>" target="_self" class="bttn">
                                    <span class="uppercase strong">
                                        <?= __d('fe','Details') ?>
                                    </span>
                                </a>

                            </div>
                        </div>

           			<?php } ?>
           		</div>
        	</section>
        <?php break;

        // TREATMENTS
        case 'treatment': ?>
            <section class="main overview <?= $element_content['details']['type']; ?>-overview inner">

                <div class="treatment-line first-line">
                    <div class="placeholder title"></div>
                    <div class="prices">
                        <div class="colored">
                            <span class="">
                                <?= __d('fe', 'Duration') ?>
                            </span>
                        </div>
                        <div class="colored">
                            <span class="">
                                <?= __d('fe', 'Price') ?>
                            </span>
                        </div>
                    </div>
                    <div class="placeholder treatment-line-button"></div>
                </div>

        		<?php foreach($element_content['details']['treatments'] as $kat_k => $kat_v){ ?>
                    <?php foreach($element_content['details']['_details']['infos'] as $__k => $treatment){ ?>
                        <?php if($treatment['category_id'] == $kat_v['id']){ ?>

                            <!-- treatment -->
                            <a href="#" target="_self" class="treatment-line treatment-click" name="treatment-line" data-id="<?= $__k ?>">

                                <!-- title -->
                                <div class="title strong">
                                        <?= $treatment['title'] ?>
                                </div>

                                <!-- price line -->
                                <div class="prices">
                                    <div class="time">
                                        <img src="data:image/png;base64,R0lGODlhAQABAAD/ACwAAAAAAQABAAACADs=" data-src="/frontend/img/uploaded/icons/Icons_Zeit.svg" alt="time" class="time-icon icon">
                                        <span class="">
                                            <?= $treatment['time'] ?>&nbsp;<?= __d('fe', 'min.') ?>
                                        </span>
                                    </div>
                                    <div class="price">
                                        <span class="">
                                            <?= __d('be', 'ab EUR') ?>&nbsp;<?= number_format($treatment['price'], 2, ',', '.') ?>
                                        </span>
                                    </div>
                                </div>

                                <!-- button -->
                                <div class="treatment-line-button">
                                    <i class="fas fa-chevron-down"></i>
                                    <span class="uppercase">
                                        <?= __d('fe','Details') ?>
                                    </span>
                                </div>

                                <!-- animated content -->
                                <div class="animated-info treatment-animated-info closed" id="box-<?= $__k ?>">
                                    <div class="positioner">
                                        <figure class="object-fit-image treatment-image">
                                            <img src="data:image/png;base64,R0lGODlhAQABAAD/ACwAAAAAAQABAAACADs=" data-src="<?= $treatment['image'][0]['details']['seo'][2] ?>" alt="<?= $treatment['image'][0]['details']['title'] ?>" style="object-position: <?= $treatment['image'][0]['details']['focus'][2]['css'] ?>;">
                                        </figure>
                                        <div class="treatment-content">
                                            <?= $treatment['content'] ?>
                                        </div>
                                    </div>
                                </div>

                            </a>

                        <?php } ?>
                    <?php } ?>
                <?php } ?>

    		</section>
        <?php break;

        // JOBS
        case 'job': ?>
            <?php
                $headline = '';
                $sub_headline = '';
                $intro = '';
                $info = '';
                $img = '';
            ?>

            <section id="overview-<?= $element_content['details']['type']; ?>-<?= $element_content['id']; ?>" class="overview <?= $element_content['details']['type']; ?><?= count($element_content['details']['_details']['infos']) > 2 ? '' : ' hidden-pager'; ?>">
                <?php if(isset($element_content['details']['headline']) && !empty($element_content['details']['headline'])){ ?>
                    <div class="green-headline jobs-headline">
                        <div class="inner">
                            <strong><?= $element_content['details']['headline'] ?></strong>
                            <?php if(isset($element_content['details']['subheadline']) && !empty($element_content['details']['subheadline'])){ ?>
                                <br>
                                <span>
                                    <?= $element_content['details']['subheadline'] ?>
                                </span>
                            <?php } ?>
                        </div>
                    </div>
                <?php } ?>
                <div class="job-overview-wrapper inner">

                    <?php foreach($element_content['details']['_details']['infos'] as $job){ ?>
                        <?php
                            $headline = $job['headline'];
                            $sub_headline = $job['subheadline'];
                            $intro = $job['intro'];
                            $info = $job['info'];
                            $img = $job['images'][0]['details']['seo']['4'];
                        ?>
                        <div class="job-wrapper">
                            <div>
                                <div class="job-headlines">
                                    <span class="job-headline uppercase bigger">
                                        <?= $headline ?></br>
                                    </span>
                                    <span class="job-subheadline">
                                        <?= $sub_headline ?>
                                    </span>
                                </div>
                            </div>
                            <div class="intro">
                                <span>
                                    <?= $img = $intro ?>
                                </span>
                            </div>
                            <div class="btns">
                                <a href="<?= $this->Url->build(['node' => 'node:'.$element_content['details']['_details']['nodes'][$job['id']], 'language' => $this->request->params['language']]); ?>" class="linkbutton">
                          			<span class="linkbutton-content">
                  						<span class="linkbutton-text">
                                            <i class="fas fa-envelope"></i>
                                            <?= __d('fe', 'apply now') ?>
                                        </span>
                    		        </span>
                    			</a>
                            </div>
                        </div>
                    <?php } ?>

                </div>
            </section>
        <?php break;

        // VIDEOS
        case 'videos': ?>
            <section id="overview-<?= $element_content['details']['type']; ?>-<?= $element_content['id']; ?>" class="overview <?= $element_content['details']['type']; ?><?= count($element_content['details']['_details']['infos']) > 2 ? '' : ' hidden-pager'; ?>">
                <?php if(isset($element_content['details']['headline']) && !empty($element_content['details']['headline'])){ ?>
                    <div class="green-headline jobs-headline">
                        <div class="inner">
                            <strong><?= $element_content['details']['headline'] ?></strong>
                            <?php if(isset($element_content['details']['subheadline']) && !empty($element_content['details']['subheadline'])){ ?>
                                <br>
                                <span>
                                    <?= $element_content['details']['subheadline'] ?>
                                </span>
                            <?php } ?>
                        </div>
                    </div>
                <?php } ?>
                <div class="videos-overview-wrapper">

                    <?php foreach($element_content['details']['_details']['infos'] as $video){ ?>
                        <div class="video-item fadeInUp animated">
                            <div class="title strong">
                                <h2><?= $video['title'] ?></h2>
                            </div>
                            <div class="video-container video video-in-gallery">

                                <?php if(isset($video['youtube']) && !empty($video['youtube'])){ ?>
                    				<iframe src="https://www.youtube-nocookie.com/embed/<?= $video['youtube'] ?>?rel=0&showinfo=0&mute=1" frameborder="0" allow="autoplay; encrypted-media" allowfullscreen width="420px" height="230px" class="vid-yt iframe-placeholder"></iframe>
                    			<?php } else{ ?>
                    			    <video id="video-<?php echo $video['id']; ?>" class="video-js vjs-default-skin iframe-placeholder" controls="controls" preload="auto" width="420px" height="230px" muted>
                    			        <source src="/files/<?php echo $video['mp4']['name']; ?>" type='<?php echo $video['mp4']['type']; ?>' />
                    			    </video>
                    			<?php } ?>
                            </div>
                        </div>
                    <?php } ?>

                </div>
            </section>
        <?php break;

        default:
            //TODO: delete this!
            if($_SERVER['REMOTE_ADDR'] == '83.175.88.51'){
                ?>
                <span>'NO CONTENT TO SHOW'</span>
                <?php
            }
            break;
    } ?>
<?php } ?>
