<?php use Cake\Core\Configure; ?>
<?php if($_SERVER['REMOTE_ADDR'] == '83.175.88.51'){ ?>
    <!-- ######   PRIVACY   ###### -->
    <!-- ###### [Elements/Website/privacy.ctp] ###### -->
<?php } ?>

<section class="privacy hidden-print">
    <h3 class="strong"><?= __d('fe', 'Data protection'); ?>*</h3>
    <span>
        <?php $link = '<a class="foo" href="' . $this->Url->build(['node' => Configure::read('config.default.legal.0.org'), 'language' => $this->request->params['language']]) . '" target="_blank">' . __d('fe', 'data protection plan') . '</a>'; ?>
        <?= $this->CustomForm->input('privacy', ['type' => 'checkbox', 'label' => sprintf(__d('fe', 'Yes, I have read and accepted the %s.'), $link), 'escape' => false, 'id' => 'privacy']); ?>
    </span>
</section>
