<?php use Cake\Core\Configure; ?>
<?php use Frontend\Controller\AppController; ?>
<?php if($_SERVER['REMOTE_ADDR'] == '83.175.88.51'){ ?>
    <!-- ######   RECENT PACKAGES   ###### -->
    <!-- ###### [Elements/Website/special-3_tabs.ctp] ###### -->
<?php } ?>

<?php $_recent_packages = array_values($recent_packages); ?>

<!-- desktop -->
<section class="tabs3-wrapper only-desktop">
    <?php if(array_key_exists('details', $special_element_content) && is_array($special_element_content['details']) && !empty($special_element_content['details']) && array_key_exists('headline', $special_element_content['details']) && !empty($special_element_content['details']['headline'])){ ?>
        <div class="inner">
            <div class="headline uppercase strong center">
                <?= $special_element_content['details']['headline'] ?>
            </div>
            <div class="design-element center">
                <span class="design-lines left"></span>
                <img src="data:image/png;base64,R0lGODlhAQABAAD/ACwAAAAAAQABAAACADs=" data-src="/frontend/img/uploaded/icons/3Punkte_weiss.svg" alt="design-element" class="icon invert">
                <span class="design-lines right"></span>
            </div>
        </div>
    <?php } ?>
    <?php if(is_array($recent_packages) && !empty($recent_packages)){ ?>

        <div class="tabs-wrapper t3-to-animate invisible">
            <div class="flex-container">
                <?php foreach ($recent_packages as $_k => $package) { ?>

                    <!-- format valid times -->
                    <?php
                        $valid_times = explode('|', $recent_packages[$_k]['valid_times']);
                        foreach ($valid_times as $__key => $__time) {
                            $valid_times[$__key] = explode(':', $__time);
                            foreach ($valid_times[$__key] as $__k => $__time) {
                                $valid_times[$__key][$__k] = date("d.m.Y", strtotime($__time));
                            }
                        }
                     ?>
                     
                    <?php if ($_k < 3) { ?>
                        <div class="flex-slide home-<?= $_k ?><?= $_k == 1 ? ' active' : '' ?>" style="background-image: linear-gradient(to bottom, rgba(0, 0, 0, 0), rgba(0, 0, 0, 0.75)), url('<?= $package['images_with_details'][0]['seo']['1_middle'] ?>'); background-position: <?= $package['images_with_details'][0]['focus'][1]['css'] ?>;">
                            <div class="flex-title flex-title-home-<?= $_k ?>">
                                <?php if(isset($package['title']) && !empty($package['title'])){ ?>
                                    <span class="headline strong">
                                        <?= $package['title'] ?>
                                    </span>
                                <?php } ?>
                                <?php if(isset($package['valid_times']) && !empty($package['valid_times'])){ ?>
                                    <span class="times">
                                         <?php $__counter = 0; ?>
                                         <?php $glue = '&nbsp;&middot;&nbsp;' ?>
                                         <?php foreach ($valid_times as $key => $_v_times) { ?>
                                             <?= $_v_times['0'] . ' - ' . $_v_times['1'] ?>
                                             <?php if ((count($valid_times) > 1) && (count($valid_times) != $__counter+1 )) {
                                                 echo $glue;
                                             } ?>
                                             <?php $__counter++; ?>
                                         <?php } ?>
                                    </span>
                                <?php } ?>
                            </div>
                            <div class="flex-about flex-about-home-<?= $_k ?>">
                                    <div class="left">
                                        <?php if(isset($package['title']) && !empty($package['title'])){ ?>
                                            <span class="headline strong uppercase">
                                                <a href="<?= $this->Url->build(['node' => 'node:' . $package->node, 'language' => $this->request->params['language']]); ?>" target="_self">
                                                    <?= $package['title'] ?>
                                                </a>
                                            </span>
                                        <?php } ?>
                                        <?php if(isset($package['valid_times']) && !empty($package['valid_times'])){ ?>
                                            <span class="times">
                                                 <?php $__counter = 0; ?>
                                                 <?php $glue = '&nbsp;&middot;&nbsp;' ?>
                                                 <?php foreach ($valid_times as $key => $_v_times) { ?>
                                                     <?= $_v_times['0'] . ' - ' . $_v_times['1'] ?>
                                                     <?php if ((count($valid_times) > 1) && (count($valid_times) != $__counter+1 )) {
                                                         echo $glue;
                                                     } ?>
                                                     <?php $__counter++; ?>
                                                 <?php } ?>
                                            </span>
                                        <?php } ?>
                                    </div>
                                <div class="right">
                                    <div class="prices">
                                        <span><?= $package['content'] ?></span>
                                    </div>
                                    <div class="bttn">
                                        <a href="<?= $this->Url->build(['node' => Configure::read('config.default.request.0.org'), 'language' => $this->request->params['language']]); ?>" target="self" class="">
                                            <img src="data:image/png;base64,R0lGODlhAQABAAD/ACwAAAAAAQABAAACADs=" data-src="/frontend/img/uploaded/icons/Icons_Anfragen.svg" alt="calendar" class="cal-icon icon">
                                            <span class="uppercase">
                                                <?= __d('fe','Request') ?>
                                            </span>
                                        </a>
                                    </div>
                                </div>
                            </div>
                        </div>
                    <?php } ?>
                <?php } ?>
            </div>
        </div>
        <!-- Link button -->
        <div class="link center">
            <a href="<?= array_key_exists('link', $special_element_content['details']['link'][0]['details']) ? $special_element_content['details']['link'][0]['details']['link'] : $this->Url->build(['node' => $special_element_content['details']['link'][0]['org'], 'language' => $this->request->params['language']]) ?>" target="<?= array_key_exists('target', $special_element_content['details']['link'][0]['details']) ? $special_element_content['details']['link'][0]['details']['target'] : '_self' ?>" class="link-btn link-btn uppercase strong">
                <span class="design-element"></span>
                <span class="content">
                    <?= $special_element_content['details']['link_txt'] ?>
                </span>
            </a>
        </div>
    <?php } ?>
</section>

<!-- mobile -->
<section class="tabs3-wrapper only-mobile">
    <?php if(array_key_exists('details', $special_element_content) && is_array($special_element_content['details']) && !empty($special_element_content['details']) && array_key_exists('headline', $special_element_content['details']) && !empty($special_element_content['details']['headline'])){ ?>
        <div class="inner">
            <div class="headline uppercase strong center">
                <?= $special_element_content['details']['headline'] ?>
            </div>
            <div class="design-element center">
                <span class="design-lines left"></span>
                <img src="data:image/png;base64,R0lGODlhAQABAAD/ACwAAAAAAQABAAACADs=" data-src="/frontend/img/uploaded/icons/3Punkte_weiss.svg" alt="design-element" class="icon invert">
                <span class="design-lines right"></span>
            </div>
        </div>
    <?php } ?>
    <?php if(is_array($recent_packages) && !empty($recent_packages)){ ?>
        <div class="tabs-wrapper">
            <div class="flex-container">
                <div class="flex-slide home-<?= $_k ?> active" style="background-image: linear-gradient(to bottom, rgba(0, 0, 0, 0), rgba(0, 0, 0, 0.75)), url('<?= $recent_packages[0]['images_with_details'][0]['seo']['1_middle'] ?>'); background-position: <?= $recent_packages[0]['images_with_details'][0]['focus'][1]['css'] ?>;">
                    <div class="flex-about flex-about-home-<?= $_k ?>">
                            <div class="left">
                                <?php if(isset($recent_packages[0]['title']) && !empty($recent_packages[0]['title'])){ ?>
                                    <span class="headline strong uppercase">
                                        <a href="<?= $this->Url->build(['node' => 'node:' . $recent_packages[0]->node, 'language' => $this->request->params['language']]); ?>" target="_self">
                                            <?= $recent_packages[0]['title'] ?>
                                        </a>
                                    </span>
                                <?php } ?>
                                <?php if(isset($recent_packages[0]['valid_times']) && !empty($recent_packages[0]['valid_times'])){ ?>
                                    <span class="times">
                                         <?php $__counter = 0; ?>
                                         <?php $glue = '&nbsp;&middot;&nbsp;' ?>
                                         <?php foreach ($valid_times as $key => $_v_times) { ?>
                                             <?= $_v_times['0'] . ' - ' . $_v_times['1'] ?>
                                             <?php if ((count($valid_times) > 1) && (count($valid_times) != $__counter+1 )) {
                                                 echo $glue;
                                             } ?>
                                             <?php $__counter++; ?>
                                         <?php } ?>
                                    </span>
                                <?php } ?>
                            </div>
                        <div class="right">
                            <div class="prices">
                                <span><?= $recent_packages[0]['content'] ?></span>
                            </div>
                            <div class="bttn">
                                <a href="<?= $this->Url->build(['node' => Configure::read('config.default.request.0.org'), 'language' => $this->request->params['language']]); ?>" target="self" class="">
                                    <img src="data:image/png;base64,R0lGODlhAQABAAD/ACwAAAAAAQABAAACADs=" data-src="/frontend/img/uploaded/icons/Icons_Anfragen.svg" alt="calendar" class="cal-icon icon">
                                    <span class="uppercase">
                                        <?= __d('fe','Request') ?>
                                    </span>
                                </a>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <!-- Link button -->
        <div class="link center">
            <a href="<?= array_key_exists('link', $special_element_content['details']['link'][0]['details']) ? $special_element_content['details']['link'][0]['details']['link'] : $this->Url->build(['node' => $special_element_content['details']['link'][0]['org'], 'language' => $this->request->params['language']]) ?>" target="<?= array_key_exists('target', $special_element_content['details']['link'][0]['details']) ? $special_element_content['details']['link'][0]['details']['target'] : '_self' ?>" class="link-btn link-btn uppercase strong">
                <span class="design-element"></span>
                <span class="content">
                    <?= $special_element_content['details']['link_txt'] ?>
                </span>
            </a>
        </div>
    <?php } ?>
</section>
