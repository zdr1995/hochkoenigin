<?php
use Cake\Core\Configure;
use Cake\Datasource\ConnectionManager;
?>
<?php if($_SERVER['REMOTE_ADDR'] == '83.175.88.51'){ ?>
    <!-- ######   LANGUAGE SWITCHER   ###### -->
    <!-- ###### [Elements/Website/language-switcher-footer.ctp] ###### -->
<?php } ?>

<?php
$connection = ConnectionManager::get('default');
$lang = $this->request->params['language'];
$i = 0;
$counter = count(Configure::read('languages'));
?>

<div class="language uppercase language-footer">
    <?php foreach(Configure::read('languages') as $k => $v){ ?>
        <?php if($v['active']){ ?>
            <?php
            $url = '/redirect/' . $k . '/' . $this->request->params['node']['route'];
            $node = $connection->execute("SELECT `id`, `foreign_id` FROM `nodes` WHERE `route` = :route", ['route' => $this->request->params['node']['route']])->fetch('assoc');
            if(is_array($node) && count($node) > 0){
                $lang_check = $connection->execute("SELECT `content` FROM `i18n` WHERE `locale`='".$k."' AND `foreign_key`='".$node['foreign_id']."'")->fetch('assoc');
                if(!is_array($lang_check) || !isset($lang_check['content'])){
                    $url = '/redirect/' . $k . '/' . Configure::read('redirects.'.$k.'.default'); //Configure::read('config.default.home.0.details.node.route');
                }
            } ?>
            <?php if($this->request->params['language'] != $k){ ?>
                <a href="<?= $url ?>" class="lang <?= $k ?>"><?= strtoupper($k) ?></a>
            <?php } else{ ?>
                <span class="lang <?= $k ?> active"><?= strtoupper($k) ?></span>
            <?php } ?>
            <?php if ($i !== $counter - 1) { ?>
                <span id="spacer-white-footer"></span>
            <?php } ?>
        <?php } ?>
        <?php $i++; ?>
    <?php } ?>
</div>
