<?php use Cake\Core\Configure; ?>
<?php
$show = isset($show) ? (bool) $show : true;
$text = isset($text) ? $text : __d('fe','Send');
$opt = [];

if(isset($options) && is_array($options)){
    foreach($options as $option){
        if(array_key_exists('type', $option) && array_key_exists('text', $option) && array_key_exists('class', $option) && array_key_exists('url', $option)){
            $opt[] = $option;
        }
    }
}
?>
<div class="captcha-error"><?= __d('fe','Beim Senden ist ein Fehler aufgetreten. Bitte aktualisieren Sie die Seite und versuchen Sie es erneut.') ?></div>

<?php if($show){ ?>
    <div class="captch-input" style="width:100%">
        <div style="display: flex;flex-direction: row;align-items: center;">
            <img src="/captcha/number" width="90" height="36"/>
            <input type="text" id="custom_captcha_field" style="margin-left: 10px;">
        </div>
    </div>
<section class="captcha hidden-print" style="display: flex;flex-direction: row-reverse;">
    <div class="input required captcha">
        <button type="button" class="g-recaptcha button" id="frm_submit_btn" style="right: 0;width: 200px"> <?= $text ?> </button>
        <div class="clear"></div>
    </div>
    <div class="clear"></div>
    </div>
    <div class="clear"></div>
    <?php } ?>

    <!--    <img src="data:image/png;base64--><?//= ob_get_contents(base_64_encode($this->set('captcha',['type'=>'text']))); ?><!--"  width="100px" height="20px"/>-->
</section>

<?php if(count($opt) > 0){ ?>
    <section class="send-option hidden-print">
        <?php foreach($opt as $o){ ?>
            <a href="<?= $o['url']; ?>" class="<?= $o['class']; ?> <?= $o['type']; ?>"><?= $o['text']; ?></a>
            <div class="clear"></div>
        <?php } ?>
    </section>
<?php } ?>


<script>
    let btn= document.getElementById(('frm_submit_btn'));
    btn.addEventListener('click', function(e) {
        onFormSubmit();
    });
    function onFormSubmit(token='') {
        //verify token
        let data= {
            value: document.getElementById('custom_captcha_field').value,
            token: token,
            remoteip: '<?= $_SERVER['REMOTE_ADDR'] ?>'
        }
        $.post( "/captcha/check",data)
            .always(function(response) {
                var response = $.parseJSON(response);
                if(response && response!==undefined && response.success === true){
                    $('button.g-recaptcha').parents('form').submit();
                } else{
                    $('.captcha-error').fadeIn();
                }
            });
    }
</script>
