<?php use Cake\Core\Configure; ?>
<?php if($_SERVER['REMOTE_ADDR'] == '83.175.88.51'){ ?>
    <!-- ######   Nevigation (mobile)   ###### -->
    <!-- ###### [Elements/Website/navigation_mobile.ctp] ###### -->
<?php } ?>

<mobnav id="mainnav-mobile" class="hidden-print only-mobile hidden">
    <a class="anchor" name="navi"></a>
    <a href="<?= $this->Url->build(['node' => 'node:' . Configure::read('config.default.home.0.id'), 'language' => $this->request->params['language']]); ?>" class="nav-logo">
        <img src="data:image/png;base64,R0lGODlhAQABAAD/ACwAAAAAAQABAAACADs=" data-src="/frontend/img/logo_black.svg" alt="logo" class="menu-logo">
    </a>
    <ul class="mainnav-mobile">
        <?php foreach($menu as $idx1 => $lvl1){ ?>
            <?php $lnk1 = $this->Url->build(['node' => 'node:' . $lvl1['id'], 'language' => $this->request->params['language']]); ?>
            <li class="<?php echo 'children-' . count($lvl1['children']); ?><?php echo $lvl1['highlight'] || $lvl1['active'] ? ' active' : ''; ?>">
                <a class="m center" href="<?= $lnk1; ?>">
                    <span class="image image-left hidden">
                        <img src="data:image/png;base64,R0lGODlhAQABAAD/ACwAAAAAAQABAAACADs=" data-src="/frontend/img/uploaded/icons/Icons_Links-mittel.svg" alt="gift" class="gift-icon icon">
                    </span>
                    <span class="wrapper">
                        <span class="uppercase strong title">
                            <?= $lvl1['content']; ?>
                        </span>
                    </span>
                    <span class="image image-right">
                        <img src="data:image/png;base64,R0lGODlhAQABAAD/ACwAAAAAAQABAAACADs=" data-src="/frontend/img/uploaded/icons/Icons_Rechts-mittel.svg" alt="gift" class="gift-icon icon">
                    </span>
                </a>
                <?php if(count($lvl1['children']) > 0){ ?>
                    <ul class="subnav<?php echo $idx1 > 2 ? ' rtl' : ''; ?> hidden">
                        <li class="<?php echo $lvl1['active'] ? ' active' : ''; ?> hoveree">
                            <a href="<?= $lnk1; ?>" class="uppercase">
                                <span class="image image-left hidden">
                                    <img src="data:image/png;base64,R0lGODlhAQABAAD/ACwAAAAAAQABAAACADs=" data-src="/frontend/img/uploaded/icons/Icons_Links-mittel.svg" alt="gift" class="gift-icon icon">
                                </span>
                                <span class="wrapper">
                                    <span class="uppercase strong title">
                                        <?= $lvl1['content']; ?>
                                    </span>
                                </span>
                                <span class="image image-right">
                                    <img src="data:image/png;base64,R0lGODlhAQABAAD/ACwAAAAAAQABAAACADs=" data-src="/frontend/img/uploaded/icons/Icons_Rechts-mittel.svg" alt="gift" class="gift-icon icon">
                                </span>
                            </a>
                        </li>
                        <?php foreach($lvl1['children'] as $idx2 => $lvl2){ ?>
                            <li class="<?php echo $idx2 == 0 ? 'first' : ''; ?><?php echo $lvl2['active'] ? ' active' : ''; ?>">
                                <a href="<?= $this->Url->build(['node' => 'node:' . $lvl2['id'], 'language' => $this->request->params['language']]); ?>" class="uppercase<?php echo count($lvl2['children']) > 0 ? ' subsub' : ''; ?>">
                                    <span class="image image-left hidden">
                                        <img src="data:image/png;base64,R0lGODlhAQABAAD/ACwAAAAAAQABAAACADs=" data-src="/frontend/img/uploaded/icons/Icons_Links-mittel.svg" alt="gift" class="gift-icon icon">
                                    </span>
                                    <span class="wrapper">
                                        <span class="uppercase strong title">
                                            <?= $lvl2['content']; ?>
                                        </span>
                                    </span>
                                    <span class="image image-right">
                                        <img src="data:image/png;base64,R0lGODlhAQABAAD/ACwAAAAAAQABAAACADs=" data-src="/frontend/img/uploaded/icons/Icons_Rechts-mittel.svg" alt="gift" class="gift-icon icon">
                                    </span>
                                </a>
                                <?php if(count($lvl2['children']) > 0){ ?>
                                    <ul class="subsubnav hidden">
                                        <?php foreach($lvl2['children'] as $idx3 => $lvl3){ ?>
                                            <li class="<?php echo $idx3 == 0 ? 'first' : ''; ?><?php echo $lvl3['active'] ? ' active' : ''; ?>">
                                                <a href="<?= $this->Url->build(['node' => 'node:' . $lvl3['id'], 'language' => $this->request->params['language']]); ?>" class="">
                                                    <span class="image image-left hidden">
                                                        <img src="data:image/png;base64,R0lGODlhAQABAAD/ACwAAAAAAQABAAACADs=" data-src="/frontend/img/uploaded/icons/Icons_Links-mittel.svg" alt="gift" class="gift-icon icon">
                                                    </span>
                                                    <span class="wrapper">
                                                        <span class="uppercase strong title">
                                                            <?= $lvl3['content']; ?>
                                                        </span>
                                                    </span>
                                                    <span class="image image-right">
                                                        <img src="data:image/png;base64,R0lGODlhAQABAAD/ACwAAAAAAQABAAACADs=" data-src="/frontend/img/uploaded/icons/Icons_Rechts-mittel.svg" alt="gift" class="gift-icon icon">
                                                    </span>
                                                </a>
                                            </li>
                                        <?php } ?>
                                    </ul>
                                <?php } ?>
                            </li>
                        <?php } ?>
                    </ul>
                <?php } ?>
            </li>
        <?php } ?>
        <li class="last mobile-nav-links">
            <a href="<?= $this->Url->build(['node' => 'node:' . Configure::read('config.fixnav.second-left.0.id'), 'language' => $this->request->params['language']]) ?>" target="_self" class="left strong first">
<!--                 <img src="data:image/png;base64,R0lGODlhAQABAAD/ACwAAAAAAQABAAACADs=" data-src="/frontend/img/uploaded/icons/Icons_360_blau.svg" alt="360grad" class="360-icon icon">
                 -->                <span class="uppercase">
                    <?= Configure::read('config.fixnav.second-left.0.details.element.title') ?>
                </span>
            </a>
            <a href="<?= $this->Url->build(['node' => 'node:' . Configure::read('config.fixnav.first-left.0.id'), 'language' => $this->request->params['language']]) ?>" target="_self" class="left strong">
                <img src="data:image/png;base64,R0lGODlhAQABAAD/ACwAAAAAAQABAAACADs=" data-src="/frontend/img/uploaded/icons/Icons_Gutschein_blau.svg" alt="gift" class="gift-icon icon">
                <span class="uppercase">
                    <?= Configure::read('config.fixnav.first-left.0.details.element.title') ?>
                </span>
            </a>
        </li>
    </ul>
</mobnav>
