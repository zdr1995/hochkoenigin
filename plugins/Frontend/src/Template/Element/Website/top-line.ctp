<?php use Cake\Core\Configure; ?>
<?php if($_SERVER['REMOTE_ADDR'] == '83.175.88.51'){ ?>
    <!-- ######   TOP-LINE   ###### -->
    <!-- ###### [Elements/Website/top-line.ctp] ###### -->
<?php } ?>


<section class="top-line">
    <div class="left">

        <?= $this->element('Frontend.Website/language-switcher') ?>

    </div>
    <div class="right">
        <a href="tel:<?= Configure::read('config.default.phone-plain') ?>" class="phone">
            <img src="/frontend/img/uploaded/icons/Icons_Telefon.svg" alt="phone" class="phone-icon icon">
            <span class=""><?= Configure::read('config.default.phone') ?></span>
        </a>
        <a href="mailto:<?= Configure::read('config.default.email') ?>" class="mail">
            <img src="/frontend/img/uploaded/icons/Icons_Mail.svg" alt="email" class="mail-icon icon">
            <span class=""><?= Configure::read('config.default.email') ?></span>
        </a>
        <a href="<?= $this->Url->build(['node' => 'node:' . Configure::read('config.default.map.0.id'), 'language' => $this->request->params['language']]); ?>" class="route">
            <img src="/frontend/img/uploaded/icons/Icons_Lage.svg" alt="routeplaner" class="map-icon icon">
            <span class=""><?= __d('fe','Routeplaner') ?></span>
        </a>
    </div>
</section>

<a href="<?= $this->Url->build(['node' => 'node:' . Configure::read('config.default.home.0.id'), 'language' => $this->request->params['language']]); ?>">
    <img src="/frontend/img/logo.svg" alt="logo" class="header-logo">
</a>
