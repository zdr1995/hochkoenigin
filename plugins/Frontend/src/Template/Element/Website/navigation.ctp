<?php use Cake\Core\Configure; ?>
<?php if($_SERVER['REMOTE_ADDR'] == '83.175.88.51'){ ?>
    <!-- ######   Nevigation   ###### -->
    <!-- ###### [Elements/Website/navigation.ctp] ###### -->
<?php } ?>

<nav id="mainnav" class="hidden-print invisible">
    <a class="anchor" name="navi"></a>
    <a href="<?= $this->Url->build(['node' => 'node:' . Configure::read('config.default.home.0.id'), 'language' => $this->request->params['language']]); ?>">
        <img src="/frontend/img/logo_black.svg" alt="logo" class="menu-logo">
    </a>
    <ul class="mainnav">
        <?php foreach($menu as $idx1 => $lvl1){ ?>
            <?php $lnk1 = $this->Url->build(['node' => 'node:' . $lvl1['id'], 'language' => $this->request->params['language']]); ?>
            <li class="<?php echo 'children-' . count($lvl1['children']); ?><?php echo $lvl1['highlight'] || $lvl1['active'] ? ' active' : ''; ?>">
                <a class="d center" href="<?= $lnk1; ?>">
                    <span class="wrapper">
                        <span class="uppercase strong title">
                            <?= $lvl1['content']; ?>
                        </span>
                        <span class="subtitle">
                            <?= $lvl1['element']['subtitle']['content']; ?>
                        </span>
                    </span>
                </a>
                <?php if(count($lvl1['children']) > 0){ ?>
                <ul class="subnav<?php echo $idx1 > 2 ? ' rtl' : ''; ?> invisible-fast">
                    <li class="<?php echo $lvl1['active'] ? ' active' : ''; ?> hoveree">
                        <a href="<?= $lnk1; ?>" class="uppercase">
                            <img src="data:image/png;base64,R0lGODlhAQABAAD/ACwAAAAAAQABAAACADs=" data-src="/frontend/img/uploaded/icons/Icons_List_Blau.svg" alt="design" class="nav-list-icon icon">
                            &nbsp;<?= $lvl1['content']; ?>
                        </a>
                    </li>
                    <?php foreach($lvl1['children'] as $idx2 => $lvl2){ ?>
                        <li class="<?php echo $idx2 == 0 ? 'first' : ''; ?><?php echo $lvl2['active'] ? ' active' : ''; ?>">
                            <a href="<?= $this->Url->build(['node' => 'node:' . $lvl2['id'], 'language' => $this->request->params['language']]); ?>" class="uppercase">
                                <img src="data:image/png;base64,R0lGODlhAQABAAD/ACwAAAAAAQABAAACADs=" data-src="/frontend/img/uploaded/icons/Icons_List_Blau.svg" alt="design" class="nav-list-icon icon active">
                                &nbsp;<?= $lvl2['content']; ?>
                            </a>
                        <?php if(count($lvl2['children']) > 0){ ?>
                        <ul class="subsubnav">
                            <?php foreach($lvl2['children'] as $idx3 => $lvl3){ ?>
                                <li class="<?php echo $idx3 == 0 ? 'first' : ''; ?><?php echo $lvl3['active'] ? ' active' : ''; ?>">
                                    <a href="<?= $this->Url->build(['node' => 'node:' . $lvl3['id'], 'language' => $this->request->params['language']]); ?>" class="">
                                        <img src="data:image/png;base64,R0lGODlhAQABAAD/ACwAAAAAAQABAAACADs=" data-src="/frontend/img/uploaded/icons/Icons_List_Blau.svg" alt="design" class="nav-list-icon icon active">
                                        &nbsp;<?= $lvl3['content']; ?>
                                    </a>
                                </li>
                            <?php } ?>
                        </ul>
                        <?php } ?>
                        </li>
                    <?php } ?>
                </ul>
                <?php } ?>
            </li>
        <?php } ?>
    </ul>
    <div class="right-links">
        <a href="tel:<?= Configure::read('config.default.phone-plain') ?>">
            <img src="data:image/png;base64,R0lGODlhAQABAAD/ACwAAAAAAQABAAACADs=" data-src="/frontend/img/uploaded/icons/Icons_Telefon.svg" alt="phone" class="phone-icon icon">
            <span class=""><?= Configure::read('config.default.phone') ?></span>
        </a>
        <a href="mailto:<?= Configure::read('config.default.email') ?>">
            <img src="data:image/png;base64,R0lGODlhAQABAAD/ACwAAAAAAQABAAACADs=" data-src="/frontend/img/uploaded/icons/Icons_Mail.svg" alt="email" class="mail-icon icon">
            <span class=""><?= Configure::read('config.default.email') ?></span>
        </a>
    </div>
    <div class="design-line" style="background: url('/frontend/img/uploaded/punkt.png'); background-size: 2rem;">
        <img src="data:image/png;base64,R0lGODlhAQABAAD/ACwAAAAAAQABAAACADs=" data-src="/frontend/img/uploaded/punkt.png" alt="design-line" class="hidden">
    </div>
</nav>
