<?php if($_SERVER['REMOTE_ADDR'] == '83.175.88.51'){ ?>
    <!-- ######   BANNER #4 (footer image)   ###### -->
    <!-- ###### [Elements/Website/special-banner_4.ctp] ###### -->
<?php } ?>

<?php if(array_key_exists('details', $special_element_content) && is_array($special_element_content['details']) && !empty($special_element_content['details'])){ ?>
    <section class="banner-wrapper footer-banner-wrapper">
        <a href="<?= array_key_exists('link', $special_element_content['details']['link'][0]['details']) ? $special_element_content['details']['link'][0]['details']['link'] : $this->Url->build(['node' => $special_element_content['details']['link'][0]['org'], 'language' => $this->request->params['language']]) ?>" target="<?= array_key_exists('target', $special_element_content['details']['link'][0]['details']) ? $special_element_content['details']['link'][0]['details']['target'] : '_self' ?>">
            <?php if(array_key_exists('image', $special_element_content['details']) && !empty($special_element_content['details']['image'])){ ?>
                <figure class="object-fit-image">
                    <img src="data:image/png;base64,R0lGODlhAQABAAD/ACwAAAAAAQABAAACADs=" data-src="<?= $special_element_content['details']['image'][0]['details']['seo']['1_footerbanner'] ?>" alt="<?= $special_element_content['details']['image'][0]['details']['title'] ?>" style="object-position: <?= $special_element_content['details']['image'][0]['details']['focus'][1]['css'] ?>;">
                    <?php if(array_key_exists('textblock', $special_element_content['details']) && !empty($special_element_content['details']['textblock'])){ ?>
                        <figcaption>
                            <div class="inner-wrapper">
                                <span class="content">
                                    <?= $special_element_content['details']['textblock'] ?>
                                </span>
                            </div>
                        </figcaption>
                    <?php } ?>
                </figure>
            <?php } ?>
        </a>
    </section>
<?php } ?>
