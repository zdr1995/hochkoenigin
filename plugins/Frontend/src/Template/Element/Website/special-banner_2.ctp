<?php if($_SERVER['REMOTE_ADDR'] == '83.175.88.51'){ ?>
    <!-- ######   BANNER #2 (2 columns)   ###### -->
    <!-- ###### [Elements/Website/special-banner_2.ctp] ###### -->
<?php } ?>

<?php if(array_key_exists('details', $special_element_content) && is_array($special_element_content['details']) && !empty($special_element_content['details'])){ ?>
    <section class="banner-wrapper">
        <div class="banner-2sp-wrapper">
            <div class="left <?= $special_element_content['details']['color_check_box'] == 1 ? 'red' : '' ?>">
                <span class="content center b2-to-animate invisible">
                    <span class="block">
                        <img src="data:image/png;base64,R0lGODlhAQABAAD/ACwAAAAAAQABAAACADs=" data-src="/frontend/img/uploaded/OE.png" alt="o">
                    </span>
                    <?= $special_element_content['details']['green_line'] ?>
                </span>
            </div>
            <div class="right">
                <a href="<?= array_key_exists('link', $special_element_content['details']['link'][0]['details']) ? $special_element_content['details']['link'][0]['details']['link'] : $this->Url->build(['node' => $special_element_content['details']['link'][0]['org'], 'language' => $this->request->params['language']]) ?>" target="<?= array_key_exists('target', $special_element_content['details']['link'][0]['details']) ? $special_element_content['details']['link'][0]['details']['target'] : '_self' ?>">
                    <?php if(array_key_exists('image', $special_element_content['details']) && !empty($special_element_content['details']['image'])){ ?>
                        <figure class="object-fit-image">
                            <img src="data:image/png;base64,R0lGODlhAQABAAD/ACwAAAAAAQABAAACADs=" data-src="<?= $special_element_content['details']['image'][0]['details']['seo']['1_middle'] ?>" alt="<?= $special_element_content['details']['image'][0]['details']['title'] ?>" style="object-position: <?= $special_element_content['details']['image'][0]['details']['focus'][1]['css'] ?>;">
                            <?php if(array_key_exists('headline', $special_element_content['details']) && !empty($special_element_content['details']['headline']) && array_key_exists('text_line', $special_element_content['details']) && !empty($special_element_content['details']['text_line'])){ ?>
                                <figcaption>
                                    <div class="inner-wrapper b2-to-animate invisible">
                                        <span class="headline center uppercase strong">
                                            <span class ="bg">
                                                <?= $special_element_content['details']['headline'] ?>
                                            </span>
                                        </span>
                                        <span class="content center strong">
                                            <?= $special_element_content['details']['text_line'] ?>
                                        </span>
                                    </div>
                                </figcaption>
                            <?php } ?>
                        </figure>
                    <?php } ?>
                </a>
            </div>
        </div>
    </section>
<?php } ?>
