<?php use Cake\Core\Configure; ?>

<div class="menu">
<!-- <div class="menu slideInUp animated-fast"> -->
    <!-- only mdesktop -->
    <a href="<?= $this->Url->build([
      'node' => 'node:' . Configure::read('config.fixnav.first-left.0.id'),
      'language' => $this->request->params['language']
    ]) ?>" target="_self" class="menu-item left strong first only-desktop">
        <img src="data:image/png;base64,R0lGODlhAQABAAD/ACwAAAAAAQABAAACADs=" data-src="/frontend/img/uploaded/icons/Icons_Gutschein.svg" alt="gift" class="gift-icon icon">
        <span class="uppercase">
            <?= Configure::read(
              'config.fixnav.first-left.0.details.element.title'
            ) ?>
        </span>
    </a>
    <a href="<?= $this->Url->build([
      'node' => 'node:' . Configure::read('config.fixnav.second-left.0.id'),
      'language' => $this->request->params['language']
    ]) ?>" target="_self" class="menu-item left strong only-desktop">
        <img src="data:image/png;base64,R0lGODlhAQABAAD/ACwAAAAAAQABAAACADs=" data-src="/frontend/img/uploaded/icons/Icons_Bildgalerie.svg" alt="Image" class="image-icon icon">
        <span class="uppercase">
            <?= Configure::read(
              'config.fixnav.second-left.0.details.element.title'
            ) ?>
        </span>
    </a>
    <!-- only mobile -->
    <a href="#" target="_self" class="menu-item left strong first only-mobile" id="open-mobile-nav">
        <div class="nav-icon">
            <div></div>
        </div>
        <span class="uppercase">
            <?= __d('fe', 'menu') ?>
        </span>
    </a>
    <a href="tel:<?= Configure::read(
      'config.default.phone-plain'
    ) ?>" class="menu-item left strong only-mobile">
        <img src="data:image/png;base64,R0lGODlhAQABAAD/ACwAAAAAAQABAAACADs=" data-src="/frontend/img/uploaded/icons/Icons_Telefon.svg" alt="phone" class="phone-icon icon">
        <span class="uppercase">
            <?= __d('fe', 'Call') ?>
        </span>
    </a>
    <!-- always -->
    <a href="#mainnav" id="menu-open" class="j2c menu-item nav-bttn strong">
        <img src="data:image/png;base64,R0lGODlhAQABAAD/ACwAAAAAAQABAAACADs=" data-src="/frontend/img/uploaded/icons/Icons_Down_blau.svg" alt="down" class="down-icon icon animated pulse infinite">
        <span class="uppercase">
            <?= __d('fe', 'menu') ?>
        </span>
    </a>
    <a href="#" id="recent-packages-toggle" class="j2c menu-item nav-bttn strong invisible">
        <img src="/frontend/img/uploaded/icons/Icons_Up.svg" alt="down" class="down-icon icon animated pulse infinite">
        <img src="/frontend/img/uploaded/icons/Icons_Down.svg" alt="down" class="down-icon icon animated pulse infinite invisible-2">
        <span class="uppercase">
            <?= __d('fe', 'TOP offers') ?>
        </span>
    </a>
    <div id="recent-packages" class="menu-item invisible slideInUp animated">
        <?= $this->element('Frontend.Website/special-recent-packages') ?>
    </div>
    <a href="<?= $this->Url->build([
      'node' => 'node:' . Configure::read('config.fixnav.first-right.0.id'),
      'language' => $this->request->params['language']
    ]) ?>" target="_self" class="menu-item right col-light strong">
        <img src="data:image/png;base64,R0lGODlhAQABAAD/ACwAAAAAAQABAAACADs=" data-src="/frontend/img/uploaded/icons/Icons_Anfragen.svg" alt="calendar" class="cal-icon icon">
        <span class="uppercase">
            <?= Configure::read(
              'config.fixnav.first-right.0.details.element.title'
            ) ?>
        </span>
    </a>
    <a href="<?= $this->Url->build([
      'node' => 'node:' . Configure::read('config.fixnav.second-right.0.id'),
      'language' => $this->request->params['language']
    ]) ?>" target="_self" class="menu-item right red strong last">
        <img src="data:image/png;base64,R0lGODlhAQABAAD/ACwAAAAAAQABAAACADs=" data-src="/frontend/img/uploaded/icons/Icons_Buchen.svg" alt="booking" class="book-icon icon">
        <span class="uppercase">
            <?= Configure::read(
              'config.fixnav.second-right.0.details.element.title'
            ) ?>
        </span>
    </a>
</div>
