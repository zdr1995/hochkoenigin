<?php if($_SERVER['REMOTE_ADDR'] == '83.175.88.51'){ ?>
    <!-- ######   GALERY   ###### -->
    <!-- ###### [Elements/Website/gallery.ctp] ###### -->
<?php } ?>

<?php  if(array_key_exists('details', $element_content) && is_array($element_content['details']) && array_key_exists('images', $element_content['details']) && is_array($element_content['details']['images']) && count($element_content['details']['images']) > 0){ ?>
<?php $purpose = '2'; $focus = 2; $nr = 0; ?>
<section class="gallery main hidden-print">
	<div class="inner-wrapper center">
	    <ul class="bxslider">
	        <?php foreach($element_content['details']['images'] as $image){ ?>
	            <?php if(array_key_exists('details', $image) && is_array($image['details']) && count($image['details']) > 0){ ?>
	                <li>
                        <figure class="bxslide object-fit-image">
                            <img src="data:image/png;base64,R0lGODlhAQABAAD/ACwAAAAAAQABAAACADs=" data-src="<?= $image['details']['seo'][$purpose]; ?>" alt="<?= $image['details']['title']; ?>" style="object-position: <?= $image['details']['focus'][$focus]['css']; ?>;">
                        </figure>
	                </li>
	                <?php $nr++; ?>
	            <?php } ?>
	        <?php } ?>
	    </ul>
        <?php $idx = 0; ?>
        <div id="bx-gallery-pager">
            <?php if(count($element_content['details']['images']) >= 1){ ?>
                <?php foreach($element_content['details']['images'] as $_item){ ?>
                    <a data-slide-index="<?= $idx ?>" href="">
                        <span class="strong">
                            <?php if (array_key_exists('text', $_item['details']) && !empty($_item['details']['text'])) { ?>
                                <?= $_item['details']['text'] ?>
                            <?php } else { ?>
                                <?= $_item['details']['title'] ?>
                            <?php } ?>
                        </span>
                    </a>
                    <?php $idx++; ?>
                <?php } ?>
            <?php } ?>
        </div>
    </div>
</section>
<?php } ?>
