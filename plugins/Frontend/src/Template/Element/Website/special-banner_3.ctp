<?php use Cake\Core\Configure; ?>

<?php if($_SERVER['REMOTE_ADDR'] == '83.175.88.51'){ ?>
    <!-- ######   BANNER #3 (big room)   ###### -->
    <!-- ###### [Elements/Website/special-banner_3.ctp] ###### -->
<?php } ?>

<?php if(array_key_exists('details', $special_element_content) && is_array($special_element_content['details']) && !empty($special_element_content['details'])){ ?>
    <section class="big-banner-wrapper">
        <div class="top">
            <?php if(array_key_exists('image', $special_element_content['details']) && is_array($special_element_content['details']['image']) && !empty($special_element_content['details']['image'])){ ?>
                <figure class="healine-img object-fit-image">
                    <img src="data:image/png;base64,R0lGODlhAQABAAD/ACwAAAAAAQABAAACADs=" data-src="<?= $special_element_content['details']['image'][0]['details']['seo']['1_bigbanner'] ?>" alt="<?= $special_element_content['details']['image'][0]['details']['title'] ?>" style="object-position: <?= $special_element_content['details']['image'][0]['details']['focus']['1']['css'] ?>;">
                </figure>
            <?php } ?>
        </div>
        <div class="bottom">
            <div class="content center">
                <div class="left only-desktop">
                    <?php if(array_key_exists('headline', $special_element_content['details']) && !empty($special_element_content['details']['headline'])){ ?>
                        <span class="headline strong">
                            <?= $special_element_content['details']['headline'] ?>
                        </span>
                    <?php } ?>
                    <?php if(array_key_exists('subheadline', $special_element_content['details']) && !empty($special_element_content['details']['subheadline'])){ ?>
                        <span class="subheadline uppercase strong">
                            <?= $special_element_content['details']['subheadline'] ?>
                        </span>
                    <?php } ?>
                    <?php if(array_key_exists('textblock', $special_element_content['details']) && !empty($special_element_content['details']['textblock'])){ ?>
                        <span class="text">
                            <?= $special_element_content['details']['textblock'] ?>
                        </span>
                    <?php } ?>
                </div>
                <div class="right">
                    <?php if(array_key_exists('text_line_1', $special_element_content['details']) && !empty($special_element_content['details']['text_line_1'])){ ?>
                        <?php if(array_key_exists('link', $special_element_content['details']) && is_array($special_element_content['details']['link']) && !empty($special_element_content['details']['link']) && array_key_exists('details', $special_element_content['details']['link'][0]) && is_array($special_element_content['details']['link'][0]['details']) && !empty($special_element_content['details']['link'][0]['details'])){ ?>
                            <a href="<?= array_key_exists('link', $special_element_content['details']['link_4'][0]['details']) ? $special_element_content['details']['link_4'][0]['details']['link'] : $this->Url->build(['node' => $special_element_content['details']['link_4'][0]['org'], 'language' => $this->request->params['language']]) ?>" target="<?= array_key_exists('target', $special_element_content['details']['link_4'][0]['details']) ? $special_element_content['details']['link_4'][0]['details']['target'] : '_self' ?>">
                        <?php } ?>
                            <figure class="red-dot object-fit-image invisible content-red-dot">
                                <img src="data:image/png;base64,R0lGODlhAQABAAD/ACwAAAAAAQABAAACADs=" data-src="/frontend/img/uploaded/Button-Pink_2.svg" alt="design-element">
                                <figcaption>
                                    <?= $special_element_content['details']['text_line_1'] ?>
                                </figcaption>
                            </figure>
                        <?php if(array_key_exists('link', $special_element_content['details']) && is_array($special_element_content['details']['link']) && !empty($special_element_content['details']['link']) && array_key_exists('details', $special_element_content['details']['link'][0]) && is_array($special_element_content['details']['link'][0]['details']) && !empty($special_element_content['details']['link'][0]['details'])){ ?>
                            </a>
                        <?php } ?>
                    <?php } ?>
                    <div class="left only-mobile">
                        <?php if(array_key_exists('headline', $special_element_content['details']) && !empty($special_element_content['details']['headline'])){ ?>
                            <span class="headline strong">
                                <?= $special_element_content['details']['headline'] ?>
                            </span>
                        <?php } ?>
                        <?php if(array_key_exists('subheadline', $special_element_content['details']) && !empty($special_element_content['details']['subheadline'])){ ?>
                            <span class="subheadline uppercase strong">
                                <?= $special_element_content['details']['subheadline'] ?>
                            </span>
                        <?php } ?>
                        <?php if(array_key_exists('textblock', $special_element_content['details']) && !empty($special_element_content['details']['textblock'])){ ?>
                            <span class="text">
                                <?= $special_element_content['details']['textblock'] ?>
                            </span>
                        <?php } ?>
                    </div>
                    <?php if(array_key_exists('price', $special_element_content['details']) && !empty($special_element_content['details']['price'])){ ?>
                        <div class="price">
                            <span class="line-1">
                                <?= __d('fe','from') . '&nbsp;' . __d('fe','EUR') . '&nbsp;' . number_format($special_element_content['details']['price'], 2, ',', '.') ?>
                            </span>
                            <span class="line-2">
                                <?= __d('fe','per person/night') ?>
                            </span>
                        </div>
                    <?php } ?>
                    <?php if(array_key_exists('link', $special_element_content['details']) && is_array($special_element_content['details']['link']) && !empty($special_element_content['details']['link']) && array_key_exists('details', $special_element_content['details']['link'][0]) && is_array($special_element_content['details']['link'][0]['details']) && !empty($special_element_content['details']['link'][0]['details'])){ ?>
                        <div class="bttn">
                            <a href="<?= array_key_exists('link', $special_element_content['details']['link'][0]['details']) ? $special_element_content['details']['link'][0]['details']['link'] : $this->Url->build(['node' => $special_element_content['details']['link'][0]['org'], 'language' => $this->request->params['language']]) ?>" target="<?= array_key_exists('target', $special_element_content['details']['link'][0]['details']) ? $special_element_content['details']['link'][0]['details']['target'] : '_self' ?>">
                                <?php $_icon = isset($special_element_content['details']['link_icon']) && !empty($special_element_content['details']['link_icon']) ? $special_element_content['details']['link_icon'] : 'Buchen' ?>
                                <img src="data:image/png;base64,R0lGODlhAQABAAD/ACwAAAAAAQABAAACADs=" data-src="/frontend/img/uploaded/icons/Icons_<?= $_icon ?>.svg" alt="<?= $_icon ?>" class="<?= $_icon ?>-icon icon">
                                <span class="uppercase strong">
                                    <?= isset($special_element_content['details']['link_txt']) && !empty($special_element_content['details']['link_txt']) ? $special_element_content['details']['link_txt'] : __d('fe','Book') ?>
                                </span>
                            </a>
                        </div>
                    <?php } ?>
                    <div class="bttn only-mobile green">
                        <a href="<?= $this->Url->build(['node' => 'node:' . Configure::read('config.default.room_overview.0.id'), 'language' => $this->request->params['language']]); ?>" target="_self">
                            <?php $_icon = isset($special_element_content['details']['link_icon']) && !empty($special_element_content['details']['link_icon']) ? $special_element_content['details']['link_icon'] : 'Buchen' ?>
                            <span class="uppercase strong">
                                <?= __d('fe','Rooms & prices') ?>
                            </span>
                        </a>
                    </div>
                </div>
            </div>
        </div>
    </section>
<?php } ?>
