<?php if($_SERVER['REMOTE_ADDR'] == '83.175.88.51'){ ?>
    <!-- ######   BANNER #5 (wellnes banner - 2 images)   ###### -->
    <!-- ###### [Elements/Website/special-banner_5.ctp] ###### -->
<?php } ?>

<?php if(array_key_exists('details', $special_element_content) && is_array($special_element_content['details']) && !empty($special_element_content['details'])){ ?>
    <section class="banner-wrapper banner-5-wrapper">
        <?php if(array_key_exists('link', $special_element_content['details']) && !empty($special_element_content['details']['link'])){ ?>
            <a href="<?= array_key_exists('link', $special_element_content['details']['link'][0]['details']) ? $special_element_content['details']['link'][0]['details']['link'] : $this->Url->build(['node' => $special_element_content['details']['link'][0]['org'], 'language' => $this->request->params['language']]) ?>" target="<?= array_key_exists('target', $special_element_content['details']['link'][0]['details']) ? $special_element_content['details']['link'][0]['details']['target'] : '_self' ?>"  class="big-img">
        <?php } else { ?>
            <div class="big-img">
        <?php } ?>
        <?php if(array_key_exists('image_big', $special_element_content['details']) && !empty($special_element_content['details']['image_big'])){ ?>
            <figure class="object-fit-image_big b5-to-animate-img-bg invisible">
                <img src="data:image/png;base64,R0lGODlhAQABAAD/ACwAAAAAAQABAAACADs=" data-src="<?= $special_element_content['details']['image_big'][0]['details']['seo']['1_middle'] ?>" alt="<?= $special_element_content['details']['image_big'][0]['details']['title'] ?>" style="object-position: <?= $special_element_content['details']['image_big'][0]['details']['focus'][1]['css'] ?>;">
            </figure>
            <?php } ?>
        <?php if(array_key_exists('link', $special_element_content['details']) && !empty($special_element_content['details']['link'])){ ?>
            </a>
        <?php } else { ?>
            </div>
        <?php } ?>
            <div class="inner-wrapper">
                <span class="content b5-to-animate invisible">
                    <?php if(array_key_exists('textblock', $special_element_content['details']) && !empty($special_element_content['details']['textblock'])){ ?>
                        <?= $special_element_content['details']['textblock'] ?>
                        <?php } ?>
                    </span>
                </div>
        <?php if(array_key_exists('link', $special_element_content['details']) && !empty($special_element_content['details']['link'])){ ?>
            <a href="<?= array_key_exists('link', $special_element_content['details']['link'][0]['details']) ? $special_element_content['details']['link'][0]['details']['link'] : $this->Url->build(['node' => $special_element_content['details']['link'][0]['org'], 'language' => $this->request->params['language']]) ?>" target="<?= array_key_exists('target', $special_element_content['details']['link'][0]['details']) ? $special_element_content['details']['link'][0]['details']['target'] : '_self' ?>"  class="small-img">
        <?php } else { ?>
            <div class="small-img">
        <?php } ?>
            <?php if(array_key_exists('image_small', $special_element_content['details']) && !empty($special_element_content['details']['image_small'])){ ?>
                <figure class="object-fit-image_small b5-to-animate-img-sm invisible">
                    <img src="data:image/png;base64,R0lGODlhAQABAAD/ACwAAAAAAQABAAACADs=" data-src="<?= $special_element_content['details']['image_small'][0]['details']['seo']['4_banner5'] ?>" alt="<?= $special_element_content['details']['image_small'][0]['details']['title'] ?>" style="object-position: <?= $special_element_content['details']['image_small'][0]['details']['focus'][4]['css'] ?>;">
                </figure>
            <?php } ?>
        <?php if(array_key_exists('link', $special_element_content['details']) && !empty($special_element_content['details']['link'])){ ?>
            </a>
        <?php } else { ?>
            </div>
        <?php } ?>
    </section>
<?php } ?>
