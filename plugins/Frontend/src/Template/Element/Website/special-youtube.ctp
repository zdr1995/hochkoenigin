<?php if($_SERVER['REMOTE_ADDR'] == '83.175.88.51'){ ?>
    <!-- ######   YOUTUBE   ###### -->
    <!-- ###### [Elements/Website/special-youtube.ctp] ###### -->
<?php } ?>

<section class="youtube-container hidden-print">
	<div class="inner">
    	<div class="youtube-video-container">
    		<iframe src="https://www.youtube-nocookie.com/embed/<?= $special_element_content['details']['video'] ?>?rel=0&amp;showinfo=0" frameborder="0" webkitallowfullscreen mozallowfullscreen allowfullscreen></iframe>
    	</div>
    </div>
</section>
