<table cellpadding="0" cellspacing="0" width="100%">
    <tr>
    <td><h1 style="color: #005d5d;margin-top: 25px;margin-bottom:15px;font-size: 26px;font-family: Adobe Garamond Pro, Arail, sans-serif;text-transform:uppercase;"><?= $headline; ?></h1></td>
    </tr>
    <tr>
        <td style="text-align:right;"><?= date("d.m.Y H:i:s"); ?></td>
    </tr>
    <?php if(isset($content) && !empty($content)){ ?>
    <tr>
        <td>&nbsp;</td>
    </tr>
    <tr>
        <td><?= $content; ?></td>
    </tr>
    <?php } ?>
    <?php if(isset($infos) && !empty($infos)){ ?>
    <tr>
        <td>&nbsp;</td>
    </tr>
    <tr>
        <td><?= $infos; ?></td>
    </tr>
    <?php } ?>
</table>