<?php use Cake\Core\Configure; ?>

<?php if($_SERVER['REMOTE_ADDR'] == '83.175.88.51'){ ?>
    <!-- ######   ROOMS   ###### -->
    <!-- ###### [Website/Rooms/index.ctp] ###### -->
<?php } ?>

<main class="fadeIn animated-fast room">
    <a class="anchor" name="content"></a>

    <!-- HEADLINE -->
    <div class="content-wrapper first-line">
        <div class="left">
            <h1 class="strong"><?= $content['headline']; ?></h1>
            <?php if (array_key_exists('subheadline', $content) && !empty($content['subheadline'])) { ?>
                <h2 class="uppercase"><?= $content['subheadline']; ?></h2>
            <?php } ?>
        </div>
        <div class="right">
            <?= $content['content']; ?>
        </div>
    </div>

    <!-- ROOM PLAN & INFO -->
    <div class="content-wrapper second-line">
        <div class="left">
            <a class="lightbox" href="#room-plan">
               <img src="data:image/png;base64,R0lGODlhAQABAAD/ACwAAAAAAQABAAACADs=" data-src="<?= $content['images_small'][0]['details']['urls']['2_plan'] ?>" alt="<?= $content['images_small'][0]['details']['title'] ?>" style="object-position: <?= $content['images_small'][0]['details']['focus']['2']['css'] ?>;">
            </a>
            <div class="lightbox-target" id="room-plan">
                <a class="lightbox-close" href="#">
                   <img src="data:image/png;base64,R0lGODlhAQABAAD/ACwAAAAAAQABAAACADs=" data-src="<?= $content['images_small'][0]['details']['urls']['2'] ?>" alt="<?= $content['images_small'][0]['details']['title'] ?>" style="object-position: <?= $content['images_small'][0]['details']['focus']['2']['css'] ?>;">
               </a>
            </div>
        </div>
        <div class="right">
            <div class="positioner">

                <!-- regular info lines -->
                <?php if (array_key_exists('ic_line_1', $content) && isset($content['ic_line_1']) && !empty($content['ic_line_1'])) { ?>
                    <span class="line-1">
                        <img src="data:image/png;base64,R0lGODlhAQABAAD/ACwAAAAAAQABAAACADs=" data-src="/frontend/img/uploaded/icons/Icons_Groesse.svg" alt="calculator" class="calculator-icon icon">
                        <span class="text">
                            <?= $content['ic_line_1'] ?>
                        </span>
                    </span>
                <?php } ?>
                <?php if (array_key_exists('ic_line_2', $content) && isset($content['ic_line_2']) && !empty($content['ic_line_2'])) { ?>
                    <span class="line-2">
                        <img src="data:image/png;base64,R0lGODlhAQABAAD/ACwAAAAAAQABAAACADs=" data-src="/frontend/img/uploaded/icons/Icons_Personen.svg" alt="person" class="person-icon icon">
                        <span class="text">
                            <?= $content['ic_line_2'] ?>
                        </span>
                    </span>
                <?php } ?>
                <?php if (array_key_exists('ic_line_3', $content) && isset($content['ic_line_3']) && !empty($content['ic_line_3'])) { ?>
                    <span class="line-3">
                        <?php if ($content['ic_line_3'] == 'Stammhaus Maria Alm') { ?>
                            <img src="data:image/png;base64,R0lGODlhAQABAAD/ACwAAAAAAQABAAACADs=" data-src="/frontend/img/uploaded/icons/Icons_Stammhaus.svg" alt="house" class="house-icon icon">
                        <?php } else { ?>
                            <img src="data:image/png;base64,R0lGODlhAQABAAD/ACwAAAAAAQABAAACADs=" data-src="/frontend/img/uploaded/icons/Icons_Lodge.svg" alt="house" class="house-icon icon">
                        <?php }?>
                        <span class="text">
                            <?= $content['ic_line_3'] ?>
                        </span>
                    </span>
                <?php } ?>

                <!-- linked info lines -->
                <?php if (array_key_exists('ic_line_4', $content) && isset($content['ic_line_4']) && !empty($content['ic_line_4'])) { ?>
                    <span class="line-4">
                        <?php if (array_key_exists('ic_link_1', $content) && isset($content['ic_link_1']) && !empty($content['ic_link_1'])) { ?>
                            <a href="<?= array_key_exists('link', $content['ic_link_1'][0]['details']) ? $content['ic_link_1'][0]['details']['link'] : $this->Url->build(['node' => $content['ic_link_1'][0]['org'], 'language' => $this->request->params['language']]) ?>" target="<?= array_key_exists('target', $content['ic_link_1'][0]['details']) ? $content['ic_link_1'][0]['details']['target'] : '_self' ?>">
                            <?php $_if_link = '</a>'; ?>
                        <?php } ?>
                            <?php $_icon = isset($content['link_icon_1']) && !empty($content['link_icon_1']) ? $content['link_icon_1'] : 'Inklusivleistungen_blau' ?>
                            <img src="data:image/png;base64,R0lGODlhAQABAAD/ACwAAAAAAQABAAACADs=" data-src="/frontend/img/uploaded/icons/Icons_<?= $_icon ?>.svg" alt="<?= $_icon ?>" class="<?= $_icon ?>-icon icon">
                            <span class="text strong">
                                <?= $content['ic_line_4'] ?>
                            </span>
                        <?= $_if_link ?>
                    </span>
                <?php } ?>
                <?php if (array_key_exists('ic_line_5', $content) && isset($content['ic_line_5']) && !empty($content['ic_line_5'])) { ?>
                    <span class="line-5">
                        <?php if (array_key_exists('ic_link_2', $content) && isset($content['ic_link_2']) && !empty($content['ic_link_2'])) { ?>
                            <a href="<?=  array_key_exists('link', $content['ic_link_2'][0]['details']) ? $content['ic_link_2'][0]['details']['link'] : $this->Url->build(['node' => $content['ic_link_2'][0]['org'], 'language' => $this->request->params['language']]) ?>" target="<?= array_key_exists('target', $content['ic_link_2'][0]['details']) ? $content['ic_link_2'][0]['details']['target'] : '_self' ?>">
                            <?php $_if_link = '</a>'; ?>
                        <?php } ?>
                            <?php $_icon = isset($content['link_icon_2']) && !empty($content['link_icon_2']) ? $content['link_icon_2'] : 'Schlafmenue_blau' ?>
                            <img src="data:image/png;base64,R0lGODlhAQABAAD/ACwAAAAAAQABAAACADs=" data-src="/frontend/img/uploaded/icons/Icons_<?= $_icon ?>.svg" alt="<?= $_icon ?>" class="<?= $_icon ?>-icon icon">
                            <span class="text strong">
                                <?= $content['ic_line_5'] ?>
                            </span>
                            <?= $_if_link ?>
                    </span>
                <?php } ?>

            </div>
        </div>
    </div>
</main>

<?= $this->element('Frontend.Website/media', ['media' => $content['media'], 'position' => 'media-top', 'wrapper' => 'media-top']) ?>

<!-- SLIDER -->
<section class="room-slider fadeIn animated">
    <div class="impressions">
        <ul class="viewport bxslider">
            <?php foreach($content['images'] as $img){ ?>
                <li>
                    <div class="bxslide">
                        <figure class="object-fit-image">
                          <img src="data:image/png;base64,R0lGODlhAQABAAD/ACwAAAAAAQABAAACADs=" data-src="<?= $img['details']['urls'][2] ?>" alt="<?= $img['details']['title'] ?>" style="object-position: <?= $img['details']['focus'][2]['css'] ?>;">
                        </figure>
                    </div>
                </li>
            <?php } ?>
        </ul>
    </div>
    <div class="link-bttn">
        <a class="degree360" name="360degree" href="#">
            <img src="data:image/png;base64,R0lGODlhAQABAAD/ACwAAAAAAQABAAACADs=" data-src="/frontend/img/uploaded/icons/Icons_360_blau.svg" alt="360grad" class="360-icon icon">
            <span class="strong">
                Rundgang starten
            </span>
        </a>
    </div>
</section>

<!-- MORE INFO -->
<?php if(isset($content['content_big']) && !empty($content['content_big'])){ ?>
<section class="room-info">
    <div class="inner">
        <div class="headline uppercase strong center">
            <?= __d('fe','Equipment') ?>
        </div>
        <div class="design-element center">
            <span class="design-lines left"></span>
            <img src="data:image/png;base64,R0lGODlhAQABAAD/ACwAAAAAAQABAAACADs=" data-src="/frontend/img/uploaded/icons/3Punkte_weiss.svg" alt="design-element" class="icon invert">
            <span class="design-lines right"></span>
        </div>
    </div>
    <div class="content inner">
        <?= $content['content_big']; ?>
    </div>
</section>
<?php } ?>

<!-- MIDDLE SPEC: ELEMENT -->
<?= $this->element('Frontend.Website/media', ['media' => $content['media'], 'position' => 'media-middle', 'wrapper' => 'media-middle']) ?>

<!-- PRICES -->
<?php if(array_key_exists($content['id'], $prices['values']) && count($prices['values'][$content['id']]) > 0){ ?>
    <a id="" class="room-prices-anchor" href="#"></a>
    <section class="room prices">
        <div class="rp-bg inner">

            <!-- manual season sorting -->
            <?php
                if (Configure::read('config.default.season') == 'wi') {
                    $_sorted_season_ids = [
                        '_global',
                        'winter',
                        'summer',
                    ];
                } else {
                    $_sorted_season_ids = [
                        '_global',
                        'summer',
                        'winter',
                    ];
                }
                $_sorted_seasons = array_merge(array_flip($_sorted_season_ids), $prices['connections'][$content['id']]['used']['drafts']);
            ?>

            <?php foreach($prices['drafts'] as $id => $draft){ ?>
                <?php foreach ($_sorted_seasons as $__name => $__season) { ?>
                    <?php if(in_array($id, $__season) && $__name !== '_global'){ ?>

                        <h2 class="uppercase strong season-title">
                            <?php switch($__name){
                                case "winter":
                                    ?><?php
                                    echo __d('fe', 'Winter prices');
                                    ?><?php
                                    break;
                                case "summer":
                                ?><?php
                                    echo __d('fe', 'Summer prices');
                                    break;
                            } ?>
                        </h2>

                        <!-- desktop price-table -->
                        <table class="price-table options-<?= count($prices['drafts'][$id]['used']['options']); ?> desktop-prices">

                            <!-- headline -->
                            <tr class="line head-line">
                                <th class="price-name">
                                    <?= __d('fe','Price per person incl. all ') ?>
                                    <a href="<?= $this->Url->build(['node' => 'node:' . Configure::read('config.default.services.0.id'), 'language' => $this->request->params['language']]); ?>" target="_blank">
                                        <?= __d('fe','services') ?>
                                    </a>
                                </th>

                                <?php foreach ($prices['options'] as $option => $name) { ?>
                                    <?php if(in_array($option, $prices['drafts'][$id]['used']['options'][$__name])){ ?>
                                        <?php
                                            switch($option){
                                                case "day":
                                                    $title = __d('fe', '1-3 nights');
                                                    break;
                                                case "short":
                                                    $title = __d('fe', '4-6 nights');
                                                    break;
                                                case "week":
                                                    $title = __d('fe', 'from 7 nights');
                                                    break;
                                                case "long":
                                                    $title = __d('fe', 'Week price');
                                                    break;
                                            }
                                        ?>
                                        <th class="price-type strong uppercase">
                                            <?= $title; ?>
                                        </th>
                                    <?php } ?>
                                <?php } ?>
                            </tr>

                            <!-- border-line -->
                            <tr class="border-line">
                                <td></td>
                                <?php foreach ($prices['options'] as $option => $name) { ?>
                                    <td></td>
                                <?php } ?>
                            </tr>

                            <!-- prices -->
                            <?php foreach($prices['seasons'] as $season){ ?>
                                <?php if(array_key_exists($season['id'], $prices['values'][$content['id']]) && array_key_exists($id, $prices['values'][$content['id']][$season['id']]) && $season['container'] == $__name){ ?>
                                    <tr class="line">
                                        <td class="season">
                                            <?php $_title = preg_replace('~[\d.-]~', '', $season['internal']); ?>
                                            <div class="title strong"><?= $_title ?></div>
                                            <?php foreach($season['times'] as $time){ ?>
                                                <div class="time sub-title"><?= date("d.m.Y", $time['from']) . ' - ' . date("d.m.Y", $time['to']); ?></div>
                                            <?php } ?>
                                        </td>
                                        <?php foreach($prices['options'] as $option => $name){ ?>
                                            <?php if(in_array($option, $prices['drafts'][$id]['used']['options'][$__name])){ ?>
                                            <td class="option">
                                                <?php echo array_key_exists($option, $prices['values'][$content['id']][$season['id']][$id]) ? '<span>EUR&nbsp;</span>' . number_format($prices['values'][$content['id']][$season['id']][$id][$option]['standard']['value'], 2, ',', '.') : '--'; ?>
                                            </td>
                                            <?php } ?>
                                        <?php } ?>
                                    </tr>
                                    <tr class="border-line">
                                        <td></td>
                                        <?php foreach ($prices['options'] as $option => $name) { ?>
                                            <td></td>
                                        <?php } ?>
                                    </tr>
                                <?php } ?>
                            <?php } ?>
                        </table>

                        <!-- mobile price-table -->
                        <div class="mobile-prices">
                            <!-- headline -->
                            <div class="title-line">
                                <?= __d('fe','Price per person incl. all ') ?>
                                <a href="<?= $this->Url->build(['node' => 'node:' . Configure::read('config.default.services.0.id'), 'language' => $this->request->params['language']]); ?>" target="_blank">
                                    <?= __d('fe','services') ?>
                                </a>
                            </div>

                            <!-- prices -->
                            <?php foreach($prices['seasons'] as $season){ ?>
                                <?php if(array_key_exists($season['id'], $prices['values'][$content['id']]) && array_key_exists($id, $prices['values'][$content['id']][$season['id']])){ ?>
                                    <div class="season-line">
                                        <?php $_title = preg_replace('~[\d.-]~', '', $season['internal']); ?>
                                        <div class="title strong"><?= $_title ?></div>
                                        <?php foreach($season['times'] as $time){ ?>
                                            <div class="time sub-title"><?= date("d.m.Y", $time['from']) . ' - ' . date("d.m.Y", $time['to']); ?></div>
                                        <?php } ?>
                                    </div>
                                    <div class="price-line">
                                        <?php foreach($prices['options'] as $option => $name){ ?>
                                            <?php if(in_array($option, $prices['drafts'][$id]['used']['options'][$__name])){ ?>
                                            <div class="option">
                                                <div class="price-title strong">
                                                    <?php if(in_array($option, $prices['drafts'][$id]['used']['options'][$__name])){ ?>
                                                        <?php
                                                            switch($option){
                                                                case "day":
                                                                    $title = __d('fe', '1-3 nights');
                                                                    break;
                                                                case "short":
                                                                    $title = __d('fe', '4-6 nights');
                                                                    break;
                                                                case "week":
                                                                    $title = __d('fe', 'from 7 nights');
                                                                    break;
                                                                case "long":
                                                                    $title = __d('fe', 'Week price');
                                                                    break;
                                                            }
                                                        ?>
                                                        <?= $title; ?>
                                                    <?php } ?>
                                                </div>
                                                <div class="price">
                                                    <?php echo array_key_exists($option, $prices['values'][$content['id']][$season['id']][$id]) ? '<span>EUR&nbsp;</span>' . number_format($prices['values'][$content['id']][$season['id']][$id][$option]['standard']['value'], 2, ',', '.') : '--'; ?>
                                                </div>
                                            </div>
                                            <?php } ?>
                                        <?php } ?>
                                    </div>
                                <?php } ?>
                            <?php } ?>
                        </div>

                    <?php } ?>
                <?php } ?>
            <?php } ?>

            <?php if(is_array($content) && array_key_exists('info', $content) && is_array($content['info']) && array_key_exists(0, $content['info']) && is_array($content['info'][0]) && array_key_exists('details', $content['info'][0])){ ?>
            <div class="footnote">
                <?= $content['info'][0]['details']['textblock']; ?>
            </div>
            <?php } ?>

        </div>
    </section>

<?php } ?>

<!-- BOTTOM SPEC: ELEMENT -->
<?= $this->element('Frontend.Website/media', ['media' => $content['media'], 'position' => 'media-bottom', 'wrapper' => 'media-bottom']) ?>
