<?php use Cake\Core\Configure; ?>
<?php if($_SERVER['REMOTE_ADDR'] == '83.175.88.51'){ ?>
    <!-- ######   PACKAGE   ###### -->
    <!-- ###### [Website/Packages/index.ctp] ###### -->
<?php } ?>

<main class="fadeIn animated-fast center package">

    <section class="headline">
        <a class="anchor" name="content"></a>
        <div class="content<?php echo array_key_exists($content['id'], $prices['values']) && count($prices['values'][$content['id']]) > 0 ? '' : ' np'; ?>">
            <h1><?= $content['title']; ?></h1>
            <?php $len = count($content['valid_times']);  ?>
            <?php if($len > 0){ ?>
                <?php $glue = '&nbsp;&bull;&nbsp;' ?>
                <?php $i = 0; ?>
                <?php foreach($content['valid_times'] as $time){ ?>
                    <?php if ($i !== 0) {
                        echo $glue;
                    } else if($i == $len - 1 && $i !== 0) {
                        echo $glue;
                    } ?>
                    <span class="time"><?= $time['from'] . '&nbsp;&ndash;&nbsp;' . $time['to'] ?></span>
                    <?php $i++ ?>
                <?php } ?>
            <?php } ?>
        </div>
    </section>


    <div class="impressions fadeIn animated">
        <?php if(count($content['images']) > 1){ ?>
            <ul class="viewport bxslider">
                <?php foreach($content['images'] as $img){ ?>
                    <li>
                        <div class="bxslide">
                            <figure class="object-fit-image">
                              <img src="data:image/png;base64,R0lGODlhAQABAAD/ACwAAAAAAQABAAACADs=" data-src="<?= $img['details']['urls'][2] ?>" alt="<?= $img['details']['title'] ?>" style="object-position: <?= $img['details']['focus'][2]['css'] ?>;">
                            </figure>
                        </div>
                    </li>
                <?php } ?>
            </ul>
        <?php } else { ?>
            <figure class="object-fit-image">
              <img src="data:image/png;base64,R0lGODlhAQABAAD/ACwAAAAAAQABAAACADs=" data-src="<?= $content['images'][0]['details']['urls'][2] ?>" alt="<?= $content['images'][0]['details']['title'] ?>" style="object-position: <?= $content['images'][0]['details']['focus'][2]['css'] ?>;">
            </figure>
        <?php }?>
    </div>

    <section class="content fadeIn animated">
        <?php if(!empty($content['main_content'])){ ?>
            <div class="content">
                <?= $content['main_content'] ?>
            </div>
        <?php } ?>
        <?php if(!empty($content['services_text'])){ ?>
            <div class="services">
                <div class="inner">
                    <div class="design-element center">
                        <span class="design-lines left"></span>
                        <img src="data:image/png;base64,R0lGODlhAQABAAD/ACwAAAAAAQABAAACADs=" data-src="/frontend/img/uploaded/icons/3Punkte_weiss.svg" alt="design-element" class="icon invert">
                        <span class="design-lines right"></span>
                    </div>
                </div>
                <div class="content inner">
                    <?= $content['services_text']; ?>
                </div>
            </div>
        <?php } ?>
        <?php if(!empty($content['teaser'])){ ?>
            <div class="package-teaser uppercase strong">
                <?= $content['teaser'] ?>
            </div>

        <?php } ?>
    </section>

</main>

<!-- MIDDLE SPEC: ELEMENT -->
<?= $this->element('Frontend.Website/media', ['media' => $content['media'], 'position' => 'media-middle', 'wrapper' => 'media-middle']) ?>

<!-- PRICES -->
<?php if(array_key_exists($content['id'], $prices['values']) && count($prices['values'][$content['id']]) > 0){ ?>
    <a id="" class="package-prices-anchor" href="#"></a>
    <section class="package prices inner">

        <!-- desktop -->
        <table class="price-table drafts-<?= count($prices['connections'][$content['id']]['used']['drafts']); ?> desktop-prices">

            <!-- headline -->
            <tr>
                <th class="price-name"><?= __d('fe','Price per person and stay') ?></th>

                <?php foreach ($prices['connections'][$content['id']]['used']['drafts']['_global'] as $__k => $__draft_id) { ?>
                    <th class="price-type strong uppercase"><?= $prices['drafts'][$__draft_id]['translations']['title'] ?></th>
                <?php } ?>

                <th class="buttons"></th>
            </tr>

            <!-- border-line -->
            <tr class="border-line">
                <td></td>
                <?php foreach ($prices['connections'][$content['id']]['used']['drafts']['_global'] as $__k => $__draft_id) { ?>
                    <td></td>
                <?php } ?>
                <td class="buttons"></td>
            </tr>

            <!-- prices -->
            <?php $colspan = count($prices['drafts']) + 2; ?>

            <!-- manual room sorting -->
            <?php
                $_sorted_room_ids = [
                    '8207e5e5-9c42-450a-bcd6-48c57145b4cd',
                    '3a69b362-c1fd-4cde-a68d-8adbf12c17a3',
                    '1901e8ca-a36f-4ce6-b5de-bd68713bbb6f',
                    'b0b03428-2024-4352-ba42-52f62f5dcca7',
                    '4b78be1c-4abc-4240-a56f-1438367d02b8',
                    '8275f21c-d45c-4e33-bb95-d67f7d6b1f27',
                    'c48cb858-db4a-4da8-8468-2e291374abfe',
                    '5085a017-e019-44e9-9c5a-8e147066a4ad',
                    'd3f5af0c-4a69-47f9-a45a-864f3f066a85',
                ];
                $_sorted_rooms = array_merge(array_flip($_sorted_room_ids), $prices['elements']);
            ?>
            
            <?php foreach($_sorted_rooms as $id => $room){ ?>
                <?php if(in_array($id, $prices['connections'][$content['id']]['used']['elements']['_global'])){ ?>
                    <tr class="line" id="bttn-<?= $id ?>">

                        <!-- room -->
                        <td class="element">
                            <div class="title strong">
                                <a href="<?= $this->Url->build(['node' => 'node:' . $room['node'], 'language' => $this->request->params['language']]) ?>" target="_self" class="package-price-buttons" name="package-price-buttons" data-id="<?= $id ?>">
                                    <?= $room['title'] ?>
                                </a>
                            </div>

                            <div class="sub-title visible" id="line-<?= $id ?>">

                                <?= $_rooms[$id]['ic_line_1'] ?>
                                <?= $_rooms[$id]['ic_line_2'] ?>

                            </div>
                        </td>

                        <!-- prices -->
                        <?php if(array_key_exists($id, $prices['values'][$content['id']])){ ?>
                            <?php foreach($prices['connections'][$content['id']]['used']['drafts']['_global'] as $draft){ ?>
                                <?php if(in_array($draft, $prices['connections'][$content['id']]['used']['drafts']['_global'])){ ?>
                                    <td class="draft center">
                                        <?php if (array_key_exists($draft, $prices['values'][$content['id']][$id])) {?>
                                            <?php if (array_key_exists('Standard', $prices['values'][$content['id']][$id][$draft]['default'])) { ?>
                                                <?= array_key_exists($draft, $prices['values'][$content['id']][$id]) ? '<span>EUR&nbsp;</span>' . number_format($prices['values'][$content['id']][$id][$draft]['default']['Standard']['value'], 2, ',', '.') : '--'; ?>
                                            <?php } else { ?>
                                                <?= array_key_exists($draft, $prices['values'][$content['id']][$id]) ? '<span>EUR&nbsp;</span>' . number_format($prices['values'][$content['id']][$id][$draft]['default']['default']['value'], 2, ',', '.') : '--'; ?>
                                            <?php } ?>
                                        <?php } ?>
                                    </td>
                                <?php } ?>
                            <?php } ?>
                        <?php } ?>

                        <!-- buttons -->
                        <td class="buttons">
                            <a class="package-price-buttons" name="package-price-buttons" href="#" data-id="<?= $id ?>">
                                <i class="fas fa-chevron-down"></i>
                                <span class="strong uppercase">
                                    <?= __d('fe','Details') ?>
                                </span>
                            </a>
                        </td>

                    </tr>

                    <!-- ANIMATED - room and booking info -->
                    <tr class="animated-info closed" id="box-<?= $id ?>">
                        <td colspan="<?= $colspan ?>" class="animated-info-wrapper">
                            <div class="animationer"><div class="positioner">
                                <div class="room-info-wrapper">
                                    <div class="left">
                                        <a href="<?= $this->Url->build(['node' => 'node:' . $room['node'], 'language' => $this->request->params['language']]) ?>" target="_self">
                                            <img src="data:image/png;base64,R0lGODlhAQABAAD/ACwAAAAAAQABAAACADs=" data-src="<?= $_rooms[$id]['images'][0]['details']['urls']['2_plan'] ?>" alt="<?= $_rooms[$id]['images'][0]['details']['title'] ?>" style="object-position: <?= $_rooms[$id]['images'][0]['details']['focus']['2']['css'] ?>;">
                                        </a>
                                        <div class="lightbox-target" id="room-plan">
                                            <a class="lightbox-close" href="#">
                                               <img src="data:image/png;base64,R0lGODlhAQABAAD/ACwAAAAAAQABAAACADs=" data-src="<?= $_rooms[$id]['images'][0]['details']['urls']['2'] ?>" alt="<?= $_rooms[$id]['images'][0]['details']['title'] ?>" style="object-position: <?= $_rooms[$id]['images'][0]['details']['focus']['2']['css'] ?>;">
                                           </a>
                                        </div>
                                    </div>
                                    <div class="right">
                                        <div class="positioner">

                                            <!-- regular info lines -->
                                            <?php if (array_key_exists('ic_line_1', $_rooms[$id]) && isset($_rooms[$id]['ic_line_1']) && !empty($_rooms[$id]['ic_line_1'])) { ?>
                                                <span class="line-1">
                                                    <img src="data:image/png;base64,R0lGODlhAQABAAD/ACwAAAAAAQABAAACADs=" data-src="/frontend/img/uploaded/icons/Icons_Groesse.svg" alt="calculator" class="calculator-icon icon">
                                                    <span class="text">
                                                        <?= $_rooms[$id]['ic_line_1'] ?>
                                                    </span>
                                                </span>
                                            <?php } ?>
                                            <?php if (array_key_exists('ic_line_2', $_rooms[$id]) && isset($_rooms[$id]['ic_line_2']) && !empty($_rooms[$id]['ic_line_2'])) { ?>
                                                <span class="line-2">
                                                    <img src="data:image/png;base64,R0lGODlhAQABAAD/ACwAAAAAAQABAAACADs=" data-src="/frontend/img/uploaded/icons/Icons_Personen.svg" alt="person" class="person-icon icon">
                                                    <span class="text">
                                                        <?= $_rooms[$id]['ic_line_2'] ?>
                                                    </span>
                                                </span>
                                            <?php } ?>
                                            <?php if (array_key_exists('ic_line_3', $_rooms[$id]) && isset($_rooms[$id]['ic_line_3']) && !empty($_rooms[$id]['ic_line_3'])) { ?>
                                                <span class="line-3">
                                                    <?php if ($_rooms[$id]['ic_line_3'] == 'Stammhaus Maria Alm') { ?>
                                                        <img src="data:image/png;base64,R0lGODlhAQABAAD/ACwAAAAAAQABAAACADs=" data-src="/frontend/img/uploaded/icons/Icons_Stammhaus.svg" alt="house" class="house-icon icon">
                                                    <?php } else { ?>
                                                        <img src="data:image/png;base64,R0lGODlhAQABAAD/ACwAAAAAAQABAAACADs=" data-src="/frontend/img/uploaded/icons/Icons_Lodge.svg" alt="house" class="house-icon icon">
                                                    <?php }?>
                                                    <span class="text">
                                                        <?= $_rooms[$id]['ic_line_3'] ?>
                                                    </span>
                                                </span>
                                            <?php } ?>

                                            <!-- linked info lines -->
                                            <?php if (array_key_exists('ic_line_4', $_rooms[$id]) && isset($_rooms[$id]['ic_line_4']) && !empty($_rooms[$id]['ic_line_4'])) { ?>
                                                <span class="line-4">
                                                    <?php if (array_key_exists('ic_link_1', $_rooms[$id]) && isset($_rooms[$id]['ic_link_1']) && !empty($_rooms[$id]['ic_link_1'])) { ?>
                                                        <a href="<?= array_key_exists('link', $_rooms[$id]['ic_link_1'][0]['details']) ? $_rooms[$id]['ic_link_1'][0]['details']['link'] : $this->Url->build(['node' => $_rooms[$id]['ic_link_1'][0]['org'], 'language' => $this->request->params['language']]) ?>" target="<?= array_key_exists('target', $_rooms[$id]['ic_link_1'][0]['details']) ? $_rooms[$id]['ic_link_1'][0]['details']['target'] : '_self' ?>">
                                                        <?php $_if_link = '</a>'; ?>
                                                    <?php } ?>
                                                        <?php //n $_icon = isset($_rooms[$id]['link_icon_1']) && !empty($_rooms[$id]['link_icon_1']) ? $_rooms[$id]['link_icon_1'] : 'Inklusivleistungen_blau' ?>
                                                        <?php $_icon = 'Inklusivleistungen_blau' ?>
                                                        <img src="data:image/png;base64,R0lGODlhAQABAAD/ACwAAAAAAQABAAACADs=" data-src="/frontend/img/uploaded/icons/Icons_<?= $_icon ?>.svg" alt="<?= $_icon ?>" class="<?= $_icon ?>-icon icon">
                                                        <span class="text strong">
                                                            <?= $_rooms[$id]['ic_line_4'] ?>
                                                        </span>
                                                    <?= $_if_link ?>
                                                </span>
                                            <?php } ?>
                                            <!--  <?php if (array_key_exists('ic_line_5', $_rooms[$id]) && isset($_rooms[$id]['ic_line_5']) && !empty($_rooms[$id]['ic_line_5'])) { ?>
                                                <span class="line-5">

                                                    <?php if (array_key_exists('ic_link_2', $_rooms[$id]) && isset($_rooms[$id]['ic_link_2']) && !empty($_rooms[$id]['ic_link_2'])) { ?>
                                                        <a href="<?= array_key_exists('link', $_rooms[$id]['ic_link_2'][0]['details']) ? $_rooms[$id]['ic_link_2'][0]['details']['link'] : $this->Url->build(['node' => $_rooms[$id]['ic_link_2'][0]['org'], 'language' => $this->request->params['language']]) ?>" target="<?= array_key_exists('target', $_rooms[$id]['ic_link_2'][0]['details']) ? $_rooms[$id]['ic_link_2'][0]['details']['target'] : '_self' ?>">
                                                        <?php $_if_link = '</a>'; ?>
                                                    <?php } ?>
                                                        <?php // $_icon = isset($_rooms[$id]['link_icon_2']) && !empty($_rooms[$id]['link_icon_2']) ? $_rooms[$id]['link_icon_2'] : 'Schlafmenue_blau' ?>
                                                        <?php $_icon = 'Schlafmenue_blau' ?>
                                                        <img src="data:image/png;base64,R0lGODlhAQABAAD/ACwAAAAAAQABAAACADs=" data-src="/frontend/img/uploaded/icons/Icons_<?= $_icon ?>.svg" alt="<?= $_icon ?>" class="<?= $_icon ?>-icon icon">
                                                        <span class="text strong">
                                                            <?= $_rooms[$id]['ic_line_5'] ?>
                                                        </span>
                                                        <?= $_if_link ?>
                                                </span>
                                            <?php } ?>-->

                                        </div>
                                    </div>
                                </div>
                                <div class="book-iframe-wrapper center">
                                    <!-- <div class="skd-widget" data-skd-widget="check-availability" data-skd-send-to-groups="A" data-skd-listen-to-groups="B">
                                        <div style="width:100%; min-height:50px; text-align:center;">
                                            <img src="data:image/png;base64,R0lGODlhAQABAAD/ACwAAAAAAQABAAACADs=" data-src="https://static.seekda.com/assets/images/skd_spinner.gif" />
                                            <noscript>Your browser doesn't support JavaScript or it has been disabled. To use the booking engine, please make sure to get JavaScript running.</noscript>
                                        </div>
                                    </div> -->
                                    <!-- <div class="skd-widget" data-skd-widget="offer-list" data-skd-send-to-groups="B" data-skd-listen-to-groups="A">
                                        <div style="width:100%; min-height:100px; text-align:center;">
                                            <img src="data:image/png;base64,R0lGODlhAQABAAD/ACwAAAAAAQABAAACADs=" data-src="https://static.seekda.com/assets/images/skd_spinner.gif" />
                                            <noscript>Your browser doesn't support JavaScript or it has been disabled. To use the booking engine, please make sure to get JavaScript running.</noscript>
                                        </div>
                                    </div> -->

                                    <?php
                                        $query_string = '';
                                        if (isset($content['id']) && !empty($content['id'])) {
                                        $query_string = '?package=' . $content['id'];
                                        $query_string .= '&room=' . $id;
                                        }
                                    ?>
                                    <a href="<?= $this->Url->build([
                                    'node' => 'node:' . Configure::read('config.fixnav.first-right.0.id'),
                                    'language' => $this->request->params['language']
                                    ]). $query_string ?>" target="_self" class="request-button package-prices right col-light strong">
                                        <img src="data:image/png;base64,R0lGODlhAQABAAD/ACwAAAAAAQABAAACADs=" data-src="/frontend/img/uploaded/icons/Icons_Anfragen.svg" alt="calendar" class="cal-icon icon">
                                        <span class="uppercase">
                                            <?= Configure::read(
                                            'config.fixnav.first-right.0.details.element.title'
                                            ) ?>
                                        </span>
                                    </a>

                                    <?php
                                        $query_string = '';
                                        if (isset($content['hs_code']) && !empty($content['hs_code'])) {
                                            $query_string = '#!/skd-ds/skd-package/' . $content['hs_code'];
                                            $query_string .= '/skd-room/' . $id;
                                        }
                                    ?>
                                    <a href="<?= $this->Url->build([
                                    'node' => 'node:' . Configure::read('config.fixnav.second-right.0.id'),
                                    'language' => $this->request->params['language']
                                    ]) . $query_string ?>" target="_self" class="book-button package-prices right red strong last">
                                        <img src="data:image/png;base64,R0lGODlhAQABAAD/ACwAAAAAAQABAAACADs=" data-src="/frontend/img/uploaded/icons/Icons_Buchen.svg" alt="booking" class="book-icon icon">
                                        <span class="uppercase">
                                            <?= Configure::read(
                                            'config.fixnav.second-right.0.details.element.title'
                                            ) ?>
                                        </span>
                                    </a>

                                </div>
                            </div></div>
                        </td>
                    </tr>

                    <!-- border-line -->
                    <tr class="border-line">
                        <td></td>
                        <?php if(array_key_exists($id, $prices['values'][$content['id']])){ ?>
                            <?php foreach($prices['drafts'] as $draft){ ?>
                                <?php if(in_array($draft['id'], $prices['connections'][$content['id']]['used']['drafts']['_global'])){ ?>
                                    <td></td>
                                <?php } ?>
                            <?php } ?>
                        <?php } ?>
                        <td></td>
                    </tr>

                <?php } ?>
            <?php } ?>

        </table>

        <!-- mobile -->
        <div class="price-table drafts-<?= count($prices['connections'][$content['id']]['used']['drafts']); ?> mobile-prices">

            <!-- headline -->
            <div class="title-line">
                <?= __d('fe','Price per person and stay') ?>
            </div>

            <!-- prices -->
            <?php $colspan = count($prices['drafts']) + 2; ?>
            <?php foreach($prices['elements'] as $id => $room){ ?>
                <?php if(in_array($id, $prices['connections'][$content['id']]['used']['elements']['_global'])){ ?>
                    <div class="line" id="bttn-<?= $id ?>">

                        <!-- room -->
                        <div class="room-title-line">
                            <div class="title strong">
                                <a href="#" target="_self" class="package-price-buttons" name="package-price-buttons" data-id="<?= $id ?>">
                                    <?= $room['title'] ?>
                                </a>
                            </div>
                            <div class="sub-title visible" id="line-<?= $id ?>">
                                <?= $_rooms[$id]['ic_line_1'] ?>
                                <?= $_rooms[$id]['ic_line_2'] ?>
                             
                            </div>
                        </div>

                        <!-- prices -->
                        <div class="price-line">
                            <?php foreach ($prices['connections'][$content['id']]['used']['drafts']['_global'] as $__k => $__draft_id) { ?>
                                <div class="option">

                                    <!-- price type -->
                                    <div class="price-title strong uppercase">
                                        <?= $prices['drafts'][$__draft_id]['translations']['title'] ?>
                                    </div>

                                    <!-- prices -->
                                    <div class="price">
                                        <?php if(array_key_exists($id, $prices['values'][$content['id']])){ ?>
                                            <?php if(in_array($draft['id'], $prices['connections'][$content['id']]['used']['drafts']['_global'])){ ?>
                                                <div class="draft center">
                                                    <?php if(array_key_exists('Standard', $prices['values'][$content['id']][$id][$__draft_id]['default'])) { ?>
                                                        <?= array_key_exists($__draft_id, $prices['values'][$content['id']][$id]) ? '<span>EUR&nbsp;</span>' . number_format($prices['values'][$content['id']][$id][$__draft_id]['default']['Standard']['value'], 2, ',', '.') : '--'; ?>
                                                    <?php } else { ?>
                                                        <?= array_key_exists($__draft_id, $prices['values'][$content['id']][$id]) ? '<span>EUR&nbsp;</span>' . number_format($prices['values'][$content['id']][$id][$__draft_id]['default']['default']['value'], 2, ',', '.') : '--'; ?>
                                                    <?php } ?>
                                                </div>
                                            <?php } ?>
                                        <?php } ?>
                                    </div>

                                </div>
                            <?php } ?>
                            <div class="option buttons-line">

                                <!-- placeholder -->
                                <div class="price-title strong uppercase placeholder"></div>

                                <!-- buttons -->
                                <div class="buttons">
                                    <a class="package-price-buttons" name="package-price-buttons" href="#" data-id="mobile-<?= $id ?>">
                                        <i class="fas fa-chevron-down"></i>
                                        <span class="strong uppercase">
                                            <?= __d('fe','Details') ?>
                                        </span>
                                    </a>
                                </div>

                            </div>
                        </div>

                        <!-- ANIMATED - room and booking info -->
                        <div class="animated-info closed" id="box-mobile-<?= $id ?>">
                            <div class="animated-info-wrapper">
                                <div class="animationer"><div class="positioner">
                                    <div class="room-info-wrapper">
                                        <div class="left">
                                            <img src="data:image/png;base64,R0lGODlhAQABAAD/ACwAAAAAAQABAAACADs=" data-src="<?= $_rooms[$id]['images'][0]['details']['urls']['2_plan'] ?>" alt="<?= $_rooms[$id]['images'][0]['details']['title'] ?>" style="object-position: <?= $_rooms[$id]['images'][0]['details']['focus']['2']['css'] ?>;">
                                            <div class="lightbox-target" id="room-plan">
                                                <a class="lightbox-close" href="#">
                                                   <img src="data:image/png;base64,R0lGODlhAQABAAD/ACwAAAAAAQABAAACADs=" data-src="<?= $_rooms[$id]['images'][0]['details']['urls']['2'] ?>" alt="<?= $_rooms[$id]['images'][0]['details']['title'] ?>" style="object-position: <?= $_rooms[$id]['images'][0]['details']['focus']['2']['css'] ?>;">
                                               </a>
                                            </div>
                                        </div>
                                        <div class="right">
                                            <div class="positioner">

                                                <!-- regular info lines -->
                                                <?php if (array_key_exists('ic_line_1', $_rooms[$id]) && isset($_rooms[$id]['ic_line_1']) && !empty($_rooms[$id]['ic_line_1'])) { ?>
                                                    <span class="line-1">
                                                        <img src="data:image/png;base64,R0lGODlhAQABAAD/ACwAAAAAAQABAAACADs=" data-src="/frontend/img/uploaded/icons/Icons_Groesse.svg" alt="calculator" class="calculator-icon icon">
                                                        <span class="text">
                                                            <?= $_rooms[$id]['ic_line_1'] ?>
                                                        </span>
                                                    </span>
                                                <?php } ?>
                                                <?php if (array_key_exists('ic_line_2', $_rooms[$id]) && isset($_rooms[$id]['ic_line_2']) && !empty($_rooms[$id]['ic_line_2'])) { ?>
                                                    <span class="line-2">
                                                        <img src="data:image/png;base64,R0lGODlhAQABAAD/ACwAAAAAAQABAAACADs=" data-src="/frontend/img/uploaded/icons/Icons_Personen.svg" alt="person" class="person-icon icon">
                                                        <span class="text">
                                                            <?= $_rooms[$id]['ic_line_2'] ?>
                                                        </span>
                                                    </span>
                                                <?php } ?>
                                                <?php if (array_key_exists('ic_line_3', $_rooms[$id]) && isset($_rooms[$id]['ic_line_3']) && !empty($_rooms[$id]['ic_line_3'])) { ?>
                                                    <span class="line-3">
                                                        <?php if ($_rooms[$id]['ic_line_3'] == 'Stammhaus Maria Alm') { ?>
                                                            <img src="data:image/png;base64,R0lGODlhAQABAAD/ACwAAAAAAQABAAACADs=" data-src="/frontend/img/uploaded/icons/Icons_Stammhaus.svg" alt="house" class="house-icon icon">
                                                        <?php } else { ?>
                                                            <img src="data:image/png;base64,R0lGODlhAQABAAD/ACwAAAAAAQABAAACADs=" data-src="/frontend/img/uploaded/icons/Icons_Lodge.svg" alt="house" class="house-icon icon">
                                                        <?php }?>
                                                        <span class="text">
                                                            <?= $_rooms[$id]['ic_line_3'] ?>
                                                        </span>
                                                    </span>
                                                <?php } ?>

                                                <!-- linked info lines -->
                                                <?php if (array_key_exists('ic_line_4', $_rooms[$id]) && isset($_rooms[$id]['ic_line_4']) && !empty($_rooms[$id]['ic_line_4'])) { ?>
                                                    <span class="line-4">
                                                        <?php if (array_key_exists('ic_link_1', $_rooms[$id]) && isset($_rooms[$id]['ic_link_1']) && !empty($_rooms[$id]['ic_link_1'])) { ?>
                                                            <a href="<?= array_key_exists('link', $_rooms[$id]['ic_link_1'][0]['details']) ? $_rooms[$id]['ic_link_1'][0]['details']['link'] : $this->Url->build(['node' => $_rooms[$id]['ic_link_1'][0]['org'], 'language' => $this->request->params['language']]) ?>" target="<?= array_key_exists('target', $_rooms[$id]['ic_link_1'][0]['details']) ? $_rooms[$id]['ic_link_1'][0]['details']['target'] : '_self' ?>">
                                                            <?php $_if_link = '</a>'; ?>
                                                        <?php } ?>
                                                            <?php // $_icon = isset($_rooms[$id]['link_icon_1']) && !empty($_rooms[$id]['link_icon_1']) ? $_rooms[$id]['link_icon_1'] : 'Inklusivleistungen_blau' ?>
                                                            <?php $_icon = 'Inklusivleistungen_blau' ?>
                                                            <img src="data:image/png;base64,R0lGODlhAQABAAD/ACwAAAAAAQABAAACADs=" data-src="/frontend/img/uploaded/icons/Icons_<?= $_icon ?>.svg" alt="<?= $_icon ?>" class="<?= $_icon ?>-icon icon">
                                                            <span class="text strong">
                                                                <?= $_rooms[$id]['ic_line_4'] ?>
                                                            </span>
                                                        <?= $_if_link ?>
                                                    </span>
                                                <?php } ?>
                                                <?php if (array_key_exists('ic_line_5', $_rooms[$id]) && isset($_rooms[$id]['ic_line_5']) && !empty($_rooms[$id]['ic_line_5'])) { ?>
                                                    <span class="line-5">
                                                        <?php if (array_key_exists('ic_link_2', $_rooms[$id]) && isset($_rooms[$id]['ic_link_2']) && !empty($_rooms[$id]['ic_link_2'])) { ?>
                                                            <a href="<?= array_key_exists('link', $_rooms[$id]['ic_link_2'][0]['details']) ? $_rooms[$id]['ic_link_2'][0]['details']['link'] : $this->Url->build(['node' => $_rooms[$id]['ic_link_2'][0]['org'], 'language' => $this->request->params['language']]) ?>" target="<?= array_key_exists('target', $_rooms[$id]['ic_link_2'][0]['details']) ? $_rooms[$id]['ic_link_2'][0]['details']['target'] : '_self' ?>">
                                                            <?php $_if_link = '</a>'; ?>
                                                        <?php } ?>
                                                            <?php // $_icon = isset($_rooms[$id]['link_icon_2']) && !empty($_rooms[$id]['link_icon_2']) ? $_rooms[$id]['link_icon_2'] : 'Schlafmenue_blau' ?>
                                                            <?php $_icon = 'Schlafmenue_blau' ?>
                                                            <img src="data:image/png;base64,R0lGODlhAQABAAD/ACwAAAAAAQABAAACADs=" data-src="/frontend/img/uploaded/icons/Icons_<?= $_icon ?>.svg" alt="<?= $_icon ?>" class="<?= $_icon ?>-icon icon">
                                                            <span class="text strong">
                                                                <?= $_rooms[$id]['ic_line_5'] ?>
                                                            </span>
                                                            <?= $_if_link ?>
                                                    </span>
                                                <?php } ?>

                                            </div>
                                        </div>
                                    </div>
                                    <div class="book-iframe-wrapper center">
                                        <!-- <div class="skd-widget" data-skd-widget="check-availability" data-skd-send-to-groups="A" data-skd-listen-to-groups="B">
                                            <div style="width:100%; min-height:50px; text-align:center;">
                                                <img src="https://static.seekda.com/assets/images/skd_spinner.gif" />
                                                <noscript>Your browser doesn't support JavaScript or it has been disabled. To use the booking engine, please make sure to get JavaScript running.</noscript>
                                            </div>
                                        </div> -->
                                        <!-- <div class="skd-widget" data-skd-widget="offer-list" data-skd-send-to-groups="B" data-skd-listen-to-groups="A">
                                            <div style="width:100%; min-height:100px; text-align:center;">
                                                <img src="https://static.seekda.com/assets/images/skd_spinner.gif" />
                                                <noscript>Your browser doesn't support JavaScript or it has been disabled. To use the booking engine, please make sure to get JavaScript running.</noscript>
                                            </div>
                                        </div> -->

                                        <?php
                                            $query_string = '';
                                            if (isset($content['id']) && !empty($content['id'])) {
                                            $query_string = '?package=' . $content['id'];
                                            $query_string .= '&room=' . $id;
                                            }
                                        ?>
                                        <a href="<?= $this->Url->build([
                                        'node' => 'node:' . Configure::read('config.fixnav.first-right.0.id'),
                                        'language' => $this->request->params['language']
                                        ]). $query_string ?>" target="_self" class="request-button package-prices right col-light strong">
                                            <img src="data:image/png;base64,R0lGODlhAQABAAD/ACwAAAAAAQABAAACADs=" data-src="/frontend/img/uploaded/icons/Icons_Anfragen.svg" alt="calendar" class="cal-icon icon">
                                            <span class="uppercase">
                                                <?= Configure::read(
                                                'config.fixnav.first-right.0.details.element.title'
                                                ) ?>
                                            </span>
                                        </a>

                                        <?php
                                            $query_string = '';
                                            if (isset($content['hs_code']) && !empty($content['hs_code'])) {
                                                $query_string = '#!/skd-ds/skd-package/' . $content['hs_code'];
                                                $query_string .= '/skd-room/' . $id;
                                            }
                                        ?>
                                        <a href="<?= $this->Url->build([
                                        'node' => 'node:' . Configure::read('config.fixnav.second-right.0.id'),
                                        'language' => $this->request->params['language']
                                        ]) . $query_string ?>" target="_self" class="book-button package-prices right red strong last">
                                            <img src="data:image/png;base64,R0lGODlhAQABAAD/ACwAAAAAAQABAAACADs=" data-src="/frontend/img/uploaded/icons/Icons_Buchen.svg" alt="booking" class="book-icon icon">
                                            <span class="uppercase">
                                                <?= Configure::read(
                                                'config.fixnav.second-right.0.details.element.title'
                                                ) ?>
                                            </span>
                                        </a>

                                    </div>
                                </div></div>
                            </div>
                        </div>
                    </div>
                <?php } ?>
            <?php } ?>

        </div>

    </section>
<?php } ?>

<?= $this->element('Frontend.Website/media', ['media' => $content['media'], 'position' => 'media-bottom', 'wrapper' => 'media-bottom']) ?>
