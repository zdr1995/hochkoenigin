<?php use Cake\Core\Configure; ?>

<?php if($_SERVER['REMOTE_ADDR'] == '83.175.88.51'){ ?>
    <!-- ######   PAGE   ###### -->
    <!-- ###### [Website/Page/index.ctp] ###### -->
<?php } ?>

<?php if (!array_key_exists('select_layout', $content) && empty($content['select_layout'])) {
    $content['select_layout'] = 'default';
} ?>

<?php $_content = empty($content['content']) ? 'no-content' : ''; ?>

<?= $this->element('Frontend.Website/media', ['media' => $content['media'], 'position' => 'media-top', 'wrapper' => 'media-top']) ?>

<main class="fadeIn animated-fast page <?= $content['internal'] ?>">
    <div class="content-wrapper <?= $_content ?>">
        <?php switch ($content['select_layout']) {
            case 'custom':
                ?>
                <div class="blank">
                    <h1 class=""><?= $content['headline']; ?></h1>
                    <?php if (array_key_exists('sub_headline', $content) && !empty($content['sub_headline'])) { ?>
                        <h2 class="uppercase strong"><?= $content['sub_headline']; ?></h2>
                    <?php } ?>
                    <span class="blank">
                        <?= $content['content']; ?>
                    </span>
                </div>
                <?php
                break;
            default:
                ?>
                <div class="left">
                    <h1 class=""><?= $content['headline']; ?></h1>
                    <?php if (array_key_exists('sub_headline', $content) && !empty($content['sub_headline'])) { ?>
                        <h2 class="uppercase strong"><?= $content['sub_headline']; ?></h2>
                    <?php } ?>
                </div>
                <div class="right">
                    <?= $content['content']; ?>
                </div>
                <?php
                break;
        } ?>
    </div>
</main>

<!-- media seasons -->
<?php $media_bottom = Configure::read('config.default.season') == 'wi' && strpos($this->element('Frontend.Website/media', ['media' => $content['media'], 'position' => 'media-bottom-wi', 'wrapper' => 'media-bottom-wi']), 'section') ? 'media-bottom-wi' : 'media-bottom'; ?>
<?= $this->element('Frontend.Website/media', ['media' => $content['media'], 'position' => $media_bottom, 'wrapper' => $media_bottom]) ?>
