<?php
if ($_SERVER['REMOTE_ADDR'] == '178.17.22.245') {
    var_dump('it works');die();
}
?>


<script src="/frontend/js/jQuery/jquery.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/jquery-validate/1.19.2/jquery.validate.min.js"></script>



<?php if($_SERVER['REMOTE_ADDR'] == '83.175.88.51'){ ?>
    <!-- ######   LASTMINUTE   ###### -->
    <!-- ###### [Website/Lastminute/index.ctp] ###### -->
<?php } ?>


<?php if (!array_key_exists('select_layout', $content) && empty($content['select_layout'])) {
    $content['select_layout'] = 'default';
} ?>

<?php $_content = empty($content['content']) ? 'no-content' : ''; ?>

<?= $this->element('Frontend.Website/media', ['media' => $content['media'], 'position' => 'media-top', 'wrapper' => 'media-top']) ?>


<link rel="stylesheet" href="/frontend/css/lastminute.css">
<main class="fadeIn animated-fast page inner">
    <a class="anchor" name="content"></a>
    <div class="content-wrapper <?= $_content ?>">
        <?php switch ($content['select_layout']) {
            case 'custom':
                ?>
                <div class="blank">
                    <h1 class=""><?= $content['headline']; ?></h1>
                    <?php if (array_key_exists('sub_headline', $content) && !empty($content['sub_headline'])) { ?>
                        <h2 class="uppercase strong"><?= $content['sub_headline']; ?></h2>
                    <?php } ?>
                    <span class="blank">
                        <?= $content['content']; ?>
                    </span>
                </div>
                <?php
                break;
            default:
                ?>
                <div class="left">
                    <h1 class=""><?= $content['headline']; ?></h1>
                    <?php if (array_key_exists('sub_headline', $content) && !empty($content['sub_headline'])) { ?>
                        <h2 class="uppercase strong"><?= $content['sub_headline']; ?></h2>
                    <?php } ?>
                </div>
                <div class="right">
                    <?= $content['content']; ?>
                </div>
                <?php
                break;
        } ?>
    </div>
</main>

<?= $this->element('Frontend.Website/media', ['media' => $content['media'], 'position' => 'media-middle', 'wrapper' => 'media-middle']) ?>

<div class="form-wrapper">
    <div class="inner">
        <?php if($messages['show']){ ?>
            <?= $this->element('Frontend.Website/form-message', ['messages' => $messages]) ?>
        <?php }else{ ?>
            <?php $nr = 0; ?>
            <?php if(count($last_minute_offers) > 0){ ?>



            <?php
                $open = array_key_exists('id', $this->request->data) && strlen($this->request->data['id']) == 36 ? $this->request->data['id'] : false;
            ?>

            <?php $_form = false; $periods = []; ?>

                <?php foreach($last_minute_offers as $k => $last_minute_offer){ ?>
                    <section class="teaser-5" data-rel="<?= $last_minute_offer['id'];?>">
                        <div class="preview" style="display: flex;flex-direction: row;">
                            <div class="radio-img" style="width: 60%;">
                                <?php
                                    $img_url = array_key_exists('c_image',$last_minute_offer) ? $last_minute_offer['c_image'] : null;

                                    if($img_url != null)
                                        $img_url = isset($img_url->urls) ? $img_url->urls : null;

                                    if(!is_null($img_url))
                                        $img_url = array_key_exists('original', $img_url) ? $img_url['original'] : null;

                                ?>
                                <div style="background-size:cover;background-repeat:no-repeat;background-position:50% 50%;width:100%;height:100%;background-image:url('<?php echo $img_url;?>')"></div>
                            </div>
                            <div class="text-wrapper">

                                <!--            TITLE       -->
                                <h2><?php echo $last_minute_offer['room'][0]['internal'];?></h2>

                                <!--     RANGES     -->
                                <?php if(array_key_exists('ranges',$last_minute_offer)){ ?>

                                    <div class="ranges">
                                        <?php $room_details = $last_minute_offer['room']; ?>
                                        <?php foreach($last_minute_offer['ranges'] as $range){ ?>
                                            <div class="range"><?= date("d.m.Y", $range['from']) . " - " . date("d.m.Y", $range['to']); ?></div>
                                        <?php } ?>
                                    </div>

                                <?php } ?>

                                <div class="desc">
                                    <?= $last_minute_offer['price_desc']; ?>
                                </div>

                                <div class="value">
                                    <?= __d('fe', 'now') . '&nbsp;&euro;&nbsp;' . @number_format($last_minute_offer['price_value'],2,",","."); ?>
                                </div>

                                <div class="buttons">
                                    <?php if(array_key_exists(0,$last_minute_offer['room'])
                                        && array_key_exists('node', $last_minute_offer['room'][0])
                                        && array_key_exists(0, $last_minute_offer['room'][0]['node'])
                                    ) { ?>
                                        <?php
                                            $room_url = $this->Url->build([
                                                    'node' => 'node:' . $last_minute_offer['room'][0]['node'][0]['id'],
                                                    'language' => $this->request->params['language']
                                            ])
                                        ?>
                                        <a class="button s2u room" href="<?php echo $room_url; ?>" >
                                            <?= __d('fe', 'Room') ?>
                                        </a>
                                    <?php } else { ?>
                                        <a class="button s2u room" href="#perica"><?= __d('fe', 'Room'); ?></a>
                                    <?php } ?>

                                    <a class="button s2u dark book" onclick="openModal(this)"><?= __d('fe', 'Book'); ?></a>
                                </div>
                            </div>
                        </div>

                        <?php if($_SERVER['REMOTE_ADDR'] == '') { ?>
                            <!--            MORE INFO               -->
                        <?php } ?>
                        <div class="more <?php echo $open == $last_minute_offer['id'] ? '' : ' hidden'; ?>">

                            <?php if(array_key_exists('content', $last_minute_offer)) {?>
                                <?= $last_minute_offer['content']; ?>
                            <?php } ?>

                            <!--// Form Start //-->
                            <?= $this->CustomForm->create($form); ?>

                            <div class="form">
                                <?php if(($open === false || $open == $last_minute_offer['id']) && $_form === false){ ?>

                                    <div id="lmf" class="form-wrapper">
                                        <input id="lmo" type="hidden" name="id" value="<?php echo $last_minute_offer['id']; ?>" />
                                        <input id="lmon" type="hidden" name="offer" value="" />
                                        <input id="lmor" type="hidden" name="room" value="<?php echo $last_minute_offer['room'][0]['id']; ?>" />
                                        <div class="col left">
                                            <?= $this->Form->input('salutation', ['label' => __d('fe', 'Salutation'), 'empty' => __d('fe', '-- Please select --')]); ?>
                                            <?= $this->Form->input('title', ['label' => __d('fe', 'Title')]); ?>
                                            <?= $this->Form->input('firstname', ['label' => __d('fe', 'Firstname')]); ?>
                                            <?= $this->Form->input('lastname', ['label' => __d('fe', 'Lastname')]); ?>
                                            <?= $this->Form->input('email', ['label' => __d('fe', 'E-Mail')]); ?>
                                        </div>
                                        <div class="col right">
                                            <?= $this->Form->input('address', ['label' => __d('fe', 'Address')]); ?>
                                            <?= $this->Form->input('zip', ['label' => __d('fe', 'ZIP')]); ?>
                                            <?= $this->Form->input('city', ['label' => __d('fe', 'City')]); ?>
                                            <?= $this->Form->input('country', ['label' => __d('fe', 'Country'), 'empty' => __d('fe', '-- Please select --')]); ?>
                                            <?= $this->Form->input('phone', ['label' => __d('fe', 'Phone')]); ?>
                                        </div>
                                        <div class="form-bottom">
                                            <div class="clear"></div>
                                            <?= $this->Form->input('message', ['label' => __d('fe', 'Message')]); ?>
                                            <?= $this->element('Frontend.Website/newsletter') ?>
                                            <?= $this->element('Frontend.Website/lastminute_captcha')?>
                                            <div class="clear"></div>
                                        </div>
                                    </div>
                                <?php } ?>
                            </div>

                            <?= $this->CustomForm->end(); ?>
                            <!--// Form End //-->
                        </div>

                    </section>

                    <?php $nr++; ?>
                <?php } ?>




            <?php } ?>
            <?php if($nr < 1){ ?>
                <div class="message space-top"><?= __d('fe', 'No offers available'); ?></div>
            <?php } ?>
        <?php } ?>

        <script>
            let checker=false;
            function openModal(el,event)
            {
                if(!checker) {
                    $(el).parents('.preview').siblings('.more').removeClass('hidden');
                    checker=true;
                }else {
                    checker=false;
                    $(el).parents('.preview').siblings('.more').addClass('hidden');
                }
            }

            window.addEventListener('DOMContentLoaded', function(){
                console.log('Loaded');
                let forms = $(document.forms);
                for(i=0;i<forms.length;i++)
                    validateForm(forms[i]);
            })
        </script>


<?= $this->element('Frontend.Website/media', ['media' => $content['media'], 'position' => 'media-bottom', 'wrapper' => 'media-bottom']) ?>
    </div>
</div>