<?php if($_SERVER['REMOTE_ADDR'] == '83.175.88.51'){ ?>
    <!-- ######   NEWSLETTER (unsubscribe)   ###### -->
    <!-- ###### [Website/Newsletter/unsubscribe.ctp] ###### -->
<?php } ?>

<?php if (!array_key_exists('select_layout', $content) && empty($content['select_layout'])) {
    $content['select_layout'] = 'default';
} ?>

<?php $_content = empty($content['content']) ? 'no-content' : ''; ?>

<?= $this->element('Frontend.Website/media', ['media' => $content['media'], 'position' => 'media-top', 'wrapper' => 'media-top']) ?>

<main class="fadeIn animated-fast page inner">
    <a class="anchor" name="content"></a>
    <div class="content-wrapper <?= $_content ?>">
        <?php switch ($content['select_layout']) {
            case 'custom':
                ?>
                <div class="blank">
                    <h1 class=""><?= $content['headline']; ?></h1>
                    <?php if (array_key_exists('sub_headline', $content) && !empty($content['sub_headline'])) { ?>
                        <h2 class="uppercase strong"><?= $content['sub_headline']; ?></h2>
                    <?php } ?>
                    <span class="blank">
            	        <?php if($messages['success'] === true){ ?>
            	            <div class="message success"><?= $messages['status']; ?></div>
            	        <?php }else if($messages['success'] === false){ ?>
            	            <div class="message error"><?= $messages['status']; ?></div>
            	        <?php } ?>
            	        <?= $content['content']; ?>
                    </span>
                </div>
                <?php
                break;
            default:
                ?>
                <div class="left">
                    <h1 class=""><?= $content['headline']; ?></h1>
                    <?php if (array_key_exists('sub_headline', $content) && !empty($content['sub_headline'])) { ?>
                        <h2 class="uppercase strong"><?= $content['sub_headline']; ?></h2>
                    <?php } ?>
                </div>
                <div class="right">
        	        <?php if($messages['success'] === true){ ?>
        	            <div class="message success"><?= $messages['status']; ?></div>
        	        <?php }else if($messages['success'] === false){ ?>
        	            <div class="message error"><?= $messages['status']; ?></div>
        	        <?php } ?>
        	        <?= $content['content']; ?>
                </div>
                <?php
                break;
        } ?>
    </div>
</main>

<?= $this->element('Frontend.Website/media', ['media' => $content['media'], 'position' => 'media-middle', 'wrapper' => 'media-middle']) ?>

<div class="form-wrapper">
	<div class="inner">
		<?php if($messages['show']){ ?>
	        <?= $this->element('Frontend.Website/form-message', ['messages' => $messages]) ?>
	    <?php }else{ ?>
	        <!--// Form Start //-->
	        <?= $this->CustomForm->create($form); ?>
            <?= $this->CustomForm->input('email', ['label' => __d('fe', 'E-Mail')]); ?>
            <?= $this->element('Frontend.Website/required') ?>
	        <?= $this->element('Frontend.Website/captcha', ['text' => __d('fe', 'Unsubscribe'), 'options' => [['text' => __d('fe', 'Subscribe'), 'class' => 'option', 'type' => 'unsubscribe', 'url' => $this->Url->build(['node' => 'node:' . $this->request->params['node']['id'], 'language' => $this->request->params['language']])]]]) ?>
	        <?= $this->CustomForm->end(); ?>
	        <!--// Form End //-->
		<?php } ?>
	</div>
</div>

<?= $this->element('Frontend.Website/media', ['media' => $content['media'], 'position' => 'media-bottom', 'wrapper' => 'media-bottom']) ?>
