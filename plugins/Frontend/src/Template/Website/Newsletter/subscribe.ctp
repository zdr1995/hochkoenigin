<?php if($_SERVER['REMOTE_ADDR'] == '83.175.88.51'){ ?>
    <!-- ######   NEWSLETTER (subscribe)   ###### -->
    <!-- ###### [Website/Newsletter/subscribe.ctp] ###### -->
<?php } ?>

<?php
    $firstname = '';
    if(isset($_POST['firstname']) && !empty($_POST['firstname'])){
        $firstname = $_POST['firstname'];
    } else if(isset($_GET['firstname']) && !empty($_GET['firstname'])){
        $firstname = $_GET['firstname'];
    }
    $lastname = '';
    if(isset($_POST['lastname']) && !empty($_POST['lastname'])){
        $lastname = $_POST['lastname'];
    } else if(isset($_GET['lastname']) && !empty($_GET['lastname'])){
        $lastname = $_GET['lastname'];
    }
    $email = '';
    if(isset($_POST['email']) && !empty($_POST['email'])){
        $email = $_POST['email'];
    } else if(isset($_GET['email']) && !empty($_GET['email'])){
        $email = $_GET['email'];
    }
?>

<?php if (!array_key_exists('select_layout', $content) && empty($content['select_layout'])) {
    $content['select_layout'] = 'default';
} ?>

<?php $_content = empty($content['content']) ? 'no-content' : ''; ?>

<?= $this->element('Frontend.Website/media', ['media' => $content['media'], 'position' => 'media-top', 'wrapper' => 'media-top']) ?>

<main class="fadeIn animated-fast page inner">
    <a class="anchor" name="content"></a>
    <div class="content-wrapper <?= $_content ?>">
        <?php switch ($content['select_layout']) {
            case 'custom':
                ?>
                <div class="blank">
                    <h1 class=""><?= $content['headline']; ?></h1>
                    <?php if (array_key_exists('sub_headline', $content) && !empty($content['sub_headline'])) { ?>
                        <h2 class="uppercase strong"><?= $content['sub_headline']; ?></h2>
                    <?php } ?>
                    <span class="blank">
                        <?php if($messages['success'] === true){ ?>
            	            <div class="message success"><?= $messages['status']; ?></div>
            	        <?php }else if($messages['success'] === false){ ?>
            	            <div class="message error"><?= $messages['status']; ?></div>
            	        <?php } ?>
            	        <?= $content['content']; ?>
                    </span>
                </div>
                <?php
                break;
            default:
                ?>
                <div class="left">
                    <h1 class=""><?= $content['headline']; ?></h1>
                    <?php if (array_key_exists('sub_headline', $content) && !empty($content['sub_headline'])) { ?>
                        <h2 class="uppercase strong"><?= $content['sub_headline']; ?></h2>
                    <?php } ?>
                </div>
                <div class="right">
                    <?php if($messages['success'] === true){ ?>
        	            <div class="message success"><?= $messages['status']; ?></div>
        	        <?php }else if($messages['success'] === false){ ?>
        	            <div class="message error"><?= $messages['status']; ?></div>
        	        <?php } ?>
        	        <?= $content['content']; ?>
                </div>
                <?php
                break;
        } ?>
    </div>
</main>

<?= $this->element('Frontend.Website/media', ['media' => $content['media'], 'position' => 'media-middle', 'wrapper' => 'media-middle']) ?>

<div class="form-wrapper">
	<div class="inner">
		<?php if($messages['show']){ ?>
	        <?= $this->element('Frontend.Website/form-message', ['messages' => $messages]) ?>
	    <?php }else{ ?>
	        <!--// Form Start //-->
	        <?= $this->CustomForm->create($form); ?>
            <div class="form-cols">
    	        <div class="col left">
    	            <?= $this->CustomForm->input('salutation', ['label' => __d('fe', 'Salutation'), 'empty' => __d('fe', '-- Please select --')]); ?>
    	            <?= $this->CustomForm->input('firstname', ['label' => __d('fe', 'Firstname'), 'value' => $firstname]); ?>
    	        </div>
    	        <div class="col right">
    	            <?= $this->CustomForm->input('lastname', ['label' => __d('fe', 'Lastname'), 'value' => $lastname]); ?>
    	            <?= $this->CustomForm->input('email', ['label' => __d('fe', 'E-Mail'), 'value' => $email]); ?>
                </div>
	        </div>
	        <div class="clear"></div>
	        <?php if(count($interests) > 0){ ?>
                <?= $this->CustomForm->input('interests', ['templateVars' => ['cc' => 'multiple-checkbox'], 'label' => __d('fe', 'Interests'), 'multiple' => 'checkbox']); ?>
	            <div class="clear"></div>
            <?php } ?>
            
            <?= $this->element('Frontend.Website/required') ?>
            <?= $this->element('Frontend.Website/privacy') ?>
	        <?= $this->element('Frontend.Website/captcha', ['text' => __d('fe', 'Subscribe'), 'options' => [['text' => __d('fe', 'Unsubscribe'), 'class' => 'option', 'type' => 'subscribe', 'url' => $this->Url->build(['node' => 'node:' . $this->request->params['node']['id'], 'language' => $this->request->params['language'], 'extend' => [__d('fe', 'unsubscribe')]])]]]) ?>
	        <?= $this->CustomForm->end(); ?>
	        <!--// Form End //-->
		<?php } ?>
	</div>
</div>

<?= $this->element('Frontend.Website/media', ['media' => $content['media'], 'position' => 'media-bottom', 'wrapper' => 'media-bottom']) ?>
