<main>
    <a class="anchor" name="content"></a>
    <h1><?= $content['headline_member']; ?></h1>
    <div class="main-content">
        <?= $this->element('Frontend.Website/media', ['media' => $content['media'], 'position' => 'right', 'wrapper' => 'right']) ?>
        <?= $content['content_member']; ?>
        <div class="clear"></div>
    </div>
</main>
<?= $this->element('Frontend.Website/media', ['media' => $content['media'], 'position' => 'media-top', 'wrapper' => 'media-top']) ?>
<?= $this->element('Frontend.Website/media', ['media' => $content['media'], 'position' => 'media', 'wrapper' => 'media-middle']) ?>
<?= $this->element('Frontend.Website/media', ['media' => $content['media'], 'position' => 'media-bottom', 'wrapper' => 'media-bottom']) ?>