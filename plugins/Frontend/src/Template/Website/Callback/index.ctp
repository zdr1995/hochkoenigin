<?php if($_SERVER['REMOTE_ADDR'] == '83.175.88.51'){ ?>
    <!-- ######   CALLBACK   ###### -->
    <!-- ###### [Website/Callback/index.ctp] ###### -->
<?php } ?>

<?php if (!array_key_exists('select_layout', $content) && empty($content['select_layout'])) {
    $content['select_layout'] = 'default';
} ?>

<?php $_content = empty($content['content']) ? 'no-content' : ''; ?>

<?= $this->element('Frontend.Website/media', ['media' => $content['media'], 'position' => 'media-top', 'wrapper' => 'media-top']) ?>

<main class="fadeIn animated-fast page inner">
    <a class="anchor" name="content"></a>
    <div class="content-wrapper <?= $_content ?>">
        <?php switch ($content['select_layout']) {
            case 'custom':
                ?>
                <div class="blank">
                    <h1 class=""><?= $content['headline']; ?></h1>
                    <?php if (array_key_exists('sub_headline', $content) && !empty($content['sub_headline'])) { ?>
                        <h2 class="uppercase strong"><?= $content['sub_headline']; ?></h2>
                    <?php } ?>
                    <span class="blank">
                        <?= $content['content']; ?>
                    </span>
                </div>
                <?php
                break;
            default:
                ?>
                <div class="left">
                    <h1 class=""><?= $content['headline']; ?></h1>
                    <?php if (array_key_exists('sub_headline', $content) && !empty($content['sub_headline'])) { ?>
                        <h2 class="uppercase strong"><?= $content['sub_headline']; ?></h2>
                    <?php } ?>
                </div>
                <div class="right">
                    <?= $content['content']; ?>
                </div>
                <?php
                break;
        } ?>
    </div>
</main>

<?= $this->element('Frontend.Website/media', ['media' => $content['media'], 'position' => 'media-middle', 'wrapper' => 'media-middle']) ?>

<div class="form-wrapper">
	<div class="inner">
		<?php if($messages['show']){ ?>
	        <?= $this->element('Frontend.Website/form-message', ['messages' => $messages]) ?>
	    <?php }else{ ?>
	        <!--// Form Start //-->
	        <?= $this->CustomForm->create($form); ?>
	        <div class="col left">
	            <?= $this->CustomForm->input('name', ['label' => __d('fe', 'Name')]); ?>
	        </div>
	        <div class="col right">
	            <?= $this->CustomForm->input('phone', ['label' => __d('fe', 'Phone')]); ?>
	        </div>
	        <div class="clear"></div>
	        <div class="col left">
	            <?= $this->CustomForm->input('date', ['label' => __d('fe', 'Date'), 'class' => 'date']); ?>
	        </div>
	        <div class="col right">
	            <?= $this->CustomForm->input('time', ['label' => __d('fe', 'Time'), 'empty' => __d('fe', '-- Please select --')]); ?>
	        </div>
	        <div class="clear"></div>
            <?= $this->CustomForm->input('message', ['label' => __d('fe', 'Message')]); ?>
            
            <?= $this->element('Frontend.Website/required') ?>
			<?= $this->element('Frontend.Website/newsletter') ?>
            <?= $this->element('Frontend.Website/privacy') ?>
	        <?= $this->element('Frontend.Website/captcha') ?>
	        <?= $this->CustomForm->end(); ?>
	        <!--// Form End //-->
		<?php } ?>
	</div>
</div>

<?= $this->element('Frontend.Website/media', ['media' => $content['media'], 'position' => 'media-bottom', 'wrapper' => 'media-bottom']) ?>
