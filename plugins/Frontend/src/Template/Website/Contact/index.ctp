<?php if($_SERVER['REMOTE_ADDR'] == '83.175.88.51'){ ?>
    <!-- ######   CONTACT   ###### -->
    <!-- ###### [Website/Contact/index.ctp] ###### -->
<?php } ?>

<?php if (!array_key_exists('select_layout', $content) && empty($content['select_layout'])) {
    $content['select_layout'] = 'default';
} ?>

<?php $_content = empty($content['content']) ? 'no-content' : ''; ?>

<?= $this->element('Frontend.Website/media', ['media' => $content['media'], 'position' => 'media-top', 'wrapper' => 'media-top']) ?>

<main class="fadeIn animated-fast page inner">
    <a class="anchor" name="content"></a>
    <div class="content-wrapper <?= $_content ?>">
        <?php switch ($content['select_layout']) {
            case 'custom':
                ?>
                <div class="blank">
                    <h1 class=""><?= $content['headline']; ?></h1>
                    <?php if (array_key_exists('sub_headline', $content) && !empty($content['sub_headline'])) { ?>
                        <h2 class="uppercase strong"><?= $content['sub_headline']; ?></h2>
                    <?php } ?>
                    <span class="blank">
                        <?= $content['content']; ?>
                    </span>
                </div>
                <?php
                break;
            default:
                ?>
                <div class="left">
                    <h1 class=""><?= $content['headline']; ?></h1>
                    <?php if (array_key_exists('sub_headline', $content) && !empty($content['sub_headline'])) { ?>
                        <h2 class="uppercase strong"><?= $content['sub_headline']; ?></h2>
                    <?php } ?>
                </div>
                <div class="right">
                    <?= $content['content']; ?>
                </div>
                <?php
                break;
        } ?>
    </div>
</main>

<?= $this->element('Frontend.Website/media', ['media' => $content['media'], 'position' => 'media-middle', 'wrapper' => 'media-middle']) ?>

<div class="form-wrapper">
	<div class="inner">
		<?php if($messages['show']){ ?>
	        <?= $this->element('Frontend.Website/form-message', ['messages' => $messages]) ?>
	    <?php }else{ ?>
			<!--// Form Start //-->
			<?= $this->CustomForm->create($form); ?>

            <!-- personal info -->
            <div class="form-cols">
                <div class="col left">
    				<?= $this->CustomForm->input('firstname', ['label' => __d('fe', 'Firstname')]); ?>
                    <?= $this->CustomForm->input('email', ['label' => __d('fe', 'E-Mail')]); ?>
    			</div>
    			<div class="col right">
    				<?= $this->CustomForm->input('lastname', ['label' => __d('fe', 'Lastname')]); ?>
                    <?= $this->CustomForm->input('salutation', ['label' => __d('fe', 'Salutation'), 'empty' => __d('fe', '-- Please select --')]); ?>
    			</div>
    		</div>

    		<?= $this->CustomForm->input('message', ['label' => __d('fe', 'Message')]); ?>

            <!-- extendable fields -->
            <div class="extendable" id="form-field-fold">
                <span class="more-fields"><a href="javascript:void(0)" class="button" id="unfold-form-btn"><i class="fa fa-plus"></i>
                    <span class="hide-toggle uppercase"><?= __d('fe', 'more details') ?></span>
                    <span class="hide-toggle hidden uppercase"><?= __d('fe', 'less details') ?></span>
                </a></span>
                <div class="form-cols closedForm">
                    <div class="col left">
                        <?= $this->CustomForm->input('title', ['label' => __d('fe', 'Title')]); ?>
                        <?= $this->CustomForm->input('phone', ['label' => __d('fe', 'Phone')]); ?>
                        <?= $this->CustomForm->input('address', ['label' => __d('fe', 'Address')]); ?>
                    </div>
                    <div class="col right">
                        <?= $this->CustomForm->input('zip', ['label' => __d('fe', 'ZIP')]); ?>
                        <?= $this->CustomForm->input('city', ['label' => __d('fe', 'City')]); ?>
                        <?= $this->CustomForm->input('country', ['label' => __d('fe', 'Country'), 'empty' => __d('fe', '-- Please select --')]); ?>
                    </div>
                </div>
            </div>
            <div class="clear"></div>
            
            <?= $this->element('Frontend.Website/required') ?>
			<?= $this->element('Frontend.Website/newsletter') ?>
            <?= $this->element('Frontend.Website/privacy') ?>
			<?= $this->element('Frontend.Website/captcha') ?>
			<?= $this->CustomForm->end(); ?>
			<!--// Form End //-->
		<?php } ?>
	</div>
</div>

<?= $this->element('Frontend.Website/media', ['media' => $content['media'], 'position' => 'media-bottom', 'wrapper' => 'media-bottom']) ?>
