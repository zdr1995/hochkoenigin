$(document).ready(function () {
  initdatepicker();
});


function initdatepicker() {
  $('input.date').each(function (k, v) {
    if ($(this).hasClass('picker__input') === false) {

      // init
      var __min = $(this).data('date-min') ? new Date($(this).data('date-min')) : undefined;
      var __max = $(this).data('date-max') ? new Date($(this).data('date-max')) : undefined;
      var __range = $(this).data('date-range');
      var __years = $(this).data('date-years') == false ? false : true;
      var __months = $(this).data('date-months') == false ? false : true;
      var __format = $(this).data('date-format') != undefined ? $(this).data('date-format') : 'dd.mm.yyyy';
      var __hidden = $(this).data('date-hidden') != undefined ? $(this).data('date-hidden') : 'yyyy-mm-dd';
      var __callback = $(this).data('date-callback') != undefined ? $(this).data('date-callback') : false;

      // classes
      var __class_year = 'picker__year';
      var __class_today = 'picker__button--today';

      $(this).data('value', $(this).val());
      var $input = $(this).pickadate({
        selectYears: __years,
        selectMonths: __months,
        container: '#datepicker-container',
        format: __format,
        formatSubmit: __hidden,
        hiddenName: true,
        closeOnSelect: true,
        closeOnClear: false,
        max: __max,
        min: __min,
        klass: {
          buttonToday: __class_today,
          year: __class_year
        }
      });

      if (__range) {
        var __clicked = $(this).hasClass('date-from') ? 'from' : 'to';
        var __opposite = __clicked == 'from' ? 'to' : 'from';
        $input.pickadate('picker').on('set', function (event) {
          if (event.select) {
            var __select = $('input.date.date-' + __clicked + '[data-date-range="' + __range + '"]').pickadate('picker').get('select');
            var __related = $('input.date.date-' + __opposite + '[data-date-range="' + __range + '"]').pickadate('picker').get('select');
            if (__opposite == 'to') {
              __select.obj.setDate(__select.obj.getDate() + 1);
              $('input.date.date-' + __opposite + '[data-date-range="' + __range + '"]').pickadate('picker').set('min', __select);
              if (__related && __related.pick <= __select.pick) {
                $('input.date.date-' + __opposite + '[data-date-range="' + __range + '"]').pickadate('picker').set('select', __select);
              }
            } else {
              __select.obj.setDate(__select.obj.getDate() - 1);
              $('input.date.date-' + __opposite + '[data-date-range="' + __range + '"]').pickadate('picker').set('max', __select);
              if (__related && __related.pick >= __select.pick) {
                $('input.date.date-' + __opposite + '[data-date-range="' + __range + '"]').pickadate('picker').set('select', __select);
              }
            }
          } else if ('clear' in event) {
            if (__opposite == 'from') {
              $('input.date.date-' + __opposite + '[data-date-range="' + __range + '"]').pickadate('picker').set('max', false);
            } else {
              $('input.date.date-' + __opposite + '[data-date-range="' + __range + '"]').pickadate('picker').set('min', false);
            }
          }
        });
      }

      if (__callback == 'validateFlatrate') {
        $input.pickadate('picker').on('set', function (event) {
          validateFlatrate();
        });
      }

    }
  });
}

function validateFlatrate() {
  var __from = $('input.date.date-from[data-date-range="request"]').pickadate('picker').get('select');
  var __to = $('input.date.date-to[data-date-range="request"]').pickadate('picker').get('select');
  if (__from !== null && __to !== null) {
    __from = __from.pick / 1000;
    __to = __to.pick / 1000;
    $('.rooms-wrap select.package-select').each(function (k, v) {
      if (flatrateRanges[$(this).val()] !== undefined) {
        var __valid = false;
        $.each(flatrateRanges[$(this).val()], function (k, v) {
          if (v.from <= __from && __to <= v.to) {
            __valid = true;
          }
        });
        if (__valid === false) {
          alert(__translations['package'].replace(/%s/g, $(this).find('option:selected').text()));
          $(this).val('');
        }
      }
    });
  }
}