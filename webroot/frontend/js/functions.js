var __header;
var __scrolling = false;
var __done = false;
var previousScrollPosition = 0;

$(document).ready(function () {

    addNavAnimation();
    toggleNavigation();
    toggleInfoBox();
    acordionAnimation();
    backToTop();
    footerLanguageAnim();
    toggleFormFields();
    hideBottomMenu();
    switchLanguages();
    jump();
    loadingAnimation();
    // tabletMenu();
    // mobileMenu();
    initchildrenaction();
    privacyPolicy();
    bxSliders();
    // toggleShareButtons();
    quickForm();
    videoJS();
    focusInForms();
    toggleJobsForm();
    toggleBrochureForm();
    toggleRoomInfoBox();
    toggleTreatmentInfoBox();
    toggleMobileNavigation();
    toggleRecentPackages();
    contentAnimations();

    //copy to clipboard
    $(".share-btn-clipboard").on("click", function (event) {
        CopyLink();
    });

    //
    if ($('#header').length <= 0) {
        if ($('#recent-packages-toggle').hasClass('invisible')) {
            $('#recent-packages-toggle').removeClass('invisible');
        }
        $('#recent-packages-toggle img').toggleClass('invisible-2');
    }

    // main content animation
    if ($('header').length) {
        $('main').addClass('invisible').removeClass('fadeIn animated-fast');
    }

});

$(window).scroll(function () {

    elementScrolled();
    countUp();
    contentAnimations();

    if (!$('div#recent-packages').hasClass('unscrollable')) {
        // menu toggle on scroll
        if ($("#header").hasClass('not-scrolled')) {
            if ($(window).scrollTop() <= 0 && $("#header").length) {
                if (!$('div#recent-packages').hasClass('invisible')) {
                    $('div#recent-packages').toggleClass('invisible');
                }
            } else {
                if ($('div#recent-packages').hasClass('invisible')) {
                    $('div#recent-packages').toggleClass('invisible');
                }
                if ($('#recent-packages-toggle').hasClass('invisible')) {
                    $('#recent-packages-toggle').toggleClass('invisible');
                    $('#recent-packages-toggle img').toggleClass('invisible-2');
                }
            }
        }
        $('#header').removeClass('not-scrolled');
        if ($(this).scrollTop() >= $('nav').offset().top) {
            $('div.menu').addClass('unreachable');
            if (!$('div#recent-packages').hasClass('invisible')) {
                $('div#recent-packages').addClass('invisible');
                $('#recent-packages-toggle img').toggleClass('invisible-2');
            }
            if ($('#recent-packages-toggle').hasClass('invisible')) {
                $('#recent-packages-toggle').removeClass('invisible');
            }
        }

        // menu toggle on scroll - allways hide on scroll-up
        var currentScrollPosition = $(window).scrollTop() + $(window).height();
        if (currentScrollPosition < previousScrollPosition) {
            if (!$('div#recent-packages').hasClass('invisible')) {
                $('div#recent-packages').addClass('invisible');
            }
            if ($('#recent-packages-toggle').hasClass('invisible')) {
                $('#recent-packages-toggle').removeClass('invisible');
            }
        }
        previousScrollPosition = currentScrollPosition
    }

});


// FUNCTIONS

function elementScrolled(elem) {
    if (typeof (elem) !== "undefined" && elem !== null) {
        var docViewTop = $(window).scrollTop();
        var docViewBottom = docViewTop + $(window).height();
        var elemTop = $(elem).offset().top;
        return ((elemTop <= docViewBottom) && (elemTop >= docViewTop));
    }
}

function scrollToContent() {
    if (__scrolling === false) {
        content_position = $('a.anchor[name=content]').offset().top;
        if ($(window).scrollTop() < content_position) {
            __scrolling = true;
            $.smoothScroll({
                scrollElement: $('html,body'),
                scrollTarget: $('a.anchor[name=content]'),
                easing: 'swing',
                speed: 1000,
                offset: 0,
                afterScroll: function () {
                    __scrolling = false;
                }
            });
        }
    }
}

function scrollToTop() {
    $.smoothScroll({
        scrollElement: $('html,body'),
        scrollTarget: $('#wrapper'),
        easing: 'swing',
        speed: 1000,
        offset: 0
    });
}

function onTop() {
    if ($(this).scrollTop() < 50) {
        $('body').addClass('ontop');
    } else {
        $('body').removeClass('ontop');
    }
}

function selecotraction(e) {
    var elementCount = $('.rooms-wrap .room-wrap').length;

    //add/remove room
    if ($(e).hasClass('room-add')) {
        //add
        var cloned = $('.rooms-wrap .room-wrap:first').clone(true);
        cloned.addClass('fadedout');
        cloned.find('input, select').val('');
        cloned.find('.age').parents('.input').remove();
        cloned.insertAfter($('.rooms-wrap .room-wrap:last'));
        setTimeout(function () {
            $('.rooms-wrap .room-wrap.fadedout').removeClass('fadedout');
        }, 10);
        elementCount++;
    } else if (!$(e).hasClass('room-nodelete')) {
        //remove
        $(e).parents('.room-wrap').addClass('fadedout');
        setTimeout(function () {
            $(e).parents('.room-wrap').remove();
        }, 300);
        elementCount--;
    }

    //reorder
    setTimeout(function () {
        $('.rooms-wrap .room-wrap').each(function (k, v) {
            var _name = 'rooms[' + k + ']';
            // change nr
            $(this).attr('data-room-key', k);
            $(this).attr('id', 'room-' + k);
            $(this).find('select.room-select').attr('id', 'rooms-' + k + '-room').attr('name', _name + '[room]');
            $(this).find('select.package-select').attr('id', 'rooms-' + k + '-package').attr('name', _name + '[package]');
            $(this).find('input.room-adults').attr('id', 'rooms-' + k + '-adults').attr('name', _name + '[adults]');
            $(this).find('input.room-children').attr('id', 'rooms-' + k + '-children').attr('name', _name + '[children]');
            $(this).find('input.age').each(function (k, v) {
                $(this).attr('id', 'rooms-' + k + '-ages-' + k + '-age').attr('name', _name + '[ages][' + k + '][age]');
            });
        });

        //show/hide delete button
        if (elementCount <= 1) {
            $('.rooms-wrap .room-wrap').find('.room-remove').addClass('room-nodelete');
        } else {
            $('.rooms-wrap .room-wrap').find('.room-remove').removeClass('room-nodelete');
        }
    }, 300);
}

function initchildrenaction() {
    $('input.room-children').on('keyup', function (event) {
        childrenageaction(this);
    });
    $('input.room-children').on('change', function (event) {
        childrenageaction(this);
    });
}

function childrenageaction(e) {
    var room = $(e).parents('.room-wrap');
    var roomKey = room.attr('data-room-key');
    var childCount = $(e).val();
    var childAgeInputsCount = room.find('input.age').length;

    //check childCount
    if (isNaN(childCount)) {
        childCount = 0;
        $(e).val(0);
    } else if (childCount > 4) {
        childCount = 4;
        $(e).val(4);
    }

    if (childCount > childAgeInputsCount) {

        for (var x = childAgeInputsCount; x < childCount; x++) {
            var __clone = $($(e).parent('div.input')).clone();
            __clone.addClass('room-child-age').addClass('fadedout');

            var _name = 'rooms[' + roomKey + '][ages][' + x + '][age]';

            // change nr
            $(__clone).find('label').text(__translations.childage).attr('for', 'room-' + roomKey + '-age-' + x);
            $(__clone).find('input').attr('id', 'room-' + roomKey + '-age-' + x);
            $(__clone).find('input').attr('name', _name);
            $(__clone).find('input').attr('max', 18);
            $(__clone).find('input').val('');
            $(__clone).find('input').removeClass('children');
            $(__clone).find('input').addClass('age');
            $(__clone).find('input').attr('placeholder', __translations.childage);


            // add
            $(e).parents('div.room-col').append(__clone);
            setTimeout(function () {
                $('.rooms-wrap .room-wrap .room-child-age.fadedout').removeClass('fadedout');
            }, 1);
        }

    } else {
        for (var x = childAgeInputsCount; x >= childCount; x--) {
            room.find('#room-' + roomKey + '-age-' + x).parent('div.input.fadedout').addClass('fadedout');
            room.find('#room-' + roomKey + '-age-' + x).parent('div.input').remove();
        }
    }
}

function copyTextToClipboard(text) {
    var textArea = document.createElement("textarea");

    // Place in top-left corner of screen regardless of scroll position.
    textArea.style.position = 'fixed';
    textArea.style.top = 0;
    textArea.style.left = 0;

    // Ensure it has a small width and height. Setting to 1px / 1em
    // doesn't work as this gives a negative w/h on some browsers.
    textArea.style.width = '2em';
    textArea.style.height = '2em';

    // We don't need padding, reducing the size if it does flash render.
    textArea.style.padding = 0;

    // Clean up any borders.
    textArea.style.border = 'none';
    textArea.style.outline = 'none';
    textArea.style.boxShadow = 'none';

    // Avoid flash of white box if rendered for any reason.
    textArea.style.background = 'transparent';

    textArea.value = text;
    document.body.appendChild(textArea);
    textArea.select();

    try {
        var successful = document.execCommand('copy');
        var msg = successful ? 'successful' : 'unsuccessful';
        // console.log('Copying text command was ' + msg);
    } catch (err) {
        // console.log('Oops, unable to copy');
    }
    document.body.removeChild(textArea);
}

function CopyLink() {
    copyTextToClipboard(location.href);
}

function toggleNavigation() {
    $('a#menu-open').on('click', function (event) {
        event.preventDefault();
    });
}

function toggleInfoBox() {
    $('.tabs-wrapper .switch-buttons > a').on('click', function () {
        event.preventDefault();
        var switchbutton = $(this);
        var rel = switchbutton.attr('rel');

        $('.tabs-wrapper > .content').css({ 'min-height': $('.tabs-wrapper').find('.content > .item.active').outerHeight() });
        switchbutton.parents('.tabs-wrapper').find('.buttons > a').removeClass('active');
        switchbutton.parents('.tabs-wrapper').find('.content > .item[rel="' + rel + '"]').addClass('beforeHidden');
        switchbutton.parents('.tabs-wrapper').find('.content > .item').removeClass('active').addClass('hidden').css('display', 'none').animate({ height: 0 }, 1);
        switchbutton.parents('.tabs-wrapper').find('.link > .link-btn').addClass('invisible');
        setTimeout(function () {
            switchbutton.parents('.tabs-wrapper').find('.link > .link-btn').removeClass('active').addClass('hidden');
            switchbutton.parents('.tabs-wrapper').find('.link > .link-btn[rel="' + rel + '"]').addClass('active').removeClass('hidden').removeClass('invisible');
        }, 150);
        switchbutton.parents('.tabs-wrapper').find('.buttons > a[rel="' + rel + '"]').addClass('active');
        switchbutton.parents('.tabs-wrapper').find('.content > .item[rel="' + rel + '"]').addClass('active').removeClass('hidden').css('display', 'block').animate({ height: switchbutton.parents('.tabs-wrapper').find('.content > .item[rel="' + rel + '"]').get(0).scrollHeight }, 900);
        setTimeout(function () {
            $('.item[rel="' + rel + '"]').removeClass('beforeHidden');
        }, 700);

    });
}

function acordionAnimation() {
    $(".flex-slide").mouseenter(function () {
        $(".flex-slide").each(function () {
            $(this).removeClass('active');
        });
        $(this).toggleClass('active');
    });
}

function backToTop() {
    $(window).scroll(function () {
        if ($(window).scrollTop() + $(window).height() >= $(document).height() * 0.3) {
            $('.scroll-to-top-btn').fadeIn(200);
        } else {
            $('.scroll-to-top-btn').fadeOut(200);
        }
    });
    $('.scroll-to-top-btn').click(function () {
        $('body,html').animate({
            scrollTop: 0
        }, 500);
    });
}

function footerLanguageAnim() {
    $(window).scroll(function () {
        if ($(window).scrollTop() + $(window).height() == $(document).height()) {
            $('#foot-lang').fadeIn(200);
        } else {
            $('#foot-lang').fadeOut(200);
        }
    });
    $('#foot-lang').click(function () {
        $('body,html').animate({
            scrollTop: 0
        }, 500);
    });
}

function toggleRoomInfoBox() {
    $('.package-price-buttons').click(function () {
        event.preventDefault();
        var id = $(this).attr("data-id");

        $('#line-' + id).toggleClass('invisible visible');
        if (!$('[data-id="' + id + '"]').hasClass('clicked')) {
            $('[data-id="' + id + '"] i').css({
                "transform": "rotate(180deg)",
                "-ms-transform": "rotate(180deg)",
                "-moz-transform": "rotate(180deg)",
                "-webkit-transform": "rotate(180deg)",
                "-o-transform": "rotate(180deg)",
                "-webkit-transition": "all 1s ease",
                "-moz-transition": "all 1s ease",
                "-o-transition": "all 1s ease",
                "transition": "all 1s ease",
            });
            $('[data-id="' + id + '"]').addClass('clicked');
        } else {
            $('[data-id="' + id + '"] i').css({
                "transform": "rotate(0deg)",
                "-ms-transform": "rotate(0deg)",
                "-moz-transform": "rotate(0deg)",
                "-webkit-transform": "rotate(0deg)",
                "-o-transform": "rotate(360deg)",
            });
            $('[data-id="' + id + '"]').removeClass('clicked');
        }

        if ($('#box-' + id).hasClass('closed')) {
            $('#box-' + id).removeClass('closed');
            $('#box-' + id + ' div.animationer').animate({ height: $('#box-' + id + ' div.animationer').get(0).scrollHeight }, 1000);
        } else {
            $('#box-' + id + ' div.animationer').animate({ height: 0 }, 1000);
            $('#box-' + id).addClass('closed');
        }

    });
}

function toggleTreatmentInfoBox() {
    $('.treatment-click').click(function () {
        event.preventDefault();
        var id = $(this).attr("data-id");

        if (!$('[data-id="' + id + '"]').hasClass('clicked')) {
            $('[data-id="' + id + '"] i').css({
                "transform": "rotate(180deg)",
                "-ms-transform": "rotate(180deg)",
                "-moz-transform": "rotate(180deg)",
                "-webkit-transform": "rotate(180deg)",
                "-o-transform": "rotate(180deg)",
                "-webkit-transition": "all 1s ease",
                "-moz-transition": "all 1s ease",
                "-o-transition": "all 1s ease",
                "transition": "all 1s ease",
            });
            $('[data-id="' + id + '"]').addClass('clicked');
        } else {
            $('[data-id="' + id + '"] i').css({
                "transform": "rotate(0deg)",
                "-ms-transform": "rotate(0deg)",
                "-moz-transform": "rotate(0deg)",
                "-webkit-transform": "rotate(0deg)",
                "-o-transform": "rotate(360deg)",
            });
            $('[data-id="' + id + '"]').removeClass('clicked');
        }

        if ($('#box-' + id).hasClass('closed')) {
            $('#box-' + id).removeClass('closed');
            $('#box-' + id).animate({ height: $('#box-' + id).get(0).scrollHeight }, 1000);
        } else {
            $('#box-' + id).animate({ height: 0 }, 1000);
            $('#box-' + id).addClass('closed');
        }

    });
}

function slideDownToggle($togglee, $toggler, $time) {
    if ($($togglee).hasClass($toggler)) {
        $($togglee).removeClass($toggler);
        $($togglee).animate({ height: $($togglee).get(0).scrollHeight }, $time);
    } else {
        $($togglee).animate({ height: 0 }, $time);
        $($togglee).addClass($toggler);
    }
}

function toggleJobsForm() {
    $('#open-jobs-form').click(function () {
        $('.hide-toggle-2').toggleClass('hidden');
        slideDownToggle('#jobs-wrapper', 'closedForm', 500);
    });
}

function toggleBrochureForm() {
    $('#open-brochure-form').click(function () {
        $('.hide-toggle-3').toggleClass('hidden');
        slideDownToggle('#brochure-wrapper', 'closedForm', 500);
    });
}

function toggleFormFields() {
    $('#unfold-form-btn').click(function () {
        $('.hide-toggle').toggleClass('hidden');
        $(this).find('.fa').toggleClass('fa-plus').toggleClass('fa-minus');
        slideDownToggle('#form-field-fold>div', 'closedForm', 500);
    });
}

function hideBottomMenu() {
    // hide bottom menu if there is no header
    if (!$("#header").length && $('div#recent-packages').hasClass('invisible')) {
        $('div#recent-packages').removeClass('invisible');
    }
}

function switchLanguages() {
    $('div.languages > span').click(function (event) {
        event.preventDefault();
        $(this).parent('div').toggleClass('open');
    });
}

function jump() {
    $('a.j2c').click(function (event) {
        event.preventDefault();
        scrollToContent(true);
    });

    $('a.j2t').click(function (event) {
        event.preventDefault();
        scrollToTop();
    });
}

function loadingAnimation() {
    //fadeout loading-animation
    var fadeoutTime = docRoute === homeRoute ? 300 : 0;
    setTimeout(function () {
        $('body, #header .bxslider figcaption, #header .bubble').addClass('fully-loaded');
        setTimeout(function () {
            $('.loading-overlay').remove();
        }, 1000);
    }, fadeoutTime);
}

function tabletMenu() {
    $('nav a.m').click(function (event) {
        var type = $(this).parents('nav').hasClass('mobile') ? 'mobile' : 'desktop';
        var state = $(this).parent('li').hasClass('open') ? 'open' : 'close';
        var drop = $(this).parent('li').hasClass('children-0') ? false : true;
        if (type == 'mobile') {
            event.preventDefault();
            if (state == 'close') {
                $(this).parent('li').addClass('open');
                $(this).nextAll('ul').animate({
                    maxHeight: '1000px'
                }, 1000, function () {
                    $(this).css('max-height', $(this).css('height'));
                });
            } else {
                $(this).parent('li').removeClass('open');
                $(this).nextAll('ul').animate({
                    maxHeight: 0
                }, 300, function () { });
            }
        } else {
            if (drop === true) {
                event.preventDefault();
            }
            $('nav > ul > li').removeClass('open');
            if (state == 'close') {
                $(this).parent('li').addClass('open');
            }
        }
    });
}

function mobileMenu() {
    $('.mobile-menu > button').click(function (event) {
        if ($('nav.mobile').height() == 0) {
            $('button.hamburger').addClass('is-active');
            $('nav.mobile').animate({
                maxHeight: $('nav.mobile > ul').css('height')
            }, 300, function () {
                $('nav.mobile').css('max-height', 'none');
            });
        } else {
            $('button.hamburger').removeClass('is-active');
            $('nav.mobile').css('max-height', $('nav.mobile').css('height'));
            $('nav.mobile').animate({
                maxHeight: 0
            }, 300, function () { });
        }
    });
}

function privacyPolicy() {
    $('a.cookie-hint-button').click(function (event) {
        event.preventDefault();
        $('#cookie-hint').remove();
        $('body').removeClass('cookie-hint');

        var d = new Date();
        d.setTime(d.getTime() + (365 * 24 * 60 * 60 * 1000));
        var expires = "expires=" + d.toUTCString();
        document.cookie = "hint=false; " + expires + "; path=/";

    });
}

function bxSliders() {

    // header
    __header = $('header .bxslider').bxSlider({
        auto: $('header .bxslider .bxslide').length > 1 ? true : false,
        pager: false,
        controls: true,
        speed: 700,
        pause: 5000,
        touchEnabled: false,
        nextText: '<img src="/frontend/img/uploaded/icons/Icons_Rechts-lang.svg" class="cal-icon icon">',
        prevText: '<img src="/frontend/img/uploaded/icons/Icons_Links-lang.svg" class="cal-icon icon">'
    });

    // impressions
    $('div.impressions .bxslider').bxSlider({
        auto: $('div.impressions .bxslider .bxslide').length > 1 ? true : false,
        pager: false,
        controls: true,
        randomStart: true,
        speed: 700,
        pause: 5000,
        nextText: '<img src="/frontend/img/uploaded/icons/Icons_Rechts-lang.svg" class="cal-icon icon invert">',
        prevText: '<img src="/frontend/img/uploaded/icons/Icons_Links-lang.svg" class="cal-icon icon invert">'
    });

    // gallery
    $('section.gallery .bxslider').bxSlider({
        auto: true,
        pager: true,
        controls: true,
        randomStart: true,
        speed: 700,
        pause: 5000,
        nextText: '<img src="/frontend/img/uploaded/icons/Icons_Rechts-lang.svg" class="cal-icon icon invert">',
        prevText: '<img src="/frontend/img/uploaded/icons/Icons_Links-lang.svg" class="cal-icon icon invert">',
        pagerCustom: '#bx-gallery-pager'
    });

    // pool
    $('section.pool .bxslider').each(function (k, v) {
        $(this).bxSlider({
            auto: true,
            pager: false,
            controls: true,
            speed: 700,
            pause: 5000,
            randomStart: true,
            slideWidth: 336,
            maxSlides: 5,
            minSlides: 1,
            moveSlides: 2,
            nextText: '<img src="/frontend/img/uploaded/icons/Icons_Rechts-lang.svg" class="cal-icon icon invert">',
            prevText: '<img src="/frontend/img/uploaded/icons/Icons_Links-lang.svg" class="cal-icon icon invert">'
        });
    });

    // menu
    $('.menu-packages-slider.bxslider').each(function (k, v) {
        $(this).bxSlider({
            auto: true,
            pager: true,
            controls: true,
            randomStart: true,
            speed: 700,
            pause: 7000,
            pagerCustom: '.bx-pool-pager-' + $(this).parent('section.pool').data('pool-id'),
            nextText: '<img src="/frontend/img/uploaded/icons/Icons_Rechts-mittel.svg" class="cal-icon icon">',
            prevText: '<img src="/frontend/img/uploaded/icons/Icons_Links-mittel.svg" class="cal-icon icon">'
        });
    });
}

function toggleShareButtons() {
    $(".share-btns-toggle").on("click", function (event) {
        $('.share-btns-wrap').addClass('open');
    });
    $(".share-btns-darken, .share-btns-close").on("click", function (event) {
        $('.share-btns-wrap').removeClass('open');
    });
}

function quickForm() {
    $('section.top div.right span.button.request a').click(function (event) {
        var windowWidth = $(window).width();
        if (windowWidth > 1024) {
            event.preventDefault();
            $(this).parent('span').toggleClass('open');
        }
    });
}

function videoJS() {
    $('video.video-js').each(function () {
        videojs($(this).attr('id'), {}, function () {
            // Player (this) is initialized and ready.
        });
    });
}

function focusInForms() {
    $(".input > *").focus(function () {
        $(this).parents('.input').addClass("focused");
    }).blur(function () {
        $(this).parents('.input').removeClass("focused");
    });
}

function countUp() {
    if ($('.countUp').length > 0) {
        if (elementScrolled('.countUp') && __done == false) {
            $('#countUp1, #countUp2, #countUp3').data('countToOptions', {
                formatter: function (value, options) {
                    return value.toFixed(options.decimals).replace(/\B(?=(?:\d{3})+(?!\d))/g, ',');
                }
            });
            $('.countUp').each(count);
            __done = true;
        }
    }
}

function count(options) {
    var $this = $(this);
    options = $.extend({}, options || {}, $this.data('countToOptions') || {});
    $this.countTo(options);
}

function toggleRecentPackages() {
    $("#recent-packages-toggle").on("click", function (event) {
        $('#recent-packages').toggleClass('invisible');
        $('#recent-packages').toggleClass('unscrollable');
        $('#recent-packages-toggle img').toggleClass('invisible-2');
        if ($('div.menu').hasClass('unreachable')) {
            setTimeout(function () {
                $('div.menu').removeClass('unreachable');
            }, 0);
        } else {
            setTimeout(function () {
                $('div.menu').addClass('unreachable');
            }, 300);
        }
    });
}

function toggleMobileNavigation(event) {
    $('#open-mobile-nav').on('click', function (event) {
        event.preventDefault();
        $('#mainnav-mobile').toggleClass('hidden');
        $('.nav-icon').toggleClass('clicked');
    });
    $('.mainnav-mobile').find('.m').on('click', function (event) {
        event.preventDefault();
        $(this).parent().toggleClass('clicked').siblings('li').toggleClass('hidden');
        $(this).parent().siblings('li.last').removeClass('hidden');
        $(this).siblings('.subnav').toggleClass('hidden');
        $(this).children('.image-left, .image-right').toggleClass('hidden');
    });
    $('.mainnav-mobile').find('.subsub').on('click', function (event) {
        event.preventDefault();
        $(this).parent().toggleClass('clicked').siblings().toggleClass('hidden');
        $(this).siblings('.subsubnav').toggleClass('hidden');
        $(this).children('.image-left, .image-right').toggleClass('hidden');
    });
}

function addNavAnimation() {
    if ($('header').length) {
        $('#mainnav').removeClass('invisible');
    } else {
        $('#mainnav').removeClass('invisible').addClass('fadeIn animated-fast');
    }
}

function contentAnimations() {

    // body animation
    $('body').waypoint(function () {
        $('body').removeClass('invisible').addClass('fadeIn animated');
    }, { offset: '100%' });

    // main content animation
    if ($('header').length) {
        $('main').waypoint(function () {
            $('main').removeClass('invisible').addClass('fadeIn animated-fast');
        }, { offset: '85%' });
    }

    // banner #6
    $('.b6-to-animate-left').waypoint(function () {
        $('.b6-to-animate-left').removeClass('invisible').addClass('fadeInLeft animated');
    }, { offset: '75%' });
    $('.b6-to-animate-right').waypoint(function () {
        $('.b6-to-animate-right').removeClass('invisible').addClass('fadeInRight animated');
    }, { offset: '75%' });

    // banner #5
    $('.b5-to-animate-img-bg').waypoint(function () {
        $('.b5-to-animate-img-bg').removeClass('invisible').addClass('fadeInRight animated');
        $('.b5-to-animate-img-sm').removeClass('invisible').addClass('fadeInLeft animated');
    }, { offset: '80%' });
    $('.b5-to-animate').waypoint(function () {
        $('.b5-to-animate').removeClass('invisible').addClass('fadeIn animated');
    }, { offset: '80%' });

    // banner #2
    $('.b2-to-animate').waypoint(function () {
        $('.b2-to-animate').removeClass('invisible').addClass('fadeIn animated-fast');
    }, { offset: '75%' });

    // banner #1
    $('.b1-to-animate').waypoint(function () {
        $('.b1-to-animate').removeClass('invisible').addClass('fadeIn animated-fast');
    }, { offset: '75%' });

    // 3-tabs
    $('.t3-to-animate').waypoint(function () {
        $('.t3-to-animate').removeClass('invisible').addClass('fadeIn animated');
    }, { offset: '75%' });

    // teaser
    $('.tea-to-animate').waypoint(function () {
        $('.tea-to-animate').removeClass('invisible').addClass('fadeIn animated');
    }, { offset: '80%' });

    // pool
    $('.pool-to-animate').waypoint(function () {
        $('.pool-to-animate').removeClass('invisible').addClass('fadeIn animated');
    }, { offset: '75%' });

    // red-dot
    $('.header-red-dot-frame').waypoint(function () {
        $('.header-red-dot-frame').removeClass('invisible').addClass('fadeIn animated');
    }, { offset: '75%' });
    $('.content-red-dot').waypoint(function () {
        $('.content-red-dot').removeClass('invisible').addClass('fadeIn animated');
    }, { offset: '75%' });
}
